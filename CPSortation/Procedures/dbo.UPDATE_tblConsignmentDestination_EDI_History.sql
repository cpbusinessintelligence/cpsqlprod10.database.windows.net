SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UPDATE_tblConsignmentDestination_EDI_History] AS

BEGIN

SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRAN

BEGIN TRY

INSERT INTO [dbo].[tblConsignmentDestination_History]
SELECT * FROM tblConsignmentDestination

---------------------- Delete from missing labels table once consignment is sent to GP. DATE : 22/10/2019

DELETE FROM dbo.[edi_incremental_consignment_missinglabels]
WHERE cd_connote in (SELECT ConsignmentNumber FROM dbo.outgoing_consignment_event_log)

----------------------------------------

END TRY

BEGIN CATCH

		IF @@TRANCOUNT >0
		BEGIN		
				ROLLBACK TRAN;
				
					SELECT
					ERROR_NUMBER() AS ErrorNumber  
					,ERROR_SEVERITY() AS ErrorSeverity  
					,ERROR_STATE() AS ErrorState  
					,ERROR_LINE () AS ErrorLine  
					,ERROR_PROCEDURE() AS ErrorProcedure  
					,ERROR_MESSAGE() AS ErrorMessage
					,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime;
		END;

		THROW;

END CATCH

	IF @@TRANCOUNT >0
	BEGIN	
		COMMIT TRAN;
	END

END
GO
