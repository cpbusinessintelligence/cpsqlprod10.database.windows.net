SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:                            <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EDI_UPDATE_CDCOUPON]
                

AS
BEGIN
                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;

    UPDATE [edi_cdcoupon]
                SET         
                                cc_company_id =  UPD.cc_company_id,
                                cc_consignment =  UPD.cc_consignment,
                                cc_coupon =  UPD.cc_coupon,
                                cc_activity_stamp =  UPD.cc_activity_stamp,
                                cc_pickup_stamp =  UPD.cc_pickup_stamp,
                                cc_accept_stamp =  UPD.cc_accept_stamp,
                                cc_indepot_stamp =  UPD.cc_indepot_stamp,
                                cc_transfer_stamp =  UPD.cc_transfer_stamp,
                                cc_deliver_stamp =  UPD.cc_deliver_stamp,
                                cc_failed_stamp =  UPD.cc_failed_stamp,
                                cc_activity_driver =  UPD.cc_activity_driver,
                                cc_pickup_driver =  UPD.cc_pickup_driver,
                                cc_accept_driver =  UPD.cc_accept_driver,
                                cc_indepot_driver =  UPD.cc_indepot_driver,
                                cc_transfer_driver =  UPD.cc_transfer_driver,
                                cc_transfer_to =  UPD.cc_transfer_to,
                                cc_toagent_driver =  UPD.cc_toagent_driver,
                                cc_toagent_stamp =  UPD.cc_toagent_stamp,
                                cc_toagent_name =  UPD.cc_toagent_name,
                                cc_deliver_driver =  UPD.cc_deliver_driver,
                                cc_failed_driver =  UPD.cc_failed_driver,
                                cc_deliver_pod =  UPD.cc_deliver_pod,
                                cc_failed =  UPD.cc_failed,
                                cc_exception_stamp =  UPD.cc_exception_stamp,
                                cc_exception_code =  UPD.cc_exception_code,
                                cc_unit_type =  UPD.cc_unit_type,
                                cc_internal =  UPD.cc_internal,
                                cc_link_coupon =  UPD.cc_link_coupon,
                                cc_dirty =  UPD.cc_dirty,
                                cc_last_status =  UPD.cc_last_status,
                                cc_last_driver =  UPD.cc_last_driver,
                                cc_last_stamp =  UPD.cc_last_stamp,
                                cc_last_info =  UPD.cc_last_info,
                                cc_accept_driver_branch =  UPD.cc_accept_driver_branch,
                                cc_activity_driver_branch =  UPD.cc_activity_driver_branch,
                                cc_deliver_driver_branch =  UPD.cc_deliver_driver_branch,
                                cc_failed_driver_branch =  UPD.cc_failed_driver_branch,
                                cc_indepot_driver_branch =  UPD.cc_indepot_driver_branch,
                                cc_last_driver_branch =  UPD.cc_last_driver_branch,
                                cc_pickup_driver_branch =  UPD.cc_pickup_driver_branch,
                                cc_toagent_driver_branch =  UPD.cc_toagent_driver_branch,
                                cc_tranfer_driver_branch =  UPD.cc_tranfer_driver_branch
                                FROM 
                                                [edi_cdcoupon] COUP WITH (NOLOCK)
                                INNER JOIN
                                                [dbo].[edi_updated_cdcoupon] UPD WITH (NOLOCK)
                                ON
                                                COUP.cc_id = UPD.cc_id


----------------------Check to see labels data for all consignments received date : 22/10/2019

INSERT INTO dbo.[edi_incremental_consignment_missinglabels] 
(
                CD_ID,
                cd_connote,
                [cd_consignment_date],
                [cd_date],
                cd_pricecode,
                [cd_account],
                [cd_pickup_addr0],
                [cd_pickup_addr1],
                [cd_pickup_addr2],
                [cd_pickup_addr3],
                [cd_pickup_suburb],
                cd_pickup_branch,
                [cd_pickup_postcode],
                [cd_delivery_addr0],
                [cd_delivery_addr1],
                [cd_delivery_addr2],
                [cd_delivery_addr3],
                [cd_delivery_suburb],
                cd_deliver_branch,
                [cd_delivery_postcode],
                [cd_delivery_contact],
                [cd_delivery_contact_phone],
                [cd_special_instructions],
                [cd_items],
                [cd_deadweight],
                [cd_volume],
                [cd_insurance],
                [cd_pickup_contact],
                [cd_pickup_contact_phone],
                [cd_delivery_status_flag]
)

SELECT 
CON.CD_ID,
CON.cd_connote,
CON.[cd_consignment_date],
CON.[cd_date],
CON.cd_pricecode,
CON.[cd_account],
CON.[cd_pickup_addr0],
CON.[cd_pickup_addr1],
CON.[cd_pickup_addr2],
CON.[cd_pickup_addr3],
CON.[cd_pickup_suburb],
CON.cd_pickup_branch,
CON.[cd_pickup_postcode],
CON.[cd_delivery_addr0],
CON.[cd_delivery_addr1],
CON.[cd_delivery_addr2],
CON.[cd_delivery_addr3],
CON.[cd_delivery_suburb],
CON.cd_deliver_branch,
CON.[cd_delivery_postcode],
CON.[cd_delivery_contact],
CON.[cd_delivery_contact_phone],
CON.[cd_special_instructions],
CON.[cd_items],
CON.[cd_deadweight],
CON.[cd_volume],
CON.[cd_insurance],
CON.[cd_pickup_contact],
CON.[cd_pickup_contact_phone],
CON.[cd_delivery_status_flag]

FROM dbo.[edi_incremental_consignment] CON WITH (NOLOCK) 
CROSS APPLY (SELECT COUNT(DISTINCT [cc_coupon]) AS CouponCount FROM dbo.[edi_cdcoupon] WITH(NOLOCK) WHERE cc_consignment = con.cd_id ) coup
WHERE coup.CouponCount = 0 and cd_id not in (select cd_id from dbo.[edi_incremental_consignment_missinglabels])
and cd_connote not in (select ConsignmentNumber from dbo.outgoing_consignment_event_log with(NOLOCK))



END

GO
