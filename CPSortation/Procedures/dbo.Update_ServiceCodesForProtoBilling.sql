SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE Proc [dbo].[Update_ServiceCodesForProtoBilling]
as

SET NOCOUNT  ON 

Begin

DROP TABLE IF EXISTS #Temp1

Select   cd_id														as ConsignmentID 
         ,TACode													AS AccountCode
         ,cd_connote												AS [ConsignmentNumber]
         ,cd_consignment_date										AS [ConsignmentDate]
         ,Service													AS Service
         ,convert(varchar(20),'')									AS NewServiceCode
         ,itemqty													AS [Item quantity]
         ,Isnull([Declaredweight],0)								AS DeclaredWEight
         ,Isnull([Declaredvolume],0)								AS DeclaredVolume
         ,Convert(decimal(12,3),Isnull([Declaredvolume],0))*250.0	AS DeclaredCubicWeight
         ,Isnull([Measuredweight],0)								AS MeasuredWeight
         ,Isnull([Measuredvolume],0)								AS MeasuredVolume
         ,Convert(decimal(12,3),Isnull([Measuredvolume],0))*250.0	AS MeasuredCubicWeight
         ,convert(decimal(12,2),0)									AS BilledWeight
         ,convert(varchar(50),'')									AS Comments
into #Temp1
--from  [dbo].[GETPricingDataForCWC_Measured] 
FROm dbo.vwCWCDataForServiceCodeUpdate
where Service in (SELECT distinct ServiceCode  FROM [dbo].[ProntoBilling_ItemServiceCodes])

  --Drop Table #Temp1
--  select * from #temp1  where ConsignmentID = 73057484

 Update #Temp1 SET BilledWeight =  (Select Max(Weights) from (Select DeclaredWEight as Weights
                                                                  union all
                                                                Select DeclaredCubicWeight
                                                                union all
                                                                Select MeasuredWeight
                                                                union all
                                                                Select MeasuredCubicWeight )D )/[Item quantity] where [Item quantity] >0

Delete from #Temp1 where Service in (SELECT distinct ServiceCode  FROM [ProntoBilling_ItemServiceCodes] where Category ='500 Gms') and BilledWeight <= 1.000 
Delete from #Temp1 where Service in (SELECT distinct ServiceCode  FROM [ProntoBilling_ItemServiceCodes] where Category ='1 Kg') and BilledWeight <= 1.100 
Delete from #Temp1 where Service in (SELECT distinct ServiceCode  FROM [ProntoBilling_ItemServiceCodes] where Category ='3 Kgs')  and BilledWeight <= 3.100 
Delete from #Temp1 where Service in (SELECT distinct ServiceCode  FROM [ProntoBilling_ItemServiceCodes] where Category ='5 Kgs') and BilledWeight <= 5.100 
Delete from #Temp1 where Service in (SELECT distinct ServiceCode  FROM [ProntoBilling_ItemServiceCodes] where Category ='10 Kgs') and BilledWeight <= 10.100 
Delete from #Temp1 where Service in (SELECT distinct ServiceCode  FROM [ProntoBilling_ItemServiceCodes] where Category ='25 Kgs') and BilledWeight <= 25.100 

--Select * from #Temp1

Update #Temp1 SET NewServiceCode = Case when Service = 'P0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'P3' 
                                        when Service = 'P0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'P5' 
                                        when Service = 'P1' and BilledWeight >1.10 and BilledWeight <=3.00 Then 'P3'
                                        when Service = 'P1' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'P5'
                                        when Service = 'P3' and BilledWeight >3.10 and BilledWeight <=5.00 Then 'P5'
                                        when Service = 'AGEP0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'AGEP3' 
                                        when Service = 'AGEP0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'AGEP5' 
                                        when Service = 'AGEP0' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AEP10' 
                                        when Service = 'AGEP0' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AEP25' 
                                        when Service = 'AGEP1' and BilledWeight >1.10 and BilledWeight <=3.00 Then 'AGEP3'
                                        when Service = 'AGEP1' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'AGEP5'
                                        when Service = 'AGEP1' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AEP10'
                                        when Service = 'AGEP1' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AEP25' 
                                        when Service = 'AGEP3' and BilledWeight >3.10 and BilledWeight <=5.00 Then 'AGEP5'
                                        when Service = 'AGEP3' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AEP10'
                                        when Service = 'AGEP3' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AEP25'
                                        when Service = 'AGEP5' and BilledWeight >5.10 and BilledWeight <=10.00 Then 'AEP10'
                                        when Service = 'AGEP5' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AEP25'
                                        when Service = 'AEP10' and BilledWeight >10.10 and BilledWeight <=25.00 Then 'AEP25'
                                        when Service = 'AGSP0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'AGSP3' 
                                        when Service = 'AGSP0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'AGSP5' 
                                        when Service = 'AGSP0' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AGP10' 
                                        when Service = 'AGSP0' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'ASP25' 
                                        when Service = 'AGSP1' and BilledWeight >1.10 and BilledWeight <=3.00 Then 'AGSP3'
                                        when Service = 'AGSP1' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'AGSP5'
                                        when Service = 'AGSP1' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AGP10'
                                        when Service = 'AGSP1' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'ASP25' 
                                        when Service = 'AGSP3' and BilledWeight >3.10 and BilledWeight <=5.00 Then 'AGSP5'
                                        when Service = 'AGSP3' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AGP10'   --- Changed to AGP10
                                        when Service = 'AGSP3' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'ASP25'
                                        when Service = 'AGSP5' and BilledWeight >5.10 and BilledWeight <=10.00 Then 'AGP10'
                                        when Service = 'AGSP5' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'ASP25'  -- Changed to ASP25
                                        when Service = 'AGP10' and BilledWeight >10.10 and BilledWeight <=25.00 Then 'ASP25'
                                        when Service = 'AGVP0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'AGVP3' 
                                        when Service = 'AGVP0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'AGVP5' 
                                        when Service = 'AGVP0' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AVP10' 
                                        when Service = 'AGVP0' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AVP25' 
                                        when Service = 'AGVP1' and BilledWeight >1.10 and BilledWeight <=3.00 Then 'AGVP3'
                                        when Service = 'AGVP1' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'AGVP5'
                                        when Service = 'AGVP1' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AVP10'
                                        when Service = 'AGVP1' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AVP25' 
                                        when Service = 'AGVP3' and BilledWeight >3.10 and BilledWeight <=5.00 Then 'AGVP5'
                                        when Service = 'AGVP3' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AVP10'
                                        when Service = 'AGVP3' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AVP25'
                                        when Service = 'AGVP5' and BilledWeight >5.10 and BilledWeight <=10.00 Then 'AVP10'
                                        when Service = 'AGVP5' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AVP25'
                                        when Service = 'AVP10' and BilledWeight >10.10 and BilledWeight <=25.00 Then 'AVP25'
                                        when Service = 'AGHP0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'AGHP3' 
                                        when Service = 'AGHP0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'AGHP5' 
                                        when Service = 'AGHP0' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AHP10' 
                                        when Service = 'AGHP0' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AHP25' 
                                        when Service = 'AGHP1' and BilledWeight >1.10 and BilledWeight <=3.00 Then 'AGHP3'
                                        when Service = 'AGHP1' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'AGHP5'
                                        when Service = 'AGHP1' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AGHP10'
                                        when Service = 'AGHP1' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AHP25' 
                                        when Service = 'AGHP3' and BilledWeight >3.10 and BilledWeight <=5.00 Then 'AGHP5'
                                        when Service = 'AGHP3' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AHP10'
                                        when Service = 'AGHP3' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AHP25'
                                        when Service = 'AGHP5' and BilledWeight >5.10 and BilledWeight <=10.00 Then 'AHP10'
                                        when Service = 'AGHP5' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AHP25'
                                        when Service = 'AHP10' and BilledWeight >10.10 and BilledWeight <=25.00 Then 'AHP25'
                                        when Service = 'FRP0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'FRP3' 
                                        when Service = 'FRP0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'FRP5' 
                                        when Service = 'FRP1' and BilledWeight >1.10 and BilledWeight <=3.00 Then 'FRP3'
                                        when Service = 'FRP1' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'FRP5' 
                                        when Service = 'FRP3' and BilledWeight >3.10 and BilledWeight <=5.00 Then 'FRP5' 
                                        when Service = 'BU0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'BU3' 
                                        when Service = 'BU0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'BU5' 
                                        when Service = 'BU0' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'BU10' 
                                        when Service = 'BU0' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'BU25' 
                                        when Service = 'BU1' and BilledWeight >1.10 and BilledWeight <=3.00 Then 'BU3'
                                        when Service = 'BU1' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'BU5'
                                        when Service = 'BU1' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'BU10'
                                        when Service = 'BU1' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'BU25' 
                                        when Service = 'BU3' and BilledWeight >3.10 and BilledWeight <=5.00 Then 'BU5'
                                        when Service = 'BU3' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'BU10'
                                        when Service = 'BU3' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'BU25'
                                        when Service = 'BU5' and BilledWeight >5.10 and BilledWeight <=10.00 Then 'BU10'
                                        when Service = 'BU5' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'BU25'
                                        when Service = 'BU10' and BilledWeight >10.10 and BilledWeight <=25.00 Then 'BU25'
                                        when Service = 'DS0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'DS3' 
                                        when Service = 'DS0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'DS5' 
                                        when Service = 'DS0' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'DS10' 
                                        when Service = 'DS1' and BilledWeight >1.10 and BilledWeight <=3.00 Then 'DS3'
                                        when Service = 'DS1' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'DS5'
                                        when Service = 'DS1' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'DS10'       
                                        when Service = 'DS3' and BilledWeight >3.10 and BilledWeight <=5.00 Then 'DS5'
                                        when Service = 'DS3' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'DS10'
                                        when Service = 'DS5' and BilledWeight >5.10 and BilledWeight <=10.00 Then 'DS10'
                                        when Service = 'P0A' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'P3A' 
                                        when Service = 'P0A' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'P5A' 
                                        when Service = 'P1A' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'P3A'
                                        when Service = 'P1A' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'P5A' 
                                        when Service = 'P3A' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'P5A' 
                                        when Service = 'PP0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'PP3' 
                                        when Service = 'PP0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PP5' 
                                        when Service = 'PP1' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'PP3'
                                        when Service = 'PP1' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PP5' 
                                        when Service = 'PP3' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PP5' 
                                        when Service = 'PS0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'PS3' 
                                        when Service = 'PS0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PS5' 
                                        when Service = 'PS1' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'PS3'
                                        when Service = 'PS1' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PS5' 
                                        when Service = 'PS3' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PS5' 
                                        when Service = 'PT20' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'PT23' 
                                        when Service = 'PT20' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PT25' 
                                        when Service = 'PT21' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'PT23'
                                        when Service = 'PT21' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PT25' 
                                        when Service = 'PT23' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PT25' 
                                        when Service = 'PT30' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'PT33' 
                                        when Service = 'PT30' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PT35' 
                                        when Service = 'PT31' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'PT33'
                                        when Service = 'PT31' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PT35' 
                                        when Service = 'PT33' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'PT35' 
                                        when Service = 'Y0A' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3A' 
                                        when Service = 'Y0A' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A' 
                                        when Service = 'Y1A' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3A'
                                        when Service = 'Y1A' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A' 
                                        when Service = 'Y3A' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A' 
                                        when Service = 'Y30' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y33' 
                                        when Service = 'Y30' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y35' 
                                        when Service = 'Y31' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y33'
                                        when Service = 'Y31' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y35' 
                                        when Service = 'Y33' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y35' 
                                        when Service = 'X0A' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'X3A' 
                                        when Service = 'X0A' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'X5A' 
                                        when Service = 'X1A' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'X3A'
                                        when Service = 'X1A' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'X5A' 
                                        when Service = 'X3A' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'X5A' 
                                        when Service = 'X30' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'X33' 
                                        when Service = 'X30' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'X35' 
                                        when Service = 'X31' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'X33'
                                        when Service = 'X31' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'X35' 
                                        when Service = 'X33' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'X35' 
                                        when Service = 'Y0A16' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3A16' 
                                        when Service = 'Y0A16' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A16' 
                                        when Service = 'Y1A16' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3A16'
                                        when Service = 'Y1A16' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A16' 
                                        when Service = 'Y3A16' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A16'
                                        when Service = 'Y0A17' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3A17' 
                                        when Service = 'Y0A17' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A17' 
                                        when Service = 'Y1A17' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3A17'
                                        when Service = 'Y1A17' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A17' 
                                        when Service = 'Y3A17' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A17'
                                        when Service = 'Y0A18' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3A18' 
                                        when Service = 'Y0A18' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A18' 
                                        when Service = 'Y1A18' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3A18'
                                        when Service = 'Y1A18' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A18' 
                                        when Service = 'Y3A18' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y5A18'
                                        when Service = 'Y3018' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3318' 
                                        when Service = 'Y3018' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y3518' 
                                        when Service = 'Y3118' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3318'
                                        when Service = 'Y3118' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y3518' 
                                        when Service = 'Y3318' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y3518'
                                        when Service = 'Y3017' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3317' 
                                        when Service = 'Y3017' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y3517' 
                                        when Service = 'Y3117' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3317'
                                        when Service = 'Y3117' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y3517' 
                                        when Service = 'Y3317' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y3517'
                                        when Service = 'Y3016' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3316' 
                                        when Service = 'Y3016' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y3516' 
                                        when Service = 'Y3116' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'Y3316'
                                        when Service = 'Y3116' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y3516' 
                                        when Service = 'Y3316' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'Y3516'
                                  Else '' end


Update #Temp1 SET NewServiceCode = Case when Service = 'AGEP0' and BilledWeight >1.00 and BilledWeight <=3.00 Then 'AGVP3' 
                                        when Service = 'AGEP0' and BilledWeight >3.00 and BilledWeight <=5.00 Then 'AGVP5' 
                                        when Service = 'AGEP0' and BilledWeight >5.00 and BilledWeight <=10.00 Then 'AVP10' 
                                        when Service = 'AGEP0' and BilledWeight >10.00 and BilledWeight <=25.00 Then 'AVP25' 
                                   Else '' end where [Accountcode] in ('112951223') and Service = 'AGEP0' 

DROP TABLE IF EXISTS #Temp2

SELECT DISTINCT [Accountcode], L.ca_account,l.[cp_pricecode] 
INTO #Temp2
FROM #Temp1 T left join [dbo].[CompanyPriceCodes] L on T.[Accountcode] = L.ca_account 
WHERE NewServiceCode = ''  
and l.[cp_pricecode]  not in (SELECT distinct ServiceCode  FROM [dbo].[ProntoBilling_ItemServiceCodes] where Category in ('500 Gms','1 Kg','3 Kgs','5 Kgs') and ServiceCode not in ('AGEBK','AGVBK','AGHBK','AGSBK'))

--Drop Table #Temp2

Update #Temp1 SET  NewServiceCode =  'AGEBK'  where NewServiceCode = '' and Service like 'AEP%'
Update #Temp1 SET  NewServiceCode =  'AGEBK'  where NewServiceCode = '' and Service like 'AGE%'
Update #Temp1 SET  NewServiceCode =  'AGHBK'  where NewServiceCode = '' and Service like 'AGH%'
Update #Temp1 SET  NewServiceCode =  'AGHBK'  where NewServiceCode = '' and Service like 'AHP%'
Update #Temp1 SET  NewServiceCode =  'AGSBK'  where NewServiceCode = '' and Service like 'AGS%'
Update #Temp1 SET  NewServiceCode =  'AGSBK'  where NewServiceCode = '' and Service like 'ASP%'
Update #Temp1 SET  NewServiceCode =  'AGVBK'  where NewServiceCode = '' and Service like 'AVP%' --
Update #Temp1 SET  NewServiceCode =  'AGVBK'  where NewServiceCode = '' and Service like 'AGVP%' --

Update #Temp1 SET  NewServiceCode =  'AGVBK'  where NewServiceCode = '' and [Accountcode]= '113102511' and BilledWeight >25.0

Update #Temp1 SET  NewServiceCode =  'P10', Comments = 'Changing to P10 assigned'   From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and BilledWeight >5.00 and BilledWeight <=10.00 and T2.cp_priceCode = 'P10' and Service in ('P0','P1','P3','P5','P10')
Update #Temp1 SET  NewServiceCode =  'P25', Comments = 'Changing to P25 assigned'   From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and BilledWeight >10.00 and BilledWeight <=25.00 and T2.cp_priceCode = 'P25' and Service in ('P0','P1','P3','P5','P10')
Update #Temp1 SET  NewServiceCode =  'BUK'  where NewServiceCode = '' and Service like 'BU%' --
Update #Temp1 SET  NewServiceCode =  'NPU'  where NewServiceCode = '' and Service like 'DS%' --
Update #Temp1 SET  NewServiceCode =  'FRAN1'  where NewServiceCode = '' and Service like 'FRP%' --


---

Update #Temp1 SET  NewServiceCode =  'I00', Comments = 'Changing to I00 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'I00'
Update #Temp1 SET  NewServiceCode =  'I05', Comments = 'Changing to I05 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'I05'
Update #Temp1 SET  NewServiceCode =  'I11', Comments = 'Changing to I11 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'I11'
Update #Temp1 SET  NewServiceCode =  'I22', Comments = 'Changing to I22 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'I22'
Update #Temp1 SET  NewServiceCode =  'I33', Comments = 'Changing to I33 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'I33'
Update #Temp1 SET  NewServiceCode =  'I44', Comments = 'Changing to I44 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'I44'
Update #Temp1 SET  NewServiceCode =  'I50', Comments = 'Changing to I50 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'I50'
Update #Temp1 SET  NewServiceCode =  'I55', Comments = 'Changing to I55 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'I55'
Update #Temp1 SET  NewServiceCode =  'I66', Comments = 'Changing to I66 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'I66'
Update #Temp1 SET  NewServiceCode =  'I77', Comments = 'Changing to I77 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'I77'


Update #Temp1 SET  NewServiceCode =  'A11', Comments = 'Changing to A11 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'A11'
Update #Temp1 SET  NewServiceCode =  'A22', Comments = 'Changing to A22 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'A22'
Update #Temp1 SET  NewServiceCode =  'A33', Comments = 'Changing to A33 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'A33'
Update #Temp1 SET  NewServiceCode =  'A44', Comments = 'Changing to A44 assigned'    From #Temp1 T1 join #Temp2 T2 on T1. [Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'A44'
Update #Temp1 SET  NewServiceCode =  'A50', Comments = 'Changing to A50 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'A50'
Update #Temp1 SET  NewServiceCode =  'A55', Comments = 'Changing to A55 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'A55'
Update #Temp1 SET  NewServiceCode =  'A66', Comments = 'Changing to A66 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'A66'
Update #Temp1 SET  NewServiceCode =  'E22', Comments = 'Changing to E22 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'E22'
Update #Temp1 SET  NewServiceCode =  'E44', Comments = 'Changing to E44 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'E44'
Update #Temp1 SET  NewServiceCode =  'E55', Comments = 'Changing to E55 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'E55'
Update #Temp1 SET  NewServiceCode =  'G77', Comments = 'Changing to G77 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'G77'
Update #Temp1 SET  NewServiceCode =  'G88', Comments = 'Changing to G88 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[Accountcode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'G88'
Update #Temp1 SET  NewServiceCode =  'AC1', Comments = 'Changing to AC1 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'AC1'
Update #Temp1 SET  NewServiceCode =  'AC2', Comments = 'Changing to AC2 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'AC2'
Update #Temp1 SET  NewServiceCode =  'AC3', Comments = 'Changing to AC3 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'AC3'
Update #Temp1 SET  NewServiceCode =  'AC4', Comments = 'Changing to AC4 assigned'    From #Temp1 T1 join #Temp2 T2 on  T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'AC4'
Update #Temp1 SET  NewServiceCode =  'AC5', Comments = 'Changing to AC5 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'AC5'

--Changing L Rates based on Kyeeless Request
Update #Temp1 SET  NewServiceCode =  'L05', Comments = 'Changing to L05 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L05'
Update #Temp1 SET  NewServiceCode =  'L10', Comments = 'Changing to L10 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L10'
Update #Temp1 SET  NewServiceCode =  'L11', Comments = 'Changing to L11 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L11'
Update #Temp1 SET  NewServiceCode =  'L22', Comments = 'Changing to L22 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L22'
Update #Temp1 SET  NewServiceCode =  'L33', Comments = 'Changing to L33 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L33'
Update #Temp1 SET  NewServiceCode =  'L44', Comments = 'Changing to L44 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L44'
Update #Temp1 SET  NewServiceCode =  'L50', Comments = 'Changing to L50 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L50'
Update #Temp1 SET  NewServiceCode =  'L55', Comments = 'Changing to L55 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L55'
Update #Temp1 SET  NewServiceCode =  'L66', Comments = 'Changing to L66 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L66'
Update #Temp1 SET  NewServiceCode =  'L77', Comments = 'Changing to L77 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L77'

Update #Temp1 SET  NewServiceCode =  'L0518', Comments = 'Changing to L0517 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L0518'
Update #Temp1 SET  NewServiceCode =  'L1018', Comments = 'Changing to L1017 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L1018'
Update #Temp1 SET  NewServiceCode =  'L1118', Comments = 'Changing to L1117 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L1118'
Update #Temp1 SET  NewServiceCode =  'L2218', Comments = 'Changing to L2217 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L2218'
Update #Temp1 SET  NewServiceCode =  'L3318', Comments = 'Changing to L3317 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L3318'
Update #Temp1 SET  NewServiceCode =  'L4418', Comments = 'Changing to L4417 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L4418'
Update #Temp1 SET  NewServiceCode =  'L5018', Comments = 'Changing to L5017 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L5018'
Update #Temp1 SET  NewServiceCode =  'L5518', Comments = 'Changing to L5517 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L5518'
Update #Temp1 SET  NewServiceCode =  'L6618', Comments = 'Changing to L6617 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L6618'
Update #Temp1 SET  NewServiceCode =  'L7718', Comments = 'Changing to L7717 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L7718'


----
  --Select * from #Temp1 where NewServiceCode = ''
Update #Temp1 SET  NewServiceCode =  'L0517', Comments = 'Changing to L0517 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L0517'
Update #Temp1 SET  NewServiceCode =  'L1017', Comments = 'Changing to L1017 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L1017'
Update #Temp1 SET  NewServiceCode =  'L1117', Comments = 'Changing to L1117 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L1117'
Update #Temp1 SET  NewServiceCode =  'L2217', Comments = 'Changing to L2217 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L2217'
Update #Temp1 SET  NewServiceCode =  'L3317', Comments = 'Changing to L3317 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L3317'
Update #Temp1 SET  NewServiceCode =  'L4417', Comments = 'Changing to L4417 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L4417'
Update #Temp1 SET  NewServiceCode =  'L5017', Comments = 'Changing to L5017 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L5017'
Update #Temp1 SET  NewServiceCode =  'L5517', Comments = 'Changing to L5517 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L5517'
Update #Temp1 SET  NewServiceCode =  'L6617', Comments = 'Changing to L6617 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L6617'
Update #Temp1 SET  NewServiceCode =  'L7717', Comments = 'Changing to L7717 assigned'     From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L7717'
Update #Temp1 SET  NewServiceCode =  'L00', Comments = 'Changing to L00 assigned'         From #Temp1 T1 join #Temp2 T2 on T1.[AccountCode] = T2.ca_account   where  NewServiceCode ='' and  T2.cp_priceCode = 'L00'

--SS removed no card rate
Update #Temp1 SET  NewServiceCode =  'L11' , Comments = 'Default L11'  where NewServiceCode = '' 
--SS added space
Update #Temp1 SET Comments=  'Changing SC from ' + Service + ' to ' + NewServiceCode where Comments = ''

--TRUNCATE TABLE dbo.ProntoBilling_SCAudit
DROP TABLE IF EXISTS dbo.ProntoBilling_SCAudit
/*
INSERT INTO dbo.ProntoBilling_SCAudit
(
		Consignmentid
		,Accountcode
		,ConsignmentNumber
		,ConsignmentDate
		,Service
		,NewServiceCode
		,[Item quantity]
		,DeclaredWEight
		,DeclaredVolume
		,DeclaredCubicWeight
		,MeasuredWeight
		,MeasuredVolume
		,MeasuredCubicWeight
		,BilledWeight
		,Comments
)
*/
SELECT [Consignmentid]
      ,[Accountcode]
      ,[ConsignmentNumber]
      ,[ConsignmentDate]
      ,[Service]
      ,[NewServiceCode]
      ,[Item quantity] AS [ItemQuantity]
      ,[DeclaredWeight]
      ,[DeclaredVolume]
      ,[DeclaredCubicWeight]
      ,[MeasuredWeight]
      ,[MeasuredVolume]
      ,[MeasuredCubicWeight]
      ,[BilledWeight]
      ,[Comments]
INTO dbo.ProntoBilling_SCAudit
FROM #Temp1

/*
update [dbo].[ProntoBilling]
set [service] = NewServiceCode,[Manifest reference] = comments,EditWho = 'System',EditDateTime = Getdate(),isServiceUpdated = 1
From [dbo].[ProntoBilling] a1
inner join #temp1 b1
On(a1.ProntoBIlling_ID = b1.ProntoBIlling_ID)
where isServiceUpdated = 0
*/

End

/*
select * from dbo.ProntoBilling_SCAudit where consignmentnumber = 'CPA358C0005644'
select  * from [dbo].[GETPricingDataForCWC_Measured] where ConsignmentID = 73057484
SELECT distinct ServiceCode  FROM [dbo].[ProntoBilling_ItemServiceCodes] where servicecode = 'I77'
*/








GO
