SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Insert_IncomingJSON]
(
	@JSON NVARCHAR(MAX)
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRAN

	BEGIN TRY

	DECLARE @SortResultJSONID int = -1
-- truncate table [dbo].[Incoming_SortResultJSON]
-- select * from [dbo].[Incoming_SortResultJSON]

	INSERT INTO [dbo].[Incoming_SortResultJSON] ([SortResultJSON])
	SELECT	@JSON

	SET @SortResultJSONID = @@IDENTITY

	END TRY

	BEGIN CATCH

		IF @@TRANCOUNT >0
		BEGIN		

				ROLLBACK TRAN;
				INSERT INTO [Incoming_ParseErrorLog]
					([SortResultJSONID]
					,[SortResultJSON]
					,[ErrorNumber]
					,[ErrorSeverity]
					,[ErrorState]
					,[ErrorLine]
					,[ErrorProcedure]
					,[ErrorMessage]
					,[CreatedDateTime])
					SELECT
					 @SortResultJSONID
					,@JSON
					,ERROR_NUMBER() AS ErrorNumber  
					,ERROR_SEVERITY() AS ErrorSeverity  
					,ERROR_STATE() AS ErrorState  
					,ERROR_LINE () AS ErrorLine  
					,ERROR_PROCEDURE() AS ErrorProcedure  
					,ERROR_MESSAGE() AS ErrorMessage
					,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime;
		END;

		THROW;

	END CATCH

	IF @@TRANCOUNT >0
	BEGIN	
		COMMIT TRAN;
		EXEC [dbo].[PARSE_IncomingJSON] @SortResultJSONID
	END
END
GO
