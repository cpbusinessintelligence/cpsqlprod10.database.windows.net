SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Insert_SortResult_TrackingEvent_History]
(
	@Lower INT,
	@Upper INT
)
AS
BEGIN

    SET NOCOUNT ON

	INSERT INTO dbo.Incoming_SortResult_Tracking_History 
	(SortResultJSONID,DWSResultId,TrackingNumber,ConsignmentNumber,EventDateTime,ScanEvent,DriverRunNumber,Branch,ExceptionReason,AlternateBarcode,RedeliveryCard,DLB,
	[URL],podname,PODImageURL,IsProcessed,CreateDateTime)

	SELECT SortResultJSONID,DWSResultId,TrackingNumber,ConsignmentNumber,EventDateTime,ScanEvent,DriverRunNumber,Branch,ExceptionReason,AlternateBarcode,RedeliveryCard,DLB,
	[URL],podname,PODImageURL,
	CAST(1 AS BIT) AS IsProcessed ,
	CONVERT(DATETIME, getdate()  AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreateDateTime
	FROM dbo.Incoming_SortResult_Tracking_Staging stg 
	WHERE STG.[RowId] BETWEEN @Lower AND @Upper		

END

GO
