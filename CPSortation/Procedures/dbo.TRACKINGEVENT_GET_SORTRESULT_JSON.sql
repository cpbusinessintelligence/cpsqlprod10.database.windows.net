SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TRACKINGEVENT_GET_SORTRESULT_JSON]
(
	@Lower INT = 1,
	@Upper INT = 5
)
AS
BEGIN

    SET NOCOUNT ON

	SELECT (
		SELECT 
			DISTINCT 
				[TrackingNumber]
				,[ConsignmentNumber]
				--,FORMAT( EventDateTime, 'yyyy-MM-dd hh:mm:ss tt' ) EventDateTime
				--,left(ltrim(rtrim(cast(eventdatetime as varchar(50)))),19) EventDateTime
				--,CAST(left(ltrim(rtrim(cast(eventdatetime as varchar(50)))),19) as varchar(20)) EventDateTime
				,EventDateTime
				,[ScanEvent]
				,[DriverRunNumber]
				,[Branch]
				,[ExceptionReason]
				,[AlternateBarcode]
				,[RedeliveryCard]
				,[DLB]
				,[URL]
				,[podname]
				,[PODImageURL]
		FROM dbo.Incoming_SortResult_Tracking_Staging 
		WHERE  [RowId] BETWEEN @Lower AND @Upper and ISNULL([ExceptionReason],'') <> ''
		FOR JSON PATH, INCLUDE_NULL_VALUES
	) AS JSONString
END

GO
