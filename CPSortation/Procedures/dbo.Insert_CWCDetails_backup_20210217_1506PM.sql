SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Insert_CWCDetails_backup_20210217_1506PM]
AS
BEGIN




DROP TABLE IF EXISTS #tempCoupon

SELECT DISTINCT cc_consignment,cc_coupon,cd_account 
INTO #tempCoupon
FROM dbo.edi_cdcoupon 
WITH(NOLOCK) join edi_consignment  WITH(NOLOCK) ON cc_consignment = cd_id
WHERE cc_coupon in (SELECT DISTINCT Barcodes FROM [Incoming_DWSResult] WITH(NOLOCK))

CREATE NONCLUSTERED INDEX idx_tempCoupon_cc_coupon ON #tempCoupon(cc_coupon)

--TRUNCATE TABLE dbo.[Incoming_CWCDetails]

DROP TABLE IF EXISTS #tempwebCoupon

 SELECT DISTINCT c.ConsignmentID ,w.LabelNumber,AccountNumber
INTO #tempwebCoupon
FROM [dbo].[website_tblItemLabel] w
WITH(NOLOCK) join [dbo].[website_tblConsignment]  c WITH(NOLOCK) ON w.consignmentID = c.ConsignmentID
WHERE LabelNumber in (SELECT DISTINCT Barcodes FROM [Incoming_DWSResult] WITH(NOLOCK))

INSERT INTO dbo.[Incoming_CWCDetails]
(
	  [DWSResultId]
      ,[SortResultJSONID]
      ,[DwsTimestamp]
      ,[CubeLength]
      ,[CubeWidth]
      ,[CubeHeight]
      ,[Volume]
      ,[Weight]
      ,[Barcodes]
	  ,[SorterName]
	  ,[cd_Connote]
	  ,[cd_id]
	  ,[AccountCode]
	  ,[CreateDateTime]
)
SELECT 
	  [DWSResultId]
      ,[SortResultJSONID]
      ,[DwsTimestamp]
      ,[CubeLength]
      ,[CubeWidth]
      ,[CubeHeight]
      ,[Volume]
      ,[Weight]
      ,[Barcodes]
	  ,[SorterName]
	  ,[ConsignmentId]
	  ,cc.cc_consignment as cd_id
	  ,CC.cd_account as AccountCode
	  ,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreateDateTime

FROM [dbo].[Incoming_DWSResult] dr WITH(NOLOCK) 
JOIN #tempCoupon cc ON cc.cc_coupon = dr.Barcodes 
WHERE [DWSResultId] NOT IN (SELECT [DWSResultId] FROM dbo.[Incoming_CWCDetails] WITH(NOLOCK))


INSERT INTO dbo.[Incoming_CWCDetails]
(
	  [DWSResultId]
      ,[SortResultJSONID]
      ,[DwsTimestamp]
      ,[CubeLength]
      ,[CubeWidth]
      ,[CubeHeight]
      ,[Volume]
      ,[Weight]
      ,[Barcodes]
	  ,[SorterName]
	  ,[cd_Connote]
	  ,[cd_id]
	  ,[AccountCode]
	  ,[CreateDateTime]
)
SELECT 
	  [DWSResultId]
      ,[SortResultJSONID]
      ,[DwsTimestamp]
      ,[CubeLength]
      ,[CubeWidth]
      ,[CubeHeight]
      ,[Volume]
      ,[Weight]
      ,[Barcodes]
	  ,[SorterName]
	  ,dr.[ConsignmentId]
	  ,cc.ConsignmentID as cd_id
	  ,CC.AccountNumber as AccountCode
	  ,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreateDateTime

FROM [dbo].[Incoming_DWSResult] dr WITH(NOLOCK) 
JOIN #tempwebCoupon cc ON cc.LabelNumber = dr.Barcodes 
WHERE [DWSResultId] NOT IN (SELECT [DWSResultId] FROM dbo.[Incoming_CWCDetails] WITH(NOLOCK))


END
GO
