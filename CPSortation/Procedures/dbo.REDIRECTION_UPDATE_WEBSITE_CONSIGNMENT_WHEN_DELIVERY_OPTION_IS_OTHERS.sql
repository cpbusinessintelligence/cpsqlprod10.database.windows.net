SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[REDIRECTION_UPDATE_WEBSITE_CONSIGNMENT_WHEN_DELIVERY_OPTION_IS_OTHERS]
	
AS
BEGIN

	SET NOCOUNT ON;

    UPDATE [website_tblConsignment]
	SET	
		[NewDestinationFirstName] = RED.NewDestinationFirstName,
		[NewDestinationLastName] = RED.NewDestinationLastName,
		[NewDestinationCompanyName] = RED.NewDestinationCompanyName,
		[NewDestinationEmail] = RED.NewDestinationEmail,
		[NewDestinationAddress1] = RED.NewDestinationAddress1,
		[NewDestinationAddress2] = RED.NewDestinationAddress2,
		[NewDestinationSuburb] = RED.NewDestinationSuburb,
		[NewDestinationStateName] = RED.NewDestinationStateName,
		[NewDestinationStateID] = RED.NewDestinationStateID,
		[NewDestinationPostCode] = RED.NewDestinationPostCode,
		[NewDestinationPhone] = RED.NewDestinationPhone,
		[NewDestinationCountry] = RED.NewDestinationCountry,
		[SpecialInstruction] = left(RED.SpecialInstruction,64),	
		[IsATl] = RED.ATL,
		[CPSortation_Updated_Stamp] = CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'),
		[Locked_For_Editing_After_Redirection] = 1
			OUTPUT
				 INSERTED.ConsignmentID
				,INSERTED.ConsignmentCode
				,INSERTED.AccountNumber
				,INSERTED.PickupFirstName
				,INSERTED.PickupLastName
				,INSERTED.PickupCompanyName
				,INSERTED.PickupEmail
				,INSERTED.PickupAddress1
				,INSERTED.PickupAddress2
				,INSERTED.PickupSuburb
				,INSERTED.PickupStateName
				,INSERTED.PickupStateID
				,INSERTED.PickupPostCode
				,INSERTED.PickupPhone
				,INSERTED.PickupCountry
				,INSERTED.NewDestinationFirstName AS DestinationFirstName
				,INSERTED.NewDestinationLastName AS DestinationLastName
				,INSERTED.NewDestinationCompanyName AS DestinationCompanyName
				,INSERTED.NewDestinationEmail AS DestinationEmail
				,INSERTED.NewDestinationAddress1 AS DestinationAddress1
				,INSERTED.NewDestinationAddress2 AS DestinationAddress2
				,INSERTED.NewDestinationSuburb AS DestinationSuburb
				,INSERTED.NewDestinationStateName AS DestinationStateName
				,INSERTED.NewDestinationStateID AS DestinationStateID
				,INSERTED.NewDestinationPostCode AS DestinationPostCode
				,INSERTED.NewDestinationPhone AS DestinationPhone
				,INSERTED.NewDestinationCountry AS DestinationCountry
				,INSERTED.TotalWeight
				,INSERTED.TotalVolume
				,INSERTED.NoOfItems
				,INSERTED.SpecialInstruction
				,INSERTED.DangerousGoods
				,INSERTED.RateCardID
				,INSERTED.CreatedDateTime
				,INSERTED.IsInternational
				,INSERTED.IsATl
				,INSERTED.InsuranceCategory
				,'UP' AS Delivery_Status_Flag
				,RED.SelectedDeliveryOption AS RedirectedDeliveryOption
				,NULL AS RescheduledDeliveryDate
			INTO
				[redirection_website_incremental_tblConsignment]
		FROM 
			[website_tblConsignment] CON  WITH (NOLOCK)
		INNER JOIN
			[redirection_staging_tblRedirectedConsignment] RED WITH (NOLOCK)
		ON 
			CON.ConsignmentCode = RED.ConsignmentCode			
		WHERE 			
			CON.[Locked_For_Editing_After_Redirection] = 0
			AND
			(RED.SelectedDeliveryOption LIKE 'Neighbour%' OR  RED.SelectedDeliveryOption LIKE 'POP%' OR  RED.SelectedDeliveryOption LIKE 'Alternate%')
			AND
			RED.ConsignmentCode LIKE ('CPW%');
END
GO
