SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[UPDATE_tblConsignmentDestination_Redirect_Web_History] AS

BEGIN

SET NOCOUNT ON;
SET XACT_ABORT ON;

BEGIN TRAN

BEGIN TRY

--TRUNCATE TABLE [dbo].[tblConsignmentDestination_Redirect_Web_History]
INSERT INTO [dbo].[tblConsignmentDestination_Redirect_Web_History]
SELECT * FROM tblConsignmentDestination_Redirect_Web

END TRY

BEGIN CATCH

		IF @@TRANCOUNT >0
		BEGIN		
				ROLLBACK TRAN;
				
					SELECT
					ERROR_NUMBER() AS ErrorNumber  
					,ERROR_SEVERITY() AS ErrorSeverity  
					,ERROR_STATE() AS ErrorState  
					,ERROR_LINE () AS ErrorLine  
					,ERROR_PROCEDURE() AS ErrorProcedure  
					,ERROR_MESSAGE() AS ErrorMessage
					,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime;
		END;

		THROW;

END CATCH

	IF @@TRANCOUNT >0
	BEGIN	
		COMMIT TRAN;
	END

END
GO
