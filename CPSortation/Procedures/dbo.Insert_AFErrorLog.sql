SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Insert_AFErrorLog]
(
	@JSON NVARCHAR(MAX),@EXCEPTION NVARCHAR(MAX)
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRAN

	BEGIN TRY

	
	INSERT INTO [dbo].[Incoming_AFErrorLog] ([SortResultJSON],[Exception])
	SELECT	@JSON,@EXCEPTION

	END TRY

	BEGIN CATCH

		IF @@TRANCOUNT >0
		BEGIN		

				ROLLBACK TRAN;
				INSERT INTO [Incoming_SortErrorLog]
					(SortResultJSON
					,[ErrorNumber]
					,[ErrorSeverity]
					,[ErrorState]
					,[ErrorLine]
					,[ErrorProcedure]
					,[ErrorMessage]
					,[CreatedDateTime])
					SELECT
					 @JSON
					,ERROR_NUMBER() AS ErrorNumber  
					,ERROR_SEVERITY() AS ErrorSeverity  
					,ERROR_STATE() AS ErrorState  
					,ERROR_LINE () AS ErrorLine  
					,ERROR_PROCEDURE() AS ErrorProcedure  
					,ERROR_MESSAGE() AS ErrorMessage
					,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime;
		END;

		THROW;

	END CATCH

	IF @@TRANCOUNT >0
	BEGIN	
		COMMIT TRAN;
	END
END
GO
