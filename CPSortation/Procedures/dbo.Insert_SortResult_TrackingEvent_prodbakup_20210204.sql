SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Insert_SortResult_TrackingEvent_prodbakup_20210204] 

AS

BEGIN


TRUNCATE TABLE dbo.Incoming_SortResult_Tracking_Staging

--select count(*) from dbo.Incoming_SortResult_TrackingEvent_Staging

INSERT INTO dbo.Incoming_SortResult_Tracking_Staging
(
	SortResultJSONID,
	DWSResultId,
	TrackingNumber,
	ConsignmentNumber,
	EventDateTime,
	ScanEvent,
	DriverRunNumber,
	Branch,
	ExceptionReason,
	AlternateBarcode,
	RedeliveryCard,
	DLB,
	[URL],
	podname,
	PODImageURL
)
SELECT	
	dws.SortResultJSONID,
	dws.DWSResultId,
	ISNULL(sd.KnownBarcodes,'') as TrackingNumber,
	ISNULL(dws.ConsignmentId,'') as ConsignmentNumber,
	CAST(FORMAT(CONVERT(DATETIME,dws.ReceivedTime),'yyyy-MM-dd HH:mm:ss') as varchar(30)) as EventDateTime,
	'Freetext' as ScanEvent,
	cast(ISNULL(cast(right(ltrim(rtrim(dws.destid)),4) as int),'') as varchar(4)) as DriverRunNumber,
    'Sydney' AS Branch,
	ISNULL(rtrim(ltrim(lp.tracking)),'') AS ExceptionReason,
	'' as AlternateBarcode,
	'' as RedeliveryCard,
	'' as DLB,
	'' as URL,
	'' as podname,
	'' as PODImageURL
FROM [dbo].[Incoming_DWSResult] dws
INNER JOIN [dbo].[Incoming_SortDetail] sd on dws.DWSResultId = sd.DWSResultId and dws.SortResultJSONID = sd.SortResultJSONID
INNER JOIN [dbo].[Incoming_SortAttempt] sa on dws.DWSResultId = sa.DWSResultId and dws.SortResultJSONID = sa.SortResultJSONID and sa.Sorted = 1
OUTER APPLY (SELECT tracking FROM [dbo].[Incoming_SortResultLookup] WHERE destinationid = dws.DestID) lp
WHERE dws.DWSResultId not in (SELECT DWSResultId FROM dbo.Incoming_SortResult_Tracking_History WHERE IsProcessed = 1)


UPDATE dbo.Incoming_SortResult_Tracking_Staging SET DriverRunNumber = '9407' WHERE DriverRunNumber = '0'

/*

truncate table dbo.Incoming_SortResult_Tracking_Staging

select * from dbo.Incoming_SortResult_Tracking_Staging with(nolock)

select *   FROM [dbo].[Incoming_SortResult_Tracking_log] with(nolock) where rowid > 1262 order by rowid desc

select * from dbo.Incoming_SortResult_Tracking_history with(nolock)  order by rowid desc

update dbo.Incoming_SortResult_Tracking_history set IsProcessed = 0 where IsProcessed = 1 and sortresultjsonid in (6029,6026,6025,6025,6023)

*/
END
GO
