SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[UPDATE_tblAddressMasterOpsCopy]
AS
BEGIN

DROP TABLE IF EXISTS dbo.tblAddressMasterOpsCopy

SELECT  addressmasterid,
		[delivy_point_id],
		CAST(HASHBYTES('SHA1', 
			CONCAT(CAST(delivy_point_id AS VARCHAR(15))
						,CAST(house_nbr_1 AS VARCHAR(15))
						,house_nbr_sfx_1
						,CAST(house_nbr_2 AS VARCHAR(15))
						,house_nbr_sfx_2
						,flat_unit_type
						,flat_unit_type_full
						,flat_unit_nbr
						,floor_level_type
						,floor_level_type_full
						,floor_level_nbr
						,lot_nbr
						,CAST(postal_delivery_nbr AS VARCHAR(15))
						,postal_delivery_nbr_pfx
						,postal_delivery_nbr_sfx
						,primary_point_ind
						,CAST(delivy_point_group_id AS VARCHAR(15))
						,postal_delivery_type
						,street_name
						,street_type
						,street_sfx
						,street_type_full
						,CAST(delivy_point_group_did AS VARCHAR(15))
						,CAST(locality_id AS VARCHAR(15))
						,locality_name
						,postcode
						,[state]
						,CAST(locality_did AS VARCHAR(15))
						,full_address
						,[address]
						,CAST(confidence_level AS VARCHAR(15))
						,feature
						,latitude
						,longitude
				)) AS BIGINT) addressmasterhash ,
			[headportsort],
			[secondarysort],
			[drivernumber],
			'O'	AS [source],
			1 AS [isactive],
			getdate()		AS [crtd_datetime],
			suser_sname()	AS [crtd_userid],
			getdate()		AS [last_upd_datetime],
			suser_sname()	AS [last_upd_userid]

INTO dbo.tblAddressMasterCopyOps 

FROM [dbo].[tblAddressMasterOps]



END
GO
