SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[WEBSITE_UPDATE_ITEM_LABEL]          

AS
BEGIN

                SET NOCOUNT ON;

    UPDATE [website_tblItemLabel]
                SET         
                                                [ConsignmentID] = UPD.ConsignmentID,
                                                [LabelNumber] = UPD.LabelNumber,
                                                [Length] = UPD.[Length],
                                                [Width] = UPD.Width,
                                                [Height] = UPD.Height,
                                                [CubicWeight] = UPD.CubicWeight,
                                                [MeasureLength] = UPD.MeasureLength,
                                                [MeasureWidth] = UPD.MeasureWidth,
                                                [MeasureHeight] = UPD.MeasureHeight,
                                                [MeasureCubicWeight] = UPD.MeasureCubicWeight,
                                                [PhysicalWeight] = UPD.PhysicalWeight,
                                                [MeasureWeight] = UPD.MeasureWeight,
                                                [DeclareVolume] = UPD.DeclareVolume,
                                                [LastActivity] = UPD.LastActivity,
                                                [LastActivityDateTime] = UPD.LastActivityDateTime,
                                                [UnitValue] = UPD.UnitValue,
                                                [Quantity] = UPD.Quantity,
                                                [CountryOfOrigin] = UPD.CountryOfOrigin,
                                                [Description] = UPD.[Description],
                                                [HSTariffNumber] = UPD.HSTariffNumber,
                                                [CreatedDateTime] = UPD.CreatedDateTime,
                                                [CreatedBy] = UPD.CreatedBy,
                                                [UpdatedDateTime] = UPD.UpdatedDateTime,
                                                [UpdatedBy] = UPD.UpdatedBy,
                                                [DHLBarCode] = UPD.DHLBarCode
                                FROM 
                                                [website_tblItemLabel] LBL WITH (NOLOCK)
                                INNER JOIN
                                                [website_updated_tblItemLabel] UPD WITH (NOLOCK)
                                ON
                                                LBL.[ItemLabelID] = UPD.[ItemLabelID]


--------------------- Insert Consignment with missing labels into seperate table 23/10/2019


INSERT INTO [website_incremental_tblConsignment_missinglabels]
(
                                ConsignmentID,
                                ConsignmentCode,
                                CreatedDateTime,
                                RateCardID,
                                AccountNumber,
                                PickupCompanyName,
                                PickupPhone,
                                PickupFirstName, 
                                PickupLastName,
                                PickupAddress1,
                                PickupAddress2,
                                PickupSuburb,                                                                   
                                PickupStateID,
                                PickupStateName,
                                PickupPostCode,
                                PickupCountry,
                                DestinationCompanyName,
                                DestinationFirstName,
                                DestinationLastName,
                                DestinationAddress1,
                                DestinationAddress2,
                                DestinationSuburb,
                                DestinationStateID,
                                DestinationStateName,
                                DestinationPostCode,
                                DestinationCountry,
                                DestinationPhone,
                                [SpecialInstruction],
                                NoOfItems,
                                TotalWeight,
                                IsInternational,
                                TotalVolume,
                                InsuranceCategory,
                                [DangerousGoods],
                                [IsATl],
                                Delivery_Status_Flag
)
SELECT 
                                ConsignmentID,
                                ConsignmentCode,
                                CreatedDateTime,
                                RateCardID,
                                AccountNumber,
                                PickupCompanyName,
                                PickupPhone,
                                PickupFirstName, 
                                PickupLastName,
                                PickupAddress1,
                                PickupAddress2,
                                PickupSuburb,                                                                   
                                PickupStateID,
                                PickupStateName,
                                PickupPostCode,
                                PickupCountry,
                                DestinationCompanyName,
                                DestinationFirstName,
                                DestinationLastName,
                                DestinationAddress1,
                                DestinationAddress2,
                                DestinationSuburb,
                                DestinationStateID,
                                DestinationStateName,
                                DestinationPostCode,
                                DestinationCountry,
                                DestinationPhone,
                                [SpecialInstruction],
                                NoOfItems,
                                TotalWeight,
                                IsInternational,
                                TotalVolume,
                                InsuranceCategory,
                                [DangerousGoods],
                                [IsATl],
                                Delivery_Status_Flag


FROM [website_incremental_tblConsignment] CON
CROSS APPLY (SELECT COUNT(DISTINCT LABELNUMBER) AS CouponCount FROM dbo.[website_tblItemLabel] WITH(NOLOCK) WHERE ConsignmentID = con.ConsignmentID ) coup
WHERE coup.CouponCount = 0 and
ConsignmentID not in (select ConsignmentID from dbo.[website_incremental_tblConsignment_missinglabels])
and ConsignmentCode not in (select ConsignmentNumber from dbo.outgoing_consignment_event_log with(NOLOCK))



END
GO
