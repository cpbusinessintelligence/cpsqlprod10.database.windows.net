SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PARSE_IncomingJSON_prod_bkup_20210204] 
(
	@SortResultJSONID INT = 0
)

AS

BEGIN

SET NOCOUNT ON;
SET XACT_ABORT ON;

DECLARE @DWSResultID int

BEGIN TRAN

BEGIN TRY

	
/*

TRUNCATE TABLE [dbo].[Incoming_ParseErrorLog]
TRUNCATE TABLE [dbo].[Incoming_DWSResult]	
TRUNCATE TABLE [dbo].[Incoming_SortDetail]
TRUNCATE TABLE [dbo].[Incoming_SortAttempt]
update [dbo].[Incoming_SortResultJSON] set IsProcessed = 0 where SortResultJSONID >=5
ItemPropVals 

EXEC [dbo].[PARSE_IncomingJSON_Bulk]
EXEC [dbo].[PARSE_IncomingJSON] 88

select * from [dbo].[Incoming_SortResultJSON] where SortResultJSONID = 183
select * FROM [dbo].[Incoming_ParseErrorLog]
select * from [dbo].[Incoming_DWSResult]	
select * from [dbo].[Incoming_SortDetail]
select * from [dbo].[Incoming_SortAttempt] where decisioninfo like '%sorted%'

*/

DECLARE @loopcounter int = 0,@ReceivedTime datetime2 = null

WHILE @loopcounter >= 0

BEGIN

		SET @ReceivedTime = (SELECT  JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.ReceivedTime')  AS ReceivedTime
		FROM [dbo].[Incoming_SortResultJSON] WHERE SortResultJSONID = @SortResultJSONID)

		IF @ReceivedTime is null BREAK

		;WITH DWS_Result_Query_1 AS
		(
		SELECT  TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.ReceivedTime') AS datetime2) AS ReceivedTime,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.DwsTimestamp') AS datetime2) AS DwsTimestamp,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.CubeLength') AS int) AS CubeLength,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.CubeWidth') AS int) AS CubeWidth,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.CubeHeight') AS int) AS CubeHeight,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.Volume') AS int) AS Volume,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.Weight') AS int) AS Weight,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.ObjectType') AS char(1)) AS ObjectType,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.Barcodes[0].Barcode') AS nvarchar(50)) AS Barcode,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.SiteField1') AS nvarchar(50)) AS SiteField1,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.SiteField2') AS nvarchar(50)) AS SiteField2,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.SiteField3') AS nvarchar(50)) AS SiteField3,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.DwsResult.Barcodes[0].Symbol') AS char(1)) AS Symbol,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SorterName') AS nvarchar(20)) AS SorterName,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].ConsigmentId') AS nvarchar(50)) AS ConsignmentId,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].ServiceCode') AS nvarchar(10)) AS ServiceCode,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].DestID') AS nvarchar(30)) AS DestID,
				TRY_CAST(JSON_QUERY([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].Photos') AS nvarchar(30)) AS Photos

		FROM [dbo].[Incoming_SortResultJSON] WHERE SortResultJSONID = @SortResultJSONID
		)
		INSERT INTO [dbo].[Incoming_DWSResult](SortResultJSONID,ReceivedTime,DwsTimestamp,CubeLength,CubeWidth,CubeHeight,Volume,[Weight],ObjectType,Barcodes,SiteField1,SiteField2,SiteField3,Symbol,SorterName,ConsignmentId,ServiceCode,DestID,Photos)
		SELECT @SortResultJSONID,ReceivedTime,DwsTimestamp,CubeLength,CubeWidth,CubeHeight,Volume,[Weight],ObjectType,Barcode,SiteField1,SiteField2,SiteField3,Symbol,SorterName,ConsignmentId,ServiceCode,DestID,Photos
		FROM DWS_Result_Query_1																																															 
														
		SET @DWSResultID = @@IDENTITY			

		;With SortDetail_Query_1 AS
		(
		SELECT  TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.SortLoopId') AS bit) AS SortLoopId,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.Sn') AS int) AS Sn,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.IsCal') AS bit) AS IsCal,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.KnownBarcodes[0]') AS nvarchar(50)) AS KnownBarcodes,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.UnknownBarcodes[0]') AS nvarchar(50)) AS UnknownBarcodes,
				--REPLACE(REPLACE(JSON_QUERY([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.SortAttempt'),']',''),'[','') AS SortAttempt,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.RemapFrom') AS int) AS RemapFrom,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.TransactionStart') AS datetime2) AS TransactionStart,
				TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.TransactionEnd') AS datetime2) AS TransactionEnd
		FROM [dbo].[Incoming_SortResultJSON] WHERE SortResultJSONID = @SortResultJSONID	
		)
		INSERT INTO [dbo].[Incoming_SortDetail](DWSResultId,SortResultJSONID,SortLoopId,Sn,IsCal,KnownBarcodes,UnknownBarcodes,RemapFrom,TransactionStart,TransactionEnd	)
		SELECT @DWSResultID,@SortResultJSONID,SortLoopId,Sn,IsCal,KnownBarcodes,UnknownBarcodes,RemapFrom,TransactionStart,TransactionEnd from 	SortDetail_Query_1	

		;With SortDetail_Query_2 AS
		(
		SELECT 
			TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.SortAttempt.AttemptBarcode') AS nvarchar(50)) AS [AttemptBarcode],
			TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.SortAttempt.Sorted') AS bit) AS Sorted,
			TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.SortAttempt.SortStatus') AS tinyint) AS [SortStatus],
			TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.SortAttempt.SortLocationId') AS int) AS [SortLocationId],
			TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.SortAttempt.SchemeTableId') AS int) AS [SchemeTableId],
			TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.SortAttempt.DecisionInfo') AS nvarchar(50)) AS [DecisionInfo],
			TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.SortAttempt.IsVcs') AS bit) AS [vcs],
			TRY_CAST(JSON_QUERY([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].SortResult.SortDetail.SortAttempt.ItemPropVals') AS nvarchar(100)) AS [ItemPropVals]
		FROM [dbo].[Incoming_SortResultJSON] WHERE SortResultJSONID = @SortResultJSONID	
		)

		INSERT INTO [dbo].[Incoming_SortAttempt](DWSResultId,SortResultJSONID,AttemptBarcode,Sorted,SortStatus,SortLocationId,SchemeTableId,DecisionInfo,vcs,ItemPropVals)
		SELECT @DWSResultID AS DWSResultID,@SortResultJSONID AS SortResultJSONID,[AttemptBarcode],Sorted,[SortStatus],[SortLocationId],[SchemeTableId],[DecisionInfo],[vcs],ItemPropVals
		FROM SortDetail_Query_2
	
		;With SortDetail_Query_3 AS
		(
		SELECT  
	
			TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.SortServerId') AS int) AS SortServerId
			,TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.DwsTrackInfo.SequenceNumber') AS bigint) AS SequenceNumber
			,TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.ReceiveTime') AS datetime2) AS ReceiveTime	
			,TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.CodingResult.CodingResultType') AS int) AS CodingResultType
			,TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.CodingResult.CodingRequestType') AS int) AS CodingRequestType
			,TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.SortAttempt.AttemptBarcode') AS nvarchar(50)) AS AttemptBarcode
			,TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.SortAttempt.Sorted') AS bit) AS Sorted
			,TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.SortAttempt.SortStatus') AS int) AS SortStatus
			,TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.SortAttempt.SortLocationId') AS int) AS SortLocationId
			,TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.SortAttempt.DecisionInfo') AS nvarchar(200)) AS DecisionInfo
			,TRY_CAST(JSON_VALUE([SortResultJSON],'$.Items['+ltrim(rtrim(cast(@loopcounter as char(5))))+'].CodingSortResult.SortAttempt.IsVcs') AS bit) AS IsVcs

		FROM [dbo].[Incoming_SortResultJSON] WHERE SortResultJSONID = @SortResultJSONID
		)

		INSERT INTO [dbo].[Incoming_VCSCodingSortResult](DWSResultID,SortResultJSONID,SortServerId,SequenceNumber,ReceiveTime,CodingResultType,CodingRequestType,AttemptBarcode,Sorted,SortStatus,SortLocationId,DecisionInfo,IsVcs)
		SELECT @DWSResultID AS DWSResultID,@SortResultJSONID AS SortResultJSONID,SortServerId,SequenceNumber,ReceiveTime,CodingResultType,CodingRequestType,AttemptBarcode,Sorted,SortStatus,SortLocationId,DecisionInfo,IsVcs 
		FROM SortDetail_Query_3

		update [dbo].[Incoming_DWSResult] set destid = destid.destinationid
		from [dbo].[Incoming_DWSResult] dws
		inner join [dbo].[Incoming_SortAttempt] sa on dws.DWSResultId = sa.DWSResultId and dws.SortResultJSONID = sa.SortResultJSONID and sa.Sorted = 1
		outer apply (select replace(replace([Value],'"12":',''),'"','') as destinationid from string_split(replace(replace(sa.ItemPropVals,'{',''),'}',''),',') where [value] like '"12"%') destid
		where destid is null and dws.SortResultJSONID = @SortResultJSONID	

		update [dbo].[Incoming_DWSResult] set ServiceCode = ServiceCode.SCode
		from [dbo].[Incoming_DWSResult] dws
		inner join [dbo].[Incoming_SortAttempt] sa on dws.DWSResultId = sa.DWSResultId and dws.SortResultJSONID = sa.SortResultJSONID and sa.Sorted = 1
		outer apply (select replace(replace([Value],'"6":',''),'"','') as SCode from string_split(replace(replace(sa.ItemPropVals,'{',''),'}',''),',') where [value] like '"6"%') ServiceCode
		where ServiceCode is null and dws.SortResultJSONID = @SortResultJSONID	


		SET @loopcounter = @loopcounter + 1													 
END

END TRY

BEGIN CATCH

		IF @@TRANCOUNT >0
		BEGIN		
				ROLLBACK TRAN;
					INSERT INTO [Incoming_ParseErrorLog]
					(SortResultJSONID
					,[ErrorNumber]
					,[ErrorSeverity]
					,[ErrorState]
					,[ErrorLine]
					,[ErrorProcedure]
					,[ErrorMessage]
					,[CreatedDateTime])
								
					SELECT
						@SortResultJSONID
						,ERROR_NUMBER() AS ErrorNumber  
						,ERROR_SEVERITY() AS ErrorSeverity  
						,ERROR_STATE() AS ErrorState  
						,ERROR_LINE () AS ErrorLine  
						,ERROR_PROCEDURE() AS ErrorProcedure  
						,ERROR_MESSAGE() AS ErrorMessage
						,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime;
		END;

		THROW;

END CATCH

	IF @@TRANCOUNT >0
	BEGIN	
		COMMIT TRAN;
		UPDATE [dbo].[Incoming_SortResultJSON] SET IsProcessed = 1 WHERE SortResultJSONID = @SortResultJSONID
	END

END
GO
