SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SpCppl_GetCWCImages]
@Barcode nvarchar(50)=null
As
BEGIN 
	SELECT REPLACE(
        Photos, 
        '["', 
        ''
    ) as Photos, Barcodes FROM Incoming_DWSResult WHERE Barcodes = @Barcode
	ORDER BY DWSResultId DESC
END
GO
