SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[EDI_GET_CONSIGNMENT_JSON_bkup_AH_20210303] 
(
	@Lower INT = 1,
	@Upper INT = 100000000
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		ConsignmentNumber, 		
		JSONOnly.JSONString,
		SRC.[SourceID]
		FROM
		(SELECT 
			[id], 
			cd_connote AS ConsignmentNumber, 			
			(
				SELECT 
					CONVERT(VARCHAR, CON.cd_connote)												AS CONSIGNMENTNUMBER,
					CONVERT(DATE, CON.[cd_consignment_date])										AS CONSIGNMENTDATE,
					CONVERT(DATE, CON.[cd_date])													AS CREATEDDATE,	
					ISNULL(CON.cd_pricecode, '')													AS SERVICECODE,
					ISNULL(CON.[cd_account], '')													AS ACCOUNTNUMBER,
					''																				AS SENDERCOMPANYNAME,
					ISNULL(CON.[cd_pickup_addr0], '')												AS SENDERNAME,
					ISNULL(CON.[cd_pickup_addr1], '')												AS SENDERADDRESS1,
					ISNULL(CON.[cd_pickup_addr2], '')												AS SENDERADDRESS2,
					ISNULL(CON.[cd_pickup_addr3], '')												AS SENDERADDRESS3,
					ISNULL(CON.[cd_pickup_suburb], '')												AS SENDERLOCATION,					
					[dbo].[EDI_GET_STATE](CON.cd_pickup_branch)										AS SENDERSTATE,
					CONVERT(VARCHAR, CON.[cd_pickup_postcode])										AS SENDERPOSTCODE,
					''																				AS SENDERCOUNTRY,
					''																				AS RECEIVERCOMPANYNAME,
					ISNULL(CON.[cd_delivery_addr0], '')												AS RECEIVERNAME,
					ISNULL(CON.[cd_delivery_addr1], '')												AS RECEIVERADDRESS1,
					ISNULL(CON.[cd_delivery_addr2], '')												AS RECEIVERADDRESS2,
					ISNULL(CON.[cd_delivery_addr3], '')												AS RECEIVERADDRESS3,
					ISNULL(CON.[cd_delivery_suburb], '')											AS RECEIVERLOCATION,				
					[dbo].[EDI_GET_STATE](CON.cd_deliver_branch)									AS RECEIVERSTATE,
					CONVERT(VARCHAR, CON.[cd_delivery_postcode])									AS RECEIVERPOSTCODE,
					''																				AS RECEIVERCOUNTRY,
					ISNULL(CON.[cd_delivery_contact], '')											AS RECEIVERCONTACT,
					ISNULL(CON.[cd_delivery_contact_phone], '')										AS RECEIVERPHONE,
					ISNULL(CON.[cd_special_instructions], '')										AS SPECIALINSTRUCTIONS,
					CONVERT(VARCHAR, CON.[cd_items])												AS TOTALLOGISTICSUNITS,
					CONVERT(VARCHAR, CON.[cd_deadweight])											AS TOTALDEADWEIGHT,
					CONVERT(VARCHAR, CON.[cd_volume])												AS TOTALVOLUME,
					[dbo].[EDI_GET_INSURANCE_CATEGORY](CON.[cd_insurance])							AS INSURANCECATEGORY,
					IIF(CDADD.ca_dg IS NULL OR TRIM(CDADD.ca_dg) = '', 'N', TRIM(CDADD.ca_dg))		AS DANGEROUSGOODSFLAG,
					ISNULL(CON.[cd_pickup_contact], '')												AS PICKUPCONTACT,
					ISNULL(CON.[cd_pickup_contact_phone], '')										AS PICKUPPHONE,
					IIF(CDADD.ca_atl IS NULL OR TRIM(CDADD.ca_atl) = '', 'N', TRIM(CDADD.ca_atl))	AS ATLFLAG,
					[dbo].[GET_FREIGHTCATEGORY](CON.cd_pricecode)									AS FREIGHTCATEGORY,
					''																				AS ALERT,
					ISNULL(CON.[cd_delivery_status_flag], '')										AS DELIVERYSTATUSFLAG,
					'Normal'																		AS DELIVERYTYPE,
					''																				AS RESCHEDULEDDELIVERYDATE,
					cd.destination_id																AS DESTINATIONID,
					'N'																				AS ISINTL,
					(SELECT 
						TRIM(COUP.[cc_coupon]) AS LABELNUMBER,
						TRIM(COUP.[cc_unit_type]) AS UNITTYPE,
						'TO BE DECIDED' AS INTERNALLABELNUMBER
						FROM [edi_cdcoupon] COUP WITH (NOLOCK) 
						WHERE CON.[cd_id] = COUP.cc_consignment FOR JSON PATH, INCLUDE_NULL_VALUES) AS 'LABELS',
					(SELECT 
						TRIM(REF.cr_reference) AS ADDITIONALREFERENCE
						FROM [edi_cdref] REF WITH (NOLOCK) 
						WHERE CON.cd_id = REF.cr_consignment
						FOR JSON PATH, INCLUDE_NULL_VALUES) AS 'REFERENCES'
				FROM [edi_incremental_consignment] CON WITH (NOLOCK) 
				LEFT JOIN [dbo].[tblConsignmentDestination] cd WITH (NOLOCK) 
					ON cd.cd_id = CON.cd_id AND cd.Active = 1
				LEFT JOIN [edi_cdadditional] CDADD WITH (NOLOCK) 
					ON CON.[cd_id] = CDADD.ca_consignment
					WHERE CON.[cd_id] = RelationalJSONData.cd_id 
				FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
			) JSONString
		FROM [edi_incremental_consignment] AS RelationalJSONData  WITH (NOLOCK) 
		WHERE  [Id] BETWEEN @Lower AND @Upper 
		AND cd_id not in (SELECT cd_id FROM dbo.[edi_incremental_consignment_missinglabels] WITH(NOLOCK))
		) AS JSONOnly
		LEFT JOIN [dbo].[outgoing_consignment_event_source] SRC WITH (NOLOCK) ON SRC.[SourceName] = 'EDI'
		WHERE JSONOnly.JSONString is not null
		
UNION ALL
		
	SELECT 
		ConsignmentNumber, 		
		JSONOnly.JSONString,
		SRC.[SourceID]
		FROM
		(SELECT 
			cd_connote AS ConsignmentNumber, 			
			(
				SELECT 
					CONVERT(VARCHAR, CON.cd_connote)												AS CONSIGNMENTNUMBER,
					CONVERT(DATE, CON.[cd_consignment_date])										AS CONSIGNMENTDATE,
					CONVERT(DATE, CON.[cd_date])													AS CREATEDDATE,	
					ISNULL(CON.cd_pricecode, '')													AS SERVICECODE,
					ISNULL(CON.[cd_account], '')													AS ACCOUNTNUMBER,
					''																				AS SENDERCOMPANYNAME,
					ISNULL(CON.[cd_pickup_addr0], '')												AS SENDERNAME,
					ISNULL(CON.[cd_pickup_addr1], '')												AS SENDERADDRESS1,
					ISNULL(CON.[cd_pickup_addr2], '')												AS SENDERADDRESS2,
					ISNULL(CON.[cd_pickup_addr3], '')												AS SENDERADDRESS3,
					ISNULL(CON.[cd_pickup_suburb], '')												AS SENDERLOCATION,					
					[dbo].[EDI_GET_STATE](CON.cd_pickup_branch)										AS SENDERSTATE,
					CONVERT(VARCHAR, CON.[cd_pickup_postcode])										AS SENDERPOSTCODE,
					''																				AS SENDERCOUNTRY,
					''																				AS RECEIVERCOMPANYNAME,
					ISNULL(CON.[cd_delivery_addr0], '')												AS RECEIVERNAME,
					ISNULL(CON.[cd_delivery_addr1], '')												AS RECEIVERADDRESS1,
					ISNULL(CON.[cd_delivery_addr2], '')												AS RECEIVERADDRESS2,
					ISNULL(CON.[cd_delivery_addr3], '')												AS RECEIVERADDRESS3,
					ISNULL(CON.[cd_delivery_suburb], '')											AS RECEIVERLOCATION,				
					[dbo].[EDI_GET_STATE](CON.cd_deliver_branch)									AS RECEIVERSTATE,
					CONVERT(VARCHAR, CON.[cd_delivery_postcode])									AS RECEIVERPOSTCODE,
					''																				AS RECEIVERCOUNTRY,
					ISNULL(CON.[cd_delivery_contact], '')											AS RECEIVERCONTACT,
					ISNULL(CON.[cd_delivery_contact_phone], '')										AS RECEIVERPHONE,
					ISNULL(CON.[cd_special_instructions], '')										AS SPECIALINSTRUCTIONS,
					CONVERT(VARCHAR, CON.[cd_items])												AS TOTALLOGISTICSUNITS,
					CONVERT(VARCHAR, CON.[cd_deadweight])											AS TOTALDEADWEIGHT,
					CONVERT(VARCHAR, CON.[cd_volume])												AS TOTALVOLUME,
					[dbo].[EDI_GET_INSURANCE_CATEGORY](CON.[cd_insurance])							AS INSURANCECATEGORY,
					IIF(CDADD.ca_dg IS NULL OR TRIM(CDADD.ca_dg) = '', 'N', TRIM(CDADD.ca_dg))		AS DANGEROUSGOODSFLAG,
					ISNULL(CON.[cd_pickup_contact], '')												AS PICKUPCONTACT,
					ISNULL(CON.[cd_pickup_contact_phone], '')										AS PICKUPPHONE,
					IIF(CDADD.ca_atl IS NULL OR TRIM(CDADD.ca_atl) = '', 'N', TRIM(CDADD.ca_atl))	AS ATLFLAG,
					[dbo].[GET_FREIGHTCATEGORY](CON.cd_pricecode)									AS FREIGHTCATEGORY,
					''																				AS ALERT,
					''																				AS DELIVERYSTATUSFLAG,
					'Normal'																		AS DELIVERYTYPE,
					''																				AS RESCHEDULEDDELIVERYDATE,
					cd.destination_id																AS DESTINATIONID,
					'N'																				AS ISINTL,
					(SELECT 
						TRIM(COUP.[cc_coupon]) AS LABELNUMBER,
						TRIM(COUP.[cc_unit_type]) AS UNITTYPE,
						'TO BE DECIDED' AS INTERNALLABELNUMBER
						FROM [edi_cdcoupon] COUP WITH (NOLOCK) 
						WHERE CON.[cd_id] = COUP.cc_consignment FOR JSON PATH, INCLUDE_NULL_VALUES) AS 'LABELS',
					(SELECT 
						TRIM(REF.cr_reference) AS ADDITIONALREFERENCE
						FROM [edi_cdref] REF WITH (NOLOCK) 
						WHERE CON.cd_id = REF.cr_consignment
						FOR JSON PATH, INCLUDE_NULL_VALUES) AS 'REFERENCES'
				FROM [edi_incremental_consignment_missinglabels] CON WITH (NOLOCK) 
				LEFT JOIN [dbo].[tblConsignmentDestination_missinglabels] cd WITH (NOLOCK) 
					ON cd.cd_id = CON.cd_id AND cd.Active = 1
				LEFT JOIN [edi_cdadditional] CDADD WITH (NOLOCK) 
					ON CON.[cd_id] = CDADD.ca_consignment
				CROSS APPLY (SELECT COUNT(DISTINCT [cc_coupon]) AS CouponCount FROM [edi_cdcoupon] WITH(NOLOCK) WHERE cc_consignment = con.cd_id ) coup
					WHERE CON.[cd_id] = RelationalJSONData.cd_id and coup.CouponCount > 0
				FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
			) JSONString
		FROM [edi_incremental_consignment_missinglabels] AS RelationalJSONData  WITH (NOLOCK) 
		WHERE cd_connote not in (SELECT ConsignmentNumber FROM dbo.outgoing_consignment_event_log WITH(NOLOCK))
		) AS JSONOnly
		LEFT JOIN [dbo].[outgoing_consignment_event_source] SRC WITH (NOLOCK) ON SRC.[SourceName] = 'EDI'
		WHERE JSONOnly.JSONString is not null
		
END


GO
