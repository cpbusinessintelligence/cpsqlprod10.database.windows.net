SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EDI_UPDATE_CDCOUPON_bkup]
	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [edi_cdcoupon]
	SET	
		cc_company_id =  UPD.cc_company_id,
		cc_consignment =  UPD.cc_consignment,
		cc_coupon =  UPD.cc_coupon,
		cc_activity_stamp =  UPD.cc_activity_stamp,
		cc_pickup_stamp =  UPD.cc_pickup_stamp,
		cc_accept_stamp =  UPD.cc_accept_stamp,
		cc_indepot_stamp =  UPD.cc_indepot_stamp,
		cc_transfer_stamp =  UPD.cc_transfer_stamp,
		cc_deliver_stamp =  UPD.cc_deliver_stamp,
		cc_failed_stamp =  UPD.cc_failed_stamp,
		cc_activity_driver =  UPD.cc_activity_driver,
		cc_pickup_driver =  UPD.cc_pickup_driver,
		cc_accept_driver =  UPD.cc_accept_driver,
		cc_indepot_driver =  UPD.cc_indepot_driver,
		cc_transfer_driver =  UPD.cc_transfer_driver,
		cc_transfer_to =  UPD.cc_transfer_to,
		cc_toagent_driver =  UPD.cc_toagent_driver,
		cc_toagent_stamp =  UPD.cc_toagent_stamp,
		cc_toagent_name =  UPD.cc_toagent_name,
		cc_deliver_driver =  UPD.cc_deliver_driver,
		cc_failed_driver =  UPD.cc_failed_driver,
		cc_deliver_pod =  UPD.cc_deliver_pod,
		cc_failed =  UPD.cc_failed,
		cc_exception_stamp =  UPD.cc_exception_stamp,
		cc_exception_code =  UPD.cc_exception_code,
		cc_unit_type =  UPD.cc_unit_type,
		cc_internal =  UPD.cc_internal,
		cc_link_coupon =  UPD.cc_link_coupon,
		cc_dirty =  UPD.cc_dirty,
		cc_last_status =  UPD.cc_last_status,
		cc_last_driver =  UPD.cc_last_driver,
		cc_last_stamp =  UPD.cc_last_stamp,
		cc_last_info =  UPD.cc_last_info,
		cc_accept_driver_branch =  UPD.cc_accept_driver_branch,
		cc_activity_driver_branch =  UPD.cc_activity_driver_branch,
		cc_deliver_driver_branch =  UPD.cc_deliver_driver_branch,
		cc_failed_driver_branch =  UPD.cc_failed_driver_branch,
		cc_indepot_driver_branch =  UPD.cc_indepot_driver_branch,
		cc_last_driver_branch =  UPD.cc_last_driver_branch,
		cc_pickup_driver_branch =  UPD.cc_pickup_driver_branch,
		cc_toagent_driver_branch =  UPD.cc_toagent_driver_branch,
		cc_tranfer_driver_branch =  UPD.cc_tranfer_driver_branch
		FROM 
			[edi_cdcoupon] COUP WITH (NOLOCK)
		INNER JOIN
			[dbo].[edi_updated_cdcoupon] UPD WITH (NOLOCK)
		ON
			COUP.cc_id = UPD.cc_id

END
GO
