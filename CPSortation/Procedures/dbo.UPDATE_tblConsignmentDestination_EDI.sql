SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/*
Below query to rerun consignment data

delete from dbo.edi_consignment where cd_id in 
(select cd_id from dbo.edi_existing_consignment)

*/

CREATE PROC [dbo].[UPDATE_tblConsignmentDestination_EDI] AS

BEGIN

--Step 1 : Get the consignment addresses from consignment table.

DROP TABLE IF EXISTS #Consignmentaddress

SELECT cd_id,cd_delivery_addr0,cd_delivery_addr1,cd_delivery_addr2,cd_delivery_addr3,cd_delivery_suburb,RIGHT('0'+ltrim(rtrim(str(cd_delivery_postcode))),4) AS cd_delivery_postcode
INTO #Consignmentaddress
FROM [dbo].[edi_incremental_consignment] -- For Production
--where cd_id not in (select cd_id from [dbo].[tblConsignmentDestination_history] where active = 1)
--FROM [dbo].[edi_consignment] where cd_id = 70946867
--select count(*) from dbo.edi_existing_consignment

--Step 2 : Create necessary indexes on #Consignmentaddress

CREATE CLUSTERED INDEX pk_Consignmentaddress_cdid ON #Consignmentaddress(cd_id)
CREATE NONCLUSTERED INDEX idx_Consignmentaddress_postcode ON #Consignmentaddress(cd_delivery_postcode)
CREATE NONCLUSTERED INDEX idx_Consignmentaddress_suburb ON #Consignmentaddress(cd_delivery_suburb)

--Step 3 : Insert unique destination ids using matching postcodes.

DROP TABLE IF EXISTS #destidbyuniquecodes

;WITH destidbyuniquecodes AS
(
SELECT postcode,destination_id,count(*) OVER (PARTITION BY postcode) AS rcount  
FROM dbo.tblCrunchTable --where street_name is null 
)
SELECT postcode,destination_id 
INTO #destidbyuniquecodes 
FROM destidbyuniquecodes 
WHERE rcount = 1



CREATE NONCLUSTERED INDEX idx_destidbyuniquecodes_postcode ON #destidbyuniquecodes(postcode)

TRUNCATE TABLE [dbo].[tblConsignmentDestination]

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT c.cd_id,d.destination_id,'Unique Destination ID By Postcode' AS Comments,1 AS Active
FROM #Consignmentaddress c 
JOIN #destidbyuniquecodes d ON c.cd_delivery_postcode = d.postcode

--Step 4 : Insert unique destination ids using matching suburbs.

DROP TABLE IF EXISTS #destidbyuniquesuburbs

;WITH destidbyuniquesuburbs AS
(
SELECT DISTINCT postcode,suburb,destination_id,count(*) OVER (PARTITION BY postcode,suburb) AS rcount  
FROM dbo.tblCrunchTable WHERE  postcode not in (SELECT postcode from #destidbyuniquecodes) --and street_name is null 
)
SELECT postcode,suburb,destination_id 
INTO #destidbyuniquesuburbs 
FROM destidbyuniquesuburbs WHERE rcount = 1

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT c.cd_id,d.destination_id,'Unique Destination ID By Suburb' AS Comments,1 AS Active
FROM #Consignmentaddress c 
--JOIN #destidbyuniquesuburbs d ON (LTRIM(RTRIM(c.cd_delivery_suburb)) = LTRIM(RTRIM(d.suburb)) OR LTRIM(RTRIM(d.suburb)) like '%'+LTRIM(RTRIM(c.cd_delivery_suburb))+'%' OR LTRIM(RTRIM(c.cd_delivery_suburb)) like '%'+LTRIM(RTRIM(d.suburb))+'%' )  and c.cd_delivery_postcode = d.postcode
JOIN #destidbyuniquesuburbs d ON (LTRIM(RTRIM(c.cd_delivery_suburb)) = LTRIM(RTRIM(d.suburb)) )  and c.cd_delivery_postcode = d.postcode
WHERE c.cd_id not in (SELECT cd_id FROM [dbo].[tblConsignmentDestination]) and LTRIM(RTRIM(c.cd_delivery_suburb)) <> ''

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT c.cd_id,d.destination_id,'Unique Destination ID By Suburb' AS Comments,1 AS Active
FROM #Consignmentaddress c 
JOIN #destidbyuniquesuburbs d ON  LTRIM(RTRIM(d.suburb)) like '%'+LTRIM(RTRIM(c.cd_delivery_suburb))+'%'   and c.cd_delivery_postcode = d.postcode
WHERE c.cd_id not in (SELECT cd_id FROM [dbo].[tblConsignmentDestination]) and LTRIM(RTRIM(c.cd_delivery_suburb)) <> ''

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT c.cd_id,d.destination_id,'Unique Destination ID By Suburb' AS Comments,1 AS Active
FROM #Consignmentaddress c 
JOIN #destidbyuniquesuburbs d ON LTRIM(RTRIM(c.cd_delivery_suburb)) like '%'+LTRIM(RTRIM(d.suburb))+'%'  and c.cd_delivery_postcode = d.postcode
WHERE c.cd_id not in (SELECT cd_id FROM [dbo].[tblConsignmentDestination]) and LTRIM(RTRIM(c.cd_delivery_suburb)) <> ''


-- Step 5 : Insert unknown destination id where addresses are completely invalid

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT cd_id,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM #Consignmentaddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination] WHERE active = 1) 
AND cd_delivery_addr3 <> '' and ltrim(rtrim(cd_delivery_addr3)) <> ltrim(rtrim(cd_delivery_suburb)) 
and ltrim(rtrim(cd_delivery_addr3)) NOT like '%[0-9]%'
AND cd_delivery_addr2 <> '' and ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb)) and ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_addr3))
AND ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%[0-9]%'
AND (cd_delivery_addr1 = '' OR ltrim(rtrim(cd_delivery_addr1)) NOT LIKE '%[0-9]%')

-- Step 6 : Check for Address Line 3 for valid house no and street address

DROP TABLE IF EXISTS #validAddress3

SELECT 
	cd_id
	,ltrim(rtrim(cd_delivery_addr3)) AS cd_delivery_addr3
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr3,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr3,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr3,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress3
FROM #Consignmentaddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination] WHERE ACTIVE = 1) 
AND cd_delivery_addr3 <> '' 
--AND ltrim(rtrim(cd_delivery_addr3)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr3)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr3,cd_delivery_postcode,''))) like '%[0-9]%'
AND right(ltrim(rtrim(cd_delivery_addr3)),1) not like '%[0-9]%'

--select * from #validAddress3

;WITH vaddr3_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM #validAddress3 WHERE cd_id NOT IN (SELECT cd_id FROM [dbo].[tblConsignmentDestination] WHERE active = 1)
)
INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 3' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr3_results --WHERE destination_id = 'NONUNIQUE'

-- select * from [dbo].[tblConsignmentDestination] where active = 0 order by cd_id

-- Step 7 : Check for Address Line 2 for valid house no and street address

DROP TABLE IF EXISTS #validAddress2

SELECT 
	cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress2
FROM #Consignmentaddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination] WHERE Active = 1) 
AND cd_delivery_addr2 <> '' 
--AND ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,''))) like '%[0-9]%'
AND right(ltrim(rtrim(cd_delivery_addr2)),1) not like '%[0-9]%'

--SELECT * FROM #validAddress2 order by 5

;WITH vaddr2_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM #validAddress2 WHERE cd_id NOT IN (SELECT cd_id FROM [dbo].[tblConsignmentDestination] WHERE destination_id <> 'NONUNIQUE' or active = 1)
)
INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 2' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr2_results 


-- Step 8 : Check for Address Line 1 for valid house no and street address

DROP TABLE IF EXISTS #validAddress1

SELECT 
	cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,cd_delivery_addr1
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress1
FROM #Consignmentaddress where CD_ID NOT IN (SELECT cd_id from [dbo].[tblConsignmentDestination] WHERE destination_id <> 'NONUNIQUE' or active = 1) 
AND cd_delivery_addr1 <> '' 
--AND ltrim(rtrim(cd_delivery_addr1)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr1)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) LIKE '%[0-9]%'
AND RIGHT(ltrim(rtrim(cd_delivery_addr1)),1) NOT LIKE '%[0-9]%'

--SELECT * FROM #validAddress1 order by 6

;WITH vaddr1_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM #validAddress1 
)
INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 1' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr1_results

-- Step 9 : Check for Address Line 1 + 2 combined for valid house no and street address

DROP TABLE IF EXISTS #validAddress1_2

SELECT 
	cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,cd_delivery_addr1
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress1_2
FROM #Consignmentaddress where CD_ID NOT IN (SELECT cd_id from [dbo].[tblConsignmentDestination] WHERE destination_id <> 'NONUNIQUE' or active = 1) 
AND cd_delivery_addr1 <> '' 
--AND ltrim(rtrim(cd_delivery_addr1))+' '+ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr1))+' '+ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,''))) LIKE '%[0-9]%'
--AND RIGHT(ltrim(rtrim(cd_delivery_addr1)),1) NOT LIKE '%[0-9]%'

--SELECT * FROM #validAddress1_2 order by 6

;WITH vaddr1_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM #validAddress1_2 
)
INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 1' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr1_results

-- Real bad addresses

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT cd_id,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM #Consignmentaddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination]) 
AND (cd_delivery_addr1 = '' OR ltrim(rtrim(cd_delivery_addr1)) NOT LIKE '%[0-9]%')
AND (cd_delivery_addr2 = '' OR ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%[0-9]%')
AND (cd_delivery_addr3 = '' OR ltrim(rtrim(cd_delivery_addr3)) NOT LIKE '%[0-9]%')


-- Bad addresses having postcode in address line

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT cd_id,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM #Consignmentaddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination]) 
AND (cd_delivery_addr1 = '' OR ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) NOT LIKE '%[0-9]%')
AND (cd_delivery_addr2 = '' OR ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) NOT LIKE '%[0-9]%')
AND (cd_delivery_addr3 = '' OR ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) NOT LIKE '%[0-9]%')

-------------------------------------

DROP TABLE IF EXISTS #validAddress1_Ext

SELECT 
	cd_id
	,cd_delivery_addr1
	,cd_delivery_addr2
	,cd_delivery_addr3
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress1_Ext
FROM #Consignmentaddress where CD_ID IN (SELECT Cd_id from [dbo].[tblConsignmentDestination] WHERE destination_id like 'UNKNOWN%' and destination_id <> 'UNKNOWN-ADDR' and active = 0) 


;WITH vaddr1_ext_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM #validAddress1_Ext WHERE cd_id NOT IN (SELECT cd_id FROM [dbo].[tblConsignmentDestination] where active =1)
)
INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 1' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr1_ext_results WHERE destination_id not like 'UNKNOWN%'

--------------Add destination ids of missing labels into seperate table to pick up in future date 22/10/2019

INSERT INTO [dbo].[tblConsignmentDestination_missinglabels]
SELECT * 
FROM [dbo].[tblConsignmentDestination]
WHERE CD_ID in (SELECT CD_ID FROM dbo.[edi_incremental_consignment_missinglabels])
and active = 1 and cd_id not in (select cd_id from [dbo].[tblConsignmentDestination_missinglabels])


END

GO
