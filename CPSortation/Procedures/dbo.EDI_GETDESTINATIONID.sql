SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[EDI_GETDESTINATIONID]

AS
BEGIN

DECLARE
    @postcode varchar(4) = 2151,
	@suburb varchar(40) = 'PARRAMATTA',
	@street_name varchar(40) = ' CHURCH STREET',
	@house_number_from bigint = null,
	@house_number_to bigint = 564


DECLARE @idsbypostcode TABLE
(
  postcode varchar(12), 
  destination_id varchar(15)
)

DECLARE @idsbysuburb TABLE
(
  --suburb varchar(40), 
  destination_id varchar(15)
)


DECLARE @rowcount int
DECLARE @destination_id varchar(15)

INSERT INTO @idsbypostcode
SELECT DISTINCT postcode,destination_id FROM [dbo].[tblCrunchTable] WHERE postcode = @postcode

SET @rowcount =  (SELECT COUNT(DISTINCT destination_id) FROM @idsbypostcode)

IF @rowcount = 0 RETURN 'UNKNOWN-CODE'

IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbypostcode)
	RETURN @destination_id
END	
/*
INSERT INTO @idsbysuburb

DECLARE
    @postcode varchar(4) = 2151,
	@suburb varchar(40) = 'PARRAMATTA',
	@street_name varchar(40) = ' CHURCH STREET',
	@house_number_from bigint = null,
	@house_number_to bigint = 564

SELECT DISTINCT destination_id FROM [dbo].[tblCrunchTable] WHERE postcode = @postcode AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb)) -- (ltrim(rtrim(suburb)) like '%'+@suburb+'%' OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR ltrim(rtrim(suburb)) = @suburb)

SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbysuburb)

IF @rowcount = 0 
BEGIN

DECLARE
    @postcode varchar(4) = 2151,
	@suburb varchar(40) = 'PARRAMATTA',
	@street_name varchar(40) = ' CHURCH STREET',
	@house_number_from bigint = null,
	@house_number_to bigint = 564

SELECT DISTINCT suburb FROM [dbo].[tblCrunchTable] WHERE postcode = @postcode AND (suburb like '%'+ltrim(rtrim(@suburb))+'%' ) -- (ltrim(rtrim(suburb)) like '%'+@suburb+'%' OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR ltrim(rtrim(suburb)) = @suburb)

END
*/
IF @rowcount = 0 RETURN 'UNKNOWN-SUBURB'

IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbysuburb)
	RETURN @destination_id
END	

IF ISNULL(@street_name,'') = '' RETURN 'UNKNOWN-STR'

DECLARE @idsbystreet TABLE
(
  --street_name varchar(40), 
  destination_id varchar(15)
)

INSERT INTO @idsbystreet
SELECT DISTINCT destination_id FROM [dbo].[tblCrunchTable] WHERE postcode = @postcode 
			AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb))
			AND (ltrim(rtrim(street_name)) = @street_name OR @street_name like '%'+ltrim(rtrim(street_name))+'%' OR SOUNDEX(@street_name) = SOUNDEX(street_name) OR SOUNDEX(REPLACE(street_name+street_type,' ','')) = SOUNDEX(REPLACE(@street_name,' ','')) )
			

SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbystreet)  

IF @rowcount = 0  RETURN 'UNKNOWN-STR'

IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbystreet)
	RETURN @destination_id
END	

IF @house_number_from > @house_number_to SET @house_number_from = null

DECLARE @idsbyhouse TABLE
(
  house_number_from int, 
  house_number_to int, 
  interval_type varchar(5),
  destination_id varchar(15)
)

INSERT INTO @idsbyhouse
SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id FROM [dbo].[tblCrunchTable] 
WHERE postcode = @postcode 
		AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb))
		AND (ltrim(rtrim(street_name)) = @street_name OR @street_name like '%'+ltrim(rtrim(street_name))+'%' OR SOUNDEX(@street_name) = SOUNDEX(street_name))
		AND ((@house_number_from >= house_number_from AND @house_number_from <= house_number_to) OR (@house_number_to >= house_number_from AND @house_number_to <= house_number_to))
		AND 
		(
		(CASE WHEN @house_number_from IS NOT NULL AND ISNUMERIC(@house_number_from) = 1 AND try_cast(@house_number_from as int)%2 <> 0 then 'ODD' 
				 WHEN @house_number_from IS NOT NULL AND ISNUMERIC(@house_number_from) = 1 AND try_cast(@house_number_from as int)%2 = 0 then 'EVEN' 
				 ELSE '' 
			END) = interval_type
		OR (CASE WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 <> 0 then 'ODD' 
				 WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 = 0 then 'EVEN' 
				 ELSE '' 
			END) = interval_type
		)

SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbyhouse) 

IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbyhouse)
	RETURN @destination_id
END	

--IF @rowcount > 1  RETURN 'NONUNIQUE' -- changed on 25/07/2019 by adding below extened logic

DECLARE @idsbyhouse_ext1 TABLE
(
  house_number_from int, 
  house_number_to int, 
  interval_type varchar(5),
  destination_id varchar(15)
)

INSERT INTO @idsbyhouse_ext1
SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id 
FROM [dbo].[tblCrunchTable] 
WHERE postcode = @postcode 
		AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb))
		AND (ltrim(rtrim(street_name)) = @street_name OR @street_name like '%'+ltrim(rtrim(street_name))+'%' OR SOUNDEX(@street_name) = SOUNDEX(street_name))
		AND (@house_number_to >= house_number_from AND @house_number_to <= house_number_to)
		AND 
		(CASE WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 <> 0 then 'ODD' 
				 WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 = 0 then 'EVEN' 
				 ELSE '' 
			END) = interval_type

		
SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbyhouse_ext1)

IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbyhouse_ext1)
	RETURN @destination_id
END	

--------------------

DECLARE @idsbyhouse_ext2 TABLE
(
  house_number_from int, 
  house_number_to int, 
  interval_type varchar(5),
  destination_id varchar(15)
)

INSERT INTO @idsbyhouse_ext2
SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id 
FROM [dbo].[tblCrunchTable] 
WHERE postcode = @postcode 
		AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb))
		AND (ltrim(rtrim(street_name)) = @street_name OR @street_name like '%'+street_name+'%' )
		AND (@house_number_to >= house_number_from AND @house_number_to <= house_number_to)
		AND 
		(CASE WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 <> 0 then 'ODD' 
				 WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 = 0 then 'EVEN' 
				 ELSE '' 
			END) = interval_type

		
SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbyhouse_ext2)

IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbyhouse_ext2)
	RETURN @destination_id
END	

------------------------------

DECLARE @idsbyhouse_ext3 TABLE
(
  house_number_from int, 
  house_number_to int, 
  interval_type varchar(5),
  destination_id varchar(15)
)

INSERT INTO @idsbyhouse_ext3
SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id 
FROM [dbo].[tblCrunchTable] 
WHERE postcode = @postcode 
		AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb))
		AND soundex(ltrim(rtrim(street_name))+' '+ltrim(rtrim(street_type))) = soundex(ltrim(rtrim(@street_name))) 
		AND LEN(REPLACE(@street_name,' ','')) - LEN(REPLACE(street_name+street_type,' ','')) <=5
		AND (@house_number_to >= house_number_from AND @house_number_to <= house_number_to)
		AND 
		(CASE WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 <> 0 then 'ODD' 
				 WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 = 0 then 'EVEN' 
				 ELSE '' 
			END) = interval_type

		
SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbyhouse_ext3)

IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbyhouse_ext3)
	RETURN @destination_id
END	
-----------------------------------------


DECLARE @idsbyhouse_ext7 TABLE
(
  house_number_from int, 
  house_number_to int, 
  interval_type varchar(5),
  destination_id varchar(15)
)

INSERT INTO @idsbyhouse_ext7
SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id 
FROM [dbo].[tblCrunchTable] 
WHERE postcode = @postcode 
		AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb))
		AND soundex(REPLACE(street_name+street_type,' ','')) = soundex(replace(@street_name,' ',''))
		AND LEN(REPLACE(@street_name,' ','')) - LEN(REPLACE(street_name+street_type,' ','')) =4
		AND (@house_number_to >= house_number_from AND @house_number_to <= house_number_to)
		AND 
		(CASE WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 <> 0 then 'ODD' 
				 WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 = 0 then 'EVEN' 
				 ELSE '' 
			END) = interval_type

SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbyhouse_ext7)
		
IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbyhouse_ext7)
	RETURN @destination_id
END	

-----------------------------------------

DECLARE @idsbyhouse_ext4 TABLE
(
  house_number_from int, 
  house_number_to int, 
  interval_type varchar(5),
  destination_id varchar(15)
)

INSERT INTO @idsbyhouse_ext4
SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id 
FROM [dbo].[tblCrunchTable] 
WHERE postcode = @postcode 
		AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb))
		AND soundex(ltrim(rtrim(street_name))+' '+ltrim(rtrim(street_type))) = soundex(ltrim(rtrim(@street_name))) 
		AND LEN(REPLACE(@street_name,' ','')) - LEN(REPLACE(street_name+street_type,' ','')) <=1
		AND (@house_number_to >= house_number_from AND @house_number_to <= house_number_to)
		AND 
		(CASE WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 <> 0 then 'ODD' 
				 WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 = 0 then 'EVEN' 
				 ELSE '' 
			END) = interval_type
		
SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbyhouse_ext4)
	
IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbyhouse_ext4)
	RETURN @destination_id
END	

-----------------------------------------

DECLARE @idsbyhouse_ext5 TABLE
(
  house_number_from int, 
  house_number_to int, 
  interval_type varchar(5),
  destination_id varchar(15)
)

INSERT INTO @idsbyhouse_ext5
SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id 
FROM [dbo].[tblCrunchTable] 
WHERE postcode = @postcode 
		AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb))
		AND soundex(ltrim(rtrim(street_name))+' '+ltrim(rtrim(street_type))) = soundex(ltrim(rtrim(@street_name))) 
		AND LEN(REPLACE(@street_name,' ','')) - LEN(REPLACE(street_name+street_type,' ','')) =0
		AND (@house_number_to >= house_number_from AND @house_number_to <= house_number_to)
		AND 
		(CASE WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 <> 0 then 'ODD' 
				 WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 = 0 then 'EVEN' 
				 ELSE '' 
			END) = interval_type

SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbyhouse_ext5)
		
IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbyhouse_ext5)
	RETURN @destination_id
END	


-----------------------------------------

DECLARE @idsbyhouse_ext6 TABLE
(
  house_number_from int, 
  house_number_to int, 
  interval_type varchar(5),
  destination_id varchar(15)
)

INSERT INTO @idsbyhouse_ext6
SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id 
FROM [dbo].[tblCrunchTable] 
WHERE postcode = @postcode 
		AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb))
		AND soundex(REPLACE(street_name+street_type,' ','')) = soundex(replace(@street_name,' ',''))
		AND (@house_number_to >= house_number_from AND @house_number_to <= house_number_to)
		AND 
		(CASE WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 <> 0 then 'ODD' 
				 WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 = 0 then 'EVEN' 
				 ELSE '' 
			END) = interval_type

SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbyhouse_ext6)
		
IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbyhouse_ext6)
	RETURN @destination_id
END	


-----------------------------------------

DECLARE @idsbyhouse_ext8 TABLE
(
  house_number_from int, 
  house_number_to int, 
  interval_type varchar(5),
  destination_id varchar(15)
)

INSERT INTO @idsbyhouse_ext8
SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id 
FROM [dbo].[tblCrunchTable] 
WHERE postcode = @postcode 
		AND (ltrim(rtrim(suburb)) = @suburb OR @suburb like '%'+ltrim(rtrim(suburb))+'%' OR SOUNDEX(@suburb) = SOUNDEX(suburb))
		AND soundex(REPLACE(street_name+street_type,' ','')) = soundex(replace(@street_name,' ',''))
		AND LEN(REPLACE(@street_name,' ','')) - LEN(REPLACE(street_name+street_type,' ','')) =1
		AND (@house_number_to >= house_number_from AND @house_number_to <= house_number_to)
		AND 
		(CASE WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 <> 0 then 'ODD' 
				 WHEN @house_number_to IS NOT NULL AND ISNUMERIC(@house_number_to) = 1 AND try_cast(@house_number_to as int)%2 = 0 then 'EVEN' 
				 ELSE '' 
			END) = interval_type

SET @rowcount = (SELECT COUNT(DISTINCT destination_id) FROM @idsbyhouse_ext8)

IF @rowcount = 0  RETURN 'UNKNOWN-HOUSE'
		
IF @rowcount = 1 
BEGIN
	SET @destination_id = (SELECT DISTINCT destination_id FROM @idsbyhouse_ext8)
	RETURN @destination_id
END	

IF @rowcount > 1  RETURN 'NONUNIQUE'

RETURN NULL

END



GO
