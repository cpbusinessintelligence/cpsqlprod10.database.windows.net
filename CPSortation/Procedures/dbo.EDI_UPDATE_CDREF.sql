SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[EDI_UPDATE_CDREF]
	
AS
BEGIN

	SET NOCOUNT ON;

    UPDATE [edi_cdref]
	SET
		cr_company_id =  UPD.cr_company_id,
		cr_consignment =  UPD.cr_consignment,
		cr_reference = UPD.cr_reference
	FROM 
		[edi_cdref] CDREF WITH (NOLOCK)
	INNER JOIN
		[dbo].[edi_updated_cdref] UPD WITH (NOLOCK)
	ON
		CDREF.cr_consignment = UPD.cr_consignment
END
GO
