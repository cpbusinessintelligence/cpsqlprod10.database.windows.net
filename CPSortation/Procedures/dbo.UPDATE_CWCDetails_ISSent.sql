SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[UPDATE_CWCDetails_ISSent]
AS
BEGIN

UPDATE dbo.Incoming_CWCDetails 
	SET Incoming_CWCDetails.IsSent = 1, 
		Incoming_CWCDetails.SentDateTime = CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time')
FROM 
(
SELECT  barcodes,rawid ,row_number() over(partition by barcodes order by rawid) row_num
FROM  dbo.[Incoming_CWCDetails] 
WHERE barcodes in (SELECT Barcodes FROM dbo.Incoming_CWCDetails_FTP_Log )
and ([cd_Connote] like 'CP%' or Barcodes like 'CP%') and FORMAT([DwsTimestamp], 'yyyyMMddHHmmss') > 20191107170000 and [CubeLength] <> 0 
) sd WHERE Incoming_CWCDetails.Barcodes = sd.Barcodes and Incoming_CWCDetails.RawID = sd.rawid 
		and Incoming_CWCDetails.IsSent = 0 and sd.row_num = 1



/*

Below query is to resend cwc records on ftp

Step 1 : Run below query to validate counts and records that you want to resend
select count(*) from dbo.Incoming_CWCDetails with(nolock) where SentDateTime >= '2019-12-05 16:40:00.000' --13798
select count(*) from dbo.Incoming_CWCDetails with(nolock) where SentDateTime < '2019-12-05 16:40:00.000' order by sentdatetime desc

Step 2 : First delete those records from FTP Log. This is very important

delete from dbo.Incoming_CWCDetails_FTP_Log where barcodes in 
(select barcodes from dbo.Incoming_CWCDetails where SentDateTime >= '2019-12-05 16:40:00.000')

Step 3 : Update below flags in incoming_cwcdetails at last after successfully run step 2

update dbo.Incoming_CWCDetails  set issent = 0,sentdatetime = null where SentDateTime >= '2019-12-05 16:40:00.000'


*/


END


GO
