SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PARSE_IncomingJSON_Bulk] 

AS

BEGIN

SET NOCOUNT ON;
SET XACT_ABORT ON;

DECLARE @SortResultJSONID int
	
/*

TRUNCATE TABLE [dbo].[Incoming_ParseErrorLog]
TRUNCATE TABLE [dbo].[Incoming_DWSResult]	
TRUNCATE TABLE [dbo].[Incoming_SortDetail]
TRUNCATE TABLE [dbo].[Incoming_SortAttempt]

update [dbo].[Incoming_SortResultJSON] set IsProcessed = 0 where SortResultJSONID between 5 and 7

EXEC [dbo].[PARSE_IncomingJSON_Bulk] 

select * from [dbo].[Incoming_SortResultJSON]
select * FROM [dbo].[Incoming_ParseErrorLog]
select * from [dbo].[Incoming_DWSResult]	
select * from [dbo].[Incoming_SortDetail]
select * from [dbo].[Incoming_SortAttempt]

*/


DECLARE parser_cursor CURSOR  FOR SELECT SortResultJSONID FROM [dbo].[Incoming_SortResultJSON] WHERE isprocessed = 0  
OPEN parser_cursor  
FETCH NEXT FROM parser_cursor INTO @SortResultJSONID 

WHILE @@FETCH_STATUS = 0 
BEGIN

BEGIN TRAN

BEGIN TRY

	EXEC [dbo].[PARSE_IncomingJSON] @SortResultJSONID

END TRY

BEGIN CATCH

		IF @@TRANCOUNT >0
		BEGIN		
				ROLLBACK TRAN;
					INSERT INTO [Incoming_ParseErrorLog]
					(SortResultJSONID
					,[ErrorNumber]
					,[ErrorSeverity]
					,[ErrorState]
					,[ErrorLine]
					,[ErrorProcedure]
					,[ErrorMessage]
					,[CreatedDateTime])
								
					SELECT
						@SortResultJSONID
						,ERROR_NUMBER() AS ErrorNumber  
						,ERROR_SEVERITY() AS ErrorSeverity  
						,ERROR_STATE() AS ErrorState  
						,ERROR_LINE () AS ErrorLine  
						,ERROR_PROCEDURE() AS ErrorProcedure  
						,ERROR_MESSAGE() AS ErrorMessage
						,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS CreatedDateTime;
		END;

		THROW;

END CATCH

	IF @@TRANCOUNT >0
	BEGIN	
		COMMIT TRAN;
		UPDATE [dbo].[Incoming_SortResultJSON] SET IsProcessed = 1 WHERE SortResultJSONID = @SortResultJSONID
	END

	FETCH NEXT FROM parser_cursor INTO @SortResultJSONID 

END -- End of Cursor

CLOSE parser_cursor;  
DEALLOCATE parser_cursor; 


END
GO
