SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[UPDATE_tblConsignmentDestination_Redelivery_EDI] AS

BEGIN

--Step 1 : Get the consignment addresses from consignment table.

DROP TABLE IF EXISTS #Consignmentaddress

SELECT
      cd_id
      ,cd_delivery_addr0 as cd_delivery_addr1
      ,cd_delivery_addr1 as cd_delivery_addr2
      ,cd_delivery_addr2 as cd_delivery_addr3
	  ,cd_delivery_addr3 as cd_delivery_addr4
      ,cd_delivery_suburb
      ,cd_delivery_postcode
INTO #Consignmentaddress
FROM [redelivery_edi_incremental_consignment] where cd_delivery_postcode is not null
--WHERE cd_id not in (SELECT cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] where active = 1)

--select count(*) from #Consignmentaddress
--Step 2 : Create necessary indexes on #Consignmentaddress

CREATE CLUSTERED INDEX pk_Consignmentaddress_cdid ON #Consignmentaddress(cd_id)
CREATE NONCLUSTERED INDEX idx_Consignmentaddress_postcode ON #Consignmentaddress(cd_delivery_postcode)
CREATE NONCLUSTERED INDEX idx_Consignmentaddress_suburb ON #Consignmentaddress(cd_delivery_suburb)

--Step 3 : Insert unique destination ids using matching postcodes.

DROP TABLE IF EXISTS #destidbyuniquecodes

;WITH destidbyuniquecodes AS
(
SELECT postcode,destination_id,count(*) OVER (PARTITION BY postcode) AS rcount  
FROM dbo.tblCrunchTable --where street_name is null 
)
SELECT postcode,destination_id 
INTO #destidbyuniquecodes 
FROM destidbyuniquecodes 
WHERE rcount = 1

CREATE NONCLUSTERED INDEX idx_destidbyuniquecodes_postcode ON #destidbyuniquecodes(postcode)

TRUNCATE TABLE [dbo].[tblConsignmentDestination_redelivery_edi]

INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT DISTINCT c.cd_id,d.destination_id,'Unique Destination ID By Postcode' AS Comments,1 AS Active
FROM #Consignmentaddress c 
JOIN #destidbyuniquecodes d ON c.cd_delivery_postcode = d.postcode

-- select * from [dbo].[tblConsignmentDestination_redelivery_edi] where active = 1
-- select * from #destidbyuniquecodes where postcode = 3134

--Step 4 : Insert unique destination ids using matching suburbs.

DROP TABLE IF EXISTS #destidbyuniquesuburbs

;WITH destidbyuniquesuburbs AS
(
SELECT postcode,suburb,destination_id,count(*) OVER (PARTITION BY postcode,suburb) AS rcount  
FROM dbo.tblCrunchTable WHERE  postcode not in (SELECT postcode from #destidbyuniquecodes)  and street_name is null
)
SELECT postcode,suburb,destination_id 
INTO #destidbyuniquesuburbs 
FROM destidbyuniquesuburbs WHERE rcount = 1

CREATE NONCLUSTERED INDEX idx_destidbyuniquesuburbs_postcode ON #destidbyuniquecodes(postcode)


INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT DISTINCT c.cd_id,d.destination_id,'Unique Destination ID By Suburb' AS Comments,1 AS Active
FROM #Consignmentaddress c 
--JOIN #destidbyuniquesuburbs d ON (LTRIM(RTRIM(c.cd_delivery_suburb)) = LTRIM(RTRIM(d.suburb)) OR LTRIM(RTRIM(d.suburb)) like '%'+LTRIM(RTRIM(c.cd_delivery_suburb))+'%' OR LTRIM(RTRIM(c.cd_delivery_suburb)) like '%'+LTRIM(RTRIM(d.suburb))+'%' )  and c.cd_delivery_postcode = d.postcode
JOIN #destidbyuniquesuburbs d ON (LTRIM(RTRIM(c.cd_delivery_suburb)) = LTRIM(RTRIM(d.suburb)) )  and c.cd_delivery_postcode = d.postcode
WHERE c.cd_id not in (SELECT cd_id FROM [dbo].[tblConsignmentDestination_redelivery_edi]) and LTRIM(RTRIM(c.cd_delivery_suburb)) <> ''

INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT DISTINCT c.cd_id,d.destination_id,'Unique Destination ID By Suburb' AS Comments,1 AS Active
FROM #Consignmentaddress c 
JOIN #destidbyuniquesuburbs d ON  LTRIM(RTRIM(d.suburb)) like '%'+LTRIM(RTRIM(c.cd_delivery_suburb))+'%'   and c.cd_delivery_postcode = d.postcode
WHERE c.cd_id not in (SELECT cd_id FROM [dbo].[tblConsignmentDestination_redelivery_edi]) and LTRIM(RTRIM(c.cd_delivery_suburb)) <> ''

INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT DISTINCT c.cd_id,d.destination_id,'Unique Destination ID By Suburb' AS Comments,1 AS Active
FROM #Consignmentaddress c 
JOIN #destidbyuniquesuburbs d ON LTRIM(RTRIM(c.cd_delivery_suburb)) like '%'+LTRIM(RTRIM(d.suburb))+'%'  and c.cd_delivery_postcode = d.postcode
WHERE c.cd_id not in (SELECT cd_id FROM [dbo].[tblConsignmentDestination_redelivery_edi]) and LTRIM(RTRIM(c.cd_delivery_suburb)) <> ''

-- select * from [dbo].[tblConsignmentDestination_redelivery_edi] where active = 0 and cd_id in (61775252)
-- select soundex('TURRAMURRA'),soundex('NORTHTURRAMURRA')
-- select * from #destidbyuniquesuburbs where suburb like '%SYDNEY%'
-- select * from dbo.tblCrunchTable where suburb like '%SYDNEY%'
--SELECT * FROM #Consignmentaddress WHERE CD_ID in (61775252)


-- Step 5 : Insert unknown destination id where addresses are completely invalid

INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT DISTINCT cd_id,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM #Consignmentaddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] WHERE active = 1) 
--AND cd_delivery_addr2 <> '' and ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb)) and ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_addr3))
AND (ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%[0-9]%' OR cd_delivery_addr2 = '')
AND (ltrim(rtrim(cd_delivery_addr1)) NOT LIKE '%[0-9]%' OR cd_delivery_addr1 = '')

--select * from [dbo].[tblConsignmentDestination_redelivery_edi] where active = 1
--SELECT * FROM #Consignmentaddress WHERE CD_ID NOT in (SELECT Cd_id from [dbo].[tblConsignmentDestination_redelivery_edi])

-- Step 6 : Check for Address Line 3 for valid house no and street address
-- select * from [dbo].[tblConsignmentDestination_redelivery_edi] where active = 0 order by cd_id

-- Step 7 : Check for Address Line 2 for valid house no and street address

DROP TABLE IF EXISTS #validAddress2

SELECT 
	cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress2
FROM #Consignmentaddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id like 'UNKNOWN%' or Active = 1) 
AND cd_delivery_addr2 <> '' 
--AND ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,''))) like '%[0-9]%'
AND right(ltrim(rtrim(cd_delivery_addr2)),1) not like '%[0-9]%'

--SELECT * FROM #validAddress2 order by 5

;WITH vaddr2_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM #validAddress2 
)
INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 2' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr2_results where cd_id not in (select cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] where active = 1)

-- SELECT * FROM [dbo].[tblConsignmentDestination_redelivery_edi] WHERE cd_id = 61769708
-- SELECT * FROM [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id like 'UNKNOWN%' and comments like 'Matching%' 

-- Step 8 : Check for Address Line 1 for valid house no and street address

DROP TABLE IF EXISTS #validAddress1

SELECT 
	cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,cd_delivery_addr1
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress1
FROM #Consignmentaddress where CD_ID NOT IN (SELECT cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id like 'UNKNOWN%' or active = 1) 
AND cd_delivery_addr1 <> '' 
--AND ltrim(rtrim(cd_delivery_addr1)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr1)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) LIKE '%[0-9]%'
AND RIGHT(ltrim(rtrim(cd_delivery_addr1)),1) NOT LIKE '%[0-9]%'

--SELECT * FROM #validAddress1 order by 6

;WITH vaddr1_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM #validAddress1 
)
INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 1' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr1_results where cd_id not in (select cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] where active = 1 )

-- Step 9 : Check for Address Line 1 + 2 combined for valid house no and street address

DROP TABLE IF EXISTS #validAddress1_2

SELECT 
	cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,cd_delivery_addr1
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress1_2
FROM #Consignmentaddress where CD_ID NOT IN (SELECT cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id like 'UNKNOWN%' or active = 1) 
AND cd_delivery_addr1 <> '' 
--AND ltrim(rtrim(cd_delivery_addr1))+' '+ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr1))+' '+ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,''))) LIKE '%[0-9]%'
--AND RIGHT(ltrim(rtrim(cd_delivery_addr1)),1) NOT LIKE '%[0-9]%'

--SELECT * FROM #validAddress1_2 order by 6

;WITH vaddr1_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM #validAddress1_2 
)
INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 1' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr1_results where cd_id not in (select cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] where active = 1)

-- Real bad addresses

INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT DISTINCT cd_id,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM #Consignmentaddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination_redelivery_edi]) 
AND (cd_delivery_addr1 = '' OR ltrim(rtrim(cd_delivery_addr1)) NOT LIKE '%[0-9]%')
AND (cd_delivery_addr2 = '' OR ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%[0-9]%')


-- Bad addresses having postcode in address line

INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT DISTINCT cd_id,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM #Consignmentaddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination_redelivery_edi]) 
AND (cd_delivery_addr1 = '' OR ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) NOT LIKE '%[0-9]%')
AND (cd_delivery_addr2 = '' OR ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) NOT LIKE '%[0-9]%')

-------------------------------------

DROP TABLE IF EXISTS #validAddress1_Ext

SELECT 
	cd_id
	,cd_delivery_addr1
	,cd_delivery_addr2
	,cd_delivery_addr3
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress1_Ext
FROM #Consignmentaddress where CD_ID IN (SELECT Cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id like 'UNKNOWN%' and destination_id <> 'UNKNOWN-ADDR' and active = 0) 


;WITH vaddr1_ext_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM #validAddress1_Ext WHERE cd_id NOT IN (SELECT cd_id FROM [dbo].[tblConsignmentDestination_redelivery_edi] where active =1)
)
INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 1' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr1_ext_results WHERE destination_id not like 'UNKNOWN%'

/*
select count(distinct cd_id) from dbo.tblConsignmentDestination_redelivery_edi with (nolock) 
select count(*) from dbo.tblconsignmentdestination with (nolock) where active = 1

select 10696358/10738916.0*100.0
select 12328201-10741880


select 10751880-1586321
select count(*) from dbo.tblconsignmentdestination with (nolock) where active =0
and cd_id not in (select cd_id from dbo.tblconsignmentdestination  with (nolock) where active = 1)

select destination_id,count(*) from dbo.tblconsignmentdestination with (nolock) where active =1 group by destination_id

select c.cd_id,c.cd_delivery_addr1,c.cd_delivery_addr2,c.cd_delivery_addr3,c.cd_delivery_suburb,c.cd_delivery_postcode,
d.destination_id,d.comments
from #Consignmentaddress c join dbo.tblConsignmentDestination_redelivery_edi d with(nolock)
on c.cd_id = d.cd_id and active = 1 --and destination_id = 'UNKNOWN-HOUSE' and comments = 'Matching Valid Address Line 3'
where c.cd_id not in (select cd_Id from dbo.tblConsignmentDestination_redelivery_edi with(nolock) where active = 1)


select destination_id,comments,count(*) from dbo.tblConsignmentDestination_redelivery_edi with(nolock) where active = 0
and cd_id not in (select cd_id from dbo.tblConsignmentDestination_redelivery_edi with(nolock) where active = 1)
group by destination_id,comments
order by 1,2

SELECT count(*) FROM [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id = 'NONUNIQUE'
SELECT * FROM [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id like 'UNKNOWN%' and destination_id <> 'UNKNOWN-ADDR' and active = 0
SELECT * FROM [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id like 'UNKNOWN%'
--SELECT count(*) FROM [dbo].[tblConsignmentDestination_redelivery_edi]

INSERT INTO [dbo].[tblConsignmentDestination_redelivery_edi](cd_id,destination_id,Comments,Active)

SELECT DISTINCT *,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM #Consignmentaddress where CD_ID IN (SELECT Cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id like 'UNKNOWN%' and destination_id <> 'UNKNOWN-ADDR') 


AND cd_delivery_addr3 <> '' and ltrim(rtrim(cd_delivery_addr3)) <> ltrim(rtrim(cd_delivery_suburb)) 
and ltrim(rtrim(cd_delivery_addr3)) NOT like '%[0-9]%'
AND cd_delivery_addr2 <> '' and ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb)) and ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_addr3))
AND ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%[0-9]%'

--select * from [dbo].[tblConsignmentDestination_redelivery_edi] where cd_id in (61832165)
SELECT 
	cd.destination_id
	,cd.Comments
	,ca.*

FROM #Consignmentaddress ca JOIN [dbo].[tblConsignmentDestination_redelivery_edi] cd ON ca.cd_id = cd.cd_id
WHERE cd.active = 0 and ca.cd_id not in (select cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] where active = 1)
and cd.destination_id like 'NONUNIQUE%'
order by comments,destination_id



SELECT 
	cd.destination_id
	,cd.Comments
	,ca.*
	,[dbo].[getStreetName](ltrim(rtrim(cd_delivery_addr2))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(cd_delivery_addr2))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(cd_delivery_addr2))) as house_number_to

FROM #Consignmentaddress ca JOIN [dbo].[tblConsignmentDestination_redelivery_edi] cd ON ca.cd_id = cd.cd_id
WHERE cd.destination_id like 'UNKNOWN%' AND Comments like '%Address line 2%'
order by comments,destination_id

SELECT 
	cd.destination_id
	,ca.*
	--,[dbo].[getStreetName](ltrim(rtrim(cd_delivery_addr1))) as street_name
	--,[dbo].[getHouseNumberStart](ltrim(rtrim(cd_delivery_addr1))) as house_number_from
	--,[dbo].[getHouseNumberEnd](ltrim(rtrim(cd_delivery_addr1))) as house_number_to
	,cd.Comments
FROM #Consignmentaddress ca JOIN [dbo].[tblConsignmentDestination_redelivery_edi] cd ON ca.cd_id = cd.cd_id
WHERE cd.destination_id like 'UNKNOWN%' AND Comments like '%Address line 1%'
order by comments,destination_id
LECT 
	cd.destination_id
	,cd.Comments
	,ca.*

FROM #Consignmentaddress ca JOIN [dbo].[tblConsignmentDestination_redelivery_edi] cd ON ca.cd_id = cd.cd_id
WHERE cd.destination_id like 'UNKNOWN%' AND Comments like '%Invalid%'
order by comments,destination_id


SELECT cd.destination_id
	,cd.Comments
	,ca.* 
	,[dbo].[getStreetName](ltrim(rtrim(cd_delivery_addr1))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(cd_delivery_addr1))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(cd_delivery_addr1))) as house_number_to

FROM #Consignmentaddress ca JOIN [dbo].[tblConsignmentDestination_redelivery_edi] cd ON ca.cd_id = cd.cd_id
WHERE cd.destination_id like 'UNKNOWN%' AND Comments like '%Address line 1%'--AND ca.cd_id in (61832165)
order by comments,destination_id


select count(*) from #Consignmentaddress where cd_id not in (select cd_id from [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id <> 'NONUNIQUE' or active = 1)
and cd_Id = 61784055

select * from #validAddress2 where cd_id = 61705051

select * from [dbo].[tblCrunchTable] where postcode in (2113) and suburb like '%Macquarie%' and street_name like '%Durham%' 

and interval_type = 'EVEN'

select * from [dbo].[tblAddressMaster] where postcode in (2000)   and locality_name like '%parliament%'  and street_name like '%MANN%'

--8 Evan Street



--Validation Queries
-----------------------------------

SELECT DISTINCT postcode,suburb,street_name,street_type,house_number_from,house_number_to,interval_type,destination_id
FROM [dbo].[tblCrunchTable]

SELECT comments,destination_id,count(*) FROM [dbo].[tblConsignmentDestination_redelivery_edi] 
WHERE destination_id like 'UNKNOWN%' 
GROUP BY comments,destination_id
ORDER BY comments,destination_id

SELECT * FROM #Consignmentaddress where cd_id in (SELECT cd_id FROM [dbo].[tblConsignmentDestination_redelivery_edi] WHERE destination_id = 'UNKNOWN' and comments = 'Matching Valid Address Line 2')


DROP TABLE IF EXISTS #tempcrunchtable

SELECT DISTINCT postcode,suburb,street_name,street_type,house_number_from,house_number_to,interval_type,destination_id
INTO #tempcrunchtable
FROM [dbo].[tblCrunchTable] WHERE postcode in (SELECT postcode FROM #validAddress2)

select * from #tempcrunchtable

select * from #validAddress2 where cd_id in (61778136)

select * from [dbo].[tblCrunchTable] where postcode in (870) and suburb = 'Araluen' and street_name like '%George%'



SELECT 
	cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,[dbo].[getStreetName](ltrim(rtrim(cd_delivery_addr2))) as street_name
	,[dbo].[getHouseNumber](ltrim(rtrim(cd_delivery_addr2))) as house_number
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode

FROM #Consignmentaddress where CD_ID = 61778136
AND cd_delivery_addr2 <> '' 
AND ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(cd_delivery_addr2)) like '%[0-9]%'
AND right(ltrim(rtrim(cd_delivery_addr2)),1) not like '%[0-9]%'


SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id FROM [dbo].[tblCrunchTable] 
WHERE postcode = 2000 
		AND (ltrim(rtrim(suburb)) = 'SYDNEY' OR 'SYDNEY' like '%'+ltrim(rtrim(suburb))+'%')
		AND (ltrim(rtrim(street_name)) = 'GEORGE STREET' or 'GEORGE STREET' like '%'+ltrim(rtrim(street_name))+'%')
		AND 225 >= house_number_from
		AND 225 <= house_number_to
		AND case when cast(225 as int)%2 <> 0 then 'ODD' ELSE 'EVEN' END = interval_type

*/

END

GO
