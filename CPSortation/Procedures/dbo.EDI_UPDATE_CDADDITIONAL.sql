SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EDI_UPDATE_CDADDITIONAL]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [edi_cdadditional]
	SET
		ca_atl =  UPD.ca_atl,
		ca_dg =  UPD.ca_dg
	FROM 
		[edi_cdadditional] CDADD WITH (NOLOCK)
	INNER JOIN
		[dbo].[edi_updated_cdadditional] UPD WITH (NOLOCK)
	ON
		CDADD.ca_consignment = UPD.ca_consignment
END
GO
