SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Generate_CWCDailyReport]

AS

BEGIN


--- Execute Service Code Update

EXEC [dbo].[Update_ServiceCodesForProtoBilling]

DROP TABLE IF EXISTS #Temp1

Select   cd_id														as ConsignmentID 
         ,TACode													AS AccountCode
         ,cd_connote												AS [ConsignmentNumber]
         ,cd_consignment_date										AS [ConsignmentDate]
         ,Service													AS Service
         ,convert(varchar(20),'')									AS NewServiceCode
         ,itemqty													AS [Item quantity]
         ,Isnull([Declaredweight],0)								AS DeclaredWEight
         ,Isnull([Declaredvolume],0)								AS DeclaredVolume
         ,Convert(decimal(12,3),Isnull([Declaredvolume],0))*250.0	AS DeclaredCubicWeight
         ,Isnull([Measuredweight],0)								AS MeasuredWeight
         ,Isnull([Measuredvolume],0)								AS MeasuredVolume
         ,Convert(decimal(12,3),Isnull([Measuredvolume],0))*250.0	AS MeasuredCubicWeight
         ,convert(decimal(12,2),0)									AS BilledWeight
         ,convert(varchar(50),'')									AS Comments
into #Temp1

FROM dbo.vwCWCDataForServiceCodeUpdate
where cd_id not in (select consignmentid from dbo.ProntoBilling_SCAudit)
--and ConsignmentID in (select distinct cd_id from dbo.vwGetCWCDetailsDataForFile )

 Update #Temp1 SET BilledWeight =  (Select Max(Weights) from (Select DeclaredWEight as Weights
                                                                  union all
                                                                Select DeclaredCubicWeight
                                                                union all
                                                                Select MeasuredWeight
                                                                union all
                                                                Select MeasuredCubicWeight )D )/[Item quantity] where [Item quantity] >0


DROP TABLE IF EXISTS #Temp2

select * into #Temp2 from dbo.ProntoBilling_SCAudit

DROP TABLE IF EXISTS #temp3

SELECT ConsignmentID,ConsignmentDate,consignmentnumber,AccountCode,DeclaredWEight AS DeclaredWeight,MeasuredWeight,DeclaredCubicWeight,MeasuredCubicWeight,BilledWeight,[Item quantity],Service,NewServiceCode,Comments
INTO #temp3
FROM #Temp1
UNION
SELECT ConsignmentID,ConsignmentDate,consignmentnumber,AccountCode,DeclaredWEight AS DeclaredWeight,MeasuredWeight,DeclaredCubicWeight,MeasuredCubicWeight,BilledWeight,[Itemquantity],Service,NewServiceCode,Comments  
FROM #Temp2

--DROP TABLE IF EXISTS dbo.tblProntoBillingData
DELETE FROM dbo.tblProntoBillingData where ConsignmentID in (select ConsignmentID from #temp3)

INSERT INTO dbo.tblProntoBillingData(ConsignmentID,ConsignmentDate,consignmentnumber,AccountCode,DeclaredWeight,MeasuredWeight,DeclaredCubicWeight,MeasuredCubicWeight,BilledWeight,[Item quantity],Service,NewServiceCode,Comments)
SELECT ConsignmentID,ConsignmentDate,consignmentnumber,AccountCode,DeclaredWeight,MeasuredWeight,DeclaredCubicWeight,MeasuredCubicWeight,BilledWeight,[Item quantity],Service,NewServiceCode,Comments
FROM #temp3

/*
----- CWC Daily Report 

select * from dbo.tblProntoBillingData

select * from #temp3 where consignmentnumber = 'CPA0YJT0116966'

select distinct cd_connote from edi_consignment with(nolock) where cd_id in (select cd_id from dbo.Incoming_CWCDetails with(nolock) where barcodes in (SELECT barcodes FROM dbo.Incoming_CWCDetails_FTP_Log))

select  * from dbo.Incoming_CWCDetails with(nolock)

select * from tblTariffAccount where ltrim(rtrim(AccountCode)) = '112951223'
select * from tblTariffAccount where ltrim(rtrim(TariffAccountCode)) = '112951223'

112951223


select * from dbo.tblProntoBillingData

-- Declared_Vs_Measured Report
SELECT * 
from  [dbo].[GETPricingDataForCWC_Measured] 
where  ConsignmentID in (select distinct cd_id from dbo.vwGetCWCDetailsDataForFile )


select * from #Temp2

select * from dbo.vwGetCWCDetailsDataForFile 
where cd_Connote not in (select consignmentnumber from #temp3)

select cd_Connote,count(*) from dbo.vwGetCWCDetailsDataForFile group by cd_Connote order by 2 desc

select * from dbo.vwGetCWCDetailsDataForFile where cd_Connote = 'CPASVYC5052314'
select * from #temp3 where consignmentnumber = 'CPASVYC5052314'
*/


END
GO
