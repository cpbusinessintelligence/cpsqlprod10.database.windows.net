SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCustomisedRateCardDetail] (
		[RawID]               [int] IDENTITY(1, 1) NOT NULL,
		[id]                  [int] NULL,
		[Account]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UniqueKey]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EffectiveDate]       [date] NULL,
		[OriginZone]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationZone]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MinimumCharge]       [float] NULL,
		[BasicCharge]         [float] NULL,
		[FuelOverride]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelPercentage]      [float] NULL,
		[Rounding]            [float] NULL,
		[ChargePerKilo]       [float] NULL,
		[CreatedDateTime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblCustomisedRateCardDetail]
		PRIMARY KEY
		CLUSTERED
		([RawID])
)
GO
ALTER TABLE [dbo].[tblCustomisedRateCardDetail]
	ADD
	CONSTRAINT [DF_tblCustomisedRateCardDetail_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblCustomisedRateCardDetail]
	ADD
	CONSTRAINT [DF_tblCustomisedRateCardDetail_CreatedBy]
	DEFAULT (suser_sname()) FOR [CreatedBy]
GO
CREATE NONCLUSTERED INDEX [ix_tblCustomisedRateCardDetail]
	ON [dbo].[tblCustomisedRateCardDetail] ([Service], [Account], [OriginZone], [DestinationZone], [EffectiveDate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCustomisedRateCardDetail] SET (LOCK_ESCALATION = TABLE)
GO
