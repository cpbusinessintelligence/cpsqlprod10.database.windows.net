SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblProntoBillingData] (
		[RawID]                   [bigint] IDENTITY(1, 1) NOT NULL,
		[ConsignmentID]           [int] NOT NULL,
		[ConsignmentDate]         [smalldatetime] NULL,
		[consignmentnumber]       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AccountCode]             [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeclaredWeight]          [decimal](20, 5) NOT NULL,
		[MeasuredWeight]          [decimal](20, 5) NOT NULL,
		[DeclaredCubicWeight]     [numeric](17, 4) NULL,
		[MeasuredCubicWeight]     [numeric](17, 4) NULL,
		[BilledWeight]            [decimal](12, 2) NULL,
		[Item quantity]           [int] NOT NULL,
		[Service]                 [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewServiceCode]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Comments]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreateDateTime]          [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblProntoBillingData]
	ADD
	CONSTRAINT [DF_tblProntoBillingData_CreateDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreateDateTime]
GO
ALTER TABLE [dbo].[tblProntoBillingData] SET (LOCK_ESCALATION = TABLE)
GO
