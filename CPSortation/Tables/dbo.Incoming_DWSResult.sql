SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_DWSResult] (
		[DWSResultId]          [int] IDENTITY(1, 1) NOT NULL,
		[SortResultJSONID]     [int] NULL,
		[ReceivedTime]         [datetime2](7) NULL,
		[DwsTimestamp]         [datetime2](7) NULL,
		[CubeLength]           [int] NULL,
		[CubeWidth]            [int] NULL,
		[CubeHeight]           [int] NULL,
		[Volume]               [int] NULL,
		[Weight]               [int] NULL,
		[ObjectType]           [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Barcodes]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SiteField1]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SiteField2]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SiteField3]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Symbol]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SorterName]           [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentId]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]          [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestID]               [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Photos]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_UAT_Incoming_DWSResult]
		PRIMARY KEY
		CLUSTERED
		([DWSResultId])
)
GO
ALTER TABLE [dbo].[Incoming_DWSResult]
	ADD
	CONSTRAINT [DF_UAT_Incoming_DWSResult_ReceivedTime]
	DEFAULT (getdate()) FOR [ReceivedTime]
GO
CREATE NONCLUSTERED INDEX [nci_wi_Incoming_DWSResult_B18EDF3EF6DD2F7B0D729DE30E045EC9]
	ON [dbo].[Incoming_DWSResult] ([ReceivedTime])
	INCLUDE ([Barcodes], [ConsignmentId], [DestID], [SortResultJSONID])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_Incoming_DWSResult_057E1124C877E802E43C6D1CB63EE1DA]
	ON [dbo].[Incoming_DWSResult] ([Barcodes])
	INCLUDE ([ConsignmentId], [CubeHeight], [CubeLength], [CubeWidth], [DwsTimestamp], [SorterName], [SortResultJSONID], [Volume], [Weight])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_DWSResult] SET (LOCK_ESCALATION = TABLE)
GO
