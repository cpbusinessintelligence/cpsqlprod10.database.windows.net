SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_FinalCalculatedPricingValues_Sortation] (
		[cd_id]                      [int] NOT NULL,
		[acct]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]          [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[TACode]                     [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[originZoneCode]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[destZoneCode]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[service]                    [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Shipdate]                   [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[volume]                     [numeric](25, 6) NULL,
		[weight]                     [decimal](20, 5) NULL,
		[itemqty]                    [int] NOT NULL,
		[insurance]                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ValidFrom_3]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ChargeMethod_3or4]          [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VolumetricDivisor_3or4]     [decimal](20, 5) NULL,
		[FuelOverride_3]             [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelPercentage_3]           [decimal](20, 5) NULL,
		[FuelOverride_4]             [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelPercentage_4]           [decimal](20, 5) NULL,
		[MinimumCharge_5or6]         [decimal](20, 5) NULL,
		[BasicCharge_5or6]           [decimal](20, 5) NULL,
		[FuelOverride_5]             [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelPercentage_5]           [decimal](20, 5) NULL,
		[Rounding_5or6]              [decimal](20, 5) NULL,
		[ChargePerKilo_5or6]         [decimal](20, 5) NULL,
		[FuelOverride_6]             [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelPercentage_6]           [decimal](20, 5) NULL,
		[CountFromStep3]             [int] NOT NULL,
		[CountFromStep4]             [int] NOT NULL,
		[CountFromStep5]             [int] NOT NULL,
		[CountFromStep6]             [int] NOT NULL,
		[DerivedWeight]              [numeric](27, 6) NULL,
		[TotalNetCharge]             [decimal](38, 6) NULL,
		[FuelSurcharge_5]            [decimal](20, 5) NULL,
		[FuelSurcharge_3]            [decimal](20, 5) NULL,
		[FuelSurcharge_6]            [decimal](20, 5) NULL,
		[FuelSurcharge_4]            [decimal](20, 5) NULL,
		[FuelSurcharge]              [decimal](38, 6) NULL,
		[TotalCharge]                [decimal](38, 6) NULL,
		[GST]                        [numeric](38, 6) NULL,
		[FinalCharge]                [decimal](20, 2) NULL
)
GO
ALTER TABLE [dbo].[Temp_FinalCalculatedPricingValues_Sortation] SET (LOCK_ESCALATION = TABLE)
GO
