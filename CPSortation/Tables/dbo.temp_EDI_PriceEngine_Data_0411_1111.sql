SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_EDI_PriceEngine_Data_0411_1111] (
		[ConsignmentID]                [int] NULL,
		[CustomerAccountCode]          [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]            [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyID]                    [int] NULL,
		[ConsignmentDate]              [int] NULL,
		[TarrifAccountCode]            [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountName]                  [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginPriceZoneCode]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationPriceZoneCode]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]                  [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ShippingDate]                 [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeclaredVolume]               [decimal](20, 5) NULL,
		[DelcaredCubicWeight]          [numeric](25, 6) NULL,
		[DeclaredWeight]               [decimal](20, 5) NULL,
		[OriginalBilledWeight]         [numeric](27, 6) NULL,
		[MeasuredVolume]               [decimal](20, 5) NULL,
		[MeasuredWeight]               [decimal](20, 5) NULL,
		[ChargebleItems]               [int] NOT NULL,
		[Insurance]                    [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ChargeMethod]                 [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalNetCharge]               [decimal](38, 6) NULL,
		[FuelSurcharge]                [numeric](38, 6) NULL,
		[TotalChargeExGST]             [numeric](38, 6) NULL,
		[GST]                          [numeric](38, 6) NULL,
		[FinalCharge]                  [decimal](20, 2) NULL,
		[SYDSortation]                 [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[temp_EDI_PriceEngine_Data_0411_1111] SET (LOCK_ESCALATION = TABLE)
GO
