SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_EDI_PriceEngine_Data_Measured] (
		[ConsignmentID]                [int] NULL,
		[CustomerAccountCode]          [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]            [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyID]                    [int] NULL,
		[ConsignmentDate]              [int] NULL,
		[TarrifAccountCode]            [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountName]                  [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginPriceZoneCode]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationPriceZoneCode]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]                  [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ShippingDate]                 [date] NULL,
		[DeclaredVolume]               [decimal](20, 5) NULL,
		[DelcaredCubicWeight]          [numeric](25, 6) NULL,
		[DeclaredWeight]               [decimal](20, 5) NULL,
		[OriginalBilledWeight]         [numeric](27, 6) NULL,
		[MeasuredVolume]               [numeric](1, 1) NOT NULL,
		[MeasuredWeight]               [numeric](1, 1) NOT NULL,
		[ChargebleItems]               [float] NOT NULL,
		[Insurance]                    [int] NOT NULL,
		[ChargeMethod]                 [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalNetCharge]               [float] NULL,
		[FuelSurcharge]                [float] NULL,
		[TotalChargeExGST]             [float] NULL,
		[GST]                          [float] NULL,
		[FinalCharge]                  [decimal](20, 2) NULL,
		[SYDSortation]                 [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[temp_EDI_PriceEngine_Data_Measured] SET (LOCK_ESCALATION = TABLE)
GO
