SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SortationData_CWCPricing] (
		[RowID]                 [bigint] NOT NULL,
		[cd_id]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[acct]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ChargeableItems]       [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ChargeableWeight]      [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorMessage]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorNumber]           [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[GstAmount]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ResponseStatus]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalNetCharge]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Insurance]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelLevy]              [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalCharge]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Active]                [bit] NULL,
		[CreateDateTime]        [datetime2](7) NULL
)
GO
ALTER TABLE [dbo].[SortationData_CWCPricing] SET (LOCK_ESCALATION = TABLE)
GO
