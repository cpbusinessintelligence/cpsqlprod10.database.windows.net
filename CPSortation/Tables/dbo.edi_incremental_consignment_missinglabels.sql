SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_incremental_consignment_missinglabels] (
		[CD_ID]                         [int] NOT NULL,
		[cd_connote]                    [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_consignment_date]           [smalldatetime] NULL,
		[cd_date]                       [smalldatetime] NOT NULL,
		[cd_pricecode]                  [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_account]                    [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr0]               [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]               [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]               [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]               [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]              [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_branch]              [int] NULL,
		[cd_pickup_postcode]            [int] NULL,
		[cd_delivery_addr0]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]            [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_branch]             [int] NULL,
		[cd_delivery_postcode]          [int] NULL,
		[cd_delivery_contact]           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]       [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_items]                      [int] NULL,
		[cd_deadweight]                 [float] NULL,
		[cd_volume]                     [float] NULL,
		[cd_insurance]                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_status_flag]       [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]               [datetime2](7) NOT NULL,
		CONSTRAINT [PK_edi_incremental_consignment_missinglabels]
		PRIMARY KEY
		CLUSTERED
		([CD_ID])
)
GO
ALTER TABLE [dbo].[edi_incremental_consignment_missinglabels]
	ADD
	CONSTRAINT [DF_edi_incremental_consignment_missinglabels_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
CREATE NONCLUSTERED INDEX [IX_edi_incremental_consignment_missinglabels]
	ON [dbo].[edi_incremental_consignment_missinglabels] ([cd_connote])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[edi_incremental_consignment_missinglabels] SET (LOCK_ESCALATION = TABLE)
GO
