SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRateCustomisedHeader] (
		[RawID]                 [int] IDENTITY(1, 1) NOT NULL,
		[id]                    [int] NULL,
		[Accountcode]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UniqueKey]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Name]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ValidFrom]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelOverride]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Fuel]                  [numeric](10, 4) NULL,
		[ChargeMethod]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VolumetricDivisor]     [numeric](10, 4) NULL,
		[TariffId]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Logwho]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Logdate]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblRateCustomisedHeader]
		PRIMARY KEY
		CLUSTERED
		([RawID])
)
GO
ALTER TABLE [dbo].[tblRateCustomisedHeader]
	ADD
	CONSTRAINT [DF_tblRateCustomisedHeader_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRateCustomisedHeader]
	ADD
	CONSTRAINT [DF_tblRateCustomisedHeader_CreatedBy]
	DEFAULT (suser_sname()) FOR [CreatedBy]
GO
CREATE NONCLUSTERED INDEX [IX_tblRateCustomisedHeader]
	ON [dbo].[tblRateCustomisedHeader] ([Service], [Accountcode], [ValidFrom])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRateCustomisedHeader] SET (LOCK_ESCALATION = TABLE)
GO
