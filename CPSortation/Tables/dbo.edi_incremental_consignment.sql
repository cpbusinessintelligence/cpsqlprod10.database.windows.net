SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_incremental_consignment] (
		[id]                               [int] IDENTITY(1, 1) NOT NULL,
		[cd_id]                            [int] NOT NULL,
		[cd_company_id]                    [int] NOT NULL,
		[cd_account]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_id]                      [int] NULL,
		[cd_import_id]                     [int] NULL,
		[cd_ogm_id]                        [int] NULL,
		[cd_manifest_id]                   [int] NULL,
		[cd_connote]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                          [smalldatetime] NULL,
		[cd_consignment_date]              [smalldatetime] NULL,
		[cd_eta_date]                      [smalldatetime] NULL,
		[cd_eta_earliest]                  [smalldatetime] NULL,
		[cd_customer_eta]                  [smalldatetime] NULL,
		[cd_pickup_addr0]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                 [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]               [int] NULL,
		[cd_pickup_record_no]              [int] NULL,
		[cd_pickup_confidence]             [int] NULL,
		[cd_pickup_contact]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]               [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]             [int] NULL,
		[cd_delivery_record_no]            [int] NULL,
		[cd_delivery_confidence]           [int] NULL,
		[cd_delivery_contact]              [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]          [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_stats_branch]                  [int] NULL,
		[cd_stats_depot]                   [int] NULL,
		[cd_pickup_branch]                 [int] NULL,
		[cd_pickup_pay_branch]             [int] NULL,
		[cd_deliver_branch]                [int] NULL,
		[cd_deliver_pay_branch]            [int] NULL,
		[cd_special_driver]                [int] NULL,
		[cd_pickup_revenue]                [float] NULL,
		[cd_deliver_revenue]               [float] NULL,
		[cd_pickup_billing]                [float] NULL,
		[cd_deliver_billing]               [float] NULL,
		[cd_pickup_charge]                 [float] NULL,
		[cd_pickup_charge_actual]          [float] NULL,
		[cd_deliver_charge]                [float] NULL,
		[cd_deliver_payment_actual]        [float] NULL,
		[cd_pickup_payment]                [float] NULL,
		[cd_pickup_payment_actual]         [float] NULL,
		[cd_deliver_payment]               [float] NULL,
		[cd_deliver_charge_actual]         [float] NULL,
		[cd_special_payment]               [float] NULL,
		[cd_insurance_billing]             [float] NULL,
		[cd_items]                         [int] NULL,
		[cd_coupons]                       [int] NULL,
		[cd_references]                    [int] NULL,
		[cd_rating_id]                     [int] NULL,
		[cd_chargeunits]                   [int] NULL,
		[cd_deadweight]                    [float] NULL,
		[cd_dimension0]                    [float] NULL,
		[cd_dimension1]                    [float] NULL,
		[cd_dimension2]                    [float] NULL,
		[cd_volume]                        [float] NULL,
		[cd_volume_automatic]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_import_deadweight]             [float] NULL,
		[cd_import_volume]                 [float] NULL,
		[cd_measured_deadweight]           [float] NULL,
		[cd_measured_volume]               [float] NULL,
		[cd_billing_id]                    [int] NULL,
		[cd_billing_date]                  [smalldatetime] NULL,
		[cd_export_id]                     [int] NULL,
		[cd_export2_id]                    [int] NULL,
		[cd_pickup_pay_date]               [smalldatetime] NULL,
		[cd_delivery_pay_date]             [smalldatetime] NULL,
		[cd_transfer_pay_date]             [smalldatetime] NULL,
		[cd_activity_stamp]                [datetime] NULL,
		[cd_activity_driver]               [int] NULL,
		[cd_pickup_stamp]                  [datetime] NULL,
		[cd_pickup_driver]                 [int] NULL,
		[cd_pickup_pay_driver]             [int] NULL,
		[cd_pickup_count]                  [int] NULL,
		[cd_pickup_notified]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_accept_stamp]                  [datetime] NULL,
		[cd_accept_driver]                 [int] NULL,
		[cd_accept_count]                  [int] NULL,
		[cd_accept_notified]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_stamp]                 [datetime] NULL,
		[cd_indepot_driver]                [int] NULL,
		[cd_indepot_count]                 [int] NULL,
		[cd_transfer_notified]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_failed_stamp]                  [datetime] NULL,
		[cd_failed_driver]                 [int] NULL,
		[cd_deliver_stamp]                 [datetime] NULL,
		[cd_deliver_driver]                [int] NULL,
		[cd_deliver_pay_driver]            [int] NULL,
		[cd_deliver_count]                 [int] NULL,
		[cd_deliver_pod]                   [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_pay_notified]           [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_pay_notified]          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_printed]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_returns]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release_stamp]                 [datetime] NULL,
		[cd_pricecode]                     [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_agent]                  [int] NULL,
		[cd_delivery_agent]                [int] NULL,
		[cd_agent_pod]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_desired]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_name]                [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_stamp]               [datetime] NULL,
		[cd_agent_pod_entry]               [datetime] NULL,
		[cd_completed]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled_stamp]               [datetime] NULL,
		[cd_cancelled_by]                  [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_test]                          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_dirty]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_transfer_stamp]                [datetime] NULL,
		[cd_transfer_driver]               [int] NULL,
		[cd_transfer_count]                [int] NULL,
		[cd_transfer_to]                   [int] NULL,
		[cd_toagent_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_toagent_stamp]                 [datetime] NULL,
		[cd_toagent_driver]                [int] NULL,
		[cd_toagent_count]                 [int] NULL,
		[cd_toagent_name]                  [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_status]                   [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_notified]                 [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_info]                     [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_driver]                   [int] NULL,
		[cd_last_stamp]                    [datetime] NULL,
		[cd_last_count]                    [int] NULL,
		[cd_accept_driver_branch]          [int] NULL,
		[cd_activity_driver_branch]        [int] NULL,
		[cd_deliver_driver_branch]         [int] NULL,
		[cd_deliver_pay_driver_branch]     [int] NULL,
		[cd_failed_driver_branch]          [int] NULL,
		[cd_indepot_driver_branch]         [int] NULL,
		[cd_last_driver_branch]            [int] NULL,
		[cd_pickup_driver_branch]          [int] NULL,
		[cd_pickup_pay_driver_branch]      [int] NULL,
		[cd_special_driver_branch]         [int] NULL,
		[cd_toagent_driver_branch]         [int] NULL,
		[cd_transfer_driver_branch]        [int] NULL,
		[cd_delivery_status_flag]          [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__edi_incr__3213E83FD38DD7EA]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__004002F9]
	DEFAULT ('0') FOR [cd_accept_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__01342732]
	DEFAULT ('0') FOR [cd_accept_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__02284B6B]
	DEFAULT ('N') FOR [cd_accept_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__031C6FA4]
	DEFAULT ('N') FOR [cd_indepot_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__041093DD]
	DEFAULT (NULL) FOR [cd_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__0504B816]
	DEFAULT ('0') FOR [cd_indepot_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__05F8DC4F]
	DEFAULT ('0') FOR [cd_indepot_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__06ED0088]
	DEFAULT ('N') FOR [cd_transfer_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_fa__07E124C1]
	DEFAULT (NULL) FOR [cd_failed_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_fa__08D548FA]
	DEFAULT ('0') FOR [cd_failed_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__09C96D33]
	DEFAULT (NULL) FOR [cd_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__0ABD916C]
	DEFAULT ('0') FOR [cd_deliver_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__0BB1B5A5]
	DEFAULT ('0') FOR [cd_deliver_pay_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__0CA5D9DE]
	DEFAULT ('0') FOR [cd_deliver_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__0D99FE17]
	DEFAULT ('') FOR [cd_deliver_pod]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__0E8E2250]
	DEFAULT ('N') FOR [cd_deliver_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__0F824689]
	DEFAULT ('N') FOR [cd_pickup_pay_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__10766AC2]
	DEFAULT ('N') FOR [cd_deliver_pay_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pr__116A8EFB]
	DEFAULT ('Y') FOR [cd_printed]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_re__125EB334]
	DEFAULT ('N') FOR [cd_returns]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_re__1352D76D]
	DEFAULT ('N') FOR [cd_release]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_re__1446FBA6]
	DEFAULT (NULL) FOR [cd_release_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pr__153B1FDF]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__162F4418]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__17236851]
	DEFAULT ('0') FOR [cd_pickup_agent]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__18178C8A]
	DEFAULT ('0') FOR [cd_delivery_agent]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__190BB0C3]
	DEFAULT ('N') FOR [cd_agent_pod]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__19FFD4FC]
	DEFAULT ('N') FOR [cd_agent_pod_desired]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__1AF3F935]
	DEFAULT ('') FOR [cd_agent_pod_name]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__1BE81D6E]
	DEFAULT (NULL) FOR [cd_agent_pod_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__1CDC41A7]
	DEFAULT (NULL) FOR [cd_agent_pod_entry]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_co__1DD065E0]
	DEFAULT ('N') FOR [cd_completed]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ca__1EC48A19]
	DEFAULT ('N') FOR [cd_cancelled]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ca__1FB8AE52]
	DEFAULT (NULL) FOR [cd_cancelled_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ca__20ACD28B]
	DEFAULT ('') FOR [cd_cancelled_by]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_te__21A0F6C4]
	DEFAULT ('N') FOR [cd_test]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_di__22951AFD]
	DEFAULT ('Y') FOR [cd_dirty]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__23893F36]
	DEFAULT (NULL) FOR [cd_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__247D636F]
	DEFAULT ('0') FOR [cd_transfer_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__257187A8]
	DEFAULT ('0') FOR [cd_transfer_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__2665ABE1]
	DEFAULT ('0') FOR [cd_transfer_to]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__2759D01A]
	DEFAULT ('N') FOR [cd_toagent_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__284DF453]
	DEFAULT (NULL) FOR [cd_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__2942188C]
	DEFAULT ('0') FOR [cd_toagent_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__2A363CC5]
	DEFAULT ('0') FOR [cd_toagent_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__2B2A60FE]
	DEFAULT ('') FOR [cd_toagent_name]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__2C1E8537]
	DEFAULT ('') FOR [cd_last_status]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__2D12A970]
	DEFAULT ('N') FOR [cd_last_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__2E06CDA9]
	DEFAULT (NULL) FOR [cd_last_info]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__2EFAF1E2]
	DEFAULT ('0') FOR [cd_last_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_co__2F2FFC0C]
	DEFAULT ('0') FOR [cd_company_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__2FEF161B]
	DEFAULT (NULL) FOR [cd_last_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__30242045]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__30E33A54]
	DEFAULT ('0') FOR [cd_last_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__3118447E]
	DEFAULT (NULL) FOR [cd_agent_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__31D75E8D]
	DEFAULT (NULL) FOR [cd_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_im__320C68B7]
	DEFAULT (NULL) FOR [cd_import_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__32CB82C6]
	DEFAULT (NULL) FOR [cd_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_og__33008CF0]
	DEFAULT ('0') FOR [cd_ogm_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__33BFA6FF]
	DEFAULT (NULL) FOR [cd_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ma__33F4B129]
	DEFAULT ('0') FOR [cd_manifest_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__34B3CB38]
	DEFAULT (NULL) FOR [cd_deliver_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_co__34E8D562]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_fa__35A7EF71]
	DEFAULT (NULL) FOR [cd_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_da__35DCF99B]
	DEFAULT (NULL) FOR [cd_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__369C13AA]
	DEFAULT (NULL) FOR [cd_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_co__36D11DD4]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__379037E3]
	DEFAULT (NULL) FOR [cd_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_et__37C5420D]
	DEFAULT (NULL) FOR [cd_eta_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__38845C1C]
	DEFAULT (NULL) FOR [cd_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_et__38B96646]
	DEFAULT (NULL) FOR [cd_eta_earliest]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__39788055]
	DEFAULT (NULL) FOR [cd_pickup_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_cu__39AD8A7F]
	DEFAULT (NULL) FOR [cd_customer_eta]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_sp__3A6CA48E]
	DEFAULT (NULL) FOR [cd_special_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__3AA1AEB8]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__3B60C8C7]
	DEFAULT (NULL) FOR [cd_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__3B95D2F1]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__3C54ED00]
	DEFAULT (NULL) FOR [cd_transfer_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__3C89F72A]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__3D7E1B63]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__3E723F9C]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__3F6663D5]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__405A880E]
	DEFAULT ('0') FOR [cd_pickup_record_no]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__414EAC47]
	DEFAULT ('0') FOR [cd_pickup_confidence]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__4242D080]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__4336F4B9]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__442B18F2]
	DEFAULT ('') FOR [cd_delivery_addr0]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__451F3D2B]
	DEFAULT ('') FOR [cd_delivery_addr1]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__46136164]
	DEFAULT ('') FOR [cd_delivery_addr2]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__4707859D]
	DEFAULT ('') FOR [cd_delivery_addr3]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__47FBA9D6]
	DEFAULT ('') FOR [cd_delivery_email]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__48EFCE0F]
	DEFAULT ('') FOR [cd_delivery_suburb]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__49E3F248]
	DEFAULT (NULL) FOR [cd_delivery_postcode]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__4AD81681]
	DEFAULT ('0') FOR [cd_delivery_record_no]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__4BCC3ABA]
	DEFAULT ('0') FOR [cd_delivery_confidence]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__4CC05EF3]
	DEFAULT ('') FOR [cd_delivery_contact]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__4DB4832C]
	DEFAULT ('') FOR [cd_delivery_contact_phone]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_sp__4EA8A765]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_st__4F9CCB9E]
	DEFAULT (NULL) FOR [cd_stats_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_st__5090EFD7]
	DEFAULT (NULL) FOR [cd_stats_depot]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__51851410]
	DEFAULT (NULL) FOR [cd_pickup_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__52793849]
	DEFAULT (NULL) FOR [cd_pickup_pay_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__536D5C82]
	DEFAULT (NULL) FOR [cd_deliver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__546180BB]
	DEFAULT (NULL) FOR [cd_deliver_pay_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_sp__5555A4F4]
	DEFAULT ('0') FOR [cd_special_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5649C92D]
	DEFAULT ('0') FOR [cd_pickup_revenue]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__573DED66]
	DEFAULT ('0') FOR [cd_deliver_revenue]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5832119F]
	DEFAULT ('0') FOR [cd_pickup_billing]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__592635D8]
	DEFAULT ('0') FOR [cd_deliver_billing]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5A1A5A11]
	DEFAULT ('0') FOR [cd_pickup_charge]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5B0E7E4A]
	DEFAULT ('0') FOR [cd_pickup_charge_actual]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__5C02A283]
	DEFAULT ('0') FOR [cd_deliver_charge]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__5CF6C6BC]
	DEFAULT ('0') FOR [cd_deliver_payment_actual]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5DEAEAF5]
	DEFAULT ('0') FOR [cd_pickup_payment]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5EDF0F2E]
	DEFAULT ('0') FOR [cd_pickup_payment_actual]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__5FD33367]
	DEFAULT ('0') FOR [cd_deliver_payment]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__60C757A0]
	DEFAULT ('0') FOR [cd_deliver_charge_actual]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_sp__61BB7BD9]
	DEFAULT ('0') FOR [cd_special_payment]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__62AFA012]
	DEFAULT ('0') FOR [cd_insurance_billing]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_it__63A3C44B]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_co__6497E884]
	DEFAULT (NULL) FOR [cd_coupons]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_re__658C0CBD]
	DEFAULT (NULL) FOR [cd_references]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ra__668030F6]
	DEFAULT (NULL) FOR [cd_rating_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ch__6774552F]
	DEFAULT (NULL) FOR [cd_chargeunits]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__68687968]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_di__695C9DA1]
	DEFAULT (NULL) FOR [cd_dimension0]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_di__6A50C1DA]
	DEFAULT (NULL) FOR [cd_dimension1]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_di__6B44E613]
	DEFAULT (NULL) FOR [cd_dimension2]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_vo__6C390A4C]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_vo__6D2D2E85]
	DEFAULT ('N') FOR [cd_volume_automatic]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_im__6E2152BE]
	DEFAULT (NULL) FOR [cd_import_deadweight]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_im__6F1576F7]
	DEFAULT (NULL) FOR [cd_import_volume]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_me__70099B30]
	DEFAULT (NULL) FOR [cd_measured_deadweight]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_me__70FDBF69]
	DEFAULT (NULL) FOR [cd_measured_volume]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_bi__71F1E3A2]
	DEFAULT ('0') FOR [cd_billing_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_bi__72E607DB]
	DEFAULT (NULL) FOR [cd_billing_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ex__73DA2C14]
	DEFAULT ('0') FOR [cd_export_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ex__74CE504D]
	DEFAULT ('0') FOR [cd_export2_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__75C27486]
	DEFAULT (NULL) FOR [cd_pickup_pay_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__76B698BF]
	DEFAULT (NULL) FOR [cd_delivery_pay_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__77AABCF8]
	DEFAULT (NULL) FOR [cd_transfer_pay_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__789EE131]
	DEFAULT (NULL) FOR [cd_activity_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__7993056A]
	DEFAULT ('0') FOR [cd_activity_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__7A8729A3]
	DEFAULT (NULL) FOR [cd_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__7B7B4DDC]
	DEFAULT ('0') FOR [cd_pickup_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__7C6F7215]
	DEFAULT ('0') FOR [cd_pickup_pay_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__7D63964E]
	DEFAULT ('0') FOR [cd_pickup_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__7E57BA87]
	DEFAULT ('N') FOR [cd_pickup_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__7F4BDEC0]
	DEFAULT (NULL) FOR [cd_accept_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment] SET (LOCK_ESCALATION = TABLE)
GO
