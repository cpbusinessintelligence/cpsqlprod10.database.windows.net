SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Outgoing_PreadviceJSON] (
		[SortResultJSONID]     [int] IDENTITY(1, 1) NOT NULL,
		[SortResultJSON]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SortMachine]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsProcessed]          [bit] NOT NULL,
		[Created_Datetime]     [datetime] NOT NULL,
		CONSTRAINT [PK_Outgoing_PreadviceJSON]
		PRIMARY KEY
		CLUSTERED
		([SortResultJSONID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Outgoing_PreadviceJSON]
	ADD
	CONSTRAINT [DF_Outgoing_PreadviceJSON_SortMachine]
	DEFAULT ('SYDSORT01') FOR [SortMachine]
GO
ALTER TABLE [dbo].[Outgoing_PreadviceJSON]
	ADD
	CONSTRAINT [DF_Outgoing_PreadviceJSON_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[Outgoing_PreadviceJSON]
	ADD
	CONSTRAINT [DF_Outgoing_PreadviceJSON_Created_Datetime]
	DEFAULT (getdate()) FOR [Created_Datetime]
GO
ALTER TABLE [dbo].[Outgoing_PreadviceJSON] SET (LOCK_ESCALATION = TABLE)
GO
