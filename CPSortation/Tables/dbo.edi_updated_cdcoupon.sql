SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_updated_cdcoupon] (
		[cc_id]                         [int] NOT NULL,
		[cc_company_id]                 [int] NOT NULL,
		[cc_consignment]                [int] NOT NULL,
		[cc_coupon]                     [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_activity_stamp]             [datetime] NULL,
		[cc_pickup_stamp]               [datetime] NULL,
		[cc_accept_stamp]               [datetime] NULL,
		[cc_indepot_stamp]              [datetime] NULL,
		[cc_transfer_stamp]             [datetime] NULL,
		[cc_deliver_stamp]              [datetime] NULL,
		[cc_failed_stamp]               [datetime] NULL,
		[cc_activity_driver]            [int] NULL,
		[cc_pickup_driver]              [int] NULL,
		[cc_accept_driver]              [int] NULL,
		[cc_indepot_driver]             [int] NULL,
		[cc_transfer_driver]            [int] NULL,
		[cc_transfer_to]                [int] NULL,
		[cc_toagent_driver]             [int] NULL,
		[cc_toagent_stamp]              [datetime] NULL,
		[cc_toagent_name]               [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_deliver_driver]             [int] NULL,
		[cc_failed_driver]              [int] NULL,
		[cc_deliver_pod]                [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_failed]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_exception_stamp]            [datetime] NULL,
		[cc_exception_code]             [char](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_unit_type]                  [char](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_internal]                   [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_link_coupon]                [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_dirty]                      [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_last_status]                [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_last_driver]                [int] NULL,
		[cc_last_stamp]                 [datetime] NULL,
		[cc_last_info]                  [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_accept_driver_branch]       [int] NULL,
		[cc_activity_driver_branch]     [int] NULL,
		[cc_deliver_driver_branch]      [int] NULL,
		[cc_failed_driver_branch]       [int] NULL,
		[cc_indepot_driver_branch]      [int] NULL,
		[cc_last_driver_branch]         [int] NULL,
		[cc_pickup_driver_branch]       [int] NULL,
		[cc_toagent_driver_branch]      [int] NULL,
		[cc_tranfer_driver_branch]      [int] NULL,
		CONSTRAINT [PK__edi_upda__9F1E187B57D14DD2]
		PRIMARY KEY
		CLUSTERED
		([cc_id])
)
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_co__3F3159AB]
	DEFAULT ('0') FOR [cc_company_id]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_co__40257DE4]
	DEFAULT ('0') FOR [cc_consignment]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_co__4119A21D]
	DEFAULT ('') FOR [cc_coupon]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__420DC656]
	DEFAULT (NULL) FOR [cc_activity_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_pi__4301EA8F]
	DEFAULT (NULL) FOR [cc_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__43F60EC8]
	DEFAULT (NULL) FOR [cc_accept_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_in__44EA3301]
	DEFAULT (NULL) FOR [cc_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_tr__45DE573A]
	DEFAULT (NULL) FOR [cc_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_de__46D27B73]
	DEFAULT (NULL) FOR [cc_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_fa__47C69FAC]
	DEFAULT (NULL) FOR [cc_failed_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__48BAC3E5]
	DEFAULT ('0') FOR [cc_activity_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_pi__49AEE81E]
	DEFAULT ('0') FOR [cc_pickup_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__4AA30C57]
	DEFAULT ('0') FOR [cc_accept_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_in__4B973090]
	DEFAULT ('0') FOR [cc_indepot_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_tr__4C8B54C9]
	DEFAULT ('0') FOR [cc_transfer_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_tr__4D7F7902]
	DEFAULT ('0') FOR [cc_transfer_to]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_to__4E739D3B]
	DEFAULT ('0') FOR [cc_toagent_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_to__4F67C174]
	DEFAULT (NULL) FOR [cc_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_to__505BE5AD]
	DEFAULT ('') FOR [cc_toagent_name]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_de__515009E6]
	DEFAULT ('0') FOR [cc_deliver_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_fa__52442E1F]
	DEFAULT ('0') FOR [cc_failed_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_de__53385258]
	DEFAULT ('') FOR [cc_deliver_pod]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_fa__542C7691]
	DEFAULT ('N') FOR [cc_failed]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ex__55209ACA]
	DEFAULT (NULL) FOR [cc_exception_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ex__5614BF03]
	DEFAULT ('') FOR [cc_exception_code]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_un__5708E33C]
	DEFAULT ('') FOR [cc_unit_type]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_in__57FD0775]
	DEFAULT ('N') FOR [cc_internal]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_li__58F12BAE]
	DEFAULT ('') FOR [cc_link_coupon]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_di__59E54FE7]
	DEFAULT ('Y') FOR [cc_dirty]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_la__5AD97420]
	DEFAULT ('') FOR [cc_last_status]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_la__5BCD9859]
	DEFAULT ('0') FOR [cc_last_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_la__5CC1BC92]
	DEFAULT (NULL) FOR [cc_last_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_la__5DB5E0CB]
	DEFAULT (NULL) FOR [cc_last_info]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__5EAA0504]
	DEFAULT (NULL) FOR [cc_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__5F9E293D]
	DEFAULT (NULL) FOR [cc_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_de__60924D76]
	DEFAULT (NULL) FOR [cc_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_fa__618671AF]
	DEFAULT (NULL) FOR [cc_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_in__627A95E8]
	DEFAULT (NULL) FOR [cc_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_la__636EBA21]
	DEFAULT (NULL) FOR [cc_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_pi__6462DE5A]
	DEFAULT (NULL) FOR [cc_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_to__65570293]
	DEFAULT (NULL) FOR [cc_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_tr__664B26CC]
	DEFAULT (NULL) FOR [cc_tranfer_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon] SET (LOCK_ESCALATION = TABLE)
GO
