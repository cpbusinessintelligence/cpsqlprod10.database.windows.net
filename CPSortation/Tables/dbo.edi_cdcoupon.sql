SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_cdcoupon] (
		[cc_id]                         [int] NOT NULL,
		[cc_company_id]                 [int] NOT NULL,
		[cc_consignment]                [int] NOT NULL,
		[cc_coupon]                     [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_activity_stamp]             [datetime] NULL,
		[cc_pickup_stamp]               [datetime] NULL,
		[cc_accept_stamp]               [datetime] NULL,
		[cc_indepot_stamp]              [datetime] NULL,
		[cc_transfer_stamp]             [datetime] NULL,
		[cc_deliver_stamp]              [datetime] NULL,
		[cc_failed_stamp]               [datetime] NULL,
		[cc_activity_driver]            [int] NULL,
		[cc_pickup_driver]              [int] NULL,
		[cc_accept_driver]              [int] NULL,
		[cc_indepot_driver]             [int] NULL,
		[cc_transfer_driver]            [int] NULL,
		[cc_transfer_to]                [int] NULL,
		[cc_toagent_driver]             [int] NULL,
		[cc_toagent_stamp]              [datetime] NULL,
		[cc_toagent_name]               [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_deliver_driver]             [int] NULL,
		[cc_failed_driver]              [int] NULL,
		[cc_deliver_pod]                [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_failed]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_exception_stamp]            [datetime] NULL,
		[cc_exception_code]             [char](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_unit_type]                  [char](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_internal]                   [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_link_coupon]                [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_dirty]                      [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_last_status]                [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_last_driver]                [int] NULL,
		[cc_last_stamp]                 [datetime] NULL,
		[cc_last_info]                  [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_accept_driver_branch]       [int] NULL,
		[cc_activity_driver_branch]     [int] NULL,
		[cc_deliver_driver_branch]      [int] NULL,
		[cc_failed_driver_branch]       [int] NULL,
		[cc_indepot_driver_branch]      [int] NULL,
		[cc_last_driver_branch]         [int] NULL,
		[cc_pickup_driver_branch]       [int] NULL,
		[cc_toagent_driver_branch]      [int] NULL,
		[cc_tranfer_driver_branch]      [int] NULL,
		CONSTRAINT [PK_edi_cdcoupon]
		PRIMARY KEY
		CLUSTERED
		([cc_id])
)
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_la__00DF2177]
	DEFAULT ('0') FOR [cc_last_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_la__01D345B0]
	DEFAULT (NULL) FOR [cc_last_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_la__02C769E9]
	DEFAULT (NULL) FOR [cc_last_info]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__03BB8E22]
	DEFAULT (NULL) FOR [cc_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__04AFB25B]
	DEFAULT (NULL) FOR [cc_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_de__05A3D694]
	DEFAULT (NULL) FOR [cc_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_fa__0697FACD]
	DEFAULT (NULL) FOR [cc_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_in__078C1F06]
	DEFAULT (NULL) FOR [cc_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_la__0880433F]
	DEFAULT (NULL) FOR [cc_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_pi__09746778]
	DEFAULT (NULL) FOR [cc_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_to__0A688BB1]
	DEFAULT (NULL) FOR [cc_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_tr__0B5CAFEA]
	DEFAULT (NULL) FOR [cc_tranfer_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_co__6442E2C9]
	DEFAULT ('0') FOR [cc_company_id]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_co__65370702]
	DEFAULT ('0') FOR [cc_consignment]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_co__662B2B3B]
	DEFAULT ('') FOR [cc_coupon]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__671F4F74]
	DEFAULT (NULL) FOR [cc_activity_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_pi__681373AD]
	DEFAULT (NULL) FOR [cc_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__690797E6]
	DEFAULT (NULL) FOR [cc_accept_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_in__69FBBC1F]
	DEFAULT (NULL) FOR [cc_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_tr__6AEFE058]
	DEFAULT (NULL) FOR [cc_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_de__6BE40491]
	DEFAULT (NULL) FOR [cc_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_fa__6CD828CA]
	DEFAULT (NULL) FOR [cc_failed_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__6DCC4D03]
	DEFAULT ('0') FOR [cc_activity_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_pi__6EC0713C]
	DEFAULT ('0') FOR [cc_pickup_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__6FB49575]
	DEFAULT ('0') FOR [cc_accept_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_in__70A8B9AE]
	DEFAULT ('0') FOR [cc_indepot_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_tr__719CDDE7]
	DEFAULT ('0') FOR [cc_transfer_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_tr__72910220]
	DEFAULT ('0') FOR [cc_transfer_to]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_to__73852659]
	DEFAULT ('0') FOR [cc_toagent_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_to__74794A92]
	DEFAULT (NULL) FOR [cc_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_to__756D6ECB]
	DEFAULT ('') FOR [cc_toagent_name]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_de__76619304]
	DEFAULT ('0') FOR [cc_deliver_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_fa__7755B73D]
	DEFAULT ('0') FOR [cc_failed_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_de__7849DB76]
	DEFAULT ('') FOR [cc_deliver_pod]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_fa__793DFFAF]
	DEFAULT ('N') FOR [cc_failed]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ex__7A3223E8]
	DEFAULT (NULL) FOR [cc_exception_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ex__7B264821]
	DEFAULT ('') FOR [cc_exception_code]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_un__7C1A6C5A]
	DEFAULT ('') FOR [cc_unit_type]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_in__7D0E9093]
	DEFAULT ('N') FOR [cc_internal]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_li__7E02B4CC]
	DEFAULT ('') FOR [cc_link_coupon]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_di__7EF6D905]
	DEFAULT ('Y') FOR [cc_dirty]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_la__7FEAFD3E]
	DEFAULT ('') FOR [cc_last_status]
GO
CREATE NONCLUSTERED INDEX [nc_idx_edi_cdcoupon_json]
	ON [dbo].[edi_cdcoupon] ([cc_consignment])
	INCLUDE ([cc_coupon], [cc_unit_type])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[edi_cdcoupon] SET (LOCK_ESCALATION = TABLE)
GO
