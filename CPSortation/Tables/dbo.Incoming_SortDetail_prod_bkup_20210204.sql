SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_SortDetail_prod_bkup_20210204] (
		[SortDetailID]         [int] IDENTITY(1, 1) NOT NULL,
		[DWSResultId]          [int] NOT NULL,
		[SortResultJSONID]     [int] NULL,
		[SortLoopId]           [bit] NULL,
		[Sn]                   [int] NULL,
		[IsCal]                [bit] NULL,
		[KnownBarcodes]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnknownBarcodes]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RemapFrom]            [int] NULL,
		[TransactionStart]     [datetime2](7) NULL,
		[TransactionEnd]       [datetime2](7) NULL,
		CONSTRAINT [PK_Incoming_SortDetail]
		PRIMARY KEY
		CLUSTERED
		([SortDetailID])
)
GO
CREATE NONCLUSTERED INDEX [nci_wi_Incoming_SortDetail_859FEE213B19E7F8A5938A980733AE4E]
	ON [dbo].[Incoming_SortDetail_prod_bkup_20210204] ([DWSResultId], [SortResultJSONID])
	INCLUDE ([KnownBarcodes])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_SortDetail_prod_bkup_20210204] SET (LOCK_ESCALATION = TABLE)
GO
