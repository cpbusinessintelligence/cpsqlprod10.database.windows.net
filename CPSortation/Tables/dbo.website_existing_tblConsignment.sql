SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[website_existing_tblConsignment] (
		[ConsignmentID]                        [int] NOT NULL,
		[ConsignmentCode]                      [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UserID]                               [int] NULL,
		[IsRegUserConsignment]                 [bit] NOT NULL,
		[PickupID]                             [int] NULL,
		[DestinationID]                        [int] NULL,
		[ContactID]                            [int] NULL,
		[TotalWeight]                          [decimal](10, 4) NULL,
		[TotalMeasureWeight]                   [decimal](10, 4) NULL,
		[TotalVolume]                          [decimal](10, 4) NULL,
		[TotalMeasureVolume]                   [decimal](10, 4) NULL,
		[NoOfItems]                            [int] NOT NULL,
		[SpecialInstruction]                   [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CustomerRefNo]                        [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentPreferPickupDate]          [date] NOT NULL,
		[ConsignmentPreferPickupTime]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ClosingTime]                          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DangerousGoods]                       [bit] NOT NULL,
		[Terms]                                [bit] NOT NULL,
		[RateCardID]                           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[LastActivity]                         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastActiivityDateTime]                [datetime] NULL,
		[ConsignmentStatus]                    [int] NULL,
		[EDIDataProcessed]                     [bit] NULL,
		[ProntoDataExtracted]                  [bit] NULL,
		[IsBilling]                            [bit] NULL,
		[IsManifested]                         [bit] NULL,
		[CreatedDateTime]                      [datetime] NOT NULL,
		[CreatedBy]                            [int] NULL,
		[UpdatedDateTTime]                     [datetime] NULL,
		[UpdatedBy]                            [int] NULL,
		[IsInternational]                      [bit] NULL,
		[IsDocument]                           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSignatureReq]                       [bit] NULL,
		[IfUndelivered]                        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReasonForExport]                      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TypeOfExport]                         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Currency]                             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsInsurance]                          [bit] NULL,
		[IsIdentity]                           [bit] NULL,
		[IdentityType]                         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IdentityNo]                           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Country-ServiceArea-FacilityCode]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InternalServiceCode]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NetSubTotal]                          [decimal](10, 2) NULL,
		[IsATl]                                [bit] NULL,
		[IsReturnToSender]                     [bit] NULL,
		[HasReadInsuranceTc]                   [bit] NULL,
		[NatureOfGoods]                        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginServiceAreaCode]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ProductShortName]                     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SortCode]                             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETA]                                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CTIManifestExtracted]                 [bit] NULL,
		[isprocessed]                          [bit] NULL,
		[IsAccountCustomer]                    [bit] NULL,
		[InsuranceAmount]                      [decimal](10, 2) NULL,
		[CourierPickupDate]                    [datetime] NULL,
		[CalculatedTotal]                      [decimal](18, 2) NULL,
		[CalculatedGST]                        [decimal](18, 2) NULL,
		[ClientCode]                           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isCreateEzy2ShipShipment]             [bit] NULL,
		[isShipmentSent2NZPost]                [bit] NULL,
		[PromotionCode]                        [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsHubApiProcessed]                    [bit] NULL,
		[InsuranceCategory]                    [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountNumber]                        [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupFirstName]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupLastName]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupCompanyName]                    [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupEmail]                          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress1]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress2]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupSuburb]                         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupStateName]                      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupStateID]                        [int] NULL,
		[PickupPostCode]                       [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupPhone]                          [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupCountry]                        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationFirstName]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationLastName]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationCompanyName]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationEmail]                     [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationAddress1]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationAddress2]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationSuburb]                    [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationStateName]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationStateID]                   [int] NULL,
		[DestinationPostCode]                  [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationPhone]                     [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationCountry]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [UC_website_existing_tblConsignment]
		UNIQUE
		NONCLUSTERED
		([ConsignmentCode]),
		CONSTRAINT [PK_website_existing_tblConsignment]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[website_existing_tblConsignment]
	ADD
	CONSTRAINT [DF__website_e__IsBil__31A25463]
	DEFAULT ((0)) FOR [IsBilling]
GO
ALTER TABLE [dbo].[website_existing_tblConsignment]
	ADD
	CONSTRAINT [DF__website_e__IsMan__3296789C]
	DEFAULT ((0)) FOR [IsManifested]
GO
ALTER TABLE [dbo].[website_existing_tblConsignment]
	ADD
	CONSTRAINT [DF__website_e__IsInt__338A9CD5]
	DEFAULT ((0)) FOR [IsInternational]
GO
ALTER TABLE [dbo].[website_existing_tblConsignment]
	ADD
	CONSTRAINT [DF__website_e__ispro__347EC10E]
	DEFAULT ((0)) FOR [isprocessed]
GO
ALTER TABLE [dbo].[website_existing_tblConsignment]
	ADD
	CONSTRAINT [DF__website_e__IsAcc__3572E547]
	DEFAULT ((0)) FOR [IsAccountCustomer]
GO
ALTER TABLE [dbo].[website_existing_tblConsignment]
	ADD
	CONSTRAINT [DF__website_e__isCre__36670980]
	DEFAULT ((0)) FOR [isCreateEzy2ShipShipment]
GO
ALTER TABLE [dbo].[website_existing_tblConsignment]
	ADD
	CONSTRAINT [DF__website_e__isShi__375B2DB9]
	DEFAULT ((0)) FOR [isShipmentSent2NZPost]
GO
ALTER TABLE [dbo].[website_existing_tblConsignment]
	ADD
	CONSTRAINT [DF__website_e__IsHub__384F51F2]
	DEFAULT ((0)) FOR [IsHubApiProcessed]
GO
ALTER TABLE [dbo].[website_existing_tblConsignment] SET (LOCK_ESCALATION = TABLE)
GO
