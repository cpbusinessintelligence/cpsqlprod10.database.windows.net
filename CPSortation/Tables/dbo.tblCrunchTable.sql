SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCrunchTable] (
		[crunchtableId]         [int] NOT NULL,
		[state]                 [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postcode]              [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[suburb]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_name]           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_type]           [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[house_number_from]     [int] NULL,
		[house_number_to]       [int] NULL,
		[interval_type]         [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[building]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[destination_id]        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[crtd_datetime]         [datetime] NULL,
		[crtd_userid]           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[last_upd_datetime]     [datetime] NULL,
		[last_upd_userid]       [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblCrunchTable_1]
		PRIMARY KEY
		CLUSTERED
		([crunchtableId])
)
GO
CREATE NONCLUSTERED INDEX [IX_tblCrunchTable_houseno]
	ON [dbo].[tblCrunchTable] ([house_number_from], [house_number_to])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCrunchTable_interval]
	ON [dbo].[tblCrunchTable] ([interval_type])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCrunchTable_postcode]
	ON [dbo].[tblCrunchTable] ([postcode])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCrunchTable_street]
	ON [dbo].[tblCrunchTable] ([street_name])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblCrunchTable_suburb]
	ON [dbo].[tblCrunchTable] ([suburb])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_tblCrunchTable_BAD56159D4A133A8AEB07546A836FD9D]
	ON [dbo].[tblCrunchTable] ([postcode], [interval_type], [house_number_from])
	INCLUDE ([house_number_to])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCrunchTable] SET (LOCK_ESCALATION = TABLE)
GO
