SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MissingDestIDs] (
		[Connote]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountCode]           [float] NULL,
		[CPCreateDateTime]      [datetime] NULL,
		[ProcessDateTime]       [datetime] NULL,
		[SerialNumber]          [float] NULL,
		[ReceivedTime]          [datetime] NULL,
		[DecisionBarcode]       [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[KnownBarcodes]         [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnknownBarcodes]       [float] NULL,
		[EDI_Address_Line0]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EDI_Address_Line1]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EDI_Address_Line2]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EDI_Address_Line3]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EDI_Suburb]            [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EDI_PostCode]          [float] NULL
)
GO
ALTER TABLE [dbo].[MissingDestIDs] SET (LOCK_ESCALATION = TABLE)
GO
