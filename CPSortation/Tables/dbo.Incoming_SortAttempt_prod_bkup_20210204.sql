SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_SortAttempt_prod_bkup_20210204] (
		[SortAttemptID]        [int] IDENTITY(1, 1) NOT NULL,
		[DWSResultId]          [int] NOT NULL,
		[SortResultJSONID]     [int] NULL,
		[AttemptBarcode]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sorted]               [bit] NULL,
		[SortStatus]           [tinyint] NULL,
		[SortLocationId]       [int] NULL,
		[SchemeTableId]        [int] NULL,
		[DecisionInfo]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[vcs]                  [bit] NULL,
		[ItemPropVals]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
CREATE NONCLUSTERED INDEX [nci_wi_Incoming_SortAttempt_32888BF1188ADEED067AF2F743A47A95]
	ON [dbo].[Incoming_SortAttempt_prod_bkup_20210204] ([Sorted], [SortResultJSONID])
	INCLUDE ([DWSResultId], [ItemPropVals])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_Incoming_SortAttempt_B2FC467ED6F6AC7CBCE5BBFF8B909663]
	ON [dbo].[Incoming_SortAttempt_prod_bkup_20210204] ([Sorted])
	INCLUDE ([DWSResultId], [SortResultJSONID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_SortAttempt_prod_bkup_20210204] SET (LOCK_ESCALATION = TABLE)
GO
