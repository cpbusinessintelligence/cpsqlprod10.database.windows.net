SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_SortResult_Tracking_log] (
		[RowID]               [int] IDENTITY(1, 1) NOT NULL,
		[TrackingJSON]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		CONSTRAINT [PK_outgoing_consignment_tracking_log]
		PRIMARY KEY
		CLUSTERED
		([RowID], [CreatedDateTime])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_SortResult_Tracking_log]
	ADD
	CONSTRAINT [DF_outgoing_consignment_tracking_log_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[Incoming_SortResult_Tracking_log] SET (LOCK_ESCALATION = TABLE)
GO
