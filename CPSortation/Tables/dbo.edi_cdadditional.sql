SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_cdadditional] (
		[ca_consignment]     [int] NOT NULL,
		[ca_atl]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ca_dg]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_edi_cdadditional]
		PRIMARY KEY
		CLUSTERED
		([ca_consignment])
)
GO
ALTER TABLE [dbo].[edi_cdadditional]
	ADD
	CONSTRAINT [DF__edi_cdadd__ca_at__625A9A57]
	DEFAULT ('N') FOR [ca_atl]
GO
ALTER TABLE [dbo].[edi_cdadditional]
	ADD
	CONSTRAINT [DF__edi_cdadd__ca_dg__634EBE90]
	DEFAULT ('N') FOR [ca_dg]
GO
CREATE NONCLUSTERED INDEX [nc_idx_cdadditional_CPSortation]
	ON [dbo].[edi_cdadditional] ([ca_consignment])
	INCLUDE ([ca_atl], [ca_dg])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[edi_cdadditional] SET (LOCK_ESCALATION = TABLE)
GO
