SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_ProntoBilling] (
		[OrderNumber]                   [bigint] NULL,
		[ConsignmentReference]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountCode]                   [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]                   [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BillingDate]                   [date] NULL,
		[OriginPostcode]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginLocality]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationPostcode]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationLocality]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentDate]               [date] NULL,
		[ItemQuantity]                  [float] NULL,
		[DeclaredWeight]                [float] NULL,
		[DeclaredVolume]                [float] NULL,
		[ChargeableWeight]              [float] NULL,
		[InsuranceDeclaredValue]        [money] NULL,
		[InsurancePriceOverride]        [money] NULL,
		[CalculatedFreightCharge]       [money] NULL,
		[CalculatedFuelSurcharge]       [money] NULL,
		[CalculatedTransportCharge]     [money] NULL,
		[PriceOverride]                 [money] NULL,
		[BilledFreightCharge]           [money] NULL,
		[BilledFuelSurcharge]           [money] NULL,
		[BilledTransportCharge]         [money] NULL,
		[BilledInsurance]               [money] NULL,
		[BilledOtherCharge]             [money] NULL,
		[BilledTotal]                   [money] NULL,
		[CardRateFreightCharge]         [money] NULL,
		[CardRateFuelSurcharge]         [money] NULL,
		[CardRateTransportCharge]       [money] NULL,
		[CardRateDiscountOff]           [money] NULL,
		[PickupCost]                    [money] NULL,
		[PickupSupplier]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupZone]                    [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LinehaulCost]                  [money] NULL,
		[LinehaulRoute]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryCost]                  [money] NULL,
		[DeliverySupplier]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryZone]                  [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalPUDCost]                  [money] NULL,
		[MarginTransportcharge]         [money] NULL
)
GO
ALTER TABLE [dbo].[Temp_ProntoBilling] SET (LOCK_ESCALATION = TABLE)
GO
