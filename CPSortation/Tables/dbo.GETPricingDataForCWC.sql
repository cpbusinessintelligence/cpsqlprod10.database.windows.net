SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GETPricingDataForCWC] (
		[ConsignmentID]                [int] NOT NULL,
		[CustomerAccountCode]          [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]            [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CompanyID]                    [int] NOT NULL,
		[ConsignmentDate]              [smalldatetime] NULL,
		[TarrifAccountCode]            [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountName]                  [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginPriceZoneCode]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationPriceZoneCode]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]                  [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ShippingDate]                 [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeclaredVolume]               [decimal](20, 5) NULL,
		[DelcaredCubicWeight]          [numeric](25, 6) NULL,
		[DeclaredWeight]               [decimal](20, 5) NULL,
		[OriginalBilledWeight]         [numeric](27, 6) NULL,
		[MeasuredVolume]               [decimal](20, 5) NULL,
		[MeasuredWeight]               [decimal](20, 5) NULL,
		[ChargebleItems]               [int] NOT NULL,
		[Insurance]                    [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ChargeMethod]                 [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalNetCharge]               [decimal](38, 6) NULL,
		[FuelSurcharge]                [decimal](38, 6) NULL,
		[TotalChargeExGST]             [decimal](38, 6) NULL,
		[GST]                          [numeric](38, 6) NULL,
		[FinalCharge]                  [decimal](20, 2) NULL,
		[SYDSortation]                 [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[GETPricingDataForCWC] SET (LOCK_ESCALATION = TABLE)
GO
