SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsignmentDestination_redelivery_edi] (
		[cd_id]              [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[destination_id]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Comments]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[crtd_datetime]      [datetime] NULL,
		[crtd_userid]        [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Active]             [bit] NOT NULL,
		CONSTRAINT [PK__tblConsi__A069199C157E71B7]
		PRIMARY KEY
		CLUSTERED
		([cd_id], [destination_id], [Active])
)
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_edi]
	ADD
	CONSTRAINT [DF__tblConsig__desti__2354350C]
	DEFAULT ('UNKNOWN') FOR [destination_id]
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_edi]
	ADD
	CONSTRAINT [DF__tblConsig__crtd___24485945]
	DEFAULT (getdate()) FOR [crtd_datetime]
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_edi]
	ADD
	CONSTRAINT [DF__tblConsig__crtd___253C7D7E]
	DEFAULT (suser_sname()) FOR [crtd_userid]
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_edi] SET (LOCK_ESCALATION = TABLE)
GO
