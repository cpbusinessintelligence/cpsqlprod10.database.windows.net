SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_SortResultJSON_prod_bkup_20210204] (
		[SortResultJSONID]     [int] IDENTITY(1, 1) NOT NULL,
		[SortResultJSON]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SortMachine]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsProcessed]          [bit] NOT NULL,
		[Created_Datetime]     [datetime] NOT NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_SortResultJSON_prod_bkup_20210204]
	ADD
	CONSTRAINT [DF_Incoming_SortResultJSON_SortMachine_1]
	DEFAULT ('SYDSORT01') FOR [SortMachine]
GO
ALTER TABLE [dbo].[Incoming_SortResultJSON_prod_bkup_20210204]
	ADD
	CONSTRAINT [DF_Incoming_SortResultJSON_IsProcessed_1]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[Incoming_SortResultJSON_prod_bkup_20210204]
	ADD
	CONSTRAINT [DF_Incoming_SortResultJSON_Created_Datetime_1]
	DEFAULT (getdate()) FOR [Created_Datetime]
GO
CREATE NONCLUSTERED INDEX [nci_wi_Incoming_SortResultJSON_E5EDDD411CDC6D981C060100597F6275]
	ON [dbo].[Incoming_SortResultJSON_prod_bkup_20210204] ([SortResultJSONID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_SortResultJSON_prod_bkup_20210204] SET (LOCK_ESCALATION = TABLE)
GO
