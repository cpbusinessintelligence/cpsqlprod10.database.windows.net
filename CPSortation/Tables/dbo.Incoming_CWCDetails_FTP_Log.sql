SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_CWCDetails_FTP_Log] (
		[RawID]              [bigint] IDENTITY(1, 1) NOT NULL,
		[Barcodes]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreateDateTime]     [datetime] NULL,
		CONSTRAINT [PK_Incoming_CWCDetails_FTP_Log]
		PRIMARY KEY
		CLUSTERED
		([RawID])
)
GO
ALTER TABLE [dbo].[Incoming_CWCDetails_FTP_Log]
	ADD
	CONSTRAINT [DF_Incoming_CWCDetails_FTP_Log_CreateDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreateDateTime]
GO
CREATE NONCLUSTERED INDEX [IX_Incoming_CWCDetails_FTP_Log]
	ON [dbo].[Incoming_CWCDetails_FTP_Log] ([Barcodes])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_CWCDetails_FTP_Log] SET (LOCK_ESCALATION = TABLE)
GO
