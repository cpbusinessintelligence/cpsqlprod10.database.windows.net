SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[branchs] (
		[b_id]                      [int] NOT NULL,
		[b_name]                    [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[b_trackhost]               [varchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[b_trackport]               [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[b_multitrack]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[b_depot]                   [int] NULL,
		[b_shortname]               [varchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[b_internal_name]           [varchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[b_emmcode]                 [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[b_in_difot]                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[b_description_metro]       [char](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[b_description_country]     [char](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__branchs__4E29C30DFDA70F62]
		PRIMARY KEY
		CLUSTERED
		([b_id])
)
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_name__57DD0BE4]
	DEFAULT ('') FOR [b_name]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_track__58D1301D]
	DEFAULT ('') FOR [b_trackhost]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_track__59C55456]
	DEFAULT ('') FOR [b_trackport]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_multi__5AB9788F]
	DEFAULT ('N') FOR [b_multitrack]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_depot__5BAD9CC8]
	DEFAULT (NULL) FOR [b_depot]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_short__5CA1C101]
	DEFAULT ('') FOR [b_shortname]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_inter__5D95E53A]
	DEFAULT (NULL) FOR [b_internal_name]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_emmco__5E8A0973]
	DEFAULT (NULL) FOR [b_emmcode]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_in_di__5F7E2DAC]
	DEFAULT ('N') FOR [b_in_difot]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_descr__607251E5]
	DEFAULT ('') FOR [b_description_metro]
GO
ALTER TABLE [dbo].[branchs]
	ADD
	CONSTRAINT [DF__branchs__b_descr__6166761E]
	DEFAULT ('') FOR [b_description_country]
GO
ALTER TABLE [dbo].[branchs] SET (LOCK_ESCALATION = TABLE)
GO
