SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_CWCDetails] (
		[RawID]                [bigint] IDENTITY(1, 1) NOT NULL,
		[DWSResultId]          [int] NULL,
		[SortResultJSONID]     [int] NULL,
		[DwsTimestamp]         [datetime2](7) NULL,
		[CubeLength]           [int] NULL,
		[CubeWidth]            [int] NULL,
		[CubeHeight]           [int] NULL,
		[Volume]               [int] NULL,
		[Weight]               [int] NULL,
		[Barcodes]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SorterName]           [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_connote]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_id]                [bigint] NULL,
		[AccountCode]          [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreateDateTime]       [datetime] NULL,
		[IsSent]               [bit] NULL,
		[SentDateTime]         [datetime] NULL,
		CONSTRAINT [PK_Incoming_CWCDetails]
		PRIMARY KEY
		CLUSTERED
		([RawID])
)
GO
ALTER TABLE [dbo].[Incoming_CWCDetails]
	ADD
	CONSTRAINT [DF_Incoming_CWCDetails_IsSent]
	DEFAULT ((0)) FOR [IsSent]
GO
CREATE NONCLUSTERED INDEX [IX_Incoming_CWCDetails]
	ON [dbo].[Incoming_CWCDetails] ([cd_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_Incoming_CWCDetails_04B33AB2ED708625D57599C7FC627141]
	ON [dbo].[Incoming_CWCDetails] ([DWSResultId])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_Incoming_CWCDetails_D8F42CD59A6FD6E72DEA325E6098FBE5]
	ON [dbo].[Incoming_CWCDetails] ([CubeLength])
	INCLUDE ([Barcodes], [cd_connote], [DwsTimestamp])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_Incoming_CWCDetails_F644AEB0E0BA23E72417C28FE1CDA003]
	ON [dbo].[Incoming_CWCDetails] ([Barcodes])
	INCLUDE ([cd_connote], [CubeLength], [DwsTimestamp])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_CWCDetails] SET (LOCK_ESCALATION = TABLE)
GO
