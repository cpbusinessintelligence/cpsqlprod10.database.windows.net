SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_consignment] (
		[cd_id]                                       [int] NOT NULL,
		[cd_company_id]                               [int] NOT NULL,
		[cd_account]                                  [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_id]                                 [int] NULL,
		[cd_import_id]                                [int] NULL,
		[cd_ogm_id]                                   [int] NULL,
		[cd_manifest_id]                              [int] NULL,
		[cd_connote]                                  [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                                     [smalldatetime] NOT NULL,
		[cd_consignment_date]                         [smalldatetime] NULL,
		[cd_eta_date]                                 [smalldatetime] NULL,
		[cd_eta_earliest]                             [smalldatetime] NULL,
		[cd_customer_eta]                             [smalldatetime] NULL,
		[cd_pickup_addr0]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                            [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]                          [int] NULL,
		[cd_pickup_record_no]                         [int] NULL,
		[cd_pickup_confidence]                        [int] NULL,
		[cd_pickup_contact]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]                     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                           [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]                          [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]                        [int] NULL,
		[cd_delivery_record_no]                       [int] NULL,
		[cd_delivery_confidence]                      [int] NULL,
		[cd_delivery_contact]                         [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]                   [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]                     [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_stats_branch]                             [int] NULL,
		[cd_stats_depot]                              [int] NULL,
		[cd_pickup_branch]                            [int] NULL,
		[cd_pickup_pay_branch]                        [int] NULL,
		[cd_deliver_branch]                           [int] NULL,
		[cd_deliver_pay_branch]                       [int] NULL,
		[cd_special_driver]                           [int] NULL,
		[cd_pickup_revenue]                           [float] NULL,
		[cd_deliver_revenue]                          [float] NULL,
		[cd_pickup_billing]                           [float] NULL,
		[cd_deliver_billing]                          [float] NULL,
		[cd_pickup_charge]                            [float] NULL,
		[cd_pickup_charge_actual]                     [float] NULL,
		[cd_deliver_charge]                           [float] NULL,
		[cd_deliver_payment_actual]                   [float] NULL,
		[cd_pickup_payment]                           [float] NULL,
		[cd_pickup_payment_actual]                    [float] NULL,
		[cd_deliver_payment]                          [float] NULL,
		[cd_deliver_charge_actual]                    [float] NULL,
		[cd_special_payment]                          [float] NULL,
		[cd_insurance_billing]                        [float] NULL,
		[cd_items]                                    [int] NULL,
		[cd_coupons]                                  [int] NULL,
		[cd_references]                               [int] NULL,
		[cd_rating_id]                                [int] NULL,
		[cd_chargeunits]                              [int] NULL,
		[cd_deadweight]                               [float] NULL,
		[cd_dimension0]                               [float] NULL,
		[cd_dimension1]                               [float] NULL,
		[cd_dimension2]                               [float] NULL,
		[cd_volume]                                   [float] NULL,
		[cd_volume_automatic]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_import_deadweight]                        [float] NULL,
		[cd_import_volume]                            [float] NULL,
		[cd_measured_deadweight]                      [float] NULL,
		[cd_measured_volume]                          [float] NULL,
		[cd_billing_id]                               [int] NULL,
		[cd_billing_date]                             [smalldatetime] NULL,
		[cd_export_id]                                [int] NULL,
		[cd_export2_id]                               [int] NULL,
		[cd_pickup_pay_date]                          [smalldatetime] NULL,
		[cd_delivery_pay_date]                        [smalldatetime] NULL,
		[cd_transfer_pay_date]                        [smalldatetime] NULL,
		[cd_activity_stamp]                           [datetime] NULL,
		[cd_activity_driver]                          [int] NULL,
		[cd_pickup_stamp]                             [datetime] NULL,
		[cd_pickup_driver]                            [int] NULL,
		[cd_pickup_pay_driver]                        [int] NULL,
		[cd_pickup_count]                             [int] NULL,
		[cd_pickup_notified]                          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_accept_stamp]                             [datetime] NULL,
		[cd_accept_driver]                            [int] NULL,
		[cd_accept_count]                             [int] NULL,
		[cd_accept_notified]                          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_notified]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_stamp]                            [datetime] NULL,
		[cd_indepot_driver]                           [int] NULL,
		[cd_indepot_count]                            [int] NULL,
		[cd_transfer_notified]                        [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_failed_stamp]                             [datetime] NULL,
		[cd_failed_driver]                            [int] NULL,
		[cd_deliver_stamp]                            [datetime] NULL,
		[cd_deliver_driver]                           [int] NULL,
		[cd_deliver_pay_driver]                       [int] NULL,
		[cd_deliver_count]                            [int] NULL,
		[cd_deliver_pod]                              [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_notified]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_pay_notified]                      [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_pay_notified]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_printed]                                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_returns]                                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release]                                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release_stamp]                            [datetime] NULL,
		[cd_pricecode]                                [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_agent]                             [int] NULL,
		[cd_delivery_agent]                           [int] NULL,
		[cd_agent_pod]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_desired]                        [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_name]                           [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_stamp]                          [datetime] NULL,
		[cd_agent_pod_entry]                          [datetime] NULL,
		[cd_completed]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled_stamp]                          [datetime] NULL,
		[cd_cancelled_by]                             [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_test]                                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_dirty]                                    [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_transfer_stamp]                           [datetime] NULL,
		[cd_transfer_driver]                          [int] NULL,
		[cd_transfer_count]                           [int] NULL,
		[cd_transfer_to]                              [int] NULL,
		[cd_toagent_notified]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_toagent_stamp]                            [datetime] NULL,
		[cd_toagent_driver]                           [int] NULL,
		[cd_toagent_count]                            [int] NULL,
		[cd_toagent_name]                             [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_status]                              [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_notified]                            [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_info]                                [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_driver]                              [int] NULL,
		[cd_last_stamp]                               [datetime] NULL,
		[cd_last_count]                               [int] NULL,
		[cd_accept_driver_branch]                     [int] NULL,
		[cd_activity_driver_branch]                   [int] NULL,
		[cd_deliver_driver_branch]                    [int] NULL,
		[cd_deliver_pay_driver_branch]                [int] NULL,
		[cd_failed_driver_branch]                     [int] NULL,
		[cd_indepot_driver_branch]                    [int] NULL,
		[cd_last_driver_branch]                       [int] NULL,
		[cd_pickup_driver_branch]                     [int] NULL,
		[cd_pickup_pay_driver_branch]                 [int] NULL,
		[cd_special_driver_branch]                    [int] NULL,
		[cd_toagent_driver_branch]                    [int] NULL,
		[cd_transfer_driver_branch]                   [int] NULL,
		[cd_pickup_state_name]                        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_state_name]                      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr0]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr1]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr2]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr3]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_email]                       [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_suburb]                      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_state_name]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_postcode]                    [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_country]                     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_contact]                     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_contact_phone]               [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_rescheduled_delivery_date]                [datetime] NULL,
		[cd_CPSortation_created_stamp]                [datetime] NOT NULL,
		[cd_CPSortation_updated_stamp]                [datetime] NOT NULL,
		[cd_locked_for_editing_after_redirection]     [bit] NOT NULL,
		[cd_locked_for_editing_after_redelivery]      [bit] NOT NULL,
		CONSTRAINT [PK_edi_consignment]
		PRIMARY KEY
		CLUSTERED
		([cd_id], [cd_date])
)
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ca__00AA174D]
	DEFAULT ('') FOR [cd_cancelled_by]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_te__019E3B86]
	DEFAULT ('N') FOR [cd_test]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__02925FBF]
	DEFAULT ('Y') FOR [cd_dirty]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__038683F8]
	DEFAULT (NULL) FOR [cd_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__047AA831]
	DEFAULT ('0') FOR [cd_transfer_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__056ECC6A]
	DEFAULT ('0') FOR [cd_transfer_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__0662F0A3]
	DEFAULT ('0') FOR [cd_transfer_to]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__075714DC]
	DEFAULT ('N') FOR [cd_toagent_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__084B3915]
	DEFAULT (NULL) FOR [cd_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__093F5D4E]
	DEFAULT ('0') FOR [cd_toagent_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__0A338187]
	DEFAULT ('0') FOR [cd_toagent_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__0B27A5C0]
	DEFAULT ('') FOR [cd_toagent_name]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__0C1BC9F9]
	DEFAULT ('') FOR [cd_last_status]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__0D0FEE32]
	DEFAULT ('N') FOR [cd_last_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__0E04126B]
	DEFAULT (NULL) FOR [cd_last_info]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__0EF836A4]
	DEFAULT ('0') FOR [cd_last_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__0F2D40CE]
	DEFAULT ('0') FOR [cd_company_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__0FEC5ADD]
	DEFAULT (NULL) FOR [cd_last_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__10216507]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__10E07F16]
	DEFAULT ('0') FOR [cd_last_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__11158940]
	DEFAULT (NULL) FOR [cd_agent_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__11D4A34F]
	DEFAULT (NULL) FOR [cd_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_im__1209AD79]
	DEFAULT (NULL) FOR [cd_import_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__12C8C788]
	DEFAULT (NULL) FOR [cd_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_og__12FDD1B2]
	DEFAULT ('0') FOR [cd_ogm_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__13BCEBC1]
	DEFAULT (NULL) FOR [cd_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ma__13F1F5EB]
	DEFAULT ('0') FOR [cd_manifest_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__14B10FFA]
	DEFAULT (NULL) FOR [cd_deliver_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__14E61A24]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_fa__15A53433]
	DEFAULT (NULL) FOR [cd_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_da__15DA3E5D]
	DEFAULT ('01-01-1900') FOR [cd_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__1699586C]
	DEFAULT (NULL) FOR [cd_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__16CE6296]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__178D7CA5]
	DEFAULT (NULL) FOR [cd_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_et__17C286CF]
	DEFAULT (NULL) FOR [cd_eta_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__1881A0DE]
	DEFAULT (NULL) FOR [cd_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_et__18B6AB08]
	DEFAULT (NULL) FOR [cd_eta_earliest]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__1975C517]
	DEFAULT (NULL) FOR [cd_pickup_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_cu__19AACF41]
	DEFAULT (NULL) FOR [cd_customer_eta]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__1A69E950]
	DEFAULT (NULL) FOR [cd_special_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__1A9EF37A]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__1B5E0D89]
	DEFAULT (NULL) FOR [cd_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__1B9317B3]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__1C5231C2]
	DEFAULT (NULL) FOR [cd_transfer_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__1C873BEC]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_CP__1D4655FB]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [cd_CPSortation_created_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__1D7B6025]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_CP__1E3A7A34]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [cd_CPSortation_updated_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__1E6F845E]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_lo__1F2E9E6D]
	DEFAULT ((0)) FOR [cd_locked_for_editing_after_redirection]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__1F63A897]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_lo__2022C2A6]
	DEFAULT ((0)) FOR [cd_locked_for_editing_after_redelivery]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__2057CCD0]
	DEFAULT ('0') FOR [cd_pickup_record_no]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__214BF109]
	DEFAULT ('0') FOR [cd_pickup_confidence]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__22401542]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__2334397B]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__24285DB4]
	DEFAULT ('') FOR [cd_delivery_addr0]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__251C81ED]
	DEFAULT ('') FOR [cd_delivery_addr1]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__2610A626]
	DEFAULT ('') FOR [cd_delivery_addr2]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__2704CA5F]
	DEFAULT ('') FOR [cd_delivery_addr3]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__27F8EE98]
	DEFAULT ('') FOR [cd_delivery_email]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__28ED12D1]
	DEFAULT ('') FOR [cd_delivery_suburb]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__29E1370A]
	DEFAULT (NULL) FOR [cd_delivery_postcode]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__2AD55B43]
	DEFAULT ('0') FOR [cd_delivery_record_no]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__2BC97F7C]
	DEFAULT ('0') FOR [cd_delivery_confidence]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__2CBDA3B5]
	DEFAULT ('') FOR [cd_delivery_contact]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__2DB1C7EE]
	DEFAULT ('') FOR [cd_delivery_contact_phone]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__2EA5EC27]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_st__2F9A1060]
	DEFAULT (NULL) FOR [cd_stats_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_st__308E3499]
	DEFAULT (NULL) FOR [cd_stats_depot]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__318258D2]
	DEFAULT (NULL) FOR [cd_pickup_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__32767D0B]
	DEFAULT (NULL) FOR [cd_pickup_pay_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__336AA144]
	DEFAULT (NULL) FOR [cd_deliver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__345EC57D]
	DEFAULT (NULL) FOR [cd_deliver_pay_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__3552E9B6]
	DEFAULT ('0') FOR [cd_special_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__36470DEF]
	DEFAULT ('0') FOR [cd_pickup_revenue]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__373B3228]
	DEFAULT ('0') FOR [cd_deliver_revenue]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__382F5661]
	DEFAULT ('0') FOR [cd_pickup_billing]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__39237A9A]
	DEFAULT ('0') FOR [cd_deliver_billing]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3A179ED3]
	DEFAULT ('0') FOR [cd_pickup_charge]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3B0BC30C]
	DEFAULT ('0') FOR [cd_pickup_charge_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__3BFFE745]
	DEFAULT ('0') FOR [cd_deliver_charge]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__3CF40B7E]
	DEFAULT ('0') FOR [cd_deliver_payment_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3DE82FB7]
	DEFAULT ('0') FOR [cd_pickup_payment]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3EDC53F0]
	DEFAULT ('0') FOR [cd_pickup_payment_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__3FD07829]
	DEFAULT ('0') FOR [cd_deliver_payment]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__40C49C62]
	DEFAULT ('0') FOR [cd_deliver_charge_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__41B8C09B]
	DEFAULT ('0') FOR [cd_special_payment]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__42ACE4D4]
	DEFAULT ('0') FOR [cd_insurance_billing]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_it__43A1090D]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__44952D46]
	DEFAULT (NULL) FOR [cd_coupons]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__4589517F]
	DEFAULT (NULL) FOR [cd_references]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ra__467D75B8]
	DEFAULT (NULL) FOR [cd_rating_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ch__477199F1]
	DEFAULT (NULL) FOR [cd_chargeunits]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__4865BE2A]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__4959E263]
	DEFAULT (NULL) FOR [cd_dimension0]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__4A4E069C]
	DEFAULT (NULL) FOR [cd_dimension1]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__4B422AD5]
	DEFAULT (NULL) FOR [cd_dimension2]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_vo__4C364F0E]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_vo__4D2A7347]
	DEFAULT ('N') FOR [cd_volume_automatic]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_im__4E1E9780]
	DEFAULT (NULL) FOR [cd_import_deadweight]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_im__4F12BBB9]
	DEFAULT (NULL) FOR [cd_import_volume]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_me__5006DFF2]
	DEFAULT (NULL) FOR [cd_measured_deadweight]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_me__50FB042B]
	DEFAULT (NULL) FOR [cd_measured_volume]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_bi__51EF2864]
	DEFAULT ('0') FOR [cd_billing_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_bi__52E34C9D]
	DEFAULT (NULL) FOR [cd_billing_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ex__53D770D6]
	DEFAULT ('0') FOR [cd_export_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ex__54CB950F]
	DEFAULT ('0') FOR [cd_export2_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__55BFB948]
	DEFAULT (NULL) FOR [cd_pickup_pay_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__56B3DD81]
	DEFAULT (NULL) FOR [cd_delivery_pay_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__57A801BA]
	DEFAULT (NULL) FOR [cd_transfer_pay_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__589C25F3]
	DEFAULT (NULL) FOR [cd_activity_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__59904A2C]
	DEFAULT ('0') FOR [cd_activity_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__5A846E65]
	DEFAULT (NULL) FOR [cd_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__5B78929E]
	DEFAULT ('0') FOR [cd_pickup_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__5C6CB6D7]
	DEFAULT ('0') FOR [cd_pickup_pay_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__5D60DB10]
	DEFAULT ('0') FOR [cd_pickup_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__5E54FF49]
	DEFAULT ('N') FOR [cd_pickup_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__5F492382]
	DEFAULT (NULL) FOR [cd_accept_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__603D47BB]
	DEFAULT ('0') FOR [cd_accept_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__61316BF4]
	DEFAULT ('0') FOR [cd_accept_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__6225902D]
	DEFAULT ('N') FOR [cd_accept_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__6319B466]
	DEFAULT ('N') FOR [cd_indepot_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__640DD89F]
	DEFAULT (NULL) FOR [cd_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__6501FCD8]
	DEFAULT ('0') FOR [cd_indepot_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__65F62111]
	DEFAULT ('0') FOR [cd_indepot_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__66EA454A]
	DEFAULT ('N') FOR [cd_transfer_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_fa__67DE6983]
	DEFAULT (NULL) FOR [cd_failed_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_fa__68D28DBC]
	DEFAULT ('0') FOR [cd_failed_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__69C6B1F5]
	DEFAULT (NULL) FOR [cd_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__6ABAD62E]
	DEFAULT ('0') FOR [cd_deliver_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__6BAEFA67]
	DEFAULT ('0') FOR [cd_deliver_pay_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__6CA31EA0]
	DEFAULT ('0') FOR [cd_deliver_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__6D9742D9]
	DEFAULT ('') FOR [cd_deliver_pod]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__6E8B6712]
	DEFAULT ('N') FOR [cd_deliver_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__6F7F8B4B]
	DEFAULT ('N') FOR [cd_pickup_pay_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__7073AF84]
	DEFAULT ('N') FOR [cd_deliver_pay_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pr__7167D3BD]
	DEFAULT ('Y') FOR [cd_printed]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__725BF7F6]
	DEFAULT ('N') FOR [cd_returns]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__73501C2F]
	DEFAULT ('N') FOR [cd_release]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__74444068]
	DEFAULT (NULL) FOR [cd_release_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pr__753864A1]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__762C88DA]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7720AD13]
	DEFAULT ('0') FOR [cd_pickup_agent]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__7814D14C]
	DEFAULT ('0') FOR [cd_delivery_agent]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__7908F585]
	DEFAULT ('N') FOR [cd_agent_pod]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__79FD19BE]
	DEFAULT ('N') FOR [cd_agent_pod_desired]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__7AF13DF7]
	DEFAULT ('') FOR [cd_agent_pod_name]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__7BE56230]
	DEFAULT (NULL) FOR [cd_agent_pod_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__7CD98669]
	DEFAULT (NULL) FOR [cd_agent_pod_entry]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__7DCDAAA2]
	DEFAULT ('N') FOR [cd_completed]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ca__7EC1CEDB]
	DEFAULT ('N') FOR [cd_cancelled]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ca__7FB5F314]
	DEFAULT (NULL) FOR [cd_cancelled_stamp]
GO
CREATE NONCLUSTERED INDEX [nc_idx_edi_consignment_cd_connote]
	ON [dbo].[edi_consignment] ([cd_connote])
	ON [partition_scheme_edi_consignment_by_month] ([cd_date])
GO
CREATE NONCLUSTERED INDEX [nc_idx_edi_consignment_redelivery_redirected_locked_fields]
	ON [dbo].[edi_consignment] ([cd_locked_for_editing_after_redelivery], [cd_locked_for_editing_after_redirection])
	INCLUDE ([cd_account], [cd_connote], [cd_consignment_date], [cd_deadweight], [cd_delivery_addr0], [cd_delivery_addr1], [cd_delivery_addr2], [cd_delivery_addr3], [cd_delivery_contact], [cd_delivery_contact_phone], [cd_delivery_email], [cd_delivery_postcode], [cd_delivery_suburb], [cd_insurance], [cd_items], [cd_pickup_addr0], [cd_pickup_addr1], [cd_pickup_addr2], [cd_pickup_addr3], [cd_pickup_contact], [cd_pickup_contact_phone], [cd_pickup_postcode], [cd_pickup_suburb], [cd_pricecode], [cd_special_instructions], [cd_volume])
	ON [partition_scheme_edi_consignment_by_month] ([cd_date])
GO
CREATE NONCLUSTERED INDEX [nci_wi_edi_consignment_2077C4A121D51BEF36B269FEA41D19B6]
	ON [dbo].[edi_consignment] ([cd_locked_for_editing_after_redirection], [cd_locked_for_editing_after_redelivery])
	INCLUDE ([cd_connote])
	ON [partition_scheme_edi_consignment_by_month] ([cd_date])
GO
CREATE NONCLUSTERED INDEX [nci_wi_edi_consignment_D3F9B2CAA774A7251856879BA592E6BA]
	ON [dbo].[edi_consignment] ([cd_locked_for_editing_after_redelivery], [cd_locked_for_editing_after_redirection])
	INCLUDE ([cd_connote])
	ON [partition_scheme_edi_consignment_by_month] ([cd_date])
GO
ALTER TABLE [dbo].[edi_consignment] SET (LOCK_ESCALATION = TABLE)
GO
