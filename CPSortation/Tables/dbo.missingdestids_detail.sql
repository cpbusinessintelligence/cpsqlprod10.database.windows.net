SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[missingdestids_detail] (
		[Connote]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountCode]           [float] NULL,
		[CPCreateDateTime]      [datetime] NULL,
		[ProcessDateTime]       [datetime] NULL,
		[SerialNumber]          [float] NULL,
		[ReceivedTime]          [datetime] NULL,
		[DecisionBarcode]       [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[KnownBarcodes]         [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnknownBarcodes]       [float] NULL,
		[EDI_Address_Line0]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EDI_Address_Line1]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EDI_Address_Line2]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EDI_Address_Line3]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EDI_Suburb]            [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EDI_PostCode]          [float] NULL,
		[cd_id]                 [int] NOT NULL,
		[destination_id]        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Comments]              [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[crtd_datetime]         [datetime] NULL,
		[crtd_userid]           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Active]                [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[missingdestids_detail] SET (LOCK_ESCALATION = TABLE)
GO
