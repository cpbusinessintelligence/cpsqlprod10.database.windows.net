SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoBilling_SCAudit] (
		[Consignmentid]           [int] NOT NULL,
		[Accountcode]             [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ConsignmentDate]         [smalldatetime] NULL,
		[Service]                 [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewServiceCode]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ItemQuantity]            [int] NOT NULL,
		[DeclaredWeight]          [decimal](20, 5) NOT NULL,
		[DeclaredVolume]          [decimal](20, 5) NOT NULL,
		[DeclaredCubicWeight]     [numeric](17, 4) NULL,
		[MeasuredWeight]          [decimal](20, 5) NOT NULL,
		[MeasuredVolume]          [decimal](20, 5) NOT NULL,
		[MeasuredCubicWeight]     [numeric](17, 4) NULL,
		[BilledWeight]            [decimal](12, 2) NULL,
		[Comments]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProntoBilling_SCAudit] SET (LOCK_ESCALATION = TABLE)
GO
