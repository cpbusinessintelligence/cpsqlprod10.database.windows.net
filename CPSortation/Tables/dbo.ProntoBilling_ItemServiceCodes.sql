SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProntoBilling_ItemServiceCodes] (
		[ProntoBilling_ItemServiceCodesID]     [int] NULL,
		[ServiceCode]                          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Category]                             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AddWho]                               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AddDateTime]                          [datetime] NULL,
		[EditWho]                              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]                         [datetime] NULL
)
GO
ALTER TABLE [dbo].[ProntoBilling_ItemServiceCodes] SET (LOCK_ESCALATION = TABLE)
GO
