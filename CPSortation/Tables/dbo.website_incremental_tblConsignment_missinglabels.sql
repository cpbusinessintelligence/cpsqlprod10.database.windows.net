SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[website_incremental_tblConsignment_missinglabels] (
		[ConsignmentID]              [int] NOT NULL,
		[ConsignmentCode]            [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]            [datetime] NULL,
		[RateCardID]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountNumber]              [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupCompanyName]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupPhone]                [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupFirstName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupLastName]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress1]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress2]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupSuburb]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupStateID]              [int] NULL,
		[PickupStateName]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupPostCode]             [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupCountry]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationCompanyName]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationFirstName]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationLastName]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationAddress1]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationAddress2]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationSuburb]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationStateID]         [int] NULL,
		[DestinationStateName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationPostCode]        [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationCountry]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationPhone]           [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SpecialInstruction]         [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NoOfItems]                  [int] NULL,
		[TotalWeight]                [decimal](10, 4) NULL,
		[IsInternational]            [bit] NULL,
		[TotalVolume]                [decimal](10, 4) NULL,
		[InsuranceCategory]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DangerousGoods]             [bit] NULL,
		[IsATl]                      [bit] NULL,
		[Delivery_Status_Flag]       [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InsertDateTime]             [datetime2](7) NULL,
		CONSTRAINT [PK_website_incremental_tblConsignment_missinglabels]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentID])
)
GO
ALTER TABLE [dbo].[website_incremental_tblConsignment_missinglabels]
	ADD
	CONSTRAINT [DF_website_incremental_tblConsignment_missinglabels_InsertDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [InsertDateTime]
GO
CREATE NONCLUSTERED INDEX [IX_website_incremental_tblConsignment_missinglabels]
	ON [dbo].[website_incremental_tblConsignment_missinglabels] ([ConsignmentCode])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[website_incremental_tblConsignment_missinglabels] SET (LOCK_ESCALATION = TABLE)
GO
