SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_ProntoBilling_OPS01] (
		[prontobilling_id]          [int] NULL,
		[Account code]              [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sender name]               [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Consignment reference]     [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Consignment date]          [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]                   [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewServiceCode]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Item quantity]             [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeclaredWEight]            [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeclaredVolume]            [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeclaredCubicWeight]       [numeric](17, 4) NULL,
		[MeasuredWeight]            [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MeasuredVolume]            [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MeasuredCubicWeight]       [numeric](17, 4) NULL,
		[BilledWeight]              [numeric](12, 2) NULL,
		[Comments]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Temp_ProntoBilling_OPS01] SET (LOCK_ESCALATION = TABLE)
GO
