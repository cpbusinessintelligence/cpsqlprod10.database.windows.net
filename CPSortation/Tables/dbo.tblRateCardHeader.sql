SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRateCardHeader] (
		[RawID]                 [int] IDENTITY(1, 1) NOT NULL,
		[id]                    [int] NULL,
		[Service]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ValidFrom]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelOverride]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Fuel]                  [decimal](10, 4) NULL,
		[ChargeMethod]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VolumetricDivisor]     [int] NULL,
		[TariffId]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Logwho]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Logdate]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblRateCardHeader]
		PRIMARY KEY
		CLUSTERED
		([RawID])
)
GO
ALTER TABLE [dbo].[tblRateCardHeader]
	ADD
	CONSTRAINT [DF_tblRateCardHeader_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblRateCardHeader]
	ADD
	CONSTRAINT [DF_tblRateCardHeader_CreatedBy]
	DEFAULT (suser_sname()) FOR [CreatedBy]
GO
CREATE NONCLUSTERED INDEX [IX_tblRateCardHeader]
	ON [dbo].[tblRateCardHeader] ([Service], [ValidFrom])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRateCardHeader] SET (LOCK_ESCALATION = TABLE)
GO
