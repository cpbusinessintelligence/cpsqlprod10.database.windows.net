SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsignmentDestination_redelivery_web] (
		[cd_id]              [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[destination_id]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Comments]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[crtd_datetime]      [datetime] NULL,
		[crtd_userid]        [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Active]             [bit] NOT NULL,
		CONSTRAINT [PK__tblConsi__A069199C26311118]
		PRIMARY KEY
		CLUSTERED
		([cd_id], [destination_id], [Active])
)
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_web]
	ADD
	CONSTRAINT [DF__tblConsig__desti__67FE6514]
	DEFAULT ('UNKNOWN') FOR [destination_id]
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_web]
	ADD
	CONSTRAINT [DF__tblConsig__crtd___68F2894D]
	DEFAULT (getdate()) FOR [crtd_datetime]
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_web]
	ADD
	CONSTRAINT [DF__tblConsig__crtd___69E6AD86]
	DEFAULT (suser_sname()) FOR [crtd_userid]
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_web] SET (LOCK_ESCALATION = TABLE)
GO
