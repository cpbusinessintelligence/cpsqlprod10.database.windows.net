SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyPriceCodes] (
		[ca_account]       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[c_code]           [varchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[c_name]           [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cp_pricecode]     [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cp_order]         [int] NULL
)
GO
ALTER TABLE [dbo].[CompanyPriceCodes] SET (LOCK_ESCALATION = TABLE)
GO
