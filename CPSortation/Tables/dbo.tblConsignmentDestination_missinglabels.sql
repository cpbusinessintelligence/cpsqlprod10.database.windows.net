SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsignmentDestination_missinglabels] (
		[cd_id]              [int] NOT NULL,
		[destination_id]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Comments]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[crtd_datetime]      [datetime] NOT NULL,
		[crtd_userid]        [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Active]             [bit] NOT NULL,
		CONSTRAINT [PK_tblConsignmentDestination_missinglabels]
		PRIMARY KEY
		CLUSTERED
		([cd_id], [destination_id], [crtd_datetime], [Active])
)
GO
ALTER TABLE [dbo].[tblConsignmentDestination_missinglabels] SET (LOCK_ESCALATION = TABLE)
GO
