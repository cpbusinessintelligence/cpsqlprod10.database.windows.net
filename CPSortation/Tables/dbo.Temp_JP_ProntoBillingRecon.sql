SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_JP_ProntoBillingRecon] (
		[Consignment reference]     [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Consignment date]          [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]                   [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Account code]              [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sender locality]           [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sender State]              [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sender postcode]           [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Receiver locality]         [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Receiver state]            [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Receiver postcode]         [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Item quantity]             [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Declared weight]           [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Measured weight]           [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Declared volume]           [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Measured volume]           [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Price override]            [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BilledTotal]               [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BilledFuel]                [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BilledFreight]             [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Temp_JP_ProntoBillingRecon] SET (LOCK_ESCALATION = TABLE)
GO
