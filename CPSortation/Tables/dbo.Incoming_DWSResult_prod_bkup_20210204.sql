SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_DWSResult_prod_bkup_20210204] (
		[DWSResultId]          [int] IDENTITY(1, 1) NOT NULL,
		[SortResultJSONID]     [int] NULL,
		[ReceivedTime]         [datetime2](7) NULL,
		[DwsTimestamp]         [datetime2](7) NULL,
		[CubeLength]           [int] NULL,
		[CubeWidth]            [int] NULL,
		[CubeHeight]           [int] NULL,
		[Volume]               [int] NULL,
		[Weight]               [int] NULL,
		[ObjectType]           [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Barcodes]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SiteField1]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SiteField2]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SiteField3]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Symbol]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SorterName]           [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentId]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]          [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestID]               [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Photos]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_Incoming_DWSResult]
		PRIMARY KEY
		CLUSTERED
		([DWSResultId])
)
GO
ALTER TABLE [dbo].[Incoming_DWSResult_prod_bkup_20210204]
	ADD
	CONSTRAINT [DF_Incoming_DWSResult_ReceivedTime]
	DEFAULT (getdate()) FOR [ReceivedTime]
GO
CREATE NONCLUSTERED INDEX [IX_Incoming_DWSResult]
	ON [dbo].[Incoming_DWSResult_prod_bkup_20210204] ([Barcodes])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Incoming_DWSResult_1]
	ON [dbo].[Incoming_DWSResult_prod_bkup_20210204] ([DwsTimestamp])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_DWSResult_prod_bkup_20210204] SET (LOCK_ESCALATION = TABLE)
GO
