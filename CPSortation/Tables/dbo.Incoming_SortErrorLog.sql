SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_SortErrorLog] (
		[SortResultJSON]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ErrorNumber]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorSeverity]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorState]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorLine]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorProcedure]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorMessage]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_SortErrorLog] SET (LOCK_ESCALATION = TABLE)
GO
