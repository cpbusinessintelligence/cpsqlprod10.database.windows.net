SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_SortDetail] (
		[SortDetailID]         [int] IDENTITY(1, 1) NOT NULL,
		[DWSResultId]          [int] NOT NULL,
		[SortResultJSONID]     [int] NULL,
		[SortLoopId]           [bit] NULL,
		[Sn]                   [int] NULL,
		[IsCal]                [bit] NULL,
		[KnownBarcodes]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UnknownBarcodes]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RemapFrom]            [int] NULL,
		[TransactionStart]     [datetime2](7) NULL,
		[TransactionEnd]       [datetime2](7) NULL,
		CONSTRAINT [PK_UAT_Incoming_SortDetail]
		PRIMARY KEY
		CLUSTERED
		([SortDetailID])
)
GO
ALTER TABLE [dbo].[Incoming_SortDetail] SET (LOCK_ESCALATION = TABLE)
GO
