SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redelivery_edi_incremental_consignment] (
		[cd_id]                            [int] NOT NULL,
		[cd_account]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_connote]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                          [smalldatetime] NULL,
		[cd_consignment_date]              [smalldatetime] NULL,
		[cd_pickup_addr0]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                 [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_state_name]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]               [int] NULL,
		[cd_pickup_contact]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_state_name]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]             [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_country]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]        [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]          [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_items]                         [int] NULL,
		[cd_deadweight]                    [float] NULL,
		[cd_volume]                        [float] NULL,
		[cd_pricecode]                     [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_status_flag]          [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_redelivery_type]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_rescheduled_delivery_date]     [datetime] NULL,
		CONSTRAINT [PK__redelive__D551B53681484033]
		PRIMARY KEY
		CLUSTERED
		([cd_id])
)
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__000AF8CF]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__00FF1D08]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__01F34141]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__02E7657A]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_sp__03DB89B3]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_it__04CFADEC]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_de__05C3D225]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_vo__06B7F65E]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pr__07AC1A97]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_in__08A03ED0]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_ac__7869D707]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_co__795DFB40]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_da__7A521F79]
	DEFAULT (NULL) FOR [cd_date]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_co__7B4643B2]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__7C3A67EB]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__7D2E8C24]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__7E22B05D]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__7F16D496]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment] SET (LOCK_ESCALATION = TABLE)
GO
