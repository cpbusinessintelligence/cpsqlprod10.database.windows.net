SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[outgoing_consignment_event_log_bkup] (
		[RowID]                 [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentNumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentJSON]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SourceID]              [int] NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[IsSent]                [bit] NULL,
		CONSTRAINT [PK_outgoing_consignment_event_log_1]
		PRIMARY KEY
		CLUSTERED
		([RowID], [CreatedDateTime])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[outgoing_consignment_event_log_bkup]
	ADD
	CONSTRAINT [DF_outgoing_consignment_event_log_1_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[outgoing_consignment_event_log_bkup]
	ADD
	CONSTRAINT [DF_outgoing_consignment_event_log_1_IsSent]
	DEFAULT ((0)) FOR [IsSent]
GO
CREATE NONCLUSTERED INDEX [nc_idx_consignment_number_outgoing_consignment_event_log_1]
	ON [dbo].[outgoing_consignment_event_log_bkup] ([ConsignmentNumber])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nc_idx_issent_outgoing_consignment_event_log_1]
	ON [dbo].[outgoing_consignment_event_log_bkup] ([IsSent])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[outgoing_consignment_event_log_bkup] SET (LOCK_ESCALATION = TABLE)
GO
