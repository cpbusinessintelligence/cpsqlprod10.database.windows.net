SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redelivery_lookup_coupons] (
		[ConsignmentCode]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime] NULL,
		CONSTRAINT [PK__redelive__3A9C08E102FA55EB]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentCode])
)
GO
ALTER TABLE [dbo].[redelivery_lookup_coupons]
	ADD
	CONSTRAINT [DF_redelivery_lookup_coupons_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[redelivery_lookup_coupons] SET (LOCK_ESCALATION = TABLE)
GO
