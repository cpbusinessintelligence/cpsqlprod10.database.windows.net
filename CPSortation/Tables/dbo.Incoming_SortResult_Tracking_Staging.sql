SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_SortResult_Tracking_Staging] (
		[RowID]                 [int] IDENTITY(1, 1) NOT NULL,
		[SortResultJSONID]      [int] NOT NULL,
		[DWSResultId]           [int] NOT NULL,
		[TrackingNumber]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ConsignmentNumber]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[EventDateTime]         [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ScanEvent]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DriverRunNumber]       [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Branch]                [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ExceptionReason]       [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AlternateBarcode]      [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[RedeliveryCard]        [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DLB]                   [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[URL]                   [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[podname]               [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PODImageURL]           [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Incoming_SortResult_Tracking_Staging] SET (LOCK_ESCALATION = TABLE)
GO
