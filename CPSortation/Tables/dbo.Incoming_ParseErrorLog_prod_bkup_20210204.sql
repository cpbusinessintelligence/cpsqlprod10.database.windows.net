SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_ParseErrorLog_prod_bkup_20210204] (
		[RowID]                [int] IDENTITY(1, 1) NOT NULL,
		[SortResultJSONID]     [int] NULL,
		[SortResultJSON]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorNumber]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorSeverity]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorState]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorLine]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorProcedure]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorMessage]         [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]          [bit] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		CONSTRAINT [PK_Incoming_ParseErrorLog]
		PRIMARY KEY
		CLUSTERED
		([RowID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_ParseErrorLog_prod_bkup_20210204]
	ADD
	CONSTRAINT [DF_Incoming_SortErrorLog_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[Incoming_ParseErrorLog_prod_bkup_20210204] SET (LOCK_ESCALATION = TABLE)
GO
