SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsignmentDestination_redirect_edi_history] (
		[cd_id]              [int] NOT NULL,
		[destination_id]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Comments]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[crtd_datetime]      [datetime] NULL,
		[crtd_userid]        [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Active]             [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redirect_edi_history] SET (LOCK_ESCALATION = TABLE)
GO
