SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblConsignmentDestination_redelivery_coupons] (
		[cd_id]              [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[destination_id]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Comments]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[crtd_datetime]      [datetime] NULL,
		[crtd_userid]        [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Active]             [bit] NOT NULL,
		CONSTRAINT [PK__tblConsi__A069199CAD45DDAB]
		PRIMARY KEY
		CLUSTERED
		([cd_id], [destination_id], [Active])
)
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_coupons]
	ADD
	CONSTRAINT [DF__tblConsig__desti__2077C861]
	DEFAULT ('UNKNOWN') FOR [destination_id]
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_coupons]
	ADD
	CONSTRAINT [DF__tblConsig__crtd___216BEC9A]
	DEFAULT (getdate()) FOR [crtd_datetime]
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_coupons]
	ADD
	CONSTRAINT [DF__tblConsig__crtd___226010D3]
	DEFAULT (suser_sname()) FOR [crtd_userid]
GO
ALTER TABLE [dbo].[tblConsignmentDestination_redelivery_coupons] SET (LOCK_ESCALATION = TABLE)
GO
