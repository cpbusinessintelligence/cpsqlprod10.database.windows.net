SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_SortAttempt] (
		[SortAttemptID]        [int] IDENTITY(1, 1) NOT NULL,
		[DWSResultId]          [int] NOT NULL,
		[SortResultJSONID]     [int] NULL,
		[AttemptBarcode]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sorted]               [bit] NULL,
		[SortStatus]           [tinyint] NULL,
		[SortLocationId]       [int] NULL,
		[SchemeTableId]        [int] NULL,
		[DecisionInfo]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[vcs]                  [bit] NULL,
		[ItemPropVals]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
CREATE NONCLUSTERED INDEX [nci_wi_UAT_Incoming_SortAttempt_83981F0FC7FC200DC868FABC5976F315]
	ON [dbo].[Incoming_SortAttempt] ([Sorted], [SortResultJSONID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_SortAttempt] SET (LOCK_ESCALATION = TABLE)
GO
