SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCrunchTable_Test] (
		[crunchtableId]         [int] NULL,
		[state]                 [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postcode]              [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[suburb]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_name]           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_type]           [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[house_number_from]     [int] NULL,
		[house_number_to]       [int] NULL,
		[interval_type]         [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[building]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[destination_id]        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[crtd_datetime]         [date] NULL,
		[crtd_userid]           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[last_upd_datetime]     [date] NULL,
		[last_upd_userid]       [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblCrunchTable_Test] SET (LOCK_ESCALATION = TABLE)
GO
