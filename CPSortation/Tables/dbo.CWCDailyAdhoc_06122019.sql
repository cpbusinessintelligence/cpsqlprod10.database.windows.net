SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CWCDailyAdhoc_06122019] (
		[barcodes]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[consignmentnumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CWCDailyAdhoc_06122019] SET (LOCK_ESCALATION = TABLE)
GO
