SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_SortResultLookup] (
		[Machine_Name]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BusinessUnit_Owner]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationID]          [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DisplayName]            [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Tracking]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Incoming_SortResultLookup] SET (LOCK_ESCALATION = TABLE)
GO
