SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblNonCustomisedRateCardDetail] (
		[RawID]               [int] IDENTITY(1, 1) NOT NULL,
		[id]                  [int] NULL,
		[UniqueKey]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Account]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EffectiveDate]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginZone]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationZone]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MinimumCharge]       [decimal](10, 4) NULL,
		[BasicCharge]         [decimal](10, 4) NULL,
		[FuelOverride]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelPercentage]      [decimal](10, 4) NULL,
		[Rounding]            [decimal](10, 4) NULL,
		[ChargePerKilo]       [decimal](10, 4) NULL,
		[CreatedDateTime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblNonCustomisedRateCardDetail]
		PRIMARY KEY
		CLUSTERED
		([RawID])
)
GO
ALTER TABLE [dbo].[tblNonCustomisedRateCardDetail]
	ADD
	CONSTRAINT [DF_tblNonCustomisedRateCardDetail_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
CREATE NONCLUSTERED INDEX [ix_tblNonCustomisedRateCardDetail]
	ON [dbo].[tblNonCustomisedRateCardDetail] ([Service], [OriginZone], [DestinationZone], [EffectiveDate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblNonCustomisedRateCardDetail] SET (LOCK_ESCALATION = TABLE)
GO
