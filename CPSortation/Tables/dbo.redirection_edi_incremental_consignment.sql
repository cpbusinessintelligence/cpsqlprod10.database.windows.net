SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redirection_edi_incremental_consignment] (
		[cd_id]                             [int] NOT NULL,
		[cd_account]                        [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_connote]                        [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                           [smalldatetime] NULL,
		[cd_consignment_date]               [smalldatetime] NULL,
		[cd_pickup_addr0]                   [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                   [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                   [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                   [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                  [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_state_name]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]                [int] NULL,
		[cd_pickup_contact]                 [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                 [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_state_name]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]              [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_country]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]         [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]           [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_items]                          [int] NULL,
		[cd_deadweight]                     [float] NULL,
		[cd_volume]                         [float] NULL,
		[cd_pricecode]                      [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                      [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_status_flag]           [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_redirected_delivery_option]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_rescheduled_delivery_date]      [datetime] NULL,
		CONSTRAINT [PK__redirect__D551B5366EA585D6]
		PRIMARY KEY
		CLUSTERED
		([cd_id])
)
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_ac__0B7CAB7B]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_co__0C70CFB4]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_da__0D64F3ED]
	DEFAULT (NULL) FOR [cd_date]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_co__0E591826]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__0F4D3C5F]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__10416098]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__113584D1]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__1229A90A]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__131DCD43]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__1411F17C]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__150615B5]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__15FA39EE]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_sp__16EE5E27]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_it__17E28260]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_de__18D6A699]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_vo__19CACAD2]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pr__1ABEEF0B]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_in__1BB31344]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment] SET (LOCK_ESCALATION = TABLE)
GO
