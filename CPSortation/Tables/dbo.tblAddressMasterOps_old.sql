SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAddressMasterOps_old] (
		[delivy_point_id]         [int] NULL,
		[house_number_from]       [int] NULL,
		[house_number_to]         [int] NULL,
		[flat_unit_type]          [varchar](7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[flat_unit_type_full]     [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[flat_unit_nbr]           [varchar](7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[lot_nbr]                 [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_name]             [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]                  [varchar](46) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postcode]                [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_type]             [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_type_full]        [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[destination_id]          [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblAddressMasterOps_old] SET (LOCK_ESCALATION = TABLE)
GO
