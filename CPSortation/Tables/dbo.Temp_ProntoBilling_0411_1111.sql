SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Temp_ProntoBilling_0411_1111] (
		[ConsignmentReference]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ConsignmentDate]           [date] NULL,
		[AccountCode]               [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AccountName]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountingDate]            [date] NULL,
		[AccountBillToCode]         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AccountBillToName]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]               [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[OriginLocality]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginPostcode]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationLocality]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationPostcode]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ItemQuantity]              [float] NULL,
		[DeclaredWeight]            [float] NULL,
		[DeadWeight]                [float] NULL,
		[DeclaredVolume]            [float] NULL,
		[Volume]                    [float] NULL,
		[ChargeableWeight]          [float] NULL,
		[BilledFreightCharge]       [money] NULL,
		[BilledFuelSurcharge]       [money] NULL,
		[BilledTransportCharge]     [money] NULL,
		[BilledInsurance]           [money] NULL,
		[BilledOtherCharge]         [money] NULL,
		[BilledTotal]               [money] NULL
)
GO
ALTER TABLE [dbo].[Temp_ProntoBilling_0411_1111] SET (LOCK_ESCALATION = TABLE)
GO
