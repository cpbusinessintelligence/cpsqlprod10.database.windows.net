SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTariffAccount_Old] (
		[Accountcode]           [float] NOT NULL,
		[TariffAccountCode]     [float] NOT NULL,
		[Shortname]             [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NotUsed]               [float] NULL,
		[Warehouse]             [float] NULL,
		[ClearFlag]             [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FreightCode]           [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TermDisc]              [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Territory]             [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[id]                    [bigint] NOT NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblTariffAccount_Old] SET (LOCK_ESCALATION = TABLE)
GO
