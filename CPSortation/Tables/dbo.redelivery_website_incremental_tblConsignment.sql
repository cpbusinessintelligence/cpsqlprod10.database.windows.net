SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redelivery_website_incremental_tblConsignment] (
		[ConsignmentID]               [int] NOT NULL,
		[ConsignmentCode]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountNumber]               [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupFirstName]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupLastName]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupCompanyName]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupEmail]                 [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress1]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress2]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupSuburb]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupStateName]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupStateID]               [int] NULL,
		[PickupPostCode]              [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupPhone]                 [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupCountry]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationFirstName]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationLastName]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationCompanyName]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationEmail]            [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationAddress1]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationAddress2]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationSuburb]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationStateName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationStateID]          [int] NULL,
		[DestinationPostCode]         [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationPhone]            [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationCountry]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalWeight]                 [decimal](10, 4) NULL,
		[TotalVolume]                 [decimal](10, 4) NULL,
		[NoOfItems]                   [int] NOT NULL,
		[SpecialInstruction]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DangerousGoods]              [bit] NOT NULL,
		[RateCardID]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedDateTime]             [datetime] NOT NULL,
		[IsInternational]             [bit] NULL,
		[IsATl]                       [bit] NULL,
		[InsuranceCategory]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Delivery_Status_Flag]        [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedeliveryType]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RescheduledDeliveryDate]     [datetime] NULL,
		CONSTRAINT [UQ__redelive__3A9C08E08C6D1EB2]
		UNIQUE
		NONCLUSTERED
		([ConsignmentCode]),
		CONSTRAINT [PK__redelive__2AB758635059FF27]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentID])
)
GO
ALTER TABLE [dbo].[redelivery_website_incremental_tblConsignment]
	ADD
	CONSTRAINT [DF__redeliver__IsInt__0A888742]
	DEFAULT ((0)) FOR [IsInternational]
GO
ALTER TABLE [dbo].[redelivery_website_incremental_tblConsignment] SET (LOCK_ESCALATION = TABLE)
GO
