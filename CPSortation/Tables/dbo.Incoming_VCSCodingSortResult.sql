SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_VCSCodingSortResult] (
		[VCSCodingSortResult]     [int] IDENTITY(1, 1) NOT NULL,
		[DWSResultID]             [int] NULL,
		[SortResultJSONID]        [int] NULL,
		[SortServerId]            [int] NULL,
		[SequenceNumber]          [bigint] NULL,
		[ReceiveTime]             [datetime2](7) NULL,
		[CodingResultType]        [int] NULL,
		[CodingRequestType]       [int] NULL,
		[AttemptBarcode]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sorted]                  [bit] NULL,
		[SortStatus]              [int] NULL,
		[SortLocationId]          [int] NULL,
		[DecisionInfo]            [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsVcs]                   [bit] NULL,
		CONSTRAINT [PK_UAT_Incoming_VCSCodingSortResult]
		PRIMARY KEY
		CLUSTERED
		([VCSCodingSortResult])
)
GO
ALTER TABLE [dbo].[Incoming_VCSCodingSortResult] SET (LOCK_ESCALATION = TABLE)
GO
