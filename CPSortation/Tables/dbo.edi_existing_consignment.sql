SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_existing_consignment] (
		[cd_id]                            [int] NOT NULL,
		[cd_company_id]                    [int] NOT NULL,
		[cd_account]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_id]                      [int] NULL,
		[cd_import_id]                     [int] NULL,
		[cd_ogm_id]                        [int] NULL,
		[cd_manifest_id]                   [int] NULL,
		[cd_connote]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                          [smalldatetime] NULL,
		[cd_consignment_date]              [smalldatetime] NULL,
		[cd_eta_date]                      [smalldatetime] NULL,
		[cd_eta_earliest]                  [smalldatetime] NULL,
		[cd_customer_eta]                  [smalldatetime] NULL,
		[cd_pickup_addr0]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                 [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]               [int] NULL,
		[cd_pickup_record_no]              [int] NULL,
		[cd_pickup_confidence]             [int] NULL,
		[cd_pickup_contact]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]               [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]             [int] NULL,
		[cd_delivery_record_no]            [int] NULL,
		[cd_delivery_confidence]           [int] NULL,
		[cd_delivery_contact]              [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]          [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_stats_branch]                  [int] NULL,
		[cd_stats_depot]                   [int] NULL,
		[cd_pickup_branch]                 [int] NULL,
		[cd_pickup_pay_branch]             [int] NULL,
		[cd_deliver_branch]                [int] NULL,
		[cd_deliver_pay_branch]            [int] NULL,
		[cd_special_driver]                [int] NULL,
		[cd_pickup_revenue]                [float] NULL,
		[cd_deliver_revenue]               [float] NULL,
		[cd_pickup_billing]                [float] NULL,
		[cd_deliver_billing]               [float] NULL,
		[cd_pickup_charge]                 [float] NULL,
		[cd_pickup_charge_actual]          [float] NULL,
		[cd_deliver_charge]                [float] NULL,
		[cd_deliver_payment_actual]        [float] NULL,
		[cd_pickup_payment]                [float] NULL,
		[cd_pickup_payment_actual]         [float] NULL,
		[cd_deliver_payment]               [float] NULL,
		[cd_deliver_charge_actual]         [float] NULL,
		[cd_special_payment]               [float] NULL,
		[cd_insurance_billing]             [float] NULL,
		[cd_items]                         [int] NULL,
		[cd_coupons]                       [int] NULL,
		[cd_references]                    [int] NULL,
		[cd_rating_id]                     [int] NULL,
		[cd_chargeunits]                   [int] NULL,
		[cd_deadweight]                    [float] NULL,
		[cd_dimension0]                    [float] NULL,
		[cd_dimension1]                    [float] NULL,
		[cd_dimension2]                    [float] NULL,
		[cd_volume]                        [float] NULL,
		[cd_volume_automatic]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_import_deadweight]             [float] NULL,
		[cd_import_volume]                 [float] NULL,
		[cd_measured_deadweight]           [float] NULL,
		[cd_measured_volume]               [float] NULL,
		[cd_billing_id]                    [int] NULL,
		[cd_billing_date]                  [smalldatetime] NULL,
		[cd_export_id]                     [int] NULL,
		[cd_export2_id]                    [int] NULL,
		[cd_pickup_pay_date]               [smalldatetime] NULL,
		[cd_delivery_pay_date]             [smalldatetime] NULL,
		[cd_transfer_pay_date]             [smalldatetime] NULL,
		[cd_activity_stamp]                [datetime] NULL,
		[cd_activity_driver]               [int] NULL,
		[cd_pickup_stamp]                  [datetime] NULL,
		[cd_pickup_driver]                 [int] NULL,
		[cd_pickup_pay_driver]             [int] NULL,
		[cd_pickup_count]                  [int] NULL,
		[cd_pickup_notified]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_accept_stamp]                  [datetime] NULL,
		[cd_accept_driver]                 [int] NULL,
		[cd_accept_count]                  [int] NULL,
		[cd_accept_notified]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_stamp]                 [datetime] NULL,
		[cd_indepot_driver]                [int] NULL,
		[cd_indepot_count]                 [int] NULL,
		[cd_transfer_notified]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_failed_stamp]                  [datetime] NULL,
		[cd_failed_driver]                 [int] NULL,
		[cd_deliver_stamp]                 [datetime] NULL,
		[cd_deliver_driver]                [int] NULL,
		[cd_deliver_pay_driver]            [int] NULL,
		[cd_deliver_count]                 [int] NULL,
		[cd_deliver_pod]                   [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_pay_notified]           [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_pay_notified]          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_printed]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_returns]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release_stamp]                 [datetime] NULL,
		[cd_pricecode]                     [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_agent]                  [int] NULL,
		[cd_delivery_agent]                [int] NULL,
		[cd_agent_pod]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_desired]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_name]                [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_stamp]               [datetime] NULL,
		[cd_agent_pod_entry]               [datetime] NULL,
		[cd_completed]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled_stamp]               [datetime] NULL,
		[cd_cancelled_by]                  [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_test]                          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_dirty]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_transfer_stamp]                [datetime] NULL,
		[cd_transfer_driver]               [int] NULL,
		[cd_transfer_count]                [int] NULL,
		[cd_transfer_to]                   [int] NULL,
		[cd_toagent_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_toagent_stamp]                 [datetime] NULL,
		[cd_toagent_driver]                [int] NULL,
		[cd_toagent_count]                 [int] NULL,
		[cd_toagent_name]                  [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_status]                   [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_notified]                 [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_info]                     [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_driver]                   [int] NULL,
		[cd_last_stamp]                    [datetime] NULL,
		[cd_last_count]                    [int] NULL,
		[cd_accept_driver_branch]          [int] NULL,
		[cd_activity_driver_branch]        [int] NULL,
		[cd_deliver_driver_branch]         [int] NULL,
		[cd_deliver_pay_driver_branch]     [int] NULL,
		[cd_failed_driver_branch]          [int] NULL,
		[cd_indepot_driver_branch]         [int] NULL,
		[cd_last_driver_branch]            [int] NULL,
		[cd_pickup_driver_branch]          [int] NULL,
		[cd_pickup_pay_driver_branch]      [int] NULL,
		[cd_special_driver_branch]         [int] NULL,
		[cd_toagent_driver_branch]         [int] NULL,
		[cd_transfer_driver_branch]        [int] NULL
)
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__00750D23]
	DEFAULT ('N') FOR [cd_deliver_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__0169315C]
	DEFAULT ('N') FOR [cd_pickup_pay_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__025D5595]
	DEFAULT ('N') FOR [cd_deliver_pay_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pr__035179CE]
	DEFAULT ('Y') FOR [cd_printed]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_re__04459E07]
	DEFAULT ('N') FOR [cd_returns]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_re__0539C240]
	DEFAULT ('N') FOR [cd_release]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_re__062DE679]
	DEFAULT (NULL) FOR [cd_release_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pr__07220AB2]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__08162EEB]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__090A5324]
	DEFAULT ('0') FOR [cd_pickup_agent]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__09FE775D]
	DEFAULT ('0') FOR [cd_delivery_agent]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__0AF29B96]
	DEFAULT ('N') FOR [cd_agent_pod]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__0BE6BFCF]
	DEFAULT ('N') FOR [cd_agent_pod_desired]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__0CDAE408]
	DEFAULT ('') FOR [cd_agent_pod_name]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__0DCF0841]
	DEFAULT (NULL) FOR [cd_agent_pod_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__0EC32C7A]
	DEFAULT (NULL) FOR [cd_agent_pod_entry]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_co__0FB750B3]
	DEFAULT ('N') FOR [cd_completed]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ca__10AB74EC]
	DEFAULT ('N') FOR [cd_cancelled]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ca__119F9925]
	DEFAULT (NULL) FOR [cd_cancelled_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ca__1293BD5E]
	DEFAULT ('') FOR [cd_cancelled_by]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_te__1387E197]
	DEFAULT ('N') FOR [cd_test]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_di__147C05D0]
	DEFAULT ('Y') FOR [cd_dirty]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__15702A09]
	DEFAULT (NULL) FOR [cd_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__16644E42]
	DEFAULT ('0') FOR [cd_transfer_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__1758727B]
	DEFAULT ('0') FOR [cd_transfer_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__184C96B4]
	DEFAULT ('0') FOR [cd_transfer_to]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__1940BAED]
	DEFAULT ('N') FOR [cd_toagent_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__1A34DF26]
	DEFAULT (NULL) FOR [cd_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__1B29035F]
	DEFAULT ('0') FOR [cd_toagent_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__1C1D2798]
	DEFAULT ('0') FOR [cd_toagent_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__1D114BD1]
	DEFAULT ('') FOR [cd_toagent_name]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__1E05700A]
	DEFAULT ('') FOR [cd_last_status]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__1EF99443]
	DEFAULT ('N') FOR [cd_last_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__1FEDB87C]
	DEFAULT (NULL) FOR [cd_last_info]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__20E1DCB5]
	DEFAULT ('0') FOR [cd_last_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_co__2116E6DF]
	DEFAULT ('0') FOR [cd_company_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__21D600EE]
	DEFAULT (NULL) FOR [cd_last_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__220B0B18]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__22CA2527]
	DEFAULT ('0') FOR [cd_last_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__22FF2F51]
	DEFAULT (NULL) FOR [cd_agent_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__23BE4960]
	DEFAULT (NULL) FOR [cd_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_im__23F3538A]
	DEFAULT (NULL) FOR [cd_import_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__24B26D99]
	DEFAULT (NULL) FOR [cd_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_og__24E777C3]
	DEFAULT ('0') FOR [cd_ogm_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__25A691D2]
	DEFAULT (NULL) FOR [cd_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ma__25DB9BFC]
	DEFAULT ('0') FOR [cd_manifest_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__269AB60B]
	DEFAULT (NULL) FOR [cd_deliver_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_co__26CFC035]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_fa__278EDA44]
	DEFAULT (NULL) FOR [cd_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_da__27C3E46E]
	DEFAULT (NULL) FOR [cd_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__2882FE7D]
	DEFAULT (NULL) FOR [cd_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_co__28B808A7]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__297722B6]
	DEFAULT (NULL) FOR [cd_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_et__29AC2CE0]
	DEFAULT (NULL) FOR [cd_eta_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__2A6B46EF]
	DEFAULT (NULL) FOR [cd_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_et__2AA05119]
	DEFAULT (NULL) FOR [cd_eta_earliest]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__2B5F6B28]
	DEFAULT (NULL) FOR [cd_pickup_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_cu__2B947552]
	DEFAULT (NULL) FOR [cd_customer_eta]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_sp__2C538F61]
	DEFAULT (NULL) FOR [cd_special_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__2C88998B]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__2D47B39A]
	DEFAULT (NULL) FOR [cd_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__2D7CBDC4]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__2E3BD7D3]
	DEFAULT (NULL) FOR [cd_transfer_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__2E70E1FD]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__2F650636]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__30592A6F]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__314D4EA8]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__324172E1]
	DEFAULT ('0') FOR [cd_pickup_record_no]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__3335971A]
	DEFAULT ('0') FOR [cd_pickup_confidence]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__3429BB53]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__351DDF8C]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__361203C5]
	DEFAULT ('') FOR [cd_delivery_addr0]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__370627FE]
	DEFAULT ('') FOR [cd_delivery_addr1]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__37FA4C37]
	DEFAULT ('') FOR [cd_delivery_addr2]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__38EE7070]
	DEFAULT ('') FOR [cd_delivery_addr3]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__39E294A9]
	DEFAULT ('') FOR [cd_delivery_email]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__3AD6B8E2]
	DEFAULT ('') FOR [cd_delivery_suburb]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__3BCADD1B]
	DEFAULT (NULL) FOR [cd_delivery_postcode]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__3CBF0154]
	DEFAULT ('0') FOR [cd_delivery_record_no]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__3DB3258D]
	DEFAULT ('0') FOR [cd_delivery_confidence]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__3EA749C6]
	DEFAULT ('') FOR [cd_delivery_contact]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__3F9B6DFF]
	DEFAULT ('') FOR [cd_delivery_contact_phone]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_sp__408F9238]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_st__4183B671]
	DEFAULT (NULL) FOR [cd_stats_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_st__4277DAAA]
	DEFAULT (NULL) FOR [cd_stats_depot]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__436BFEE3]
	DEFAULT (NULL) FOR [cd_pickup_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4460231C]
	DEFAULT (NULL) FOR [cd_pickup_pay_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__45544755]
	DEFAULT (NULL) FOR [cd_deliver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__46486B8E]
	DEFAULT (NULL) FOR [cd_deliver_pay_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_sp__473C8FC7]
	DEFAULT ('0') FOR [cd_special_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4830B400]
	DEFAULT ('0') FOR [cd_pickup_revenue]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__4924D839]
	DEFAULT ('0') FOR [cd_deliver_revenue]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4A18FC72]
	DEFAULT ('0') FOR [cd_pickup_billing]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__4B0D20AB]
	DEFAULT ('0') FOR [cd_deliver_billing]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4C0144E4]
	DEFAULT ('0') FOR [cd_pickup_charge]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4CF5691D]
	DEFAULT ('0') FOR [cd_pickup_charge_actual]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__4DE98D56]
	DEFAULT ('0') FOR [cd_deliver_charge]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__4EDDB18F]
	DEFAULT ('0') FOR [cd_deliver_payment_actual]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4FD1D5C8]
	DEFAULT ('0') FOR [cd_pickup_payment]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__50C5FA01]
	DEFAULT ('0') FOR [cd_pickup_payment_actual]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__51BA1E3A]
	DEFAULT ('0') FOR [cd_deliver_payment]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__52AE4273]
	DEFAULT ('0') FOR [cd_deliver_charge_actual]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_sp__53A266AC]
	DEFAULT ('0') FOR [cd_special_payment]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__54968AE5]
	DEFAULT ('0') FOR [cd_insurance_billing]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_it__558AAF1E]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_co__567ED357]
	DEFAULT (NULL) FOR [cd_coupons]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_re__5772F790]
	DEFAULT (NULL) FOR [cd_references]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ra__58671BC9]
	DEFAULT (NULL) FOR [cd_rating_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ch__595B4002]
	DEFAULT (NULL) FOR [cd_chargeunits]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__5A4F643B]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_di__5B438874]
	DEFAULT (NULL) FOR [cd_dimension0]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_di__5C37ACAD]
	DEFAULT (NULL) FOR [cd_dimension1]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_di__5D2BD0E6]
	DEFAULT (NULL) FOR [cd_dimension2]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_vo__5E1FF51F]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_vo__5F141958]
	DEFAULT ('N') FOR [cd_volume_automatic]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_im__60083D91]
	DEFAULT (NULL) FOR [cd_import_deadweight]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_im__60FC61CA]
	DEFAULT (NULL) FOR [cd_import_volume]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_me__61F08603]
	DEFAULT (NULL) FOR [cd_measured_deadweight]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_me__62E4AA3C]
	DEFAULT (NULL) FOR [cd_measured_volume]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_bi__63D8CE75]
	DEFAULT ('0') FOR [cd_billing_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_bi__64CCF2AE]
	DEFAULT (NULL) FOR [cd_billing_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ex__65C116E7]
	DEFAULT ('0') FOR [cd_export_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ex__66B53B20]
	DEFAULT ('0') FOR [cd_export2_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__67A95F59]
	DEFAULT (NULL) FOR [cd_pickup_pay_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__689D8392]
	DEFAULT (NULL) FOR [cd_delivery_pay_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__6991A7CB]
	DEFAULT (NULL) FOR [cd_transfer_pay_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__6A85CC04]
	DEFAULT (NULL) FOR [cd_activity_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__6B79F03D]
	DEFAULT ('0') FOR [cd_activity_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__6C6E1476]
	DEFAULT (NULL) FOR [cd_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__6D6238AF]
	DEFAULT ('0') FOR [cd_pickup_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__6E565CE8]
	DEFAULT ('0') FOR [cd_pickup_pay_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__6F4A8121]
	DEFAULT ('0') FOR [cd_pickup_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__703EA55A]
	DEFAULT ('N') FOR [cd_pickup_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__7132C993]
	DEFAULT (NULL) FOR [cd_accept_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__7226EDCC]
	DEFAULT ('0') FOR [cd_accept_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__731B1205]
	DEFAULT ('0') FOR [cd_accept_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__740F363E]
	DEFAULT ('N') FOR [cd_accept_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__75035A77]
	DEFAULT ('N') FOR [cd_indepot_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__75F77EB0]
	DEFAULT (NULL) FOR [cd_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__76EBA2E9]
	DEFAULT ('0') FOR [cd_indepot_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__77DFC722]
	DEFAULT ('0') FOR [cd_indepot_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__78D3EB5B]
	DEFAULT ('N') FOR [cd_transfer_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_fa__79C80F94]
	DEFAULT (NULL) FOR [cd_failed_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_fa__7ABC33CD]
	DEFAULT ('0') FOR [cd_failed_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__7BB05806]
	DEFAULT (NULL) FOR [cd_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__7CA47C3F]
	DEFAULT ('0') FOR [cd_deliver_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__7D98A078]
	DEFAULT ('0') FOR [cd_deliver_pay_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__7E8CC4B1]
	DEFAULT ('0') FOR [cd_deliver_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__7F80E8EA]
	DEFAULT ('') FOR [cd_deliver_pod]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_nc_edi_existing_consignment]
	ON [dbo].[edi_existing_consignment] ([cd_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[edi_existing_consignment] SET (LOCK_ESCALATION = TABLE)
GO
