SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_cwcdetails_06122019] (
		[DwsTimeStamp]        [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MachineLocation]     [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[MachineID]           [varchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Barcodes]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CubeLength]          [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CubeWidth]           [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CubeHeight]          [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Weight]              [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Volume]              [varchar](13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[dwsresultid]         [int] NULL,
		[cd_Connote]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountCode]         [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_id]               [bigint] NULL,
		[DWSDateTime]         [datetime2](7) NULL
)
GO
ALTER TABLE [dbo].[temp_cwcdetails_06122019] SET (LOCK_ESCALATION = TABLE)
GO
