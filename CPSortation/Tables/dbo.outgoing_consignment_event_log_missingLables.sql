SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[outgoing_consignment_event_log_missingLables] (
		[RowID]                 [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentNumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentJSON]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SourceID]              [int] NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[IsSent]                [bit] NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[outgoing_consignment_event_log_missingLables] SET (LOCK_ESCALATION = TABLE)
GO
