SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_SortResult_Tracking_History] (
		[RowID]                 [int] IDENTITY(1, 1) NOT NULL,
		[SortResultJSONID]      [int] NOT NULL,
		[DWSResultId]           [int] NOT NULL,
		[TrackingNumber]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ConsignmentNumber]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[EventDateTime]         [datetime2](7) NOT NULL,
		[ScanEvent]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DriverRunNumber]       [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Branch]                [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ExceptionReason]       [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AlternateBarcode]      [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[RedeliveryCard]        [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DLB]                   [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[URL]                   [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[podname]               [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PODImageURL]           [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsProcessed]           [bit] NOT NULL,
		[CreateDateTime]        [datetime] NULL,
		CONSTRAINT [PK_Outgoing_SortResult_TrackingEvent_History]
		PRIMARY KEY
		CLUSTERED
		([RowID])
)
GO
ALTER TABLE [dbo].[Incoming_SortResult_Tracking_History]
	ADD
	CONSTRAINT [DF_Outgoing_SortResult_TrackingEvent_History_IsProcessed]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[Incoming_SortResult_Tracking_History]
	ADD
	CONSTRAINT [DF_Outgoing_SortResult_TrackingEvent_History_CreateDateTime]
	DEFAULT (getdate()) FOR [CreateDateTime]
GO
CREATE NONCLUSTERED INDEX [nci_wi_Incoming_SortResult_Tracking_History_43CD0AD84CDF66D0EA5E1FBF3C43DF90]
	ON [dbo].[Incoming_SortResult_Tracking_History] ([IsProcessed], [TrackingNumber])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_SortResult_Tracking_History] SET (LOCK_ESCALATION = TABLE)
GO
