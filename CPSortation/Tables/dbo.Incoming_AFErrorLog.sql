SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incoming_AFErrorLog] (
		[RowID]               [int] IDENTITY(1, 1) NOT NULL,
		[SortResultJSON]      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Exception]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		CONSTRAINT [PK_Incoming_AFErrorLog]
		PRIMARY KEY
		CLUSTERED
		([RowID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Incoming_AFErrorLog]
	ADD
	CONSTRAINT [DF__Incoming___Creat__6A1BB7B0]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[Incoming_AFErrorLog] SET (LOCK_ESCALATION = TABLE)
GO
