SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[outgoing_consignment_event_log] (
		[RowID]                 [int] IDENTITY(1, 1) NOT NULL,
		[ConsignmentNumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentJSON]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SourceID]              [int] NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[IsSent]                [bit] NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[outgoing_consignment_event_log]
	ADD
	CONSTRAINT [DF_outgoing_consignment_event_log_2_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[outgoing_consignment_event_log]
	ADD
	CONSTRAINT [DF_outgoing_consignment_event_log_2_IsSent]
	DEFAULT ((0)) FOR [IsSent]
GO
CREATE NONCLUSTERED INDEX [nci_wi_outgoing_consignment_event_log_CA78CE06B7425862C9B8B6150985071D]
	ON [dbo].[outgoing_consignment_event_log] ([IsSent])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_outgoing_consignment_event_log_5BD9784FA181980B29B5C79316FFF107]
	ON [dbo].[outgoing_consignment_event_log] ([ConsignmentNumber])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[outgoing_consignment_event_log] SET (LOCK_ESCALATION = TABLE)
GO
