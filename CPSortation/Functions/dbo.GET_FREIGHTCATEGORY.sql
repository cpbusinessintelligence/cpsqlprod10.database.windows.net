SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GET_FREIGHTCATEGORY] 
(
	@SERVICECODE VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN

	DECLARE @FREIGHTCATEGORY AS VARCHAR(50);


	SET @FREIGHTCATEGORY = CASE 
						WHEN @SERVICECODE = 'DAH' OR  @SERVICECODE = 'DAM' OR  @SERVICECODE = 'DAL' THEN 'Domestic Air'
						WHEN @SERVICECODE = 'REC' OR  @SERVICECODE = 'PEC' OR  @SERVICECODE = 'GEC' OR  @SERVICECODE = 'SGE' THEN 'Domestic Saver'
						WHEN @SERVICECODE LIKE 'EXP%' THEN 'International Express'
						WHEN @SERVICECODE LIKE 'SAV%' THEN 'International Saver'						
					ELSE 
						'Road Express' 
					END
	RETURN @FREIGHTCATEGORY;
END
GO
