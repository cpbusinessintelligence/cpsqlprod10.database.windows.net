SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_GET_DELIVERY_OPTION_ALERT] 
(
	@SELECTED_DELIVERY_OPTION VARCHAR(50)
)
RETURNS VARCHAR(100)
AS
BEGIN
	DECLARE @DELIVERY_OPTION_ALERT AS VARCHAR(100);

	SELECT @DELIVERY_OPTION_ALERT	=	CASE 
										WHEN @SELECTED_DELIVERY_OPTION = 'Alternate Address' THEN 'Redirection! New address label required.'
										WHEN @SELECTED_DELIVERY_OPTION = 'Authority To Leave' THEN 'This consignment now has Authority To Leave (ATL).'
										WHEN @SELECTED_DELIVERY_OPTION = 'Neighbour Delivery' THEN 'Neighbour Delivery! Ensure delivered to correct address.'
										WHEN @SELECTED_DELIVERY_OPTION = 'POPPoint' THEN 'Direct to POPPoint! Ensure delivered to correct POPPoint.'
										WHEN @SELECTED_DELIVERY_OPTION = 'Reschedule Delivery' THEN 'Rescheduled Delivery! Ensure delivered on the correct date.'
									ELSE 
										'' 
									END
	RETURN @DELIVERY_OPTION_ALERT;
END
GO
