SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[WEBSITE_GET_STATE] 
(
	@STATE_ID int
)
RETURNS VARCHAR(3)
AS
BEGIN

	DECLARE @STATE AS VARCHAR(3);


	SELECT @STATE = CASE 
						WHEN @STATE_ID = 1 THEN 'NSW'
						WHEN @STATE_ID = 2 THEN 'QLD'
						WHEN @STATE_ID = 3 THEN 'SA'
						WHEN @STATE_ID = 4 THEN 'TAS'
						WHEN @STATE_ID = 5 THEN 'VIC'
						WHEN @STATE_ID = 6 THEN 'WA'
						WHEN @STATE_ID = 10 THEN 'ACT'
						WHEN @STATE_ID = 11 THEN 'NT'
					ELSE 
						NULL 
					END
	RETURN @STATE;
END
GO
