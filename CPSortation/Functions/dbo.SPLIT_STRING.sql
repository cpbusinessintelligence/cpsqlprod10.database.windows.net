SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[SPLIT_STRING]
(   
    @INPUT_STRING VARCHAR(MAX),
    @LENGTH int
)
RETURNS @SOME_TABLE TABLE(CONTENT VARCHAR(MAX),SEQUENCE INT ) 
AS
BEGIN

DECLARE @END INT
DECLARE @SEQUENCE INT 
SET @SEQUENCE = 1

WHILE (LEN(@INPUT_STRING) > 0)
BEGIN
    IF (LEN(@INPUT_STRING) > @LENGTH)
    BEGIN
        SET @END = LEN(LEFT(@INPUT_STRING, @LENGTH)) - CHARINDEX(' ', REVERSE(LEFT(@INPUT_STRING, @LENGTH)))
        INSERT INTO @SOME_TABLE VALUES (RTRIM(LTRIM(LEFT(LEFT(@INPUT_STRING, @LENGTH), @END))), @SEQUENCE)
		SET @SEQUENCE = @SEQUENCE + 1
        SET @INPUT_STRING = SUBSTRING(@INPUT_STRING, @END + 1, LEN(@INPUT_STRING))

    END
    ELSE
    BEGIN
        INSERT INTO @SOME_TABLE VALUES (RTRIM(LTRIM(@INPUT_STRING)),@SEQUENCE)
		SET @SEQUENCE = @SEQUENCE + 1
        SET @INPUT_STRING = ''
    END
END
RETURN
END
GO
