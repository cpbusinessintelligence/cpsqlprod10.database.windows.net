SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GET_FULL_NAME] 
(
	@FIRST_NAME [varchar](50) NULL,
	@LAST_NAME [varchar](50) NULL
)
RETURNS [varchar](100)
AS
BEGIN
	DECLARE @FULL_NAME AS VARCHAR(100);
	DECLARE @FILLER AS VARCHAR(1);


	IF (@FIRST_NAME IS NULL OR @FIRST_NAME = '' OR @LAST_NAME IS NULL OR @LAST_NAME = '')
		SET @FILLER = '';
	ELSE
		SET @FILLER = ' ';

	SET @FULL_NAME = ISNULL(@FIRST_NAME,'') + @FILLER + ISNULL(@LAST_NAME,'');
	RETURN @FULL_NAME;

END
GO
