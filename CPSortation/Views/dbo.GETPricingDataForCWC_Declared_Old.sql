SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[GETPricingDataForCWC_Declared_Old] AS

WITH tempFinalCalculatedPricingValues1 as
(

SELECT 

		ed.cd_id
		,ed.acct
		,ed.cd_connote 
		,ed.cd_company_id
		,coalesce(tacode.TariffAccountCode,ed.acct)			as TACode
		,cast(oc.originZoneCode as varchar(20))				as originZoneCode
		,cast(dc.destZoneCode as varchar(20))				as destZoneCode
		,ed.service
		,ISNULL(ed.Shipdate,cast(FORMAT(getdate(),'yyyy-MM-dd') as nvarchar(10))) as Shipdate
		,ISNULL(ed.itemqty,1) as itemqty
		,volume * 250.0															AS Volume
		,case when [weight] < 1 and [weight] > 0 then 1 else [weight] end		AS [Weight]	
		,insurance																AS insurance
		
		,tblRateCustomisedHeader.ValidFrom_3 as ValidFrom_3a
		,tblRateCustomisedHeader.ChargeMethod_3or4 as ChargeMethod_3or4a
		,tblRateCustomisedHeader.VolumetricDivisor_3or4	as VolumetricDivisor_3or4a
		,tblRateCustomisedHeader.FuelOverride_3	as FuelOverride_3
		,tblRateCustomisedHeader.FuelPercentage_3 as FuelPercentage_3
		,ISNULL(tblRateCustomisedHeader.CountFromStep3,0) as CountFromStep3
		
		,tblRateCardHeader.ValidFrom_3 as ValidFrom_3b
		,tblRateCardHeader.ChargeMethod_3or4 as ChargeMethod_3or4b
		,tblRateCardHeader.VolumetricDivisor_3or4	as VolumetricDivisor_3or4b
		,tblRateCardHeader.FuelOverride_4 as FuelOverride_4
		,tblRateCardHeader.FuelPercentage_4 
		,ISNULL(tblRateCardHeader.CountFromStep4,0) as CountFromStep4

		,tblRateCardHeader_nonacct.ValidFrom_3 as ValidFrom_3c
		,tblRateCardHeader_nonacct.ChargeMethod_3or4 as ChargeMethod_3or4c
		,tblRateCardHeader_nonacct.VolumetricDivisor_3or4	as VolumetricDivisor_3or4c
		,tblRateCardHeader_nonacct.FuelOverride_4 as FuelOverride_4c
		,tblRateCardHeader_nonacct.FuelPercentage_4				as FuelPercentage_4c
		,ISNULL(tblRateCardHeader_nonacct.CountFromStep4,0) as CountFromStep4c
		
		,tblCustomisedRateCardDetail.MinimumCharge_5or6	as MinimumCharge_5or6a
		,tblCustomisedRateCardDetail.BasicCharge_5or6	 as BasicCharge_5or6a
		,tblCustomisedRateCardDetail.FuelOverride_5 as FuelOverride_5
		,tblCustomisedRateCardDetail.FuelPercentage_5	 as FuelPercentage_5
		,tblCustomisedRateCardDetail.Rounding_5or6	as Rounding_5or6a
		,tblCustomisedRateCardDetail.ChargePerKilo_5or6 as ChargePerKilo_5or6a
		,ISNULL(tblCustomisedRateCardDetail.CountFromStep5,0) as CountFromStep5

		,tblNonCustomisedRateCardDetail.MinimumCharge_5or6 	as MinimumCharge_5or6b
		,tblNonCustomisedRateCardDetail.BasicCharge_5or6	 as BasicCharge_5or6b
		,tblNonCustomisedRateCardDetail.FuelOverride_6 as FuelOverride_6
		,tblNonCustomisedRateCardDetail.FuelPercentage_6	 as FuelPercentage_6
		,tblNonCustomisedRateCardDetail.Rounding_5or6	as Rounding_5or6b
		,tblNonCustomisedRateCardDetail.ChargePerKilo_5or6 as ChargePerKilo_5or6b
		,ISNULL(tblNonCustomisedRateCardDetail.CountFromStep6,0) as CountFromStep6
		

from [dbo].[GetEnrichedDataForCWCPricing]  ed WITH(NOLOCK)
outer apply (SELECT distinct cast(cast(TariffAccountCode as decimal(20,0)) as varchar(20)) as TariffAccountCode FROM tblTariffAccount where cast(cast(TariffAccountCode as decimal(20,0)) as varchar(20)) = ed.acct) tacode
outer apply (SELECT top(1) PrizeZone as originZoneCode FROM tblPricingZoneLookUp where PostCode = ed.originpostcode and Suburb = ed.originsuburb) oc
outer apply (SELECT top(1) PrizeZone as destZoneCode FROM tblPricingZoneLookUp where PostCode = ed.destpostcode and Suburb = ed.destsuburb) dc
outer apply
(
SELECT Top 1 cast(ValidFrom as varchar(10))  AS ValidFrom_3, cast(ChargeMethod as varchar(1)) AS ChargeMethod_3or4, try_cast(VolumetricDivisor as decimal(20,5)) AS VolumetricDivisor_3or4, cast(FuelOverride as varchar(1)) AS FuelOverride_3, try_cast(Fuel as decimal(20,5))  AS FuelPercentage_3 ,1 as CountFromStep3 
FROM tblRateCustomisedHeader 
		WHERE Accountcode = coalesce(tacode.TariffAccountCode,ed.acct)
		and Service = ed.service
		and ValidFrom <= ed.Shipdate
		ORDER BY Cast(ValidFrom AS date) DESC
) tblRateCustomisedHeader

outer apply
(
SELECT Top 1 cast(ValidFrom as varchar(10)) AS ValidFrom_3, cast(ChargeMethod as varchar(1)) AS ChargeMethod_3or4, try_cast(VolumetricDivisor as decimal(20,5)) AS VolumetricDivisor_3or4, cast(FuelOverride as varchar(1)) AS FuelOverride_4, try_cast(Fuel as decimal(20,5)) AS FuelPercentage_4, 1 as CountFromStep4 
FROM tblRateCardHeader 
	   	    WHERE Service = ed.service  and ValidFrom <= ed.Shipdate
			ORDER BY Cast(ValidFrom AS date) DESC
) tblRateCardHeader

outer apply
(		
		 SELECT Top 1 try_cast(MinimumCharge as decimal(20,5)) AS MinimumCharge_5or6,try_cast(BasicCharge as decimal(20,5)) AS BasicCharge_5or6,cast(FuelOverride as varchar(1)) AS FuelOverride_5 ,try_cast(FuelPercentage as decimal(20,5)) AS FuelPercentage_5,try_cast(Rounding as decimal(20,5)) AS Rounding_5or6,try_cast(ChargePerKilo as decimal(20,5)) AS ChargePerKilo_5or6, 1 AS CountFromStep5
		 FROM tblCustomisedRateCardDetail 
		 WHERE Account = coalesce(tacode.TariffAccountCode,ed.acct)  
		 and Service = ed.service  
		 and OriginZone = oc.originZoneCode
		 and DestinationZone = dc.destZoneCode 
		 and EffectiveDate <= ed.Shipdate 	 
		 ORDER BY Cast(EffectiveDate AS date) DESC
) tblCustomisedRateCardDetail

outer apply
(
		SELECT Top 1 try_cast(MinimumCharge as decimal(20,5)) AS MinimumCharge_5or6,try_cast(BasicCharge as decimal(20,5)) AS BasicCharge_5or6,cast(FuelOverride as varchar(1)) AS FuelOverride_6 ,try_cast(FuelPercentage as decimal(20,5)) AS FuelPercentage_6,try_cast(Rounding as decimal(20,5)) AS Rounding_5or6,try_cast(ChargePerKilo as decimal(20,5)) AS ChargePerKilo_5or6, 1 as CountFromStep6
		FROM tblNonCustomisedRateCardDetail 
		WHERE Service = ed.service  
		and OriginZone = oc.originZoneCode  
		and DestinationZone = dc.destZoneCode   
		and EffectiveDate <= ed.Shipdate 	
		ORDER BY Cast(EffectiveDate AS date) DESC
) tblNonCustomisedRateCardDetail

outer apply
(
SELECT Top 1 cast(ValidFrom as varchar(10)) AS ValidFrom_3, cast(ChargeMethod as varchar(1)) AS ChargeMethod_3or4, try_cast(VolumetricDivisor as decimal(20,5)) AS VolumetricDivisor_3or4, cast(FuelOverride as varchar(1)) AS FuelOverride_4, try_cast(Fuel as decimal(20,5)) AS FuelPercentage_4, 1 as CountFromStep4  FROM tblRateCardHeader 
			WHERE Service = Service  and ValidFrom <= shipdate
			ORDER BY Cast(ValidFrom AS date) DESC
) tblRateCardHeader_nonacct



),tempFinalCalculatedPricingValues2 as
(
SELECT 
	
	cd_id	
	,acct	
	,cd_connote	as ConsignmentNumber
	,cd_company_id
	,TACode	
	,originZoneCode	
	,destZoneCode	
	,service
	,Shipdate
	,volume
	,[weight]   
	,itemqty
	,insurance
	,coalesce(ValidFrom_3a,ValidFrom_3b,ValidFrom_3c)									as ValidFrom_3
	,coalesce(ChargeMethod_3or4a,ChargeMethod_3or4b,ChargeMethod_3or4c)					as ChargeMethod_3or4
	,coalesce(VolumetricDivisor_3or4a,VolumetricDivisor_3or4b,VolumetricDivisor_3or4c)	as VolumetricDivisor_3or4	

	,FuelOverride_3	
	,FuelPercentage_3	
	,coalesce(FuelOverride_4,FuelOverride_4c)						as FuelOverride_4
	,coalesce(FuelPercentage_4,FuelPercentage_4c)					as FuelPercentage_4
	,isnull(coalesce(MinimumCharge_5or6a,MinimumCharge_5or6b),0.0)	as MinimumCharge_5or6
	,isnull(coalesce(BasicCharge_5or6a,BasicCharge_5or6b),0.0)		as BasicCharge_5or6
	,FuelOverride_5	
	,FuelPercentage_5	
	,coalesce(Rounding_5or6a,Rounding_5or6b)							AS Rounding_5or6
	,isnull(coalesce(ChargePerKilo_5or6a,ChargePerKilo_5or6b),0.0)		as ChargePerKilo_5or6
	,FuelOverride_6	
	,FuelPercentage_6	
	,CountFromStep3
	,(case when CountFromStep4 = 0 then CountFromStep4c else CountFromStep4 end) as CountFromStep4
	,CountFromStep5
	,CountFromStep6
	,(case when volume > [weight] then volume else [weight] end)		as [DerivedWeight]

FROM tempFinalCalculatedPricingValues1
),tempFinalCalculatedPricingValues3 as
(

SELECT 
	
	cd_id	
	,acct	
	,ConsignmentNumber	
	,cd_company_id
	,TACode	
	,originZoneCode	
	,destZoneCode	
	,service
	,Shipdate
	,volume	
	,[weight]
	,itemqty
	,insurance
	,ValidFrom_3	
	,ChargeMethod_3or4
	,cast(VolumetricDivisor_3or4 	as decimal(20,5) )	as VolumetricDivisor_3or4
	,FuelOverride_3
	,cast(FuelPercentage_3		as decimal(20,5) )		as FuelPercentage_3
	,FuelOverride_4
	,cast(FuelPercentage_4		as decimal(20,5) )		as FuelPercentage_4
	,cast(MinimumCharge_5or6		as decimal(20,5) )	as MinimumCharge_5or6
	,cast(BasicCharge_5or6		as decimal(20,5) )		as BasicCharge_5or6
	,FuelOverride_5
	,cast(FuelPercentage_5		as decimal(20,5) )		as FuelPercentage_5
	,cast(Rounding_5or6		as decimal(20,5) )			as Rounding_5or6
	,cast(ChargePerKilo_5or6		as decimal(20,5) )	as ChargePerKilo_5or6
	,FuelOverride_6
	,cast(FuelPercentage_6		as decimal(20,5) )		as FuelPercentage_6
	,CountFromStep3
	,CountFromStep4
	,CountFromStep5
	,CountFromStep6
	,(case when [DerivedWeight] % Rounding_5or6 <>0 then Round(([DerivedWeight]-([DerivedWeight] % Rounding_5or6))+Rounding_5or6,0) else [DerivedWeight] end) as [DerivedWeight] 

FROM tempFinalCalculatedPricingValues2
),tempFinalCalculatedPricingValues4 as
(
SELECT 
	temp3.*
	,(case when ChargeMethod_3or4 = 'Q' and itemqty*ChargePerKilo_5or6 + BasicCharge_5or6 + ISNULL(Insurance, 0) > MinimumCharge_5or6 then itemqty*ChargePerKilo_5or6 + BasicCharge_5or6 + ISNULL(Insurance, 0) 
		   when ChargeMethod_3or4 = 'W' and DerivedWeight*ChargePerKilo_5or6 + BasicCharge_5or6 + ISNULL(Insurance, 0) > MinimumCharge_5or6 then DerivedWeight*ChargePerKilo_5or6 + BasicCharge_5or6 + ISNULL(Insurance, 0) 
		  else MinimumCharge_5or6
	 end) TotalNetCharge
	,case when ISNULL(temp3.FuelOverride_5,'N') = 'Y' then FuelPercentage_5 else null end as FuelSurcharge_5
	,case when ISNULL(temp3.FuelOverride_5,'N') = 'N' AND ISNULL(temp3.FuelOverride_3,'N') = 'Y' then FuelPercentage_3 else null end as FuelSurcharge_3
	,case when ISNULL(temp3.FuelOverride_5,'N') = 'N' AND ISNULL(temp3.FuelOverride_3,'N') = 'N' AND ISNULL(temp3.FuelOverride_6,'N') = 'Y' then FuelPercentage_6 else null end as FuelSurcharge_6
	,case when ISNULL(temp3.FuelOverride_5,'N') = 'N' AND ISNULL(temp3.FuelOverride_3,'N') = 'N' AND ISNULL(temp3.FuelOverride_6,'N') = 'N' AND ISNULL(temp3.FuelOverride_4,'N') = 'Y' then FuelPercentage_4 else null end as FuelSurcharge_4

FROM tempFinalCalculatedPricingValues3 as temp3
),tempFinalCalculatedPricingValues5 as
(
SELECT 
	
	temp4.*
	,ISNULL(NULLIF(TotalNetCharge,0),MinimumCharge_5or6) * coalesce(FuelSurcharge_5,FuelSurcharge_3,FuelSurcharge_6,FuelSurcharge_4,0)																				as FuelSurcharge 
	,ISNULL(NULLIF(TotalNetCharge,0),MinimumCharge_5or6) + (ISNULL(NULLIF(TotalNetCharge,0),MinimumCharge_5or6) * coalesce(FuelSurcharge_5,FuelSurcharge_3,FuelSurcharge_6,FuelSurcharge_4,0))						as TotalCharge
	,Round(0.1 * (ISNULL(NULLIF(TotalNetCharge,0),MinimumCharge_5or6) + (ISNULL(NULLIF(TotalNetCharge,0),MinimumCharge_5or6) * coalesce(FuelSurcharge_5,FuelSurcharge_3,FuelSurcharge_6,FuelSurcharge_4,0))),2)		as GST

FROM tempFinalCalculatedPricingValues4 as temp4
)
select 
	cd_id										AS ConsignmentID
	,acct										AS CustomerAccountCode
	,ConsignmentNumber							AS ConsignmentNumber
	,cd_company_id								AS CompanyID
	,TACode										AS TarrifAccountCode
	,originZoneCode								AS OriginPriceZoneCode
	,destZoneCode								AS DestinationPriceZoneCode
	,service									AS PriceCode
	,Shipdate									AS ShippingDate
	,volume										AS Volume
	,weight										AS DeadWeight
	,DerivedWeight								AS DerivedWeight
	,itemqty									AS ChargebleItems
	,insurance									AS Insurance
	,ChargeMethod_3or4							AS ChargeMethod
	,TotalNetCharge								AS TotalNetCharge
	,FuelSurcharge								AS FuelSurcharge
	,TotalCharge								AS TotalChargeExGST
	,GST										AS GST
	,CAST(TotalCharge + GST as decimal(20,2))	AS FinalCharge  

from tempFinalCalculatedPricingValues5

GO
