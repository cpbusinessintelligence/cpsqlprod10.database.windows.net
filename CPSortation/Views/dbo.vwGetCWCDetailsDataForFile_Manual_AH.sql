SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO













/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vwGetCWCDetailsDataForFile_Manual_AH]

AS

 --select DwsTimeStamp,MachineLocation,MachineID,Barcodes,CubeLength,CubeWidth,CubeHeight,Weight,Volume,space(0) as spacecolumn from dbo.vwGetCWCDetailsDataForFile
 -- select [DwsTimestamp],count(*) from dbo.vwGetCWCDetailsDataForFile group by [DwsTimestamp] order by 1 
 -- select * from dbo.Incoming_CWCDetails where barcodes = '00193499680030202194'
WITH uniquebarcodedata as
(
SELECT DISTINCT barcodes,min(rawid) OVER(PARTITION BY barcodes) as rawid 
FROM  dbo.[Incoming_CWCDetails] 
WHERE  barcodes in 
  ('CPBBGRZ2437068001')
--CreateDateTime >= '2019-11-26 11:00:00.000' and CreateDateTime <= '2019-11-26 11:04:00.000' 
--barcodes not in (SELECT Barcodes FROM dbo.Incoming_CWCDetails_FTP_Log )
--and barcodes not in (select barcodes from [dbo].[Temp_ExcludeBarcodesForCWCFile]


)
SELECT TOP 1000
	   FORMAT([DwsTimestamp], 'yyyyMMddHHmmss') DwsTimeStamp
      ,(CASE WHEN sortername like 'SYD%' THEN 'Sydney' ELSE '' END) AS MachineLocation
	  ,'SC-SYD-02' AS MachineID
	  ,cwc.[Barcodes]
	  ,'LL '+LTRIM(RTRIM(CAST(CAST([CubeLength]/10.0 as decimal(5,1)) as varchar(10))))+'cm' AS [CubeLength]
	  ,'WW '+LTRIM(RTRIM(CAST(CAST([CubeWidth]/10.0 as decimal(5,1)) as varchar(10))))+'cm' AS [CubeWidth]
	  ,'HH '+LTRIM(RTRIM(CAST(CAST([CubeHeight]/10.0 as decimal(5,1)) as varchar(10))))+'cm' AS [CubeHeight]
	  ,LTRIM(RTRIM(CAST(CAST([Weight]/1000.0 as decimal(5,2)) as varchar(10))))+'kg' AS [Weight]      
	  ,'Vm '+LTRIM(RTRIM(CAST(CAST([Volume]/1000000.0 as decimal(5,2)) as varchar(10)))) AS [Volume]
      ,dwsresultid
	  ,[cd_Connote]
	  ,AccountCode
	  ,cwc.cd_id
	  ,[DwsTimestamp] AS DWSDateTime

FROM dbo.Incoming_CWCDetails cwc  JOIN uniquebarcodedata d on cwc.barcodes = d.Barcodes and cwc.RawID = d.rawid
WHERE [CubeLength] <> 0  and ([cd_Connote] like 'CP%' or cwc.Barcodes like 'CP%') 
--and FORMAT([DwsTimestamp], 'yyyyMMddHHmmss') between 20191028000001 and 20191101120000
--and FORMAT([DwsTimestamp], 'yyyyMMddHHmmss') >  20191107170000
/*
AND AccountCode in 
('112939285','113073928','112989793','112634068','113089809','112927785','112933916','112927835','112927819','112933908','112933924')
and FORMAT([DwsTimestamp], 'yyyyMMddHHmmss') between 20191021000000 and 20191025090000
*/
GO
