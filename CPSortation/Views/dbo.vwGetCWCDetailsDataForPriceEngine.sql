SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO










/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vwGetCWCDetailsDataForPriceEngine]

AS
/*
select * from [dbo].[vwGetCWCDetailsDataForPriceEngine] where cd_id = 73057484
select * from [dbo].[vwGetCWCDetailsDataForFile]
select * from dbo.[Incoming_CWCDetails] where barcodes = 'CPA358T0006515'
select * from dbo.[Incoming_CWCDetails] where  cd_id = 73057484 

select barcodes,count(*) from [dbo].[Incoming_CWCDetails] group by barcodes having count(*) > 1

*/
WITH uniquebarcodedata as
(
SELECT DISTINCT barcodes,min(rawid) OVER(PARTITION BY barcodes) as rawid 
FROM  dbo.[Incoming_CWCDetails] 
)

SELECT 

	  cwd.[cd_id]
	  ,CAST(SUM([Weight]) /1000.0 as decimal(20,5)) AS [Weight]      
	  ,CAST(SUM(volume) /1000000.0 as decimal(20,5))  AS [Volume]
	  ,COUNT(*) as [ItemQty]
FROM [dbo].[Incoming_CWCDetails] cwd
JOIN uniquebarcodedata t on cwd.RawID = t.rawid
WHERE cwd.barcodes is not null --and cd_id = 73057484 --and cwd.cd_connote = 'CPAI12C0034204'
GROUP BY cwd.[cd_id]
--and NULLIF(CubeLength,0) is not null
--and convert(varchar(10),DwsTimestamp,101) = convert(varchar(10),getdate()-3,101)
--and cd_id in (select cd_id from [dbo].[Incoming_CWCDetails] where barcodes in (SELECT [Labels] FROM [dbo].[SampleLabelsForCWC]))

GO
