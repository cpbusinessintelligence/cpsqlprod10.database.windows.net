SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW dbo.vwCWCDataForServiceCodeUpdate
as
SELECT 
		ed.cd_id
		,ed.acct
		,ed.cd_connote 
		,ed.cd_company_id
		,ed.cd_consignment_date
		,coalesce(tacode.TariffAccountCode,ed.acct)			as TACode
		,cast(oc.originZoneCode as varchar(20))				as originZoneCode
		,cast(dc.destZoneCode as varchar(20))				as destZoneCode
		,ed.service
		,ISNULL(ed.Shipdate,cast(FORMAT(getdate(),'yyyy-MM-dd') as nvarchar(10))) as Shipdate
		,ISNULL(cwc.itemqty,1)																AS itemqty
		,cwc.volume * 250.0																	AS MeasuredCubicWeight
		,cwc.volume																			AS MeasuredVolume
		,case when cwc.[Weight] < 1 and cwc.[Weight]  > 0 then 1 else cwc.[Weight]  end		AS [MeasuredWeight]	
		,ed.weight																			AS DeclaredWeight
		,ed.volume																			AS DeclaredVolume
	

from [dbo].[GetEnrichedDataForCWCPricing]  ed WITH(NOLOCK)
INNER JOIN [dbo].[vwGetCWCDetailsDataForPriceEngine] cwc on cwc.cd_id = ed.cd_id
outer apply (SELECT distinct cast(cast(TariffAccountCode as decimal(20,0)) as varchar(20)) as TariffAccountCode FROM tblTariffAccount where cast(cast(TariffAccountCode as decimal(20,0)) as varchar(20)) = ed.acct) tacode
outer apply (SELECT top(1) PrizeZone as originZoneCode FROM tblPricingZoneLookUp where PostCode = ed.originpostcode and Suburb = ed.originsuburb) oc
outer apply (SELECT top(1) PrizeZone as destZoneCode FROM tblPricingZoneLookUp where PostCode = ed.destpostcode and Suburb = ed.destsuburb) dc
GO
