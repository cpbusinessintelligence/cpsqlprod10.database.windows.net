SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW dbo.SendCrunchTableToOPS as

SELECT [crunchtableId]
	  ,cast('S1.'+FORMAT([crtd_datetime],'yyyyMMdd')+'.01' as varchar(20)) as [crunchtable_Version]
      ,[state]
      ,[postcode]
      ,[suburb]
      ,[street_name]
      ,[street_type]
      ,[house_number_from]
      ,[house_number_to]
      ,[interval_type]
      ,[building]
      ,[destination_id]
      ,[crtd_datetime]
      ,[crtd_userid]
      ,[last_upd_datetime]
      ,[last_upd_userid]
  FROM [dbo].[tblCrunchTable]

GO
