SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO














/****** Script for SelectTopNRows command from SSMS  ******/

CREATE VIEW [dbo].[vwGetCWCDetailsDataForFile_bkup_20210204_AH]

AS

 --select DwsTimeStamp,MachineLocation,MachineID,Barcodes,CubeLength,CubeWidth,CubeHeight,Weight,Volume,space(0) as spacecolumn from dbo.vwGetCWCDetailsDataForFile
 -- select [DwsTimestamp],count(*) from dbo.vwGetCWCDetailsDataForFile group by [DwsTimestamp] order by 1 
 -- select * from dbo.Incoming_CWCDetails where barcodes = '00193499680030202194'
WITH uniquebarcodedata as
(
SELECT  barcodes,rawid ,row_number() over(partition by barcodes order by rawid) row_num
FROM  dbo.[Incoming_CWCDetails] 
WHERE ([cd_Connote] like 'CP%' or Barcodes like 'CP%') and FORMAT([DwsTimestamp], 'yyyyMMddHHmmss') > 20191107170000 and [CubeLength] <> 0 
)
SELECT TOP 1000
	   FORMAT([DwsTimestamp], 'yyyyMMddHHmmss') DwsTimeStamp
      ,(CASE WHEN sortername like 'SYD%' THEN 'Sydney' ELSE '' END) AS MachineLocation
	  ,'SC-SYD-02' AS MachineID
	  ,cwc.[Barcodes]
	  ,'LL '+LTRIM(RTRIM(CAST(CAST([CubeLength]/10.0 as decimal(5,1)) as varchar(10))))+'cm' AS [CubeLength]
	  ,'WW '+LTRIM(RTRIM(CAST(CAST([CubeWidth]/10.0 as decimal(5,1)) as varchar(10))))+'cm' AS [CubeWidth]
	  ,'HH '+LTRIM(RTRIM(CAST(CAST([CubeHeight]/10.0 as decimal(5,1)) as varchar(10))))+'cm' AS [CubeHeight]
	  ,LTRIM(RTRIM(CAST(CAST([Weight]/1000.0 as decimal(5,2)) as varchar(10))))+'kg' AS [Weight]      
	  ,'Vm '+LTRIM(RTRIM(CAST(CAST([Volume]/1000000.0 as decimal(5,2)) as varchar(10)))) AS [Volume]
      ,dwsresultid
	  ,[cd_Connote]
	  ,AccountCode
	  ,cwc.cd_id
	  ,[DwsTimestamp] AS DWSDateTime

FROM dbo.Incoming_CWCDetails cwc  JOIN uniquebarcodedata d on cwc.barcodes = d.Barcodes and cwc.RawID = d.rawid
WHERE  d.row_num = 1 and cwc.issent = 0

ORDER BY cwc.RawID

/*
AND AccountCode in 
('112939285','113073928','112989793','112634068','113089809','112927785','112933916','112927835','112927819','112933908','112933924')
and FORMAT([DwsTimestamp], 'yyyyMMddHHmmss') between 20191021000000 and 20191025090000
*/
GO
