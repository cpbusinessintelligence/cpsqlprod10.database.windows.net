SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 09-JULY-2019 4:30PM AST
-- Description	: UPDATE REQUEST FRO UPDATE REVIEW SEND VIA MAIL OR VIEW tblChangeRequest TABLE 
-- =============================================

CREATE PROCEDURE [dbo].[SP_UpdateChangeRequest]
@RequestNumber NVarchar(200)='',
@RequestID   varchar(50)='',
@ChangeStatus  INT=0, 
@ReviewedBy varchar(50)=''
AS 
BEGIN
	BEGIN TRAN
		BEGIN TRY 
				Declare @CurrentAusDateTime DateTime 
		SET @CurrentAusDateTime=(Select dbo.ReturnAUSEasternStandardTime())
			 UPDATE tblChangeRequest 
			 SET ReviewedBy=@ReviewedBy,ReviewedDateTime=@CurrentAusDateTime,ChangeStatus=@ChangeStatus
			 WHERE RequestNumber=@RequestNumber AND RequestID=@RequestID
			 
			 SELECT RequestNumber,Requester,Reason,FileName 
			 FROM tblChangeRequest 
			 WHERE RequestNumber=@RequestNumber AND RequestID=@RequestID AND ChangeStatus=@ChangeStatus
			 
			 COMMIT TRAN
		END TRY 
		BEGIN CATCH
			SELECT 0
			SELECT ERROR_NUMBER(), ERROR_MESSAGE()
		
			ROLLBACK TRAN

		END CATCH
	

END
GO
