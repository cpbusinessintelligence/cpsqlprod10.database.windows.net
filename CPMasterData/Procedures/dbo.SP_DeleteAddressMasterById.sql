SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/*CPMD_DeleteAddressMasterFindById 1,@source='U' */
-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 29-MAY-2019 7:09PM IST
-- Description	: DELETE RECORDS FORM THE TABLE TBLADDRESSMASTER  
-- =============================================
CREATE PROCEDURE [dbo].[SP_DeleteAddressMasterById]
@addressmasterid INT=0,
@SOURCEIND VARCHAR(3)='ALL', 
@source varchar(1)=''

--{"addressmasterid":2,"source":"A","locality_name":"121","postcode":"211","state":"Sydney","street_name":"Test","street_type":"Test","headportsort":"111","secondarysort":"111","drivernumber":"12124"}
AS
BEGIN
    
	 BEGIN TRAN
		BEGIN TRY
		IF(@addressmasterid<>0)
			BEGIN
					-- Need to update Ind column for soft Delete 
					IF @source='A'
					BEGIN 
						UPDATE tbladdressmasteralias SET ISACTIVE=0 WHERE addressmasteraliasid =@addressmasterid			 
					END 
					ELSE IF @source='U'
					BEGIN 
						UPDATE tblUnknownAddress SET ISACTIVE=0 WHERE unknownaddressid =@addressmasterid				 
					END 
					ELSE 
					BEGIN
						SELECT @addressmasterid	
						SELECT '401','INVALID INPUT OR SOURCE NOT VALID'
					END
					  
					   SELECT @addressmasterid	
			END
			ELSE
			BEGIN
				SELECT 0; SELECT '401','INVALID INPUT OR PLEASE ENTER VALID KEY'
			END
			COMMIT TRANSACTION
		 END TRY 
	BEGIN CATCH
		   -- if error, roll back any chanegs done by any of the sql statements
		   SELECT ERROR_NUMBER(),ERROR_MESSAGE()
		   SELECT 0
           ROLLBACK TRANSACTION
	END CATCH
	
	
END
GO
