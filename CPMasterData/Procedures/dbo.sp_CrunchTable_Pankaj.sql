SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create proc sp_CrunchTable_Pankaj

-- Date created : 23-July-2019

as

begin

drop table if exists #address_dups
select [state],postcode,locality_name as suburb,street_name,street_type,
house_nbr_1 as house_number_from,house_nbr_2 as house_number_to,headportsort,secondarysort
,cast(drivernumber as int) drivernumber,headportsort+'_'+secondarysort+'_'+drivernumber as destination_Id
into #address_dups
from dbo.[tblAddressMasterOps_2] 
--where HeadPortSort = 'SYD' and SecondarySort in ('Z01', 'Z02', 'Z03', 'Z04', 'Z05','GOS', 'NTL', 'WOL' )  
--and drivernumber <> '2000' and house_nbr_1 > 0 and drivernumber <> '0000'
--and locality_name = 'SYDNEY' AND street_name = 'CASTLEREAGH' and street_type = 'ST' 
--and drivernumber in ('0058','0060','0091','')
order by house_nbr_1

--select * from #address_dups

drop table if exists #Result_1

select [state],postcode,suburb,street_name,street_type,house_number_from,destination_Id,drivernumber
,LEAD(drivernumber,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) DriverNumber_Next
,LAG(drivernumber,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) DriverNumber_Prev 
,LEAD(street_name,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) Street_Next
,LAG(street_name,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) Street_Prev 
INTO #Result_1
from #address_dups where  try_cast(house_number_from as int)%2 = 0 --and postcode = '2121'--and drivernumber in (60,91)
group by [state],postcode,suburb,street_name,street_type,destination_Id,drivernumber,house_number_from

----------------------------------------

drop table if exists #Result_2
select ROW_NUMBER() OVER (ORDER BY (SELECT 100)) AS Rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,
(case when (DriverNumber_Prev <> drivernumber and drivernumber <> DriverNumber_Next) or (DriverNumber_Prev <> drivernumber and drivernumber = DriverNumber_Next) or  DriverNumber_Prev is null or street_name <> Street_Prev then house_number_from ELSE 0 END) house_num_min,
(case when DriverNumber_Next is null or drivernumber <> DriverNumber_Next or street_name <> Street_Next then house_number_from ELSE 0 END) house_num_max,
drivernumber,DriverNumber_Prev,DriverNumber_Next
,Street_Prev,Street_Next
into #Result_2
from #result_1

----------------------------------------

drop table if exists #Result_3

select Rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_num_min,house_num_max
,LEAD(house_number_from,1,null) OVER (partition BY [state],postcode,suburb,street_name,street_type,destination_Id order by house_number_from) as house_number_to
INTO #Result_3
from #result_2 where house_num_min+house_num_max <> 0 
order by [state],postcode,suburb,street_name,house_number_from,destination_Id

--select * from #Result_3 order by rownumber
----------------------------------------

drop table if exists #Result_4

select rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from, house_num_max as house_number_to 
into #Result_4
from #Result_3 
where house_number_from = house_num_min and  house_number_from = house_num_max
order by [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from

----------------------------------------

select [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_number_to
into tblCrunchTable_Even
from #Result_3 
where rownumber not in (select rownumber from #Result_4) and house_num_max = 0 and house_num_min = house_number_from
union
select [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_number_to
from #Result_4 
order by [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from


-------------------------------------------------Odd Number Logic


---------------------------------------
drop table if exists #Result_1_Odd

select [state],postcode,suburb,street_name,street_type,house_number_from,destination_Id,drivernumber
,LEAD(drivernumber,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) DriverNumber_Next
,LAG(drivernumber,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) DriverNumber_Prev 
,LEAD(street_name,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) Street_Next
,LAG(street_name,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) Street_Prev 
INTO #Result_1_Odd
from #address_dups where  try_cast(house_number_from as int)%2 <> 0 --and postcode = '2121'--and drivernumber in (60,91)
group by [state],postcode,suburb,street_name,street_type,destination_Id,drivernumber,house_number_from

-----------------------------------------

drop table if exists #Result_2_Odd

select ROW_NUMBER() OVER (ORDER BY (SELECT 100)) AS Rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,
(case when (DriverNumber_Prev <> drivernumber and drivernumber <> DriverNumber_Next) or (DriverNumber_Prev <> drivernumber and drivernumber = DriverNumber_Next) or  DriverNumber_Prev is null or street_name <> Street_Prev then house_number_from ELSE 0 END) house_num_min,
(case when DriverNumber_Next is null or drivernumber <> DriverNumber_Next or street_name <> Street_Next then house_number_from ELSE 0 END) house_num_max,
drivernumber,DriverNumber_Prev,DriverNumber_Next
,Street_Prev,Street_Next
into #Result_2_Odd
from #Result_1_Odd

------------------------------------------------

drop table if exists #Result_3_Odd

select Rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_num_min,house_num_max
,LEAD(house_number_from,1,null) OVER (partition BY [state],postcode,suburb,street_name,street_type,destination_Id 
order by house_number_from) as house_number_to
INTO #Result_3_Odd
from #result_2_odd where house_num_min+house_num_max <> 0 
order by [state],postcode,suburb,street_name,house_number_from,destination_Id

--select * from #Result_3_Odd order by rownumber
---------------------------------------------------

drop table if exists #Result_4_Odd

select rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from, house_num_max as house_number_to 
into #Result_4_Odd
from #Result_3_Odd
where house_number_from = house_num_min and  house_number_from = house_num_max
order by [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from

--------------------------------------------------------------------------------------
--Final Output Query

select [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_number_to
into tblCrunchTable_Odd
from #Result_3_Odd 
where rownumber not in (select rownumber from #Result_4_Odd) and house_num_max = 0 and house_num_min = house_number_from

UNION

select [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_number_to
from #Result_4_Odd 
order by [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from

End
GO
