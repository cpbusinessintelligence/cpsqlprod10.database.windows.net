SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SPCPIT_CreateRequestLog]
@IPaddress nvarchar(50)=null,
@RequestURL nvarchar(100)=null,
@Request nvarchar(max)=null,
@Function varchar(50)=null,
@Application varchar(50)=null,
@UTCDateTime varchar(50)=null,
@LocalTime varchar(50)=null,
@CreatedBy nvarchar(100)=null,
@ClientCode nvarchar(50)=null,
@ServiceCode nvarchar(50)=null

As
Begin

INSERT INTO [dbo].[tblRequestLog]
           ([IPaddress],[RequestURL],[Request],[Function],[Application],[UTCDateTime],[LocalTime],[CreatedBy],[ClientCode],[ServiceCode])
     VALUES
           (@IPaddress,@RequestURL,@Request,@Function,@Application,@UTCDateTime,@LocalTime,@CreatedBy,@ClientCode,@ServiceCode)
Select SCOPE_IDENTITY () as RequestID

End
GO
