SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 17-JULY-2019 7:57 PM AST
-- Description	: CHANGE STATUS SUBMITED TO APPROVER APPROVE 
-- =============================================

CREATE PROCEDURE [dbo].[SP_UpdateStatusChangeRequest]
@RequestID int=0,
@ChangeStatus int=2 , -- if status 1-Lodged, 2-submitted , 3-approved, 4-Reject
@UpdatedBy Nvarchar(50)='',
@ComponentName varchar(100)='BulkUpdateSubmitRequestForApproval',
@ReviewerComment varchar(400)=''

AS
BEGIN
	BEGIN TRAN
	BEGIN TRY 		
		-- update change request status 
		Declare @datetime datetime
		--set @datetime=(select  cast(getdate() +replace(format (cast(getdate() as datetime) AT TIME ZONE 'AUS Eastern Standard Time' ,'zzz'),'+','') as datetime) AT TIME ZONE 'AUS Eastern Standard Time')
		set @datetime=(select  DBO.ReturnAUSEasternStandardTime())
		IF @ChangeStatus=2
		UPDATE tblChangeRequest  SET ChangeStatus=@ChangeStatus,UpdatedDateTime=@datetime,UpdatedBy=@UpdatedBy WHERE RequestID=@RequestID
		ELSE 
		UPDATE tblChangeRequest  SET ChangeStatus=@ChangeStatus,ReviewedDateTime=@datetime,ReviewedBy=@UpdatedBy,UpdatedBy=@UpdatedBy,UpdatedDateTime=@datetime,ReviewerComment=@ReviewerComment WHERE RequestID=@RequestID

		--UPDATE tblAddressMasterCopyOpsStaging set UpdatedDateTime=getdate(),UpdatedBy=@UpdatedBy WHERE RequestID=@RequestID
		-- Resend Request Id For approval link request
		SELECT Reason,RequestNumber,ReviewerComment,ReviewedBy,ReviewedDateTime,Requester,RequesterEmail From  tblChangeRequest where RequestID=@RequestID
		-- mail send to approver and requester data 
		Select EmailType,Subject  ,body ,MDMComponent  from tblEmailTemplate  Where  MDMComponent= @ComponentName
		COMMIT TRANSACTION
	END TRY 
	BEGIN CATCH
		   -- if error, roll back any chanegs done by any of the sql statements
		   SELECT ERROR_NUMBER(),ERROR_MESSAGE()		   
           ROLLBACK TRANSACTION
	END CATCH

END

/*
eXEC [CPMD_DeleteBulkAddressMasterCopyOpsStaging] 120,'120160,120161,120162'
*/
GO
