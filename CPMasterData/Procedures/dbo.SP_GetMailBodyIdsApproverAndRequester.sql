SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SP_GetMailBodyIdsApproverAndRequester]
@Requester varchar(50)='MDMRequester',
@Approver Varchar(50)='MDMApprover'
AS
BEGIN 
;with a as (
		Select Subject AS MDMApproverSubject,body AS MDMApproverBody ,'' AS MDMRequesterSubject,'' AS MDMRequesterBody from tblEmailTemplate  Where EmailType=@Approver
		)
		Select a.MDMApproverSubject,a.MDMApproverBody, Subject AS MDMRequesterSubject,body AS MDMRequesterBody from tblEmailTemplate,a Where EmailType=@Requester
END
GO
