SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		:      RAJKUMAR SAHU
-- Create Date	:	   03-JUNE-2019 3:24PM IST
-- Description	:	   WILL RETURN ALL AVAILABLE STREET TYPE FROM THE TABLE ADDRESS MASTER
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetStreet_Type]
 
AS
BEGIN
   --SET NOCOUNT ON
   SELECT STREET_TYPE 
   FROM  tblstreetType     
    
END


--CREATE NONCLUSTERED INDEX IX_tblAddressMaster_STREET_TYPE ON tblAddressMaster(STREET_TYPE ASC);

--create view View_StreetType
--AS

--Create table tblstreetType(StreetId int primary key identity(1,1),Street_Type Varchar(6))
--insert into tblstreetType(Street_Type )

--SELECT DISTINCT  STREET_TYPE 
--   FROM  tblAddressMaster 
--   WHERE ISNULL (STREET_TYPE,'')<>''
GO
