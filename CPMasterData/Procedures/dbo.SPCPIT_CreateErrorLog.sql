SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].SPCPIT_CreateErrorLog
@ErrorCode varchar(50)=null,
@ErrorType varchar(50)=null,
@Error varchar(max)=null,
@Function varchar(200)=null,
@Application varchar(200) = null,
@Request varchar(max)=null,
@Response varchar(max)=null,
@URL varchar(max)=null,
@AffectedUser varchar(50)=null,
@UTCDateTime Nvarchar(50)=null,
@LocalTime datetime=null,
@CreatedBy int=null,
@ClientCode varchar(10) = null,
@ServiceCode varchar(50) = null

As
Begin

Declare @datetime datetime
		set @datetime=(select  cast(getdate() +replace(format (cast(getdate() as datetime) AT TIME ZONE 'AUS Eastern Standard Time' ,'zzz'),'+','') as datetime) AT TIME ZONE 'AUS Eastern Standard Time')
	
INSERT INTO [dbo].[tblErrorLog]
           ([ErrorCode],[ErrorType],[Error],[Function],[Application],[Request],[Response],[URL],[AffectedUser],[UTCDateTime],[LocalTime],[CreatedBy],[ClientCode],[ServiceCode])
     VALUES
           (@ErrorCode,@ErrorType,@Error,@Function,@Application,@Request,@Response,@URL,@AffectedUser,@datetime,@LocalTime,@CreatedBy,@ClientCode,@ServiceCode)
Select SCOPE_IDENTITY () as ID

End
GO
