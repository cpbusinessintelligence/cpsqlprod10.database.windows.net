SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
CREATE PROCEDURE [dbo].[sp_CrunchTable]

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

    -- Insert statements for procedure here
    
truncate table tblCrunchTable

drop table if exists #tblCrunchTable_Temp
drop table if exists #tblCrunchTable
drop table if exists #tblAddressMasterOps
drop table if exists #temp4
drop table if exists #temp5
drop table if exists #temp6

CREATE TABLE #tblAddressMasterOps(
	[addressmasterid] [int] NULL,
	[house_nbr_1] [int] NULL,
	[street_name] [varchar](30) NULL,
	[street_type] [varchar](4) NULL,
	[locality_name] [varchar](46) NULL,
	[postcode] [varchar](12) NULL,
	[state] [varchar](3) NULL,
	[headportsort] [varchar](10) NULL,
	[secondarysort] [varchar](10) NULL,
	[drivernumber] [varchar](10) NULL,
	[IsProcessed] int)

insert into #tblAddressMasterOps
select a.[addressmasterid], a.HOUSE_NBR_1, a.STREET_NAME,a.STREET_TYPE,a.LOCALITY_NAME,a.POSTCODE,a.state, a.HeadPortSort,a.SecondarySort, a.drivernumber, 0
from tblAddressMasterOps_2 a
--join tblAddressMasterCopyOps c on  a.addressmasterid = c.addressmasterid  
where a.drivernumber <> '0000'


select distinct secondarysort into #tempSecondarySort 
from [tblAddressMasterOps_2] nolock where headportsort in ('SYD','CBR') 
and secondarysort <> 'CTY'

CREATE TABLE [#tblCrunchTable_Temp] (
    [tableCount] INTEGER  NOT NULL,
    [state] VARCHAR(3),
    [postcode] VARCHAR(12),
    [suburb] VARCHAR(40),
    [street_name] VARCHAR(40),
    [street_type] VARCHAR(4),
    [house_number_from] INTEGER,
    [house_number_to] INTEGER,
    [interval_type] VARCHAR(5),
    [building] VARCHAR(20),
    [HeadPortSort] varchar(3),
	[SecondarySort] varchar(3),
	[Driver] varchar(4),
	[destination_id] VARCHAR(15),
	[IsProcessed] integer,
    [crtd_datetime] DATE,
    [crtd_userid] VARCHAR(40),
    [last_upd_datetime] DATE,
    [last_upd_userid] VARCHAR(40)
)


--Entire Postcode serviced by one driver
--Insert into [#tblCrunchTable_Temp] (
--[tableCount],
--    [state] ,
--    [postcode],
--    [suburb],
--    [street_name],
--    [street_type],
--    [house_number_from],
--    [house_number_to],
--    [interval_type],
--    [building],
--	[HeadPortSort] ,
--	[SecondarySort],
--	[Driver] ,
--    [destination_id],
--	[IsProcessed],
--    [crtd_datetime],
--    [crtd_userid],
--    [last_upd_datetime],
--    [last_upd_userid]
--)
--select count(distinct POSTCODE), --DELIVY_POINT_GROUP_ID, 
--state, POSTCODE, null , null, null, null, null, null, null, m.HeadPortSort,  m.SecondarySort, m.drivernumber ,  m.HeadPortSort+ '_' + m.SecondarySort+'_'+ m.drivernumber as DestinationId,1 , getdate(), SYSTEM_USER,getdate(), SYSTEM_USER
--from tblAddressMasterOps_2 m
--where m.HeadPortSort = 'SYD' and m.SecondarySort in ('Z01', 'Z02', 'Z03', 'Z04', 'Z05','GOS', 'NTL', 'WOL' ) --and locality_name = 'DAWES POINT'
--group by --DELIVY_POINT_GROUP_ID,
--state, POSTCODE, m.HeadPortSort, m.SecondarySort, m.drivernumber 
--having count(distinct POSTCODE) =1 


--UPDATE #tblAddressMasterOps_2 
--SET IsProcessed = 1 
--FROM (
--    select  --DELIVY_POINT_GROUP_ID, 
--state, POSTCODE,  m.HeadPortSort,  m.SecondarySort, m.drivernumber
--from #tblAddressMasterOps_2 m
--where m.HeadPortSort = 'SYD' and m.SecondarySort in ('Z01', 'Z02', 'Z03', 'Z04', 'Z05','GOS', 'NTL', 'WOL' ) --and locality_name = 'DAWES POINT'
--group by --DELIVY_POINT_GROUP_ID,
--state, POSTCODE, m.HeadPortSort, m.SecondarySort, m.drivernumber 
--having count(distinct POSTCODE) =1) o	
--WHERE 
--   o.state = #tblAddressMasterOps_2.state 
--   and o.postcode = #tblAddressMasterOps_2.postcode 
--   and o.headportsort = #tblAddressMasterOps_2.headportsort 
--   and o.drivernumber = #tblAddressMasterOps_2.drivernumber
--   and  #tblAddressMasterOps_2.HeadPortSort = 'SYD' and #tblAddressMasterOps_2.SecondarySort in ('Z01', 'Z02', 'Z03', 'Z04', 'Z05','GOS', 'NTL', 'WOL' ) 

--select * from #tblAddressMasterOps_2 where headportsort = 'SYD' and secondarysort = 'z02' and drivernumber = '0051'

--Entire Postcode and Suburb serviced by one driver
Insert into [#tblCrunchTable_Temp] (
[tableCount],
    [state] ,
    [postcode],
    [suburb],
    [street_name],
    [street_type],
    [house_number_from],
    [house_number_to],
    [interval_type],
    [building],
	[HeadPortSort] ,
	[SecondarySort],
	[Driver] ,
    [destination_id],
	[IsProcessed],
    [crtd_datetime],
    [crtd_userid],
    [last_upd_datetime],
    [last_upd_userid]
)
--select sum(distinct B.Destination_Id), --DELIVY_POINT_GROUP_ID, 
--state, POSTCODE, LOCALITY_NAME, B.HeadPortSort,  B.SecondarySort, B.drivernumber , B.HeadPortSort+ '_' + B.SecondarySort+'_'+ B.drivernumber as DestinationId,
--from (
select count(distinct A.DestinationId) as Destination_Id, --DELIVY_POINT_GROUP_ID, 
state, POSTCODE, LOCALITY_NAME, null, null, null, null, null, null, A.HeadPortSort,  A.SecondarySort, A.drivernumber , A.HeadPortSort+ A.SecondarySort+ A.drivernumber as DestinationId,1 , getdate(), SYSTEM_USER,getdate(), SYSTEM_USER
from (
select count(*) as Incount, --DELIVY_POINT_GROUP_ID, 
state, POSTCODE, LOCALITY_NAME,  m.HeadPortSort,  m.SecondarySort, m.drivernumber ,  m.HeadPortSort+ m.SecondarySort+ m.drivernumber as DestinationId
from #tblAddressMasterOps m
where m.HeadPortSort in ('SYD', 'CBR') and m.SecondarySort in (select secondarysort from #tempSecondarySort) -- in ('Z01', 'Z02', 'Z03', 'Z04', 'Z05','GOS', 'NTL', 'WOL', 'CFS' ) --and locality_name = 'lindfield'
group by 
state, POSTCODE,LOCALITY_NAME, m.HeadPortSort, m.SecondarySort, m.drivernumber) A
group by --
state, POSTCODE,LOCALITY_NAME, A.HeadPortSort, A.SecondarySort, A.drivernumber
 having count(distinct A.DestinationId) =1
-- )B 
-- group by --
--state, POSTCODE,LOCALITY_NAME, B.HeadPortSort, B.SecondarySort, B.drivernumber
-- having sum(distinct B.Destination_Id) =1


--Entire Postcode, Suburb and Street serviced by one driver
Insert into [#tblCrunchTable_Temp] (
[tableCount],
    [state] ,
    [postcode],
    [suburb],
    [street_name],
    [street_type],
    [house_number_from],
    [house_number_to],
    [interval_type],
    [building],
	[HeadPortSort] ,
	[SecondarySort],
	[Driver] ,
    [destination_id],
	[IsProcessed],
    [crtd_datetime],
    [crtd_userid],
    [last_upd_datetime],
    [last_upd_userid]
)
select count(distinct A.DestinationId), A.state, A.POSTCODE, A.LOCALITY_NAME,A.street_name, A.street_type,A.house_number_from , A.house_number_to , A.interval_type, A. Building , A.HeadPortSort,   A.SecondarySort,A.drivernumber , A.HeadPortSort+  A.SecondarySort+ A.drivernumber as DestinationId,1 , getdate(), SYSTEM_USER,getdate(), SYSTEM_USER
from (
	select distinct count(*) as Incount, --DELIVY_POINT_GROUP_ID, 
	m.state, m.POSTCODE, m.LOCALITY_NAME,m.street_name, m.street_type,null as house_number_from, null as house_number_to, null as interval_type, null as Building, m.HeadPortSort,   m.SecondarySort,drivernumber , m.HeadPortSort+  m.SecondarySort+ m.drivernumber as DestinationId
	from #tblAddressMasterOps m
	--join tblAddressMasterCopyOps c on  m.addressmasterid = c.addressmasterid 
	left outer join #tblCrunchTable_Temp t on t.state = m.state and t.postcode = m.postcode and t.suburb = m.locality_name 
	and t.street_name = m.street_name and t.street_type = m.street_type 
	where m.HeadPortSort in ('SYD', 'CBR') and m.SecondarySort in (select secondarysort from #tempSecondarySort) --in ('Z01', 'Z02', 'Z03', 'Z04', 'Z05','GOS', 'NTL', 'WOL', 'CFS' ) 
	--and m.locality_name = 'PORT BOTANY' --and m.street_name = 'ACRES'
	--and m.locality_name = 'PARRAMATTA' and m.street_name = 'BETTS'
	--and (m.Postcode is not null and m.state is not null)
	group by --DELIVY_POINT_GROUP_ID,
	m.state, m.POSTCODE,m.LOCALITY_NAME, m.street_name, m.street_type,m.HeadPortSort, m.SecondarySort, m.drivernumber 
	--having count(*) = 1
	)A
group by --DELIVY_POINT_GROUP_ID,
	A.state, A.POSTCODE, A.LOCALITY_NAME,A.street_name, A.street_type,A.HeadPortSort, A.SecondarySort, A.drivernumber,A.house_number_from , A.house_number_to , A.interval_type, A. Building  
having count(A.DestinationId) = 1



--Multiple Drivers servicing a street - Split the house number into Odd and Even lines


-----------------------------------------------------------------------------------------------------------------------
-----------------------Even Lines---------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------

drop table if exists ##address_dups
select o.[state],o.postcode,locality_name as suburb,o.street_name,o.street_type,house_nbr_1 as house_number_from,house_nbr_2 as house_number_to,o.headportsort,o.secondarysort
,cast(drivernumber as int) drivernumber,
o.headportsort+o.secondarysort+drivernumber as destination_Id
into ##address_dups
from dbo.[tblAddressMasterOps_2] o
--left outer join #tblCrunchTable_Temp t on t.state = o.state and t.suburb = o.locality_name and t.postcode = o.postcode and t.street_name = o.street_name and t.street_type = o.street_type 
where o.HeadPortSort in ('SYD', 'CBR') and o.SecondarySort in (select secondarysort from #tempSecondarySort)--in ('Z01', 'Z02', 'Z03', 'Z04', 'Z05','GOS', 'NTL', 'WOL' , 'CFS')  
and drivernumber <> '2000' and house_nbr_1 > 0 and drivernumber <> '0000'
--and (o.locality_name   is null or o.postcode is null or o.street_name is null or o.street_type is null)
--and locality_name = 'DAWES POINT' --AND street_name = 'CASTLEREAGH' and street_type = 'ST' 
--and ((o.postcode not in (select distinct postcode from #tblCrunchTable_Temp)) 
--or (o.locality_name not in (select distinct suburb from #tblCrunchTable_Temp))
--or (o.street_name not in (select distinct street_name from #tblCrunchTable_Temp))
--or (o.street_type not in (select distinct street_type from #tblCrunchTable_Temp)))
--and drivernumber in ('0058','0060','0091','') 
order by house_nbr_1

--select * from ##address_dups

drop table if exists ##Result_1

select [state],postcode,suburb,street_name,street_type,house_number_from,destination_Id,drivernumber
,LEAD(drivernumber,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) DriverNumber_Next
,LAG(drivernumber,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) DriverNumber_Prev 
,LEAD(street_name,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) Street_Next
,LAG(street_name,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) Street_Prev 
INTO ##Result_1
from ##address_dups where  try_cast(house_number_from as int)%2 = 0 --and postcode = '2121'--and drivernumber in (60,91)
group by [state],postcode,suburb,street_name,street_type,destination_Id,drivernumber,house_number_from

----------------------------------------

drop table if exists ##Result_2
select ROW_NUMBER() OVER (ORDER BY (SELECT 100)) AS Rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,
(case when (DriverNumber_Prev <> drivernumber and drivernumber <> DriverNumber_Next) or (DriverNumber_Prev <> drivernumber and drivernumber = DriverNumber_Next) or  DriverNumber_Prev is null or street_name <> Street_Prev then house_number_from ELSE 0 END) house_num_min,
(case when DriverNumber_Next is null or drivernumber <> DriverNumber_Next or street_name <> Street_Next then house_number_from ELSE 0 END) house_num_max,
drivernumber,DriverNumber_Prev,DriverNumber_Next
,Street_Prev,Street_Next
into ##Result_2
from ##result_1

----------------------------------------

drop table if exists ##Result_3

select Rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_num_min,house_num_max
,LEAD(house_number_from,1,null) OVER (partition BY [state],postcode,suburb,street_name,street_type,destination_Id order by house_number_from) as house_number_to
INTO ##Result_3
from ##result_2 where house_num_min+house_num_max <> 0 
order by [state],postcode,suburb,street_name,house_number_from,destination_Id

--select * from ##Result_3 order by rownumber
----------------------------------------

drop table if exists ##Result_4


select rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from, house_num_max as house_number_to 
into ##Result_4
from ##Result_3 
where house_number_from = house_num_min and  house_number_from = house_num_max
order by [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from

----------------------------------------

drop table if exists ##Result_5


select [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_number_to into ##Result_5
from (
	select [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_number_to
	from ##Result_3 
	where rownumber not in (select rownumber from ##Result_4) and house_num_max = 0 and house_num_min = house_number_from
	union
	select [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_number_to
	from ##Result_4 
	
)A order by [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from


insert into tblCrunchTable 
(
[state] ,
    [postcode] ,
    [suburb] ,
    [street_name],
    [street_type] ,
    [house_number_from],
    [house_number_to] ,
    [interval_type] ,
    [building] ,
    [destination_id] ,
    [crtd_datetime] ,

    [crtd_userid] ,
    [last_upd_datetime],
    [last_upd_userid] 
)
select state,POSTCODE, suburb, STREET_NAME, STREET_TYPE, house_number_from, house_number_to,'EVEN', null,   destination_id, getdate(), SYSTEM_USER,getdate(), SYSTEM_USER  
from ##Result_5


drop table if exists ##Result_5

-----------------------------------------------------------------------------------------------------------------------
-----------------------Odd Lines---------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
drop table if exists ##address_dups_odd

select [state],postcode,locality_name as suburb,street_name,street_type,house_nbr_1 as house_number_from,house_nbr_2 as house_number_to,headportsort,secondarysort
,cast(drivernumber as int) drivernumber,
headportsort+secondarysort+drivernumber as destination_Id
into ##address_dups_odd
from dbo.[tblAddressMasterOps_2] 
where HeadPortSort in ('SYD', 'CBR') and SecondarySort in (select secondarysort from #tempSecondarySort) --in ('Z01', 'Z02', 'Z03', 'Z04', 'Z05','GOS', 'NTL', 'WOL', 'CFS' )  
and drivernumber <> '2000' and house_nbr_1 > 0 and drivernumber <> '0000'
--and locality_name = 'SYDNEY' AND street_name = 'CASTLEREAGH' and street_type = 'ST' 
--and drivernumber in ('0058','0060','0091','')
--and ((postcode not in (select distinct postcode from #tblCrunchTable_Temp)) 
--or (locality_name not in (select distinct suburb from #tblCrunchTable_Temp))
--or (street_name not in (select distinct street_name from #tblCrunchTable_Temp))
--or (street_type not in (select distinct street_type from #tblCrunchTable_Temp)))

order by house_nbr_1

--select * from ##address_dups


---------------------------------------
drop table if exists ##Result_1_odd

select [state],postcode,suburb,street_name,street_type,house_number_from,destination_Id,drivernumber
,LEAD(drivernumber,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) DriverNumber_Next
,LAG(drivernumber,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) DriverNumber_Prev 
,LEAD(street_name,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) Street_Next
,LAG(street_name,1,null) OVER (ORDER BY [state],postcode,suburb,street_name,house_number_from) Street_Prev 
INTO ##Result_1_odd
from ##address_dups_odd where  try_cast(house_number_from as int)%2 <> 0 --and postcode = '2121'--and drivernumber in (60,91)
group by [state],postcode,suburb,street_name,street_type,destination_Id,drivernumber,house_number_from

-----------------------------------------

drop table if exists ##Result_2_odd

select ROW_NUMBER() OVER (ORDER BY (SELECT 100)) AS Rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,
(case when (DriverNumber_Prev <> drivernumber and drivernumber <> DriverNumber_Next) or (DriverNumber_Prev <> drivernumber and drivernumber = DriverNumber_Next) or  DriverNumber_Prev is null or street_name <> Street_Prev then house_number_from ELSE 0 END) house_num_min,
(case when DriverNumber_Next is null or drivernumber <> DriverNumber_Next or street_name <> Street_Next then house_number_from ELSE 0 END) house_num_max,
drivernumber,DriverNumber_Prev,DriverNumber_Next
,Street_Prev,Street_Next
into ##Result_2_odd
from ##Result_1_odd

------------------------------------------------

drop table if exists ##Result_3_odd

select Rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_num_min,house_num_max
,LEAD(house_number_from,1,null) OVER (partition BY [state],postcode,suburb,street_name,street_type,destination_Id order by house_number_from) as house_number_to
INTO ##Result_3_odd
from ##Result_2_odd where house_num_min+house_num_max <> 0 
order by [state],postcode,suburb,street_name,house_number_from,destination_Id

--select * from ##Result_3 order by rownumber
---------------------------------------------------

drop table if exists ##Result_4_odd

drop table if exists ##Result_5_odd

select rownumber,[state],postcode,suburb,street_name,street_type,destination_Id,house_number_from, house_num_max as house_number_to 
into ##Result_4_odd
from ##Result_3_odd
where house_number_from = house_num_min and  house_number_from = house_num_max
order by [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from

select [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_number_to into ##Result_5_odd from

(
	select [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_number_to
	from ##Result_3_odd 
	where rownumber not in (select rownumber from ##Result_4) and house_num_max = 0 and house_num_min = house_number_from

	UNION

	select [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from,house_number_to
	from ##Result_4_odd
	
)A order by [state],postcode,suburb,street_name,street_type,destination_Id,house_number_from
 
 
insert into tblCrunchTable 
(
[state] ,
    [postcode] ,
    [suburb] ,
    [street_name],
    [street_type] ,
    [house_number_from],
    [house_number_to] ,
    [interval_type] ,
    [building] ,
    [destination_id] ,
    [crtd_datetime] ,

    [crtd_userid] ,
    [last_upd_datetime],
    [last_upd_userid] 
)
select state,POSTCODE, suburb, STREET_NAME, STREET_TYPE, house_number_from, house_number_to,'ODD', null,   destination_id, getdate(), SYSTEM_USER,getdate(), SYSTEM_USER  
from ##Result_5_odd


insert into tblCrunchTable 
(
[state] ,
    [postcode] ,
    [suburb] ,
    [street_name],
    [street_type] ,
    [house_number_from],
    [house_number_to] ,
    [interval_type] ,
    [building] ,
    [destination_id] ,
    [crtd_datetime] ,

    [crtd_userid] ,
    [last_upd_datetime],
    [last_upd_userid] 
)
select state,POSTCODE, suburb, STREET_NAME, STREET_TYPE, house_number_from, house_number_to,Interval_type, null,   destination_id, getdate(), SYSTEM_USER,getdate(), SYSTEM_USER  
from #tblCrunchTable_Temp

--select * from  #tblCrunchTable_Temp


--select  distinct Postcode into #temp3 from MasterData_AddressMaster


--select * from #temp2

--select * FROM [dbo].[MasterData_Location_Ops] where Postcode_type = 'GAZETTED' 

--Add all the interstate postcodes from Location Master
DROP INDEX IF EXISTS [MasterData_Location_Ops].idx_Postcode_suburb
DROP INDEX  IF EXISTS tblCrunchTable.idx_Postcode_suburb1
CREATE INDEX idx_Postcode_suburb
ON [MasterData_Location_Ops] (postcode,suburb);

CREATE INDEX idx_Postcode_suburb1
ON tblCrunchTable (postcode,suburb);


SELECT distinct a.Postcode
      ,a.Suburb
      ,a.[State]
      ,CASE WHEN a.[HeadPortSort] = 'XXX' THEN '000' ELSE a.[HeadPortSort] END as [HeadPortSort]
      ,CASE WHEN a.[SecondarySort] = 'XXX' THEN '000' ELSE a.[SecondarySort] END as [SecondarySort]
	  ,CONVERT(VARCHAR(4),'0000') AS DriverID
INTO #Temp4
FROM [dbo].[MasterData_Location_Ops] AS a
where Postcode_type = 'GAZETTED' --and Suburb = 'BUNDEENA'
--and a.Suburb = 'PORT BOTANY'
and a.Postcode+A.suburb not in (select distinct postcode+suburb from tblCrunchTable) 



--select * from #temp4



insert into tblCrunchTable 
select  state,Postcode, Suburb, null, null, null, null,null, null, [HeadPortSort]+  SecondarySort+ driverid as destination_id, getdate(), SYSTEM_USER,getdate(), SYSTEM_USER  
from #temp4

--select * from #tblCrunchTable_Temp where suburb =  'SPRINGWOOD' --and street_name = 'ELIZABETH'
--select * from tblCrunchTable where suburb = 'SPRINGWOOD' --and street_name = 'ELIZABETH'


---Removing records from tblCrunchtable where postcode and suburb is serviced by 1 driver
drop table if exists #temp5
select count(distinct A.destination_id) as destdistinct, A.state, A.postcode, A.suburb
into #temp5 
from (
select count(*) as count1, state,postcode,suburb
--, street_name, street_type 
, destination_id  from #tblCrunchTable_Temp 
--where suburb = 'ABERDARE'
--and street_type is not null and street_name is not null 
group by  state,postcode,suburb, destination_id
 --,street_name, street_type 
 --having count(*) = 1
 ) A
group by  A.state, A.postcode, A.suburb 
having count(distinct A.destination_id) = 1

--select * from #tblCrunchTable_temp
--select * from tblCrunchTable where suburb = 'DAWES POINT' 

DELETE t
--select * 
from tblCrunchTable t join #temp5 c on c.postcode = t.postcode and c.state = t.state and c.suburb = t.suburb  
where street_name is not null and street_type is not null --and house_number_from is not null and house_number_to is not null
--and 
--c.suburb = 'ABERDARE'

---Removing records from tblCrunchTable where postcode, suburb and street is serviced by 1 driver

drop table if exists #temp6

	select count(distinct A.destination_id) as destdistinct, b.crunchtableId , A.state, A.postcode, A.suburb , A.street_name, A.street_type 
	into #temp6
	from (
	select count(*) as count1, 
	t.state,t.postcode,t.suburb
	, c.street_name, c.street_type 
	, t.destination_id  from #tblCrunchTable_Temp t
	join tblCrunchTable c on c.state = t.state and c.postcode = t.postcode and t.suburb = c.suburb 
	and t.street_name = c.street_name and t.street_type = c.street_type 
	where --t.suburb = 'ARTARMON' and t.street_name = 'CLEG' --and
	c.street_name is not null --and  c.street_type is not null
	and c.house_number_from is not null
	group by  t.state,t.postcode,t.suburb, t.destination_id 
	, c.street_name, c.street_type
	--having count(*) >= 1 
	) A 
	join tblCrunchTable b on b.State = A.State and b.postcode = A.postcode and b.suburb = A.suburb 
	and b.street_name = A.street_name and b.street_type = A.street_type
	where --t.suburb = 'MOSMAN' --and t.street_name = 'CRIMSON' --and
	b.street_name is not null --and  c.street_type is not null
	and b.house_number_from is not null
	group by b.crunchtableId,  A.state, A.postcode, A.suburb ,A.street_name, A.street_type 
	having count(distinct A.destination_id) = 1


DELETE t
--select *
from tblCrunchTable t join #temp6 c on c.crunchtableid = t.crunchtableId and c.postcode = t.postcode and c.state = t.state and c.suburb = t.suburb and t.street_name = c.street_name and t.street_type = c.street_type  



drop table if exists #tempr
drop table if exists #tempdelete

select distinct t.state, t.suburb, t.postcode 
into #tempr 
from tblCrunchTable t
where t.suburb  in (select distinct suburb from [#tblCrunchTable_Temp]  )
--or t.postcode  in (select distinct postcode from [#tblCrunchTable_Temp])) 
--and state = 'NSW' and t.suburb = 'LINDFIELD'
and t.house_number_from is not null and t.house_number_to is not null 
and t.interval_type is not null 



select crunchtableid, postcode , suburb  into #tempdelete from tblCrunchTable t  where 
postcode in (select distinct postcode from #tempr) 
and suburb in (select distinct suburb from #tempr)
and  house_number_from is  null and house_number_to is  null 
and interval_type is  null and street_name is null and street_type is null

delete from tblCrunchTable where crunchtableId in (select crunchtableid from #tempdelete)

drop table if exists #tempr1
drop table if exists #tempdelete1

select distinct t.state, t.suburb, t.postcode, t.street_name, t.street_type 
into #tempr1 
from tblCrunchTable t
where t.suburb  in (select distinct suburb from tblCrunchTable  )
--or t.postcode  in (select distinct postcode from [#tblCrunchTable_Temp])) 
--and state = 'NSW' and t.suburb = 'SYDNEY' and street_name = 'ELIZABETH'
and t.house_number_from is not null and t.house_number_to is not null 
and t.interval_type is not null 


select crunchtableid, t.postcode , t.suburb , t.street_name, t.street_type
into #tempdelete1 
from tblCrunchTable t  
join #tempr1 r on r.postcode = t.postcode and r.suburb = t.suburb and r.street_name = t.street_name and r.street_type = t.street_type 
where 
--postcode in (select distinct postcode from #tempr1) 
--and suburb in (select distinct suburb from #tempr1)
--and  
house_number_from is  null and house_number_to is  null 
and interval_type is  null --and street_name is null and street_type is null 

END
GO
