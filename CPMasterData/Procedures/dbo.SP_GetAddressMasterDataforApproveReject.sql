SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 23-JULY-2019 7:57 PM AST
-- Description	: Get Address Master data For Reject and approved review change request 
-- =============================================

CREATE PROCEDURE [dbo].[SP_GetAddressMasterDataforApproveReject]
@RequestID int=0, 
@ReviewedBy  Nvarchar(50)=''
AS
BEGIN	 
		DECLARE @Status INT=0, @RequestNumber NVARCHAR(200)=''

		SELECT @Status=ChangeStatus,@RequestNumber=RequestNumber FROM tblChangeRequest WHERE RequestID=@RequestID
		
		IF @Status=2
		BEGIN 
			EXEC [dbo].[SP_GetBulkAddressMasterUploadDataCompare] @RequestID ,@Status,0
		END
		ELSE 
 		BEGIN
			SELECT @Status ChangeStatus,CASE WHEN @Status=3 THEN 'Change request already approved.' WHEN @Status=4 THEN 'Change request already rejected.' WHEN @Status=1 THEN 'Change request lodged' ELSE 'Change request not found.' END as [Message], @RequestNumber  AS RequestNumber
		END
END
GO
