SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 25-JUNE-2019 5:27PM IST
-- Description	: BULK INSERT DATA IN ADDRESSMASTER TABLES USING JSON STRING 
-- =============================================
CREATE PROCEDURE [dbo].[SP_BulkInsertAddressMaster]
@JSONSTRING NVARCHAR(MAX)=''
AS
BEGIN
    SET NOCOUNT ON
	SET XACT_ABORT ON;
	 BEGIN TRAN
		BEGIN TRY
		--Create table tempTest(jsonstring nvarchar(max),CreatedDate datetime default getdate())
		 --	insert into tempTest(jsonstring) values (@jsonstring)
			--Declare @TempAddressmaster table   
			--(	 
			--  house_nbr_1 int NULL,house_nbr_sfx_1 int NULL,house_nbr_2 int NULL,house_nbr_sfx_2 int NULL,flat_unit_type varchar(7) NULL,
			--  flat_unit_type_full varchar(40) NULL,flat_unit_nbr varchar(7) NULL,floor_level_type varchar(2) NULL,floor_level_type_full varchar(40) NULL,floor_level_nbr varchar(5) NULL,
			--  lot_nbr varchar(6) NULL,postal_delivery_nbr int NULL,postal_delivery_nbr_pfx varchar(3) NULL,postal_delivery_nbr_sfx varchar(3) NULL,primary_point_ind varchar(1) NULL,
			--  delivy_point_group_id int NULL,postal_delivery_type varchar(11) NULL,street_name varchar(30) NULL,street_type varchar(4) NULL,street_sfx varchar(2) NULL,
			--  street_type_full varchar(40) NULL,delivy_point_group_did int NULL,locality_id int NULL,locality_name varchar(46) NULL,postcode varchar(12) NULL,state varchar(3) NULL,
			--  locality_did int NULL,full_address varchar(100) NULL,address varchar(100) NULL
			--)
		
			--INSERT INTO @TempAddressmaster 
			INSERT INTO tblUnknownAddress_Test
			(
			house_nbr_1,house_nbr_sfx_1,house_nbr_2,house_nbr_sfx_2,flat_unit_type,flat_unit_type_full,flat_unit_nbr,floor_level_type,floor_level_type_full,
			floor_level_nbr,lot_nbr,postal_delivery_nbr,postal_delivery_nbr_pfx,postal_delivery_nbr_sfx,primary_point_ind,delivy_point_group_id,postal_delivery_type,
			street_name,street_type,street_sfx,street_type_full,delivy_point_group_did,locality_id,locality_name,postcode,state,locality_did,full_address,address
			)
			SELECT house_nbr_1,house_nbr_sfx_1,house_nbr_2,house_nbr_sfx_2,flat_unit_type,flat_unit_type_full,flat_unit_nbr,floor_level_type,floor_level_type_full,
			floor_level_nbr,lot_nbr,postal_delivery_nbr,postal_delivery_nbr_pfx,postal_delivery_nbr_sfx,primary_point_ind,delivy_point_group_id,postal_delivery_type,
			street_name,street_type,street_sfx,street_type_full,delivy_point_group_did,locality_id,locality_name,postcode,state,locality_did,full_address,address
			FROM  OPENJSON(@JSONSTRING,'$.LstAddress')
			WITH(
			house_nbr_1 int '$.house_nbr_1',house_nbr_sfx_1 int '$.house_nbr_sfx_1',house_nbr_2 int '$.house_nbr_2',house_nbr_sfx_2 int '$.house_nbr_sfx_2',
			flat_unit_type varchar(7) '$.flat_unit_type',flat_unit_type_full varchar(40) '$.flat_unit_type_full',flat_unit_nbr varchar(7) '$.flat_unit_nbr',floor_level_type varchar(2) '$.floor_level_type',
			floor_level_type_full varchar(40) '$.floor_level_type_full',floor_level_nbr varchar(5) '$.floor_level_nbr',lot_nbr varchar(6) '$.lot_nbr',postal_delivery_nbr int '$.postal_delivery_nbr',
			postal_delivery_nbr_pfx varchar(3) '$.postal_delivery_nbr_pfx',postal_delivery_nbr_sfx varchar(3) '$.postal_delivery_nbr_sfx',primary_point_ind varchar(1) '$.primary_point_ind',
			delivy_point_group_id int '$.delivy_point_group_id',postal_delivery_type varchar(11) '$.postal_delivery_type',street_name varchar(30) '$.street_name',street_type varchar(4) '$.street_type',
			street_sfx varchar(2) '$.street_sfx',street_type_full varchar(40) '$.street_type_full',delivy_point_group_did int '$.delivy_point_group_did',locality_id int '$.locality_id',locality_name varchar(46) '$.locality_name',
			postcode varchar(12) '$.postcode',state varchar(3) '$.state',locality_did int '$.locality_did',full_address varchar(100) '$.full_address',address varchar(100) '$.address'
			  
			)   
 	 
			--- NOW WE CAN INSERT ANY FILTERD DATA FROM @TempAddressmaster 
			
			--INSERT INTO tblUnknownAddress_Test(
			--house_nbr_1,house_nbr_sfx_1,house_nbr_2,house_nbr_sfx_2,flat_unit_type,flat_unit_type_full,flat_unit_nbr,floor_level_type,
			--floor_level_type_full,floor_level_nbr,lot_nbr,postal_delivery_nbr,postal_delivery_nbr_pfx,postal_delivery_nbr_sfx,primary_point_ind,delivy_point_group_id,
			--postal_delivery_type,street_name,street_type,street_sfx,street_type_full,delivy_point_group_did,locality_id,locality_name,postcode,state,locality_did,full_address,
			--address,crtd_datetime,crtd_userid,last_upd_datetime,last_upd_userid
			--)			
			--SELECT house_nbr_1,house_nbr_sfx_1,house_nbr_2,house_nbr_sfx_2,flat_unit_type,flat_unit_type_full,flat_unit_nbr,floor_level_type,
			--floor_level_type_full,floor_level_nbr,lot_nbr,postal_delivery_nbr,postal_delivery_nbr_pfx,postal_delivery_nbr_sfx,primary_point_ind,delivy_point_group_id,
			--postal_delivery_type,street_name,street_type,street_sfx,street_type_full,delivy_point_group_did,locality_id,locality_name,postcode,state,locality_did,full_address,
			--address,GETDATE(),'-1',GETDATE(),'-1' FROM @TempAddressmaster
		   -- if not error, commit the transcation
		   SELECT @@IDENTITY 
           COMMIT TRANSACTION
		   
	END TRY 
	BEGIN CATCH
		   -- if error, roll back any chanegs done by any of the sql statements
		   --	insert into tempTest(jsonstring) values (ERROR_MESSAGE())
		   SELECT ERROR_NUMBER(),ERROR_MESSAGE()
		   SELECT 0
           ROLLBACK TRANSACTION
	END CATCH
	
END
GO
