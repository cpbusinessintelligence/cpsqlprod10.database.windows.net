SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 9-JULY-2019 3:51 PM AST
-- Description	: GET COMPARED DATA FROM  tblAddressMasterCopyOps AND tblAddressMasterCopyOpsStaging  FILTER BY RequestID
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetBulkAddressMasterUploadDataCompare]
@RequestID int=0,
@ChangeStatus int=1,
@IsDeleted tinyInt=0
AS
BEGIN

SELECT  AMCOS.addressmasterid AS UniqueID, AMCOS.RequestID as changeRequestID, AMCOS.delivery_point_id AS deliveyPointId,
		AMCOS.headportsort AS headPortSort_New,ISNULL(AMCO.headportsort,'NEW') AS  headPortSort_Old,
        AMCOS.secondarysort AS secondarySort_New,ISNULL(AMCO.secondarysort,'NEW') AS  secondarySort_Old,
		AMCOS.drivernumber AS driverNumber_New ,ISNULL(AMCO.drivernumber,'NEW') AS  driverNumber_Old,
		CR.ChangeStatus,AMCOS.IsDeleted as isDeleted,EffectiveDate  as effectiveDate,RequestNumber
		FROM tblAddressMasterCopyOpsStaging AS AMCOS 
		LEFT JOIN [dbo].[tblAddressMasterCopyOps] AS AMCO ON AMCOS.delivery_point_id=AMCO.delivy_point_id
		INNER JOIN tblChangeRequest CR On CR.RequestID=AMCOS.RequestID
		WHERE AMCOS.IsDeleted=@IsDeleted AND AMCOS.RequestID=@RequestID AND ChangeStatus=@ChangeStatus  
		ORDER BY AMCO.delivy_point_id,AMCOS.delivery_point_id
END

/*
select RequestID, a. EffectiveDate from tblChangeRequest a where a.RequestID=120
Exec [SP_GetBulkAddressMasterUploadDataCompare] 120
*/
GO
