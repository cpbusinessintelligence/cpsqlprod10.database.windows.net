SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


--exec [dbo].[UPDATE_tblConsignmentDestination]


CREATE PROC [dbo].[UPDATE_tblConsignmentDestination] AS

BEGIN

--Step 1 : Get the consignment addresses from consignment table.
-- Truncate table [dbo].[tblConsignmentDestination]
--  select * from [dbo].[tblConsignmentDestination]
-- select cd_id,cd_delivery_addr0,cd_delivery_addr1,cd_delivery_addr2,cd_delivery_addr3,cd_delivery_suburb,cd_delivery_postcode FROM [dbo].[Consignment] 
/*
DROP TABLE IF EXISTS tmp_ConsignmentAddress

SELECT cd_id,cd_delivery_addr0,cd_delivery_addr1,cd_delivery_addr2,cd_delivery_addr3,cd_delivery_suburb,RIGHT('0'+ltrim(rtrim(str(cd_delivery_postcode))),4) AS cd_delivery_postcode
INTO tmp_Consignmentaddress
FROM [dbo].[consignment]  --WHERE cd_date = '20181126'
--WHERE cd_id not in (SELECT cd_id from [dbo].[tblConsignmentDestination] where active = 1 or destination_id in ( 'UNKNOWN-CODE','UNKNOWN-SUBURB'))

--Step 2 : Create necessary indexes on tmp_ConsignmentAddress

CREATE CLUSTERED INDEX pk_Consignmentaddress_cdid ON tmp_ConsignmentAddress(cd_id)
CREATE NONCLUSTERED INDEX idx_Consignmentaddress_postcode ON tmp_ConsignmentAddress(cd_delivery_postcode)
CREATE NONCLUSTERED INDEX idx_Consignmentaddress_suburb ON tmp_ConsignmentAddress(cd_delivery_suburb)
*/
--Step 3 : Insert unique destination ids using matching postcodes.

DROP TABLE IF EXISTS #destidbyuniquecodes

;WITH destidbyuniquecodes AS
(
SELECT postcode,destination_id,count(*) OVER (PARTITION BY postcode) AS rcount  
FROM dbo.tblCrunchTable where street_name is null 
)
SELECT postcode,destination_id
INTO #destidbyuniquecodes
FROM destidbyuniquecodes 
WHERE rcount = 1

CREATE NONCLUSTERED INDEX idx_destidbyuniquecodes_postcode ON #destidbyuniquecodes(postcode)

TRUNCATE TABLE [dbo].[tblConsignmentDestination]

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT c.cd_id,d.destination_id,'Unique Destination ID By Postcode' AS Comments,1 AS Active
FROM tmp_ConsignmentAddress c 
JOIN #destidbyuniquecodes d ON c.cd_delivery_postcode = d.postcode

-- select * from [dbo].[tblConsignmentDestination] where active = 1
-- select * from #destidbyuniquecodes where postcode = 3134

--Step 4 : Insert unique destination ids using matching suburbs.

DROP TABLE IF EXISTS #destidbyuniquesuburbs

;WITH destidbyuniquesuburbs AS
(
SELECT postcode,suburb,destination_id,count(*) OVER (PARTITION BY postcode,suburb) AS rcount  
FROM dbo.tblCrunchTable WHERE  postcode not in (SELECT postcode from #destidbyuniquecodes) and street_name is null
)
SELECT postcode,suburb,destination_id 
INTO #destidbyuniquesuburbs 
FROM destidbyuniquesuburbs WHERE rcount = 1

CREATE NONCLUSTERED INDEX idx_destidbyuniquesuburbs_postcode ON #destidbyuniquecodes(postcode)


INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT c.cd_id,d.destination_id,'Unique Destination ID By Suburb' AS Comments,1 AS Active
FROM tmp_ConsignmentAddress c 
JOIN #destidbyuniquesuburbs d ON 
(LTRIM(RTRIM(c.cd_delivery_suburb)) = LTRIM(RTRIM(d.suburb)) or c.cd_delivery_suburb like '%'+LTRIM(RTRIM(d.suburb))+'%' or d.suburb like '%'+LTRIM(RTRIM(c.cd_delivery_suburb))+'%' or soundex(replace(c.cd_delivery_suburb,' ','')) = soundex(replace(d.suburb,' ','')))
and c.cd_delivery_postcode = d.postcode
WHERE c.cd_id not in (SELECT cd_id FROM [dbo].[tblConsignmentDestination])


--Step 4.1 : Insert unique destination ids using matching suburbs.

DROP TABLE IF EXISTS #destidbyuniquestreets

;WITH destidbyuniquestreets AS
(
SELECT postcode,suburb,street_name,destination_id,count(*) OVER (PARTITION BY postcode,suburb,street_name) AS rcount  
FROM dbo.tblCrunchTable WHERE  postcode not in (SELECT postcode from #destidbyuniquecodes) 
and suburb not in (SELECT suburb from #destidbyuniquesuburbs)
)
SELECT postcode,suburb,destination_id 
INTO #destidbyuniquestreets 
FROM destidbyuniquestreets WHERE rcount = 1

CREATE NONCLUSTERED INDEX idx_destidbyuniquestreets_postcode ON #destidbyuniquestreets(postcode)


-- select count(*) from [dbo].[tblConsignmentDestination] where active = 1 and cd_id in (61775252)
--select * from [dbo].[tblConsignmentDestination]
--10,166,564
-- select * from #destidbyuniquesuburbs where suburb like '%RINGWOOD%'
-- select * from dbo.tblCrunchTable where suburb = 'RINGWOOD'
--SELECT * FROM tmp_ConsignmentAddress WHERE CD_ID in (61775252)

-- Step 5 : Insert unknown destination id where addresses are completely invalid

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT cd_id,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM tmp_ConsignmentAddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination] WHERE active = 1) 
AND cd_delivery_addr3 <> '' and ltrim(rtrim(cd_delivery_addr3)) <> ltrim(rtrim(cd_delivery_suburb)) 
and ltrim(rtrim(cd_delivery_addr3)) NOT like '%[0-9]%'
AND cd_delivery_addr2 <> '' and ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb)) and ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_addr3))
AND ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%[0-9]%'
AND (cd_delivery_addr1 <> '' and ltrim(rtrim(cd_delivery_addr1)) NOT LIKE '%[0-9]%')

--select * from [dbo].[tblConsignmentDestination] where active = 0
-- select * from dbo.tblcrunchtable where postcode = 2560 and suburb like '%APPIN%'
--SELECT * FROM tmp_ConsignmentAddress WHERE CD_ID = 55647018

-- Step 6 : Check for Address Line 3 for valid house no and street address

DROP TABLE IF EXISTS #validAddress3

SELECT 
	cd_id
	,ltrim(rtrim(cd_delivery_addr3)) AS cd_delivery_addr3
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr3,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr3,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr3,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress3
FROM tmp_ConsignmentAddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination] WHERE ACTIVE = 1 or destination_id like 'UNKNOWN%') 
AND cd_delivery_addr3 <> '' 
--AND ltrim(rtrim(cd_delivery_addr3)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr3)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr3,cd_delivery_postcode,''))) like '%[0-9]%'
AND right(ltrim(rtrim(cd_delivery_addr3)),1) not like '%[0-9]%'

--select * from #validAddress3

DROP TABLE IF Exists tmp_validAddress3

SELECT IDENTITY(INT,1,1) AS RawID,* 
INTO tmp_validAddress3  FROM #validAddress3

CREATE CLUSTERED INDEX pk_tmp_validAddress3_rawid ON tmp_validAddress3(rawid)

DECLARE @startrawid int = 1,
		@batchcount int	= 10000
DECLARE @TotalRows int

SET @TotalRows = (select count(*) from tmp_validAddress3)

WHILE @startrawid <= @TotalRows

BEGIN

;WITH vaddr3_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM tmp_validAddress3 WHERE rawid >= @startrawid and rawid < @startrawid+@batchcount 
)
INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 3' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr3_results --WHERE destination_id = 'NONUNIQUE'

SET @startrawid = @startrawid+@batchcount
PRINT @startrawid
--IF @startrawid > 3000000 break

END


-- select * from [dbo].[tblConsignmentDestination] where active = 0 order by cd_id

-- Step 7 : Check for Address Line 2 for valid house no and street address


DROP TABLE IF EXISTS ##validAddress2

SELECT 
	cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO ##validAddress2
FROM tmp_ConsignmentAddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination] WHERE destination_id like 'UNKNOWN%' OR Active = 1) 
AND cd_delivery_addr2 <> '' 
--AND ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,''))) like '%[0-9]%'
AND right(ltrim(rtrim(cd_delivery_addr2)),1) not like '%[0-9]%'

--SELECT * FROM #validAddress2 order by 5

DROP TABLE IF Exists tmp_validAddress2

SELECT IDENTITY(INT,1,1) AS RawID,* 
INTO tmp_validAddress2  FROM ##validAddress2

CREATE CLUSTERED INDEX pk_tmp_validAddress2_rawid ON tmp_validAddress2(rawid)


SET @startrawid  = 1
SET	@batchcount  = 10000

SET @TotalRows = (select count(*) from tmp_validAddress2)

WHILE @startrawid <= @TotalRows

BEGIN

;WITH vaddr2_results AS
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM tmp_validAddress2 WHERE rawid >= @startrawid and rawid < @startrawid+@batchcount 
)
INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 2' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr2_results 

SET @startrawid = @startrawid+@batchcount
PRINT @startrawid
--IF @startrawid > 3000000 break

END

-- Step 8 : Check for Address Line 1 for valid house no and street address

DROP TABLE IF EXISTS #validAddress1

SELECT 
	IDENTITY(INT,1,1) AS RawID
	,cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,cd_delivery_addr1
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress1
FROM tmp_ConsignmentAddress where CD_ID NOT IN (SELECT cd_id from [dbo].[tblConsignmentDestination] WHERE destination_id <> 'NONUNIQUE' or active = 1) 
AND cd_delivery_addr1 <> '' 
--AND ltrim(rtrim(cd_delivery_addr1)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr1)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) LIKE '%[0-9]%'
AND RIGHT(ltrim(rtrim(cd_delivery_addr1)),1) NOT LIKE '%[0-9]%'

--SELECT count(*) FROM #validAddress1 --order by 6
--DROP TABLE IF Exists tmp_validAddress1

--SELECT IDENTITY(INT,1,1) AS RawID,* 
--INTO tmp_validAddress1  FROM #validAddress1


CREATE CLUSTERED INDEX pk_tmp_validAddress1_rawid ON #validAddress1(rawid)


SET @startrawid  = 1
SET	@batchcount = 10000
SET @TotalRows = (select count(*) from #validAddress1)

WHILE @startrawid <= @TotalRows

BEGIN

;WITH vaddr1_results as
(
SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
FROM #validAddress1 where rawid >= @startrawid and rawid < @startrawid+@batchcount  --where cd_id not in (select cd_id from [dbo].[tblConsignmentDestination])
)
INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 1' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM vaddr1_results 


SET @startrawid = @startrawid+@batchcount
PRINT @startrawid
--IF @startrawid > 3000000 break

END

-- Step 9 : Check for Address Line 1 + 2 combined for valid house no and street address

DROP TABLE IF EXISTS #validAddress1_2

SELECT 
	cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,cd_delivery_addr1
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress1_2
FROM tmp_ConsignmentAddress where CD_ID NOT IN (SELECT cd_id from [dbo].[tblConsignmentDestination] WHERE destination_id <> 'NONUNIQUE' or active = 1) 
AND cd_delivery_addr1 <> '' 
--AND ltrim(rtrim(cd_delivery_addr1))+' '+ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr1))+' '+ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))+' '+ltrim(rtrim(replace(cd_delivery_addr2,cd_delivery_postcode,''))) LIKE '%[0-9]%'
--AND RIGHT(ltrim(rtrim(cd_delivery_addr1)),1) NOT LIKE '%[0-9]%'

--SELECT * FROM #validAddress1_2 order by 6

DROP TABLE IF EXISTS #vaddr1_results1_2

SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
INTO #vaddr1_results1_2
FROM #validAddress1_2

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 1' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM #vaddr1_results1_2  where CD_ID NOT IN (SELECT cd_id from [dbo].[tblConsignmentDestination]) 

-- Real bad addresses

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT cd_id,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM tmp_ConsignmentAddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination]) 
AND (cd_delivery_addr1 = '' OR ltrim(rtrim(cd_delivery_addr1)) NOT LIKE '%[0-9]%')
AND (cd_delivery_addr2 = '' OR ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%[0-9]%')
AND (cd_delivery_addr3 = '' OR ltrim(rtrim(cd_delivery_addr3)) NOT LIKE '%[0-9]%')

-- Bad addresses having postcode in address line

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT DISTINCT cd_id,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM tmp_ConsignmentAddress where CD_ID NOT IN (SELECT Cd_id from [dbo].[tblConsignmentDestination]) 
AND (cd_delivery_addr1 = '' OR ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) NOT LIKE '%[0-9]%')
AND (cd_delivery_addr2 = '' OR ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) NOT LIKE '%[0-9]%')
AND (cd_delivery_addr3 = '' OR ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,''))) NOT LIKE '%[0-9]%')

-------------------------------------

DROP TABLE IF EXISTS #validAddress1_Ext

SELECT 
	cd_id
	,cd_delivery_addr1
	,cd_delivery_addr2
	,cd_delivery_addr3
	,[dbo].[getStreetName](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(replace(cd_delivery_addr1,cd_delivery_postcode,'')))) as house_number_to
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode
INTO #validAddress1_Ext
FROM tmp_ConsignmentAddress where CD_ID IN (SELECT Cd_id from [dbo].[tblConsignmentDestination] WHERE destination_id like 'UNKNOWN%' and destination_id <> 'UNKNOWN-ADDR' and active = 0) 

DROP TABLE IF EXISTS #vaddr1_ext_results

SELECT cd_id,[dbo].[getDestinationID](ltrim(rtrim(postcode)),ltrim(rtrim(suburb)),ltrim(rtrim(street_name)),ltrim(rtrim(house_number_from)),ltrim(rtrim(house_number_to))) destination_id 
INTO #vaddr1_ext_results
FROM #validAddress1_Ext --WHERE cd_id NOT IN (SELECT cd_id FROM [dbo].[tblConsignmentDestination] where active =1)

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)
SELECT cd_id,destination_id,'Matching Valid Address Line 1' AS Comments,(CASE WHEN destination_id <> 'NONUNIQUE' AND destination_id NOT LIKE '%UNKNOWN%' THEN 1 ELSE 0 END) AS Active 
FROM #vaddr1_ext_results WHERE destination_id not like 'UNKNOWN%'

/*

SELECT count(*) FROM [dbo].[tblConsignmentDestination] WHERE destination_id = 'NONUNIQUE'
SELECT * FROM [dbo].[tblConsignmentDestination] WHERE destination_id like 'UNKNOWN%' and destination_id <> 'UNKNOWN-ADDR' and active = 0
SELECT * FROM [dbo].[tblConsignmentDestination] WHERE destination_id like 'UNKNOWN%'
--SELECT count(*) FROM [dbo].[tblConsignmentDestination]

INSERT INTO [dbo].[tblConsignmentDestination](cd_id,destination_id,Comments,Active)

SELECT DISTINCT *,'UNKNOWN-ADDR' AS destination_id,'Invalid Addresses' AS Comments,0 AS Active
FROM tmp_ConsignmentAddress where CD_ID IN (SELECT Cd_id from [dbo].[tblConsignmentDestination] WHERE destination_id like 'UNKNOWN%' and destination_id <> 'UNKNOWN-ADDR') 


AND cd_delivery_addr3 <> '' and ltrim(rtrim(cd_delivery_addr3)) <> ltrim(rtrim(cd_delivery_suburb)) 
and ltrim(rtrim(cd_delivery_addr3)) NOT like '%[0-9]%'
AND cd_delivery_addr2 <> '' and ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb)) and ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_addr3))
AND ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%[0-9]%'

--select * from [dbo].[tblConsignmentDestination] where cd_id in (61832165)
SELECT 
	cd.destination_id
	,cd.Comments
	,ca.*

FROM tmp_ConsignmentAddress ca JOIN [dbo].[tblConsignmentDestination] cd ON ca.cd_id = cd.cd_id
WHERE cd.active = 0 and ca.cd_id not in (select cd_id from [dbo].[tblConsignmentDestination] where active = 1)
and cd.destination_id like 'NONUNIQUE%'
order by comments,destination_id



SELECT 
	cd.destination_id
	,cd.Comments
	,ca.*
	,[dbo].[getStreetName](ltrim(rtrim(cd_delivery_addr2))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(cd_delivery_addr2))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(cd_delivery_addr2))) as house_number_to

FROM tmp_ConsignmentAddress ca JOIN [dbo].[tblConsignmentDestination] cd ON ca.cd_id = cd.cd_id
WHERE cd.destination_id like 'UNKNOWN%' AND Comments like '%Address line 2%'
order by comments,destination_id

SELECT 
	cd.destination_id
	,ca.*
	--,[dbo].[getStreetName](ltrim(rtrim(cd_delivery_addr1))) as street_name
	--,[dbo].[getHouseNumberStart](ltrim(rtrim(cd_delivery_addr1))) as house_number_from
	--,[dbo].[getHouseNumberEnd](ltrim(rtrim(cd_delivery_addr1))) as house_number_to
	,cd.Comments
FROM tmp_ConsignmentAddress ca JOIN [dbo].[tblConsignmentDestination] cd ON ca.cd_id = cd.cd_id
WHERE cd.destination_id like 'UNKNOWN%' AND Comments like '%Address line 1%'
order by comments,destination_id


SELECT 
	cd.destination_id
	,cd.Comments
	,ca.*

FROM tmp_ConsignmentAddress ca JOIN [dbo].[tblConsignmentDestination] cd ON ca.cd_id = cd.cd_id
WHERE cd.destination_id like 'UNKNOWN%' AND Comments like '%Invalid%'
order by comments,destination_id


SELECT cd.destination_id
	,cd.Comments
	,ca.* 
	,[dbo].[getStreetName](ltrim(rtrim(cd_delivery_addr1))) as street_name
	,[dbo].[getHouseNumberStart](ltrim(rtrim(cd_delivery_addr1))) as house_number_from
	,[dbo].[getHouseNumberEnd](ltrim(rtrim(cd_delivery_addr1))) as house_number_to

FROM tmp_ConsignmentAddress ca JOIN [dbo].[tblConsignmentDestination] cd ON ca.cd_id = cd.cd_id
WHERE cd.destination_id like 'UNKNOWN%' AND Comments like '%Address line 1%'--AND ca.cd_id in (61832165)
order by comments,destination_id


select count(*) from tmp_ConsignmentAddress where cd_id not in (select cd_id from [dbo].[tblConsignmentDestination] WHERE destination_id <> 'NONUNIQUE' or active = 1)
and cd_Id = 61784055

select * from #validAddress2 where cd_id = 61705051

select * from [dbo].[tblCrunchTable] where postcode in (2113) and suburb like '%Macquarie%' and street_name like '%Durham%' 

and interval_type = 'EVEN'

select * from [dbo].[tblAddressMaster] where postcode in (2000)   and locality_name like '%parliament%'  and street_name like '%MANN%'

--8 Evan Street



--Validation Queries
-----------------------------------

SELECT DISTINCT postcode,suburb,street_name,street_type,house_number_from,house_number_to,interval_type,destination_id
FROM [dbo].[tblCrunchTable]

SELECT comments,destination_id,count(*) FROM [dbo].[tblConsignmentDestination] 
WHERE destination_id like 'UNKNOWN%' 
GROUP BY comments,destination_id
ORDER BY comments,destination_id

SELECT * FROM tmp_ConsignmentAddress where cd_id in (SELECT cd_id FROM [dbo].[tblConsignmentDestination] WHERE destination_id = 'UNKNOWN' and comments = 'Matching Valid Address Line 2')


DROP TABLE IF EXISTS #tempcrunchtable

SELECT DISTINCT postcode,suburb,street_name,street_type,house_number_from,house_number_to,interval_type,destination_id
INTO #tempcrunchtable
FROM [dbo].[tblCrunchTable] WHERE postcode in (SELECT postcode FROM #validAddress2)

select * from #tempcrunchtable

select * from #validAddress2 where cd_id in (61778136)

select * from [dbo].[tblCrunchTable] where postcode in (870) and suburb = 'Araluen' and street_name like '%George%'



SELECT 
	cd_id
	,cd_delivery_addr3
	,cd_delivery_addr2
	,[dbo].[getStreetName](ltrim(rtrim(cd_delivery_addr2))) as street_name
	,[dbo].[getHouseNumber](ltrim(rtrim(cd_delivery_addr2))) as house_number
	,cd_delivery_suburb AS suburb
	,cd_delivery_postcode AS postcode

FROM tmp_ConsignmentAddress where CD_ID = 61778136
AND cd_delivery_addr2 <> '' 
AND ltrim(rtrim(cd_delivery_addr2)) NOT LIKE '%'+ltrim(rtrim(cd_delivery_postcode))+'%' 
AND ltrim(rtrim(cd_delivery_addr2)) <> ltrim(rtrim(cd_delivery_suburb))
AND ltrim(rtrim(cd_delivery_addr2)) like '%[0-9]%'
AND right(ltrim(rtrim(cd_delivery_addr2)),1) not like '%[0-9]%'


SELECT DISTINCT house_number_from,house_number_to,interval_type,destination_id FROM [dbo].[tblCrunchTable] 
WHERE postcode = 2000 
		AND (ltrim(rtrim(suburb)) = 'SYDNEY' OR 'SYDNEY' like '%'+ltrim(rtrim(suburb))+'%')
		AND (ltrim(rtrim(street_name)) = 'GEORGE STREET' or 'GEORGE STREET' like '%'+ltrim(rtrim(street_name))+'%')
		AND 225 >= house_number_from
		AND 225 <= house_number_to
		AND case when cast(225 as int)%2 <> 0 then 'ODD' ELSE 'EVEN' END = interval_type

*/

END
GO
