SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		:      RAJKUMAR SAHU
-- Create Date	:	   29-MAY-2019
-- Description	:	   THIS PROCEDURE WILL RETURN ALL DATA FROM GIVEN CONDITION FOR ALL UNION DATA WILL WE RETURN 
-- =============================================
CREATE PROCEDURE [dbo].[SP_GetAddressMaster]
@SOURCEIND VARCHAR(3)='ALL',
@FilterCondition Nvarchar(max)=''
AS
BEGIN
DECLARE @QUERY NVARCHAR(MAX)=''
	-----------------------------------------------------------CONDITION PART ----------------------------------------------------------
	---------------------------------------------------------ALL ADDRESS TABLE ---------------------------------------------------------
	
	IF @SOURCEIND='ALL'
	BEGIN 
    SET NOCOUNT ON
	
	
	-- ADDRESSCOPY
	    SET @QUERY='SELECT top 4000 AMC. addressmasterid AS addressmasterID,'
		SET @QUERY=@QUERY+'ISNULL(AMC.delivy_point_id,0)delivy_point_id,'
		SET @QUERY=@QUERY+'ISNULL(house_nbr_1,0)house_nbr_1,ISNULL(house_nbr_sfx_1,0)house_nbr_sfx_1,ISNULL(house_nbr_2,0)house_nbr_2,ISNULL(house_nbr_sfx_2,'''')house_nbr_sfx_2,ISNULL(flat_unit_type,'''')flat_unit_type,ISNULL(flat_unit_nbr,'''')flat_unit_nbr,'
		--SET @QUERY=@QUERY+'ISNULL(flat_unit_type_full,'''')flat_unit_type_full,ISNULL(floor_level_type,'''')floor_level_type,'
		--SET @QUERY=@QUERY+'ISNULL(floor_level_type_full,'''')floor_level_type_full,ISNULL(floor_level_nbr,'''')floor_level_nbr,'
		--SET @QUERY=@QUERY+'ISNULL(lot_nbr,'''')lot_nbr,ISNULL(postal_delivery_nbr,0)postal_delivery_nbr,ISNULL(postal_delivery_nbr_pfx,'''')postal_delivery_nbr_pfx,'
		--SET @QUERY=@QUERY+'ISNULL(postal_delivery_nbr_sfx,'''')postal_delivery_nbr_sfx,ISNULL(primary_point_ind,'''')primary_point_ind,ISNULL(delivy_point_group_id,0)delivy_point_group_id,'
		--SET @QUERY=@QUERY+'ISNULL(postal_delivery_type,'''')postal_delivery_type,'
		SET @QUERY=@QUERY+'ISNULL(street_name,'''')street_name,ISNULL(street_type,'''')street_type,'
		--SET @QUERY=@QUERY+'ISNULL(street_sfx,'''')street_sfx,ISNULL(street_type_full,'''')street_type_full,ISNULL(delivy_point_group_did,0)delivy_point_group_did,ISNULL(locality_id,0)locality_id,'
		SET @QUERY=@QUERY+'ISNULL(locality_name,'''')locality_name,ISNULL(postcode,'''')postcode,ISNULL(state,'''')state,'
		--SET @QUERY=@QUERY+'ISNULL(locality_did,0)locality_did,'
		SET @QUERY=@QUERY+'iif(isnull(full_address,'''')<>'''',full_address,iif(isnull(Flat_Unit_Nbr,'''')<>'''', cast(Flat_Unit_Nbr as varchar) +''/'','''')+ Cast(House_Nbr_1 as varchar) +''-''+ iif(isnull(House_nbr_2,'''')<>'''', Cast(House_nbr_2 as varchar)+'','','''')+ street_name+'' ''+ street_type +'',''+ Locality_name+'',''+ Postcode+'', ''+ State)full_address,ISNULL(address,'''')address,'
		--SET @QUERY=@QUERY+'ISNULL(confidence_level,0)confidence_level,ISNULL(feature,'''')feature,ISNULL(latitude,0)latitude,ISNULL(longitude,0)longitude,ISNULL(AMC.crtd_datetime,GETDATE())crtd_datetime,ISNULL(AMC.crtd_userid,'''')crtd_userid,'
		--SET @QUERY=@QUERY+'ISNULL(AMC.last_upd_datetime,GETDATE())last_upd_datetime,ISNULL(AMC. last_upd_userid,'''')last_upd_userid,'
		SET @QUERY=@QUERY+'ISNULL(headportsort,'''')headportsort,ISNULL(secondarysort,'''')secondarysort,ISNULL(drivernumber,'''')drivernumber,'
		SET @QUERY=@QUERY+'''G'' SOURCE,isnull(locality_name,'''')+'' ''+isnull(state,'''')+'' ''+isnull(Postcode,'''') As SuburbStatePostCode,''Gold Address'' AS SourceFullName '
		SET @QUERY=@QUERY+' FROM tblAddressMasterCopyOps AS AMC		 '
		SET @QUERY=@QUERY+'INNER JOIN TBLADDRESSMASTER AS AM ON AMC.addressmasterid=AM.addressmasterid '
		SET @QUERY=@QUERY+'WHERE  ISNULL(isactive,0) =1  '+@FilterCondition	
		SET @QUERY=@QUERY+' UNION  '
		-- ADDRESSALIAS
		--SET @QUERY=@QUERY+' SELECT top 2000 addressmasteraliasid AS addressmasterID,ISNULL(delivy_point_id,0)delivy_point_id,ISNULL(house_nbr_1,0)house_nbr_1,'
		--SET @QUERY=@QUERY+'ISNULL(house_nbr_sfx_1,'''')house_nbr_sfx_1,ISNULL(house_nbr_2,'''')house_nbr_2,ISNULL(house_nbr_sfx_2,'''')house_nbr_sfx_2,'
		--SET @QUERY=@QUERY+'ISNULL(flat_unit_type,'''')flat_unit_type,ISNULL(flat_unit_nbr,'''')flat_unit_nbr,'
		----SET @QUERY=@QUERY+'ISNULL(flat_unit_type_full,'''')flat_unit_type_full,'
		----SET @QUERY=@QUERY+'ISNULL(floor_level_type,'''')floor_level_type,ISNULL(floor_level_type_full,'''')floor_level_type_full,ISNULL(floor_level_nbr,'''')floor_level_nbr,'
		----SET @QUERY=@QUERY+'ISNULL(lot_nbr,'''')lot_nbr,ISNULL(postal_delivery_nbr,0)postal_delivery_nbr,ISNULL(postal_delivery_nbr_pfx,'''')postal_delivery_nbr_pfx,'
		----SET @QUERY=@QUERY+'ISNULL(postal_delivery_nbr_sfx,'''')postal_delivery_nbr_sfx,ISNULL(primary_point_ind,'''')primary_point_ind,ISNULL(delivy_point_group_id,0)delivy_point_group_id,ISNULL(postal_delivery_type,'''')postal_delivery_type,'
		--SET @QUERY=@QUERY+'ISNULL(street_name,'''')street_name,ISNULL(street_type,'''')street_type,'
		----SET @QUERY=@QUERY+'ISNULL(street_sfx,'''')street_sfx,ISNULL(street_type_full,'''')street_type_full,ISNULL(delivy_point_group_did,0)delivy_point_group_did,ISNULL(locality_id,0)locality_id,'
		--SET @QUERY=@QUERY+'ISNULL(locality_name,'''')locality_name,ISNULL(postcode,'''')postcode,ISNULL(state,'''')state,'
		--	--SET @QUERY=@QUERY+'--ISNULL(locality_did,0)locality_did,'
		--SET @QUERY=@QUERY+'iif(isnull(full_address,'''')<>'''',full_address,iif(isnull(Flat_Unit_Nbr,'''')<>'''', cast(Flat_Unit_Nbr as varchar) +''/'','''')+ Cast(House_Nbr_1 as varchar) +''-''+ iif(isnull(House_nbr_2,'''')<>'''', Cast(House_nbr_2 as varchar)+'','','''')+ street_name+'' ''+ street_type +'',''+ Locality_name+'',''+ Postcode+'', ''+ State)full_address,ISNULL(address,'''')address,'
		----SET @QUERY=@QUERY+'ISNULL(confidence_level,0)confidence_level,'
		----SET @QUERY=@QUERY+'ISNULL(feature,'''')feature,ISNULL(latitude,0)latitude,ISNULL(longitude,0)longitude,'
		----SET @QUERY=@QUERY+'ISNULL(AMC.crtd_datetime,GETDATE())crtd_datetime,ISNULL(AMC.crtd_userid,'''')crtd_userid,ISNULL(AMC.last_upd_datetime,GETDATE())last_upd_datetime,ISNULL(AMC. last_upd_userid,'''')last_upd_userid,'
		--SET @QUERY=@QUERY+'ISNULL(AMA.headportsort,'''')headportsort,ISNULL(AMA.secondarysort,'''')secondarysort,ISNULL(AMA.drivernumber,'''')drivernumber, ''A'' SOURCE,'
		--SET @QUERY=@QUERY+'isnull(locality_name,'''')+'' ''+isnull(state,'''')+'' ''+isnull(Postcode,'''') As SuburbStatePostCode,''Alias Address'' As SourceFullName '
		--SET @QUERY=@QUERY+' FROM TBLADDRESSMASTERALIAS AS AMA '
		--SET @QUERY=@QUERY+'INNER JOIN  TBLADDRESSMASTERCopy AS AMC ON AMC.addressmastercopyid=AMA.addressmastercopyid  '
		--SET @QUERY=@QUERY+'INNER JOIN [dbo].[tblAddressMasterHash] AMH ON AMH.addressmasterhash=AMC.addressmasterhash '
		--SET @QUERY=@QUERY+'INNER JOIN TBLADDRESSMASTER AS AM ON AMH.addressmasterid=AM.addressmasterid '
		--SET @QUERY=@QUERY+'WHERE  ISNULL(AMA.isactive,0) =1 '+@FilterCondition

		--SET @QUERY=@QUERY+' UNION '
	-- UnknownAddress
		SET @QUERY=@QUERY+'SELECT Top 4000 unknownaddressid AS addressmasterID,0 delivy_point_id,isnull(house_nbr_1,0)house_nbr_1,'
		SET @QUERY=@QUERY+'ISNULL(house_nbr_sfx_1,'''')house_nbr_sfx_1,ISNULL(house_nbr_2,0)house_nbr_2,Cast(ISNULL(house_nbr_sfx_2,0)as varchar)house_nbr_sfx_2,ISNULL(flat_unit_type,'''')flat_unit_type,ISNULL(flat_unit_nbr,'''')flat_unit_nbr,'
		--SET @QUERY=@QUERY+'ISNULL(flat_unit_type_full,'''')flat_unit_type_full,'
		--SET @QUERY=@QUERY+'ISNULL(floor_level_type,'''')floor_level_type,ISNULL(floor_level_type_full,'''')floor_level_type_full,ISNULL(floor_level_nbr,'''')floor_level_nbr,'
		--SET @QUERY=@QUERY+'ISNULL(lot_nbr,'''')lot_nbr,ISNULL(postal_delivery_nbr,0)postal_delivery_nbr,ISNULL(postal_delivery_nbr_pfx,'''')postal_delivery_nbr_pfx,'
		--SET @QUERY=@QUERY+'ISNULL(postal_delivery_nbr_sfx,'''')postal_delivery_nbr_sfx,ISNULL(primary_point_ind,'''')primary_point_ind,ISNULL(delivy_point_group_id,0)delivy_point_group_id,ISNULL(postal_delivery_type,'''')postal_delivery_type,'
		SET @QUERY=@QUERY+'ISNULL(street_name,'''')street_name,ISNULL(street_type,'''')street_type,'
		--SET @QUERY=@QUERY+'ISNULL(street_sfx,'''')street_sfx,ISNULL(street_type_full,'''')street_type_full,ISNULL(delivy_point_group_did,0)delivy_point_group_did,ISNULL(locality_id,0)locality_id,'
		SET @QUERY=@QUERY+'ISNULL(locality_name,'''')locality_name,ISNULL(postcode,'''')postcode,ISNULL(state,'''')state,'
		--SET @QUERY=@QUERY+' ISNULL(locality_did,0)locality_did,'
		SET @QUERY=@QUERY+'iif(isnull(full_address,'''')<>'''',full_address,iif(isnull(Flat_Unit_Nbr,'''')<>'''', cast(Flat_Unit_Nbr as varchar) +''/'','''')+ Cast(House_Nbr_1 as varchar) +''-''+ iif(isnull(House_nbr_2,'''')<>'''', Cast(House_nbr_2 as varchar)+'','','''')+ street_name+'' ''+ street_type +'',''+ Locality_name+'',''+ Postcode+'', ''+ State)full_address,'
		SET @QUERY=@QUERY+'ISNULL(address,'''')address,'
		--SET @QUERY=@QUERY+'0 confidence_level,'''' feature,0 latitude,0 longitude,ISNULL(crtd_datetime,GETDATE())crtd_datetime,ISNULL(crtd_userid,'''')crtd_userid,ISNULL(last_upd_datetime,GETDATE())last_upd_datetime,ISNULL(last_upd_userid,'''')last_upd_userid,'
		SET @QUERY=@QUERY+'ISNULL(headportsort,'''')headportsort,ISNULL(secondarysort,'''')secondarysort,ISNULL(drivernumber,'''')drivernumber, ''U'' SOURCE,'
		SET @QUERY=@QUERY+'isnull(locality_name,'''')+'' ''+isnull(state,'''')+'' ''+isnull(Postcode,'''') As SuburbStatePostCode,''Unknown Address'' SourceFullName'
		SET @QUERY=@QUERY+' FROM TBLUNKNOWNADDRESS  '
		SET @QUERY=@QUERY+'WHERE  ISNULL(isactive,0) =1 '+@FilterCondition
		--insert into tblTempSearchQuery(SearchQuery)  select @QUERY as SearchQuery 
		PRINT(@QUERY);

		EXEC(@QUERY);
	 SET NOCOUNT OFF;
	 
	END

	----------------------------------------------------------------ELSE PART ----------------------------------------------------------
	----------------------------------------------------------- ALIAS ADDRESS TABLE ----------------------------------------------------
	ELSE IF(@SOURCEIND='A')
	BEGIN 
	SET NOCOUNT ON;
		SET @QUERY=''
		SET @QUERY=@QUERY+' SELECT top 10000 addressmasteraliasid AS addressmasterID,ISNULL(delivy_point_id,0)delivy_point_id,ISNULL(house_nbr_1,0)house_nbr_1,'
		SET @QUERY=@QUERY+'ISNULL(house_nbr_sfx_1,'''')house_nbr_sfx_1,ISNULL(house_nbr_2,'''')house_nbr_2,ISNULL(house_nbr_sfx_2,'''')house_nbr_sfx_2,'
		SET @QUERY=@QUERY+'ISNULL(flat_unit_type,'''')flat_unit_type,ISNULL(flat_unit_nbr,'''')flat_unit_nbr,'
		--SET @QUERY=@QUERY+'ISNULL(flat_unit_type_full,'''')flat_unit_type_full,'
		--SET @QUERY=@QUERY+'ISNULL(floor_level_type,'''')floor_level_type,ISNULL(floor_level_type_full,'''')floor_level_type_full,ISNULL(floor_level_nbr,'''')floor_level_nbr,'
		--SET @QUERY=@QUERY+'ISNULL(lot_nbr,'''')lot_nbr,ISNULL(postal_delivery_nbr,0)postal_delivery_nbr,ISNULL(postal_delivery_nbr_pfx,'''')postal_delivery_nbr_pfx,'
		--SET @QUERY=@QUERY+'ISNULL(postal_delivery_nbr_sfx,'''')postal_delivery_nbr_sfx,ISNULL(primary_point_ind,'''')primary_point_ind,ISNULL(delivy_point_group_id,0)delivy_point_group_id,ISNULL(postal_delivery_type,'''')postal_delivery_type,'
		SET @QUERY=@QUERY+'ISNULL(street_name,'''')street_name,ISNULL(street_type,'''')street_type,'
		--SET @QUERY=@QUERY+'ISNULL(street_sfx,'''')street_sfx,ISNULL(street_type_full,'''')street_type_full,ISNULL(delivy_point_group_did,0)delivy_point_group_did,ISNULL(locality_id,0)locality_id,'
		SET @QUERY=@QUERY+'ISNULL(locality_name,'''')locality_name,ISNULL(postcode,'''')postcode,ISNULL(state,'''')state,'
			--SET @QUERY=@QUERY+'--ISNULL(locality_did,0)locality_did,'
		SET @QUERY=@QUERY+'iif(isnull(full_address,'''')<>'''',full_address,iif(isnull(Flat_Unit_Nbr,'''')<>'''', cast(Flat_Unit_Nbr as varchar) +''/'','''')+ Cast(House_Nbr_1 as varchar) +''-''+ iif(isnull(House_nbr_2,'''')<>'''', Cast(House_nbr_2 as varchar)+'','','''')+ street_name+'' ''+ street_type +'',''+ Locality_name+'',''+ Postcode+'', ''+ State)full_address,ISNULL(address,'''')address,'
		--SET @QUERY=@QUERY+'ISNULL(confidence_level,0)confidence_level,'
		--SET @QUERY=@QUERY+'ISNULL(feature,'''')feature,ISNULL(latitude,0)latitude,ISNULL(longitude,0)longitude,'
		--SET @QUERY=@QUERY+'ISNULL(AMC.crtd_datetime,GETDATE())crtd_datetime,ISNULL(AMC.crtd_userid,'''')crtd_userid,ISNULL(AMC.last_upd_datetime,GETDATE())last_upd_datetime,ISNULL(AMC. last_upd_userid,'''')last_upd_userid,'
		SET @QUERY=@QUERY+'ISNULL(AMA.headportsort,'''')headportsort,ISNULL(AMA.secondarysort,'''')secondarysort,ISNULL(AMA.drivernumber,'''')drivernumber, ''A'' SOURCE,'
		SET @QUERY=@QUERY+'isnull(locality_name,'''')+'' ''+isnull(state,'''')+'' ''+isnull(Postcode,'''') As SuburbStatePostCode,''Alias Address'' As SourceFullName '
		SET @QUERY=@QUERY+' FROM TBLADDRESSMASTERALIAS AS AMA '
		SET @QUERY=@QUERY+'INNER JOIN  TBLADDRESSMASTERCopy AS AMC ON AMC.addressmastercopyid=AMA.addressmastercopyid  '
		SET @QUERY=@QUERY+'INNER JOIN [dbo].[tblAddressMasterHash] AMH ON AMH.addressmasterhash=AMC.addressmasterhash '
		SET @QUERY=@QUERY+'INNER JOIN TBLADDRESSMASTER AS AM ON AMH.addressmasterid=AM.addressmasterid '
		SET @QUERY=@QUERY+'WHERE  ISNULL(AMA.isactive,0) =1 '+@FilterCondition
		--OPTION (RECOMPILE)
		EXEC(@QUERY)
	SET NOCOUNT OFF;
	END
	----------------------------------------------------------------ELSE PART ----------------------------------------------------------
	--------------------------------------------------------- COPY OR GOLD ADDRESS TABLE -----------------------------------------------
	ELSE IF(@SOURCEIND='G')
	BEGIN 
	SET NOCOUNT ON;
		SET @QUERY=''
		SET @QUERY=@QUERY+'SELECT top 10000 AMC. addressmasterid AS addressmasterID,'
		SET @QUERY=@QUERY+'ISNULL(AMC.delivy_point_id,0)delivy_point_id,'
		SET @QUERY=@QUERY+'ISNULL(house_nbr_1,0)house_nbr_1,ISNULL(house_nbr_sfx_1,0)house_nbr_sfx_1,ISNULL(house_nbr_2,0)house_nbr_2,ISNULL(house_nbr_sfx_2,'''')house_nbr_sfx_2,ISNULL(flat_unit_type,'''')flat_unit_type,ISNULL(flat_unit_nbr,'''')flat_unit_nbr,'
		--SET @QUERY=@QUERY+'ISNULL(flat_unit_type_full,'''')flat_unit_type_full,ISNULL(floor_level_type,'''')floor_level_type,'
		--SET @QUERY=@QUERY+'ISNULL(floor_level_type_full,'''')floor_level_type_full,ISNULL(floor_level_nbr,'''')floor_level_nbr,'
		--SET @QUERY=@QUERY+'ISNULL(lot_nbr,'''')lot_nbr,ISNULL(postal_delivery_nbr,0)postal_delivery_nbr,ISNULL(postal_delivery_nbr_pfx,'''')postal_delivery_nbr_pfx,'
		--SET @QUERY=@QUERY+'ISNULL(postal_delivery_nbr_sfx,'''')postal_delivery_nbr_sfx,ISNULL(primary_point_ind,'''')primary_point_ind,ISNULL(delivy_point_group_id,0)delivy_point_group_id,'
		--SET @QUERY=@QUERY+'ISNULL(postal_delivery_type,'''')postal_delivery_type,'
		SET @QUERY=@QUERY+'ISNULL(street_name,'''')street_name,ISNULL(street_type,'''')street_type,'
		--SET @QUERY=@QUERY+'ISNULL(street_sfx,'''')street_sfx,ISNULL(street_type_full,'''')street_type_full,ISNULL(delivy_point_group_did,0)delivy_point_group_did,ISNULL(locality_id,0)locality_id,'
		SET @QUERY=@QUERY+'ISNULL(locality_name,'''')locality_name,ISNULL(postcode,'''')postcode,ISNULL(state,'''')state,'
		--SET @QUERY=@QUERY+'ISNULL(locality_did,0)locality_did,'
		SET @QUERY=@QUERY+'iif(isnull(full_address,'''')<>'''',full_address,iif(isnull(Flat_Unit_Nbr,'''')<>'''', cast(Flat_Unit_Nbr as varchar) +''/'','''')+ Cast(House_Nbr_1 as varchar) +''-''+ iif(isnull(House_nbr_2,'''')<>'''', Cast(House_nbr_2 as varchar)+'','','''')+ street_name+'' ''+ street_type +'',''+ Locality_name+'',''+ Postcode+'', ''+ State)full_address,ISNULL(address,'''')address,'
		--SET @QUERY=@QUERY+'ISNULL(confidence_level,0)confidence_level,ISNULL(feature,'''')feature,ISNULL(latitude,0)latitude,ISNULL(longitude,0)longitude,ISNULL(AMC.crtd_datetime,GETDATE())crtd_datetime,ISNULL(AMC.crtd_userid,'''')crtd_userid,'
		--SET @QUERY=@QUERY+'ISNULL(AMC.last_upd_datetime,GETDATE())last_upd_datetime,ISNULL(AMC. last_upd_userid,'''')last_upd_userid,'
		SET @QUERY=@QUERY+'ISNULL(headportsort,'''')headportsort,ISNULL(secondarysort,'''')secondarysort,ISNULL(drivernumber,'''')drivernumber,'
		SET @QUERY=@QUERY+'''G'' SOURCE,isnull(locality_name,'''')+'' ''+isnull(state,'''')+'' ''+isnull(Postcode,'''') As SuburbStatePostCode,''Gold Address'' AS SourceFullName '
		SET @QUERY=@QUERY+' FROM tblAddressMasterCopyOps AS AMC		 '
		SET @QUERY=@QUERY+'INNER JOIN TBLADDRESSMASTER AS AM ON AMC.addressmasterid=AM.addressmasterid '
		SET @QUERY=@QUERY+'WHERE  ISNULL(isactive,0) =1  '+@FilterCondition--Option(Recompile)
		EXEC(@QUERY)
SET NOCOUNT OFF;
	END 
	----------------------------------------------------------------ELSE PART ----------------------------------------------------------
	--------------------------------------------------------- UNKNOWN ADDRESS TABLE ----------------------------------------------------
	ELSE IF(@SOURCEIND='U')
	BEGIN 
	SET NOCOUNT ON;
		SET @QUERY=''
		SET @QUERY=@QUERY+'SELECT Top 10000 unknownaddressid AS addressmasterID,0 delivy_point_id,isnull(house_nbr_1,0)house_nbr_1,'
		SET @QUERY=@QUERY+'ISNULL(house_nbr_sfx_1,'''')house_nbr_sfx_1,ISNULL(house_nbr_2,'''')house_nbr_2,ISNULL(house_nbr_sfx_2,'''')house_nbr_sfx_2,ISNULL(flat_unit_type,'''')flat_unit_type,ISNULL(flat_unit_nbr,'''')flat_unit_nbr,'
		--SET @QUERY=@QUERY+'ISNULL(flat_unit_type_full,'''')flat_unit_type_full,'
		--SET @QUERY=@QUERY+'ISNULL(floor_level_type,'''')floor_level_type,ISNULL(floor_level_type_full,'''')floor_level_type_full,ISNULL(floor_level_nbr,'''')floor_level_nbr,'
		--SET @QUERY=@QUERY+'ISNULL(lot_nbr,'''')lot_nbr,ISNULL(postal_delivery_nbr,0)postal_delivery_nbr,ISNULL(postal_delivery_nbr_pfx,'''')postal_delivery_nbr_pfx,'
		--SET @QUERY=@QUERY+'ISNULL(postal_delivery_nbr_sfx,'''')postal_delivery_nbr_sfx,ISNULL(primary_point_ind,'''')primary_point_ind,ISNULL(delivy_point_group_id,0)delivy_point_group_id,ISNULL(postal_delivery_type,'''')postal_delivery_type,'
		SET @QUERY=@QUERY+'ISNULL(street_name,'''')street_name,ISNULL(street_type,'''')street_type,'
		--SET @QUERY=@QUERY+'ISNULL(street_sfx,'''')street_sfx,ISNULL(street_type_full,'''')street_type_full,ISNULL(delivy_point_group_did,0)delivy_point_group_did,ISNULL(locality_id,0)locality_id,'
		SET @QUERY=@QUERY+'ISNULL(locality_name,'''')locality_name,ISNULL(postcode,'''')postcode,ISNULL(state,'''')state,'
		--SET @QUERY=@QUERY+' ISNULL(locality_did,0)locality_did,'
		SET @QUERY=@QUERY+'iif(isnull(full_address,'''')<>'''',full_address,iif(isnull(Flat_Unit_Nbr,'''')<>'''', cast(Flat_Unit_Nbr as varchar) +''/'','''')+ Cast(House_Nbr_1 as varchar) +''-''+ iif(isnull(House_nbr_2,'''')<>'''', Cast(House_nbr_2 as varchar)+'','','''')+ street_name+'' ''+ street_type +'',''+ Locality_name+'',''+ Postcode+'', ''+ State)full_address,'
		SET @QUERY=@QUERY+'ISNULL(address,'''')address,'
		--SET @QUERY=@QUERY+'0 confidence_level,'''' feature,0 latitude,0 longitude,ISNULL(crtd_datetime,GETDATE())crtd_datetime,ISNULL(crtd_userid,'''')crtd_userid,ISNULL(last_upd_datetime,GETDATE())last_upd_datetime,ISNULL(last_upd_userid,'''')last_upd_userid,'
		SET @QUERY=@QUERY+'ISNULL(headportsort,'''')headportsort,ISNULL(secondarysort,'''')secondarysort,ISNULL(drivernumber,'''')drivernumber, ''U'' SOURCE,'
		SET @QUERY=@QUERY+'isnull(locality_name,'''')+'' ''+isnull(state,'''')+'' ''+isnull(Postcode,'''') As SuburbStatePostCode,''Unknown Address'' SourceFullName'
		SET @QUERY=@QUERY+' FROM TBLUNKNOWNADDRESS  '
		SET @QUERY=@QUERY+'WHERE  ISNULL(isactive,0) =1 '+@FilterCondition
		EXEC(@QUERY)
		SET NOCOUNT OFF;
	END
END
GO
