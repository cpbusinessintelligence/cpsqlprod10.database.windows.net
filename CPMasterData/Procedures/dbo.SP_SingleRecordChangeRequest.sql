SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Rajkumar Sahu
-- Create date: 08/08/2019
-- Description:	Single record change request
-- =============================================

CREATE PROCEDURE [dbo].[SP_SingleRecordChangeRequest]
@RequestNumber NVARCHAR(200)='',
@Requester VARCHAR(50)='',
@Reason  VARCHAR(50)='',
@Component  VARCHAR(50)='',
@RequestedDateTime  datetime=null,
@FileName  VARCHAR(200)='',
@FilePath   VARCHAR(max)='',
@ChangeStatus  INT=1,
@EffectiveDate DATETIME =NULL,
@CreatedBy VARCHAR(50)='',
@delivery_point_id INT=0,
@headportsort VARCHAR(50)='',
@secondarysort VARCHAR(50)='',
@drivernumber VARCHAR(50)='',
@RequesterEmail VARCHAR(50)='',
@ReviewerGroup VARCHAR(50)=''
AS
BEGIN
    SET NOCOUNT ON
	 BEGIN TRAN
		BEGIN TRY
			Declare @datetime datetime
		 	--set @datetime=(select  cast(getdate() +replace(format (cast(getdate() as datetime) AT TIME ZONE 'AUS Eastern Standard Time' ,'zzz'),'+','') as datetime) AT TIME ZONE 'AUS Eastern Standard Time')
			SET @datetime=(Select dbo.ReturnAUSEasternStandardTime())
			DECLARE @REQUERSID INT =0
			INSERT INTO [dbo].[tblChangeRequest](RequestNumber,Requester,Reason,Component,RequestedDateTime,FileName,FilePath,ChangeStatus,EffectiveDate,CreatedDateTime,CreatedBy,RequesterEmail,ReviewerGroup)
			VALUES (@RequestNumber ,@Requester,@Reason,@Component,@RequestedDateTime,@FileName,@FilePath,@ChangeStatus,@EffectiveDate,@datetime,@CreatedBy,@RequesterEmail,@ReviewerGroup)
			SET @REQUERSID =(SELECT @@IDENTITY)

			INSERT INTO [dbo].[tblAddressMasterCopyOpsStaging]
			(
			RequestID,delivery_point_id,headportsort,secondarysort,drivernumber,CreatedDateTime,CreatedBy	
			) 
			VALUES
			(
			@REQUERSID,@delivery_point_id,@headportsort,@secondarysort,@drivernumber,@datetime,@CreatedBy	
			)
							 
			SELECT RequestID,Reason,RequestNumber,ReviewerComment,ReviewedBy,ReviewedDateTime,Requester,RequesterEmail,ChangeStatus From  tblChangeRequest where RequestID=@REQUERSID
		-- mail send to approver and requester data 
			Select EmailType,Subject  ,body ,MDMComponent  from tblEmailTemplate  Where  MDMComponent= 'BulkUpdateSubmitRequestForApproval'


			COMMIT TRANSACTION 
		END TRY
		BEGIN CATCH
			SELECT ERROR_NUMBER(),ERROR_MESSAGE()
			ROLLBACK TRANSACTION 
		END CATCH
END
GO
