SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 17-JULY-2019 3:51 PM AST
-- Description	: SOFT DELETE FROM tblAddressMasterCopyOpsStaging FILTER BY RequestID
-- =============================================

CREATE PROCEDURE [dbo].[SP_DeleteBulkAddressMasterCopyOpsStaging]
@RequestID int=0,
@UniquIds NVarchar(max)=null,
@UpdatedBy Nvarchar(100)='' 
AS
BEGIN
	BEGIN TRAN
	BEGIN TRY 		
	--AEST datetime (calculate Daylight saving)
		--select FORMAT(cast(getdate() as datetime) AT TIME ZONE 'AUS Eastern Standard Time', 'zzz') 
		Declare @updatedatetime datetime
		--set @updatedatetime=( select  cast(getdate() +replace(format (cast(getdate() as datetime) AT TIME ZONE 'AUS Eastern Standard Time' ,'zzz'),'+','') as datetime) AT TIME ZONE 'AUS Eastern Standard Time' )
		set @updatedatetime=( select  dbo.ReturnAUSEasternStandardTime())
		EXEC('UPDATE tblAddressMasterCopyOpsStaging SET IsDeleted=1 ,UpdatedDateTime='''+@updatedatetime+''',UpdatedBy='''+@UpdatedBy+''' WHERE addressmasterid IN ('+@UniquIds+')')
		Exec [SP_GetBulkAddressMasterUploadDataCompare] @RequestID,1,1
		COMMIT TRANSACTION
	END TRY 
	BEGIN CATCH
		   -- if error, roll back any chanegs done by any of the sql statements
		   SELECT ERROR_NUMBER(),ERROR_MESSAGE()
		   SELECT 0
           ROLLBACK TRANSACTION
	END CATCH

END

/*

eXEC [CPMD_DeleteBulkAddressMasterCopyOpsStaging] 120,'120160,120161,120162'
*/
GO
