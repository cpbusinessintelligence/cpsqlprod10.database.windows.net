SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 09-JULY-2019 4:21PM AST
-- Description	: CREATE NEW REQUEST FOR BULK UPLOAD DATA IN tblChangeRequest TABLE 
-- =============================================
CREATE PROCEDURE [dbo].[sp_InsertChangeRequest]
@RequestNumber NVARCHAR(200)='',
@Requester VARCHAR(50)='',
@Reason  VARCHAR(50)='',
@Component  VARCHAR(50)='',
@RequestedDateTime  datetime=null,
@FileName  VARCHAR(200)='',
@FilePath   VARCHAR(50)='',
@ChangeStatus  INT=1,
@EffectiveDate DATETIME =NULL,
@CreatedBy VARCHAR(50)='',
@UpdatedBy VARCHAR(50)=''
AS 
BEGIN
	BEGIN TRAN
		BEGIN TRY 
		Declare @CurrentAusDateTime DateTime 
		SET @CurrentAusDateTime=(Select dbo.ReturnAUSEasternStandardTime())

			INSERT INTO [dbo].[tblChangeRequest](RequestNumber,Requester,Reason,Component,RequestedDateTime,FileName,FilePath,ChangeStatus,EffectiveDate,CreatedDateTime,CreatedBy)--,UpdatedDateTime,UpdatedBy)
			VALUES (@RequestNumber ,@Requester,@Reason,@Component,@RequestedDateTime,@FileName,@FilePath,@ChangeStatus,@EffectiveDate,@CurrentAusDateTime,@CreatedBy)--,Getdate(),@UpdatedBy)
			SELECT @@IDENTITY 
           COMMIT TRANSACTION
		END TRY 
		BEGIN CATCH
		   SELECT ERROR_NUMBER(),ERROR_MESSAGE()
		   SELECT 0
           ROLLBACK TRANSACTION

		END CATCH
	

END
GO
