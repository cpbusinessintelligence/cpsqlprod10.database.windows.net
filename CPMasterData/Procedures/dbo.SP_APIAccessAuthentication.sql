SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		:      RAJKUMAR SAHU
-- Create Date	:	   16-JULY-2019
-- Description	:	   THIS PROCEDURE CHECKED API AUTHENTICATION
-- =============================================
CREATE Procedure [dbo].[SP_APIAccessAuthentication]
@ClientCode  varchar(100)=null,
@APIName  varchar(100) =null,
@APIToken Nvarchar(300)=null 
AS
Begin

	SELECT  APIName FROM tblAPIAccess
	WHERE ClientCode=@ClientCode AND APIName = @APIName AND APIToken = @APIToken and IsActive=1

End
GO
