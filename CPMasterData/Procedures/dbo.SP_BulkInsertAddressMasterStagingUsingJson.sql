SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 10-JULY-2019 5:27PM IST
-- Description	: BULK Insert / UPDATE  DATA IN ADDRESSMASTERCOPYOPSSTAGING TABLES USING JSON STRING 
-- =============================================
CREATE PROCEDURE [dbo].[SP_BulkInsertAddressMasterStagingUsingJson]
@JSONSTRING NVARCHAR(MAX)='',

@RequestNumber NVARCHAR(200)='',
@Requester VARCHAR(50)='',
@Reason  VARCHAR(100)='',
@Component  VARCHAR(50)='',
@RequestedDateTime  datetime=null,
@FileName  VARCHAR(200)='',
@FilePath   NVARCHAR(max)='',
@ChangeStatus  INT=1,
@EffectiveDate DATETIME =NULL,
@CreatedBy VARCHAR(50)='',
@UpdatedBy VARCHAR(50)='',
@RequesterEmail VARCHAR(500)='',
@ReviewerGroup VARCHAR(100)=''
AS
BEGIN
    SET NOCOUNT ON
	SET XACT_ABORT ON;
	 BEGIN TRAN
		BEGIN TRY
			Declare @datetime datetime
		 	--set @datetime=(select  cast(getdate() +replace(format (cast(getdate() as datetime) AT TIME ZONE 'AUS Eastern Standard Time' ,'zzz'),'+','') as datetime) AT TIME ZONE 'AUS Eastern Standard Time')
			SET @datetime=(Select dbo.ReturnAUSEasternStandardTime())
			DECLARE @REQUERSID INT =0
			INSERT INTO [dbo].[tblChangeRequest](RequestNumber,Requester,Reason,Component,RequestedDateTime,FileName,FilePath,ChangeStatus,EffectiveDate,CreatedDateTime,CreatedBy,RequesterEmail,ReviewerGroup  )
			VALUES (@RequestNumber ,@Requester,@Reason,@Component,@RequestedDateTime,@FileName,@FilePath,@ChangeStatus,@EffectiveDate,@datetime,@CreatedBy ,@RequesterEmail,@ReviewerGroup ) 
			SET @REQUERSID =(SELECT @@IDENTITY)

				INSERT INTO [dbo].[tblAddressMasterCopyOpsStaging]
				(
				RequestID,delivery_point_id,headportsort,secondarysort,drivernumber,CreatedDateTime,CreatedBy	
				)
				SELECT 
				@REQUERSID,delivery_point_id,headportsort,secondarysort,drivernumber,@datetime,@CreatedBy	
				FROM  OPENJSON(@JSONSTRING )
				WITH(
				delivery_point_id int '$.DeliveyPointId',headportsort varchar(10) '$.HeadPortSort',secondarysort varchar(10) '$.SecondarySort',
				drivernumber varchar(10) '$.DriverNumber'			   
				) 
				 
				EXEC SP_GetBulkAddressMasterUploadDataCompare  @REQUERSID

			COMMIT TRANSACTION 
		END TRY
		BEGIN CATCH
			SELECT ERROR_NUMBER(),ERROR_MESSAGE()
			ROLLBACK TRANSACTION 
		END CATCH
END
GO
