SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author		: RAJKUMAR SAHU
-- CREATE DATE	: 10-JUNE-2019 3:27PM IST
-- DESCRIPTION	: INSERT ALIAS ADDRESS MASTER DATA
-- =============================================
CREATE PROCEDURE [dbo].[SP_InsertAliasAddressMaster]
@addressMasterId int =0,
@house_nbr_1 INT=0,
@house_nbr_sfx_1 INT=0,
@house_nbr_2 INT=0,
@house_nbr_sfx_2 INT=0,
@flat_unit_type VARCHAR(7)='',
@flat_unit_type_full VARCHAR(40)='',
@flat_unit_nbr VARCHAR(7)='',
@floor_level_type VARCHAR(2)='',
@floor_level_type_full VARCHAR(40)='',
@floor_level_nbr VARCHAR(5)='',
@lot_nbr VARCHAR(6)='',
@postal_delivery_nbr INT =0,
@postal_delivery_nbr_pfx VARCHAR(3)='',
@postal_delivery_nbr_sfx VARCHAR(3)='',
@primary_point_ind CHAR(1)='',
@delivy_point_group_id INT=0,
@postal_delivery_type VARCHAR(11)='',
@street_name VARCHAR(30)='',
@street_type VARCHAR(4)='',
@street_sfx VARCHAR(2)='',
@street_type_full VARCHAR(40)='',
@delivy_point_group_did INT=0,
@locality_id INT =0,
@locality_name VARCHAR(46)='',
@postcode VARCHAR(12)='',
@state VARCHAR(3)='',
@locality_did INT =0, 
@full_address VARCHAR(100)='',
@address VARCHAR(100)='',  
@crtd_userid VARCHAR(40)='',  
@last_upd_userid VARCHAR(40)='',
@headportSort Varchar(20)='',
@secondarySort  Varchar(20)='',
@driverNumber  Varchar(20)='',
@source  Varchar(2)=''

AS
BEGIN
    SET NOCOUNT ON
	 BEGIN TRAN
		BEGIN TRY
		 
			INSERT INTO [dbo].[tblUnknownAddress](unknownaddressid,
			house_nbr_1,house_nbr_sfx_1,house_nbr_2,house_nbr_sfx_2,flat_unit_type,flat_unit_type_full,flat_unit_nbr,floor_level_type,
			floor_level_type_full,floor_level_nbr,lot_nbr,postal_delivery_nbr,postal_delivery_nbr_pfx,postal_delivery_nbr_sfx,primary_point_ind,delivy_point_group_id,
			postal_delivery_type,street_name,street_type,street_sfx,street_type_full,delivy_point_group_did,locality_id,locality_name,postcode,state,locality_did,full_address,
			address,crtd_datetime,crtd_userid,last_upd_datetime,last_upd_userid,IsActive,source
			)
			VALUES
			((select Max(isnull(unknownaddressid,0))+1 from tblUnknownAddress),
			@house_nbr_1,@house_nbr_sfx_1,@house_nbr_2,@house_nbr_sfx_2,@flat_unit_type,@flat_unit_type_full,@flat_unit_nbr,@floor_level_type,@floor_level_type_full,
			@floor_level_nbr,@lot_nbr,@postal_delivery_nbr,@postal_delivery_nbr_pfx,@postal_delivery_nbr_sfx,@primary_point_ind,@delivy_point_group_id,@postal_delivery_type,@street_name,
			@street_type,@street_sfx,@street_type_full,@delivy_point_group_did,@locality_id,@locality_name,@postcode,@state,@locality_did,@full_address,@address,Getdate(),'-1',Getdate(),'-1'
			,1,'U')
		   -- if not error, commit the transcation
           COMMIT TRANSACTION
		   SELECT @@IDENTITY
	END TRY 
	BEGIN CATCH
		   -- if error, roll back any chanegs done by any of the sql statements
		   
		    ROLLBACK TRANSACTION			 
			SELECT ERROR_NUMBER(), ERROR_MESSAGE();			 
			SELECT 0;

	END CATCH
	
END
GO
