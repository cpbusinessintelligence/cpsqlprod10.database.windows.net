SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 29-MAY-2019 7:00PM IST
-- Description	: UPDATE addressmaster TABLE WITH RESPECT TO ADDRESSMASTERID
-- =============================================
CREATE PROCEDURE [dbo].[SP_UpdateAddressMaster]
@addressmasterid INT=0,
@delivery_point_id INT=0,
@house_nbr_1 INT=0,
@house_nbr_sfx_1 INT=0,
@house_nbr_2 INT=0,
@house_nbr_sfx_2 INT=0,
@flat_unit_type VARCHAR(7)='',
@flat_unit_type_full VARCHAR(40)='',
@flat_unit_nbr VARCHAR(7)='',
@floor_level_type VARCHAR(2)='',
@floor_level_type_full VARCHAR(40)='',
@floor_level_nbr VARCHAR(5)='',
@lot_nbr VARCHAR(6)='',
@postal_delivery_nbr INT =0,
@postal_delivery_nbr_pfx VARCHAR(3)='',
@postal_delivery_nbr_sfx VARCHAR(3)='',
@primary_point_ind CHAR(1)='',
@delivy_point_group_id INT=0,
@postal_delivery_type VARCHAR(11)='',
@street_name VARCHAR(30)='',
@street_type VARCHAR(4)='',
@street_sfx VARCHAR(2)='',
@street_type_full VARCHAR(40)='',
@delivy_point_group_did INT=0,
@locality_id INT =0,
@locality_name VARCHAR(46)='',
@postcode VARCHAR(12)='',
@state VARCHAR(3)='',
@locality_did INT =0, 
@full_address VARCHAR(100)='',
@address VARCHAR(100)='',
@confidence_level INT=0,
@feature VARCHAR(40)='',
@latitude FLOAT=0,
@longitude FLOAT=0,
@crtd_datetime DATETIME =NULL,
@crtd_userid VARCHAR(40)='', 
@last_upd_datetime DATETIME=NULL,
@last_upd_userid VARCHAR(40)='',
@SOURCE VARCHAR(1)='',
@headportsort VARCHAR(10)='',
@secondarysort VARCHAR(10)='',
@drivernumber VARCHAR(10)='',
@SuburbStatePostCode  VARCHAR(100)='',
@SourceFullName VARCHAR(100)=''
AS
BEGIN
    SET NOCOUNT ON
	 BEGIN TRAN
		BEGIN TRY
		Declare @CurrentAusDateTime DateTime 
		SET @CurrentAusDateTime=(Select dbo.ReturnAUSEasternStandardTime())
		IF @addressmasterid<>0
		BEGIN
		  IF @SOURCE='U'
		  BEGIN 
		  --delivy_point_id=@delivy_point_id,confidence_level=@confidence_level,feature=@feature,latitude=@latitude,longitude=@longitude,
				--UPDATE [dbo].[tblUnknownAddress] SET house_nbr_1=@house_nbr_1,house_nbr_sfx_1=@house_nbr_sfx_1,house_nbr_2=@house_nbr_2,
				--	house_nbr_sfx_2=@house_nbr_sfx_2,flat_unit_type=@flat_unit_type,flat_unit_type_full=@flat_unit_type_full,flat_unit_nbr=@flat_unit_nbr,
				--	floor_level_type=@floor_level_type,floor_level_type_full=@floor_level_type_full,floor_level_nbr=@floor_level_nbr,lot_nbr=@lot_nbr,postal_delivery_nbr=@postal_delivery_nbr,
				--	postal_delivery_nbr_pfx=@postal_delivery_nbr_pfx,postal_delivery_nbr_sfx=@postal_delivery_nbr_sfx,primary_point_ind=@primary_point_ind,delivy_point_group_id=@delivy_point_group_id,
				--	postal_delivery_type=@postal_delivery_type,street_name=@street_name,street_type=@street_type,street_sfx=@street_sfx,street_type_full=@street_type_full,delivy_point_group_did=@delivy_point_group_did,
				--	locality_id=@locality_id,locality_name=@locality_name,postcode=@postcode,state=@state,locality_did=@locality_did,full_address=@full_address,
				--	address=@address,crtd_userid=-1,last_upd_datetime=GETDATE(),last_upd_userid=-1
				--WHERE unknownaddressid=@addressmasterid
	
				UPDATE [dbo].[tblUnknownAddress] 
				SET locality_name=@locality_name,postcode=@postcode,state=@state,
				street_name=@street_name,street_type=@street_type,
				house_nbr_1=@house_nbr_1,house_nbr_2=@house_nbr_2,flat_unit_nbr=@flat_unit_nbr,
				crtd_userid=-1,last_upd_datetime=@CurrentAusDateTime,last_upd_userid=-1
				WHERE unknownaddressid=@addressmasterid
		  END
		  ELSE IF @SOURCE='A'
		  BEGIN 
		 -- 	UPDATE [DBO].[TBLADDRESSMASTERALIAS] SET [HeadPortSort]=iif(@HeadPortSort='',[HeadPortSort],@HeadPortSort),[SecondarySort]=iif(@SecondarySort='',SecondarySort,@SecondarySort),[DriverNumber]=iif(@DriverNumber='',[DriverNumber],@DriverNumber),[last_upd_datetime]=GETDATE(),[last_upd_userid]=1
			--WHERE addressmasteraliasid=@addressmasterid
			
			--UPDATE [dbo].[tblAddressMaster] SET locality_name=@locality_name,postcode=@postcode,state=@state,street_name=@street_name,street_type=@street_type 
			--WHERE addressmasterid =
			--						(
			--						 SELECT TOP 1 addressmasterid 
			--						 FROM TBLADDRESSMASTERCOPY 
			--						 WHERE addressmastercopyid=
			--													(
			--													SELECT addressmastercopyid 
			--													FROM [TBLADDRESSMASTERALIAS] 
			--													WHERE addressmasteraliasid=@addressmasterid
			--													)
			--						 )
			select 'InProgress'
		  END  
		  ELSE IF @SOURCE='G'
		  BEGIN
			UPDATE [DBO].tblAddressMasterCopyOps SET [HeadPortSort]=@HeadPortSort,[SecondarySort]=@SecondarySort,[DriverNumber]=@DriverNumber,[last_upd_datetime]=@CurrentAusDateTime,[last_upd_userid]=1
			WHERE addressmasterid=@addressmasterid
		  END 
		  ELSE 
		  BEGIN
				 SELECT @addressmasterid	
				SELECT '401','INVALID SOURCE TYPE' 
		  END
		   SELECT @addressmasterid	
		END
		ELSE
		BEGIN
			  SELECT @addressmasterid	
			  SELECT '401','INVALID SOURCE TYPE' 
		END
           COMMIT TRANSACTION
		   
	END TRY 
	BEGIN CATCH
		   -- if error, roll back any chanegs done by any of the sql statements
		   SELECT ERROR_NUMBER(),ERROR_MESSAGE()
		   SELECT 0
           ROLLBACK TRANSACTION
	END CATCH
	
END
GO
