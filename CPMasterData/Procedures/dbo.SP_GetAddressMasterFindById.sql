SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: RAJKUMAR SAHU
-- Create Date	: 29-MAY-2019 4:39PM IST
-- Description	: FATCH THE DATA FROM FILTER BY addressmasterid ID  
-- =============================================
CREATE PROCEDURE [SP_GetAddressMasterFindById]
@addressmasterid INT=0
AS
BEGIN
    SET NOCOUNT ON
	SELECT addressmasterid,delivy_point_id,house_nbr_1,house_nbr_sfx_1,house_nbr_2,house_nbr_sfx_2,flat_unit_type,flat_unit_type_full,flat_unit_nbr,
	floor_level_type,floor_level_type_full,floor_level_nbr,lot_nbr,postal_delivery_nbr,postal_delivery_nbr_pfx,postal_delivery_nbr_sfx,primary_point_ind,
	delivy_point_group_id,postal_delivery_type,street_name,street_type,street_sfx,street_type_full,delivy_point_group_did,locality_id,locality_name,
	postcode,state,locality_did,full_address,address,confidence_level,feature,latitude,longitude,crtd_datetime,crtd_userid,last_upd_datetime,last_upd_userid 
    FROM TBLADDRESSMASTER 
	WHERE addressmasterid =@addressmasterid
END
GO
