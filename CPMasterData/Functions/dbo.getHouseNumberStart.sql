SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[getHouseNumberStart]
(   
    @INPUT_STRING VARCHAR(MAX)
)
RETURNS BIGINT
AS
BEGIN

DECLARE @HOUSE_NUMBER_STR1 VARCHAR(40)
DECLARE @HOUSE_NUMBER_TO  VARCHAR(40)
DECLARE @HOUSE_NUMBER_FROM  VARCHAR(40)
DECLARE @HOUSE_NUMBER_STR2 VARCHAR(40)

SET @HOUSE_NUMBER_STR1 = LEFT(@INPUT_STRING,LEN(@INPUT_STRING)-PATINDEX('%[0-9]%',REVERSE(@INPUT_STRING))+1)

--SELECT @HOUSE_NUMBER_STR1

SET @HOUSE_NUMBER_TO = CASE WHEN PATINDEX('%[^0-9]%',REVERSE(@HOUSE_NUMBER_STR1)) = 0 THEN @HOUSE_NUMBER_STR1 
							    WHEN PATINDEX('%[^0-9]%',REVERSE(LTRIM(RTRIM(@HOUSE_NUMBER_STR1)))) <> 0 THEN RIGHT(LTRIM(RTRIM(@HOUSE_NUMBER_STR1)),PATINDEX('%[^0-9]%',REVERSE(LTRIM(RTRIM(@HOUSE_NUMBER_STR1))))-1)	
						 END
SET @HOUSE_NUMBER_STR2 = ltrim(rtrim(TRANSLATE(ltrim(rtrim(REPLACE(@HOUSE_NUMBER_STR1,@HOUSE_NUMBER_TO,''))),'-/','  ')))

--SELECT @HOUSE_NUMBER_STR2

SET @HOUSE_NUMBER_FROM = CASE WHEN PATINDEX('%[^0-9]%',REVERSE(@HOUSE_NUMBER_STR2)) = 0 THEN @HOUSE_NUMBER_STR2 
							    WHEN PATINDEX('%[^0-9]%',REVERSE(LTRIM(RTRIM(@HOUSE_NUMBER_STR2)))) <> 0 THEN RIGHT(LTRIM(RTRIM(@HOUSE_NUMBER_STR2)),PATINDEX('%[^0-9]%',REVERSE(LTRIM(RTRIM(@HOUSE_NUMBER_STR2))))-1)	
						 END
--SELECT @HOUSE_NUMBER_FROM

IF ISNUMERIC(@HOUSE_NUMBER_FROM) = 1 RETURN @HOUSE_NUMBER_FROM

RETURN NULL

END
GO
