SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		:	Rajkumar Sahu
-- Create date	:	10-09-2019
-- Description	:	Get Day number from dayname
-- =============================================
CREATE FUNCTION ReturnAUSEasternStandardTime()
Returns datetime
AS 
begin 
return(
SELECT  cast(getdate() +replace(format (cast(getdate() as datetime) AT TIME ZONE 'AUS Eastern Standard Time' ,'zzz'),'+','') as datetime) AT TIME ZONE 'AUS Eastern Standard Time')
End
GO
