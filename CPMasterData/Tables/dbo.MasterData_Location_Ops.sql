SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterData_Location_Ops] (
		[PostCodeID]                            [int] IDENTITY(1, 1) NOT NULL,
		[PostCode]                              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Suburb]                                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[State]                                 [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PriceZone]                             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ETAZone]                               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[RedemptionZone]                        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DepotCode]                             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SortCode]                              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DeliveryAgent]                         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryAgentRunCode]                  [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AlternateDeliveryAgent]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AlternateDeliveryAgentRunCode]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedeliveryFlag]                        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SatchelLinks]                          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RoadExpressPickupFlag]                 [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RoadExpressDeliveryFlag]               [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RoadExpressCutOffTime]                 [time](7) NULL,
		[CosmosCutLetter]                       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DomesticPriorityPickupFlag]            [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DomesticPriorityDeliveryFlag]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DomesticPriorityCutOffTime]            [time](7) NULL,
		[DomesticMidTierPickupFlag]             [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DomesticMidTierDeliveryFlag]           [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DomesticMidTierCutOffTime]             [time](7) NULL,
		[Internationl_PriorityPickUpFlag]       [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Internationl_PriorityDeliveryFlag]     [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Internationl_PriorityCutOffTime]       [time](7) NULL,
		[Internationl_SaverPickUpFlag]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Internationl_SaverDeliveryFlag]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Internationl_SaverCutOffTime]          [time](7) NULL,
		[Internationl_MidTierPickUpFlag]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CosmosDepotName]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HeadPortSort]                          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SecondarySort]                         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode_Type]                         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ChangeReference]                       [float] NULL,
		[AddWho]                                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]                           [datetime] NOT NULL,
		[EditWho]                               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]                          [datetime] NULL,
		CONSTRAINT [PK_[MasterData_Location_Masterfile]
		PRIMARY KEY
		CLUSTERED
		([PostCodeID])
)
GO
ALTER TABLE [dbo].[MasterData_Location_Ops]
	ADD
	CONSTRAINT [DF_MasterData_Location_Masterfile__AddWho]
	DEFAULT (user_name()) FOR [AddWho]
GO
ALTER TABLE [dbo].[MasterData_Location_Ops]
	ADD
	CONSTRAINT [DF_MasterData_Location_Masterfile__EditWho]
	DEFAULT (user_name()) FOR [EditWho]
GO
ALTER TABLE [dbo].[MasterData_Location_Ops]
	ADD
	CONSTRAINT [DF_MasterData_Location_Masterfile_AddDateTime]
	DEFAULT (getdate()) FOR [AddDateTime]
GO
ALTER TABLE [dbo].[MasterData_Location_Ops]
	ADD
	CONSTRAINT [DF_MasterData_Location_Masterfile_EditDateTime]
	DEFAULT (getdate()) FOR [EditDateTime]
GO
CREATE NONCLUSTERED INDEX [idx_Postcode_suburb]
	ON [dbo].[MasterData_Location_Ops] ([PostCode], [Suburb])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[MasterData_Location_Ops] SET (LOCK_ESCALATION = TABLE)
GO
