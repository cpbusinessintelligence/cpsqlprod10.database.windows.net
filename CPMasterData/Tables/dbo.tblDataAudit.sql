SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDataAudit] (
		[ID]                  [int] IDENTITY(1, 1) NOT NULL,
		[ComponentName]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AuditType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OldValue]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NewValue]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblDataAudit]
		PRIMARY KEY
		CLUSTERED
		([ID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDataAudit] SET (LOCK_ESCALATION = TABLE)
GO
