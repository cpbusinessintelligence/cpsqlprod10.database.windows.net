SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAddressMasterOps_2] (
		[addressmasterid]             [int] NULL,
		[delivy_point_id]             [int] NULL,
		[house_nbr_1]                 [int] NULL,
		[house_nbr_sfx_1]             [nvarchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[house_nbr_2]                 [int] NULL,
		[house_nbr_sfx_2]             [nvarchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[flat_unit_type]              [varchar](7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[flat_unit_type_full]         [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[flat_unit_nbr]               [varchar](7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[floor_level_type]            [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[floor_level_type_full]       [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[floor_level_nbr]             [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[lot_nbr]                     [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postal_delivery_nbr]         [int] NULL,
		[postal_delivery_nbr_pfx]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postal_delivery_nbr_sfx]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[primary_point_ind]           [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[delivy_point_group_id]       [int] NULL,
		[postal_delivery_type]        [varchar](11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_name]                 [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_type]                 [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_sfx]                  [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_type_full]            [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[delivy_point_group_did]      [int] NULL,
		[locality_id]                 [int] NULL,
		[locality_name]               [varchar](46) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postcode]                    [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state]                       [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[locality_did]                [int] NULL,
		[full_address]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address]                     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[confidence_level]            [int] NULL,
		[feature]                     [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[latitude]                    [float] NULL,
		[longitude]                   [float] NULL,
		[abs_ref]                     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[headportsort]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[secondarysort]               [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[drivernumber]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address_type]                [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_ref]                  [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[crtd_datetime]               [datetime] NULL,
		[crtd_userid]                 [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[last_upd_datetime]           [datetime] NULL,
		[last_upd_userid]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblAddressMasterOps_2] SET (LOCK_ESCALATION = TABLE)
GO
