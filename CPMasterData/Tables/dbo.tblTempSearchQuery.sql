SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTempSearchQuery] (
		[SearchQuery]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[id]              [int] IDENTITY(1, 1) NOT NULL,
		[EntrtyDate]      [datetime] NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTempSearchQuery] SET (LOCK_ESCALATION = TABLE)
GO
