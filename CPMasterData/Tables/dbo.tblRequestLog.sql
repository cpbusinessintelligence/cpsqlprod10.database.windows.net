SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRequestLog] (
		[RequestID]       [int] IDENTITY(1, 1) NOT NULL,
		[IPaddress]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RequestURL]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Request]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Function]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Application]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]     [datetime] NULL,
		[LocalTime]       [datetime] NULL,
		[CreatedBy]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientCode]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblRequestLog_1]
		PRIMARY KEY
		CLUSTERED
		([RequestID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRequestLog] SET (LOCK_ESCALATION = TABLE)
GO
