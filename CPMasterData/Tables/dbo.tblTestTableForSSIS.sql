SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTestTableForSSIS] (
		[addressmasterid]       [int] IDENTITY(1, 1) NOT NULL,
		[RequestID]             [int] NULL,
		[delivery_point_id]     [int] NULL,
		[headportsort]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[secondarysort]         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[drivernumber]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isactive]              [int] NULL,
		[IsDeleted]             [int] NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblTestTableForSSIS]
		PRIMARY KEY
		CLUSTERED
		([addressmasterid])
)
GO
ALTER TABLE [dbo].[tblTestTableForSSIS] SET (LOCK_ESCALATION = TABLE)
GO
