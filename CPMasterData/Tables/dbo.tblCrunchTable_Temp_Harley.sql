SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCrunchTable_Temp_Harley] (
		[crunchtableId]         [int] IDENTITY(1, 1) NOT NULL,
		[state]                 [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postcode]              [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[suburb]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_name]           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_type]           [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[house_number_from]     [int] NULL,
		[house_number_to]       [int] NULL,
		[interval_type]         [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[building]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[destination_id]        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[crtd_datetime]         [datetime] NULL,
		[crtd_userid]           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[last_upd_datetime]     [datetime] NULL,
		[last_upd_userid]       [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblCrunchTable_new_Harley]
		PRIMARY KEY
		CLUSTERED
		([crunchtableId])
)
GO
CREATE NONCLUSTERED INDEX [idx_Postcode_suburb1]
	ON [dbo].[tblCrunchTable_Temp_Harley] ([postcode], [suburb])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCrunchTable_Temp_Harley] SET (LOCK_ESCALATION = TABLE)
GO
