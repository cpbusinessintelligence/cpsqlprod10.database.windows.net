SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAPIAccess] (
		[ID]                   [int] IDENTITY(1, 1) NOT NULL,
		[ClientCode]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[APIToken]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[APIName]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedBy]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedBy]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]             [int] NULL,
		[InActiveDateTime]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isComplete]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdateDateTime]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdateBy]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Name]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceStatus]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblAPIAccess]
		PRIMARY KEY
		CLUSTERED
		([ID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAPIAccess] SET (LOCK_ESCALATION = TABLE)
GO
