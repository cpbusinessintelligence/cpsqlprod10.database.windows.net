SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmailTemplate] (
		[EmailID]             [int] IDENTITY(1, 1) NOT NULL,
		[MDMComponent]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EmailType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Subject]             [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Body]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblEmailTemplate]
		PRIMARY KEY
		CLUSTERED
		([EmailID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEmailTemplate] SET (LOCK_ESCALATION = TABLE)
GO
