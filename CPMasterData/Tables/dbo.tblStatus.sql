SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStatus] (
		[StatusID]              [int] IDENTITY(1, 1) NOT NULL,
		[StatusContext]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[StatusCode]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[StatusDescription]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsActive]              [bit] NOT NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblStatus]
		PRIMARY KEY
		CLUSTERED
		([StatusID])
)
GO
ALTER TABLE [dbo].[tblStatus]
	ADD
	CONSTRAINT [DF_tblStatus_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblStatus] SET (LOCK_ESCALATION = TABLE)
GO
