SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmailLogging] (
		[EmailID]             [int] IDENTITY(1, 1) NOT NULL,
		[EmailType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Subject]             [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sender]              [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Receiver]            [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CcOrBcc]             [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SendDateTime]        [datetime] NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblEmailLogging]
		PRIMARY KEY
		CLUSTERED
		([EmailID])
)
GO
ALTER TABLE [dbo].[tblEmailLogging] SET (LOCK_ESCALATION = TABLE)
GO
