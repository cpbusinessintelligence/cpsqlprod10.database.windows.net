SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblChangeRequest] (
		[RequestID]             [int] IDENTITY(1, 1) NOT NULL,
		[RequestNumber]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Requester]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[RequesterEmail]        [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Reason]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Component]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[RequestedDateTime]     [datetime] NOT NULL,
		[FileName]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FilePath]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ChangeStatus]          [int] NOT NULL,
		[EffectiveDate]         [datetime] NOT NULL,
		[ReviewerGroup]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReviewerComment]       [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReviewedBy]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReviewedDateTime]      [datetime] NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblChangeRequest]
		PRIMARY KEY
		CLUSTERED
		([RequestID])
) TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblChangeRequest_RequestId]
	ON [dbo].[tblChangeRequest] ([RequestID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblChangeRequest] SET (LOCK_ESCALATION = TABLE)
GO
